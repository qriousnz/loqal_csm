#! /usr/bin/env python3

"""
Must run from inside tourism folder so imports from voyager_shared work

The CSM (now CSM 1) has numerous data problems as documented on the wiki.
Fortunately there is a much superior source of location data (CSM 2) which can
be built with mod_p data as the initial SEED and network data as the ongoing
FEED we use to keep it up-to-date.

Mod_P data is not perfect and reflects its human-generated origins but it is a
much better starting point for locating cell_id_rti's than our existing CSM 1
because it explicitly captures relocations.

The network data is also superior to the trucall data because it captures all
locations, not just those that meet the (irrelevant to us) trucall requirements
(e.g. matching power requirements when compared with the Performance Management
system; or appearing in Atoll).

What sorts of data corrections are required?

a) Switching lat/lon (Northing and Easting) in the handful of cases where it is
back-the-front. Note -- this correction happens during the import of mod_p data.

b) Fixing overlaps. One row finishing on a day and another starting on the same
day also counts as an overlap.
    #1 12345 2015-01-01  2016-01-01
    #2 12345 2016-01-01

Fixing date overlaps (thousands of cell_id_rti's) in existing data. Although
most can be automated, a handful are a mess so need some specific investigation
and custom additions/replacements.

c) Changing 4G cell_id_rti's where they are recorded against their new
cell_id_rti instead of their older one (the one that was in use when connection
data was being recorded).

Some are manual because there are multiple Mod_P records for the cell_id_rti
under its newest 4G alternative cell_id_rti.

Note - rti(s) is used in preference to cell_id_rti(s) in variable names for
brevity even though rti refers to a data system prior to trucall that is no
longer in use ;-)
"""
from collections import namedtuple, Counter
import csv
import datetime  ## don't import date from datetime - need to be able to produce configuration data which has datetime.date() in it without having to edit. Or datetime to avoid confusion ;-)
from pprint import pprint as pp
import unittest
from webbrowser import open_new_tab

import matplotlib.pyplot as plt
import psycopg2 as pg
import requests

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import (spatial, conf, dates, db, folium_map, grids_regions,
    mpl, utils)
Sqlite = db.Sqlite
extract_4g_node_and_cell_id, prog, warn = (utils.extract_4g_node_and_cell_id,
    utils.prog, utils.warn)
from csm import mod_mod_4G_config as conf4G
from csm import mod_mod_consolidation_conf as consol
from csm import mod_mod_consolidation_extra_conf as consol_extra
from csm import mod_mod_extra as extra
from csm import mod_mod_stale as stale
from csm import mod_mod_connections_conf as conconf

SwitchDets = namedtuple('SwitchDets', 'csm_rti, csm_start, csm_end, mod_p_rti, '
    'mod_p_start, mod_p_end')

LARGE_GAP = 28
FRESH_RUN_MSG = "Stopping so config can be updated before a fresh run"


class SeedSource():

    @staticmethod
    def overwrite_cell_location_seed(cur_local, skip_csv=False):
        debug = False
        con_gp, cur_gp = db.Pg.gp('gpadmin')
        src_csv_fpath = os.path.join(conf.CSM_ROOT, 'storage',
            'cell_location_seed.csv')
        ## get local data
        if not skip_csv:
            sql_local_csm = """\
            SELECT *
            FROM {mod_p_locations}
            WHERE tower_lat IS NOT NULL AND tower_lon IS NOT NULL
            """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
            cur_local.execute(sql_local_csm)
            print("Extracted local cell location data")
            ## Make csv
            ##         rti   tech  cell node rnc nname ndesc lat  lon  grid curr start end start end 
            line_tpl = '{} , "{}" , {} , {} , , "{}" , "{}" , {} , {} , {} , {} , {} , {} , {} , {} \n'.replace(' ','')
            n = 0
            with open(src_csv_fpath, 'w') as f:
                while True:
                    n += 1
                    row = cur_local.fetchone()
                    if not row:
                        break
                    lat, lon = row['tower_lat'], row['tower_lon']
                    if not (lat and lon):
                        grid12 = None
                    else:
                        s2_12_id = grids_regions.Grids.s2_id(lat, lon, level=12)
                        grid12 = spatial.Spatial.grid12(s2_12_id)
                    current_flag = ''  ## should be calculated
                    line = line_tpl.format(
                        utils.none2empty(row['cell_id_rti']),
                        utils.none2empty(row['technology_class']),
                        utils.none2empty(row['cell_id']),
                        utils.none2empty(row['node_id']),
                        ## rnc_id
                        utils.none2empty(row['location_alpha']),
                        utils.none2empty(row['description']).replace('|', ','),
                        utils.none2empty(row['tower_lat']),
                        utils.none2empty(row['tower_lon']),
                        utils.none2empty(grid12),
                        utils.none2empty(current_flag),
                        utils.none2empty(dates.Dates.get_epoch_int_from_date(row['effective_start'])),
                        utils.none2empty(dates.Dates.get_epoch_int_from_date(row['effective_end'])),
                        utils.none2empty(row['effective_start']),
                        utils.none2empty(row['effective_end']))
                    f.write(line)
                    if debug and n >= 10:
                        break
        ## truncate dest table
        sql_truncate = """\
        TRUNCATE {cell_location}
        """.format(cell_location=conf.CELL_LOCATION_SEED)
        cur_gp.execute(sql_truncate)
        con_gp.commit()
        ## copy fields across
        dest_fields = ['cell_id_rti', 'tech_class',
          'cell_id', 'node_id', 'rnc_id', 'node_name', 'node_description',
          'tower_lat', 'tower_lon', 'grid12',
          'current_flag', 'eff_start', 'eff_end', 'start_date', 'end_date']
        db.Pg.csv2tbl(src_csv_fpath, dest_tbl=conf.CELL_LOCATION_SEED,
            dest_fields=dest_fields, dest_user='gpadmin')

    @staticmethod
    def make_empty_cell_location(cur_local):
        print("Making {}".format(conf.CELL_LOCATION_SEED))
        con_gp, cur_gp = db.Pg.gp(user='gpadmin')
        db.Pg.drop_tbl(con_gp, cur_gp, tbl=conf.CELL_LOCATION_SEED)
        sql_make_tbl = """\
        CREATE TABLE {cell_location}
        (
          cell_location_id SERIAL,
          cell_id_rti integer,
          tech_class text,
          cell_id integer,
          node_id integer,
          rnc_id integer,
          node_name text,
          node_description text,
          tower_lat double precision,
          tower_lon double precision,
          grid12 integer,
          current_flag text,
          eff_start integer,
          eff_end integer,
          start_date text,
          end_date text,
          PRIMARY KEY (cell_location_id)
        )
        """.format(cell_location=conf.CELL_LOCATION_SEED)
        cur_gp.execute(sql_make_tbl)
        con_gp.commit()
        print("Made '{}'".format(conf.CELL_LOCATION_SEED))
        ## Index crap out of it^H^H^H^H^H^H^H^H^H^H^H^H^H^H  http://answers.google.com/answers/threadview/id/386870.html
        ## Note -- must supply a name unlike in vanilla PostgreSQL. And using %s breaks it so complete .format() interpolation it is ;-)
        sql_add_idx_tpl = """\
        CREATE INDEX {fldname} ON {cell_location} ({fldname})
        """
        for fldname in ['cell_id_rti', 'effective_start', 'effective_end']:
            sql = sql_add_idx_tpl.format(cell_location=conf.CELL_LOCATION_SEED,
                fldname=fldname)
            try:
                cur_gp.execute(sql)
            except pg.ProgrammingError:
                con_gp.commit()
        con_gp.commit()
        db.Pg.grant_public_read_permissions(tblname=conf.CELL_LOCATION_SEED)

    @staticmethod
    def make_csm_history(con_local, cur_local, start_after_cob_date_str=None):
        """
        master.cell_sector_master_history
        """
        if not start_after_cob_date_str:
            db.Pg.drop_tbl(con_local, cur_local, conf.CSM_HISTORY)
            sql_make_tbl = """\
            CREATE TABLE {csm_history} (
                id SERIAL,
                cob_date date,
                cell_id text,
                cell_id_rti integer,
                tower text,
                band text,
                freq integer,
                sector integer,
                tower_latitude double precision,
                tower_longitude double precision,
                sector_latitude double precision,
                sector_longitude double precision,
                mb_code text,
                prop_cell_is_of_mb double precision,
                prop_mb_is_of_cell double precision,
                au_code text,
                prop_cell_is_of_au double precision,
                prop_au_is_of_cell double precision,
                unified_longitude double precision,
                unified_latitude double precision,
                address text,
                suburb text,
                city text,
                region text,
                postcode text,
                au_name text,
                region_nz text,
                rto text,
                community_id integer,
                community text,
                persona_id integer,
                persona text,
                PRIMARY KEY (id)
            )
            """.format(csm_history=conf.CSM_HISTORY)
            cur_local.execute(sql_make_tbl)
            con_local.commit()
        sql_insert_tpl = """\
        INSERT INTO {csm_history}
           (cob_date,
            cell_id,
            cell_id_rti,
            tower,
            band,
            freq,
            sector,
            tower_latitude,
            tower_longitude,
            sector_latitude,
            sector_longitude,
            mb_code,
            prop_cell_is_of_mb,
            prop_mb_is_of_cell,
            au_code,
            prop_cell_is_of_au,
            prop_au_is_of_cell,
            unified_longitude,
            unified_latitude,
            address,
            suburb,
            city,
            region,
            postcode,
            au_name,
            region_nz,
            rto,
            community_id,
            community,
            persona_id,
            persona)
        VALUES (
        %s, %s, %s, %s, %s,
        %s, %s, %s, %s, %s,
        %s, %s, %s, %s, %s,
        %s, %s, %s, %s, %s,
        %s, %s, %s, %s, %s,
        %s, %s, %s, %s, %s,
        %s)
        """.format(csm_history=conf.CSM_HISTORY)
        unused_con_gp, cur_gp = db.Pg.gp()
        WHERE_filter = ("WHERE cob_date > '{}'".format(start_after_cob_date_str)
            if start_after_cob_date_str else '')
        sql_get_src = """\
        SELECT *
        FROM {csm_history_src}
        {WHERE_filter}
        ORDER BY cob_date, cell_id_rti
        """.format(csm_history_src=conf.CSM_HISTORY_SRC,
            WHERE_filter=WHERE_filter)
        cur_gp.execute(sql_get_src)
        n = 1
        while True:
            row = cur_gp.fetchone()
            if not row:
                break
            cur_local.execute(sql_insert_tpl, tuple(row))
            if n % 5000 == 0:
                print(prog(n))
            n += 1
        con_local.commit()
        print("Finished making '{}'".format(conf.CSM_HISTORY))

    @staticmethod
    def make_mod_p_locations(con_local, cur_local, fpath, remake_orig=True):
        """
        For every cell_id_rti show all ranges when commissioned/ decommissioned.

        Make an _orig version and then copy to a non-orig version which we are
        likely to tamper with err ... correct.

        remake_orig -- True means fresh ingestion from csv for
        mod_p_locations_orig before copy made.
        """
        debug = False
        db.Pg.drop_tbl(con_local, cur_local, conf.MOD_P_LOCATIONS)
        if remake_orig:
            db.Pg.drop_tbl(con_local, cur_local, conf.MOD_P_LOCATIONS_ORIG)
            sql_make_tbl = """\
            CREATE TABLE {mod_p_locations_orig} (
              id SERIAL,
              cell_id_rti integer,
              cell_id integer,
              node_id integer,
              effective_start date,
              effective_end date,
              tower_lat double precision,
              tower_lon double precision,
              physical_address text,
              description text,
              sector text,
              technology_class text,
              sectortech_alpha text,
              commission_date date,
              decommission_date date,
              location integer,
              sector_status text,
              location_status text,
              location_alpha text,
              easting integer,
              northing integer,
              PRIMARY KEY (id)
            )
            """.format(mod_p_locations_orig=conf.MOD_P_LOCATIONS_ORIG)
            cur_local.execute(sql_make_tbl)
            con_local.commit()
            sql_insert_tpl = """\
            INSERT INTO {mod_p_locations_orig}
            (cell_id_rti,
            cell_id,
            node_id,
            effective_start,
            effective_end,
            tower_lat,
            tower_lon,
            physical_address,
            description,
            sector,
            technology_class,
            sectortech_alpha,
            commission_date,
            decommission_date,
            location,
            sector_status,
            location_status,
            location_alpha,
            easting,
            northing)
            VALUES (
            %s, %s, %s, %s, %s,
            %s, %s, %s, %s, %s,
            %s, %s, %s, %s, %s,
            %s, %s, %s, %s, %s)
            """.format(mod_p_locations_orig=conf.MOD_P_LOCATIONS_ORIG)
            csv.register_dialect('piper', delimiter='|',
                quoting=csv.QUOTE_MINIMAL)
            with open(fpath) as csvfile:
                reader = csv.DictReader(csvfile, dialect='piper')
                for n, row in enumerate(reader, 1):
                    if debug: pp(row)
                    desc = row['DESCRIPTION']
                    ## coordinates
                    lat, lon = spatial.Pg.lat_lon_from_nzmg(cur_local,
                        row['EASTING'], row['NORTHING'])
                    in_bb = (
                        (conf.NZ_BB_MIN_LAT < lat < conf.NZ_BB_MAX_LAT)
                        and
                        (conf.NZ_BB_MIN_LON < lon < conf.NZ_BB_MAX_LON)
                    )
                    if not in_bb:
                        msg = ("On row {:,} {}, {} not in credible NZ bounding "
                            "box (Original easting: {}; northing: {})".format(n,
                            lat, lon, row['EASTING'], row['NORTHING']))
                        print(msg)
                        lat, lon = spatial.Pg.lat_lon_from_nzmg(cur_local,
                            row['NORTHING'], row['EASTING'])
                        in_bb = (
                            (conf.NZ_BB_MIN_LAT < lat < conf.NZ_BB_MAX_LAT)
                            and
                            (conf.NZ_BB_MIN_LON < lon < conf.NZ_BB_MAX_LON)
                        )
                        if in_bb:
                            print("On row {:,} found swapped coordinates so "
                            "successfully corrected them".format(n))
                        else:
                            print("Unable to correct coordinates. Description: "
                                "{}".format(desc))
                            lat, lon = None, None
                        #raise Exception(msg)
                    daniels_latest_date_format = '%d/%m/%y %H:%M:%S'  ## subject to random change according to Daniel's mood
                    start_date = dates.Dates.str_to_date_or_default(
                        row['EFFECTIVE_DATE'],
                        default=datetime.date(1970, 1, 1),
                        date_format=daniels_latest_date_format)
                    end_date = dates.Dates.str_to_date_or_default(
                        row['END_DATE'], date_format=daniels_latest_date_format)
                    if start_date and end_date and start_date > end_date:
                        start_date, end_date = end_date, start_date
                    commissioned_date = dates.Dates.str_to_date_or_default(
                        row['COMMISSION_DATE'],
                        default=datetime.date(1970, 1, 1),
                        date_format=daniels_latest_date_format)
                    decommissioned_date = dates.Dates.str_to_date_or_default(
                        row['DECOMMISSION_DATE'],
                        date_format=daniels_latest_date_format)
                    args = (
                        row['CELL_ID_RTI'],
                        row['CELL_ID'],
                        row['NODE_ID'] or None,
                        start_date,
                        end_date,
                        lat,
                        lon,
                        row['PHYSICAL_ADDRESS'],
                        row['DESCRIPTION'],
                        row['SECTOR'],
                        row['TECHNOLOGY_CLASS'],
                        row['SECTORTECH_ALPHA'],
                        commissioned_date,
                        decommissioned_date,
                        row['LOCATION'],
                        row['SECTOR_STATUS'],
                        row['LOCATION_STATUS'],
                        row['LOCATION_ALPHA'],
                        row['EASTING'],
                        row['NORTHING']
                    )
                    if debug: print(sql_insert_tpl, args)
                    cur_local.execute(sql_insert_tpl, args)
                    if n % 100 == 0:
                        print(prog(n))
            ## Add index
            sql_add_cell_id_rti_index_orig = """\
            CREATE INDEX ON {mod_p_locations_orig} (cell_id_rti)
            """.format(mod_p_locations_orig=conf.MOD_P_LOCATIONS_ORIG)
            cur_local.execute(sql_add_cell_id_rti_index_orig)
            con_local.commit()
        ## Make copy
        sql_make_copy = """\
        CREATE TABLE {mod_p_locations}
        (LIKE {mod_p_locations_orig} INCLUDING INDEXES INCLUDING DEFAULTS)
        """.format(mod_p_locations_orig=conf.MOD_P_LOCATIONS_ORIG,
            mod_p_locations=conf.MOD_P_LOCATIONS)
        cur_local.execute(sql_make_copy)
        sql_insert_into = """\
        INSERT INTO {mod_p_locations} SELECT * FROM {mod_p_locations_orig}
        """.format(mod_p_locations_orig=conf.MOD_P_LOCATIONS_ORIG,
            mod_p_locations=conf.MOD_P_LOCATIONS)
        cur_local.execute(sql_insert_into)
        con_local.commit()
        if remake_orig:
            print("Finished making '{mod_p_locations_orig}' and "
                "'{mod_p_locations}'".format(
                mod_p_locations_orig=conf.MOD_P_LOCATIONS_ORIG,
                mod_p_locations=conf.MOD_P_LOCATIONS))
        else:
            print("Made fresh copy of '{mod_p_locations}' from "
                "'{mod_p_locations_orig}' but didn't refresh "
                "'{mod_p_locations_orig}'".format(
                mod_p_locations_orig=conf.MOD_P_LOCATIONS_ORIG,
                mod_p_locations=conf.MOD_P_LOCATIONS))

    @staticmethod
    def fix4csv(val):
        if val is None:
            val2return = ''
        else:
            try:
                val2return = val.replace('"', "'")
            except (TypeError, AttributeError):
                val2return = val
        return val2return

    @staticmethod
    def overwrite_test_csm(cur_local, skip_csv=False):
        debug = False
        con_gp, cur_gp = db.Pg.gp(user='gpadmin')
        src_csv_fpath = os.path.join(conf.CSM_ROOT, 'storage',
            'test_cell_location.csv')
        ## get local data
        if not skip_csv:
            sql_local_csm = """\
            SELECT *
            FROM {mod_p_locations}
            """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
            cur_local.execute(sql_local_csm)
            print("Extracted local cell location data")
            ## Make csv
            ## 1,386169,121,1508,2016-03-18,,-45...,168...,"112 Night...","Nightcaps 2",257412,L,SNICL2,2016-03-18,,2574,O,O,SNIC
            ##         id   rti  cell node start end lat  lon address desc  sect tech  sta start end  loc   ss   ls   la   e    n
            line_tpl = '{} , {} , {} , {} , {} , {} , {} , {} , "{}" , "{}" , {} , {} , {} , {} , {} , {} , {} , {} , {} , {} , {}\n'.replace(' ','')
            with open(src_csv_fpath, 'w') as f:
                n = 0
                while True:
                    row = cur_local.fetchone()
                    if not row:
                        break
                    n += 1
                    fixed_row = [SeedSource.fix4csv(x) for x in row]
                    line = line_tpl.format(*fixed_row)
                    f.write(line)
                    if debug and n > 10000:
                        break
        ## truncate dest table
        sql_truncate = "TRUNCATE {csm2}".format(csm2=conf.CSM2_GREENPLUM)
        cur_gp.execute(sql_truncate)
        con_gp.commit()
        ## copy fields across
        dest_fields = ['id', 'cell_id_rti', 'cell_id', 'node_id',
            'effective_start', 'effective_end',
            'tower_lat', 'tower_lon',
            'physical_address', 'description',
            'sector', 'technology_class', 'sectortech_alpha',
            'commission_date', 'decommission_date',
            'location', 'sector_status', 'location_status', 'location_alpha',
            'easting', 'northing']
        db.Pg.csv2tbl(src_csv_fpath, dest_tbl=conf.CSM2_GREENPLUM,
            dest_fields=dest_fields, dest_user='gpadmin')

    @staticmethod
    def make_empty_test_csm2(cur_local):
        print("Making {}".format(conf.CSM2_GREENPLUM))
        con_gp, cur_gp = db.Pg.gp()
        db.Pg.drop_tbl(con_gp, cur_gp, tbl=conf.CSM2_GREENPLUM)
        sql_make_tbl = """\
        CREATE TABLE {csm2}
        (
          id integer NOT NULL,
          cell_id_rti integer,
          cell_id integer,
          node_id integer,
          effective_start date,
          effective_end date,
          tower_lat double precision,
          tower_lon double precision,
          physical_address text,
          description text,
          sector text,
          technology_class text,
          sectortech_alpha text,
          commission_date date,
          decommission_date date,
          location integer,
          sector_status text,
          location_status text,
          location_alpha text,
          easting integer,
          northing integer,
          PRIMARY KEY (id)
        )
        """.format(csm2=conf.CSM2_GREENPLUM)
        cur_gp.execute(sql_make_tbl)
        con_gp.commit()
        ## Index crap out of it^H^H^H^H^H^H^H^H^H^H^H^H^H^H  http://answers.google.com/answers/threadview/id/386870.html
        ## Note -- must supply a name unlike in vanilla PostgreSQL. And using %s breaks it so complete .format() interpolation it is ;-)
        sql_add_idx_tpl = """\
        CREATE INDEX {fldname} ON {csm2} ({fldname})
        """
        for fldname in ['cell_id_rti', 'effective_start', 'effective_end']:
            sql = sql_add_idx_tpl.format(csm2=conf.CSM2_GREENPLUM,
                fldname=fldname)
            try:
                cur_gp.execute(sql)
            except pg.ProgrammingError:
                con_gp.commit()
        #cur_rem.execute('CREATE INDEX fred ON john_test.csm2 (cell_id_rti)')
        con_gp.commit()
        db.Pg.grant_public_read_permissions(tblname=conf.CSM2_GREENPLUM)
        print("Finished making '{csm2}'".format(csm2=conf.CSM2_GREENPLUM))

def freshen_src(con_local, cur_local, some_stale_used=False,
        make_fresh_mod_p_src=True, make_fresh_csm_history_src=True,
        mod_p_src_fname=None, start_after_cob_date_str=None):
    """
    Freshen two data sources - CSM and mod_p. Also makes an orig version of
    mod_p which is used to work out how to modify the working mod_p (CSM 2.0).
    """
    some_fresh_src = make_fresh_mod_p_src or make_fresh_csm_history_src
    if some_fresh_src and some_stale_used:
        resp = input("Do not proceed using stale data unless source data is "
            "unchanged (even though refreshing it). Source data unchanged? "
            "(y/n)")
        if resp.lower() != 'y':
            raise Exception("Set all stale settings to False before "
                "refreshing any source data unless just redoing the "
                "mod_p_locations table even though no new data")
    if make_fresh_csm_history_src:
        if start_after_cob_date_str is None:
            raise Exception("Please set start_after_cob_date_str")
        SeedSource.make_csm_history(con_local, cur_local,
            start_after_cob_date_str)
    else:
        warn("Not making fresh '{}' table".format(conf.CSM_HISTORY))
    mod_p_src_fpath = ("{}/storage/{}".format(conf.CSM_ROOT, mod_p_src_fname))
    warn("Make sure '{}' is the most up-to-date version".format(
        mod_p_src_fpath))
    if make_fresh_mod_p_src:
        if mod_p_src_fname is None:
            raise Exception("Please set mod_p_src_fname")
        SeedSource.make_mod_p_locations(con_local, cur_local, mod_p_src_fpath,
            remake_orig=True)
    else:
        SeedSource.make_mod_p_locations(con_local, cur_local, mod_p_src_fpath,
            remake_orig=False)  ## still need fresh copy or else making additions and replacements again and again to the same table ;-)
        warn("Not making fresh '{}' and '{}' tables".format(
            conf.MOD_P_LOCATIONS_ORIG, conf.MOD_P_LOCATIONS))
    if make_fresh_mod_p_src or make_fresh_csm_history_src:
        input("Set make_fresh_mod_p_src and make_fresh_csm_history_src to False"
            " before proceeding")

def get_dribblers_and_bounds(cell_id_rtis):
    """
    Dribbler - the average for days when it appears activated (at least 100
    imsi's) is less than 500.
    """
    debug = False
    verbose = False
    cell_id_rti_clause = db.Pg.nums2clause(cell_id_rtis)
    sql = """\
    SELECT
    cell_id_rti,
      AVG(CASE WHEN n_imsis >= 100 THEN n_imsis ELSE 0 END) AS -- ignore dribblers when determining importance
    avg_imsis_ex_dribblers,
    -- SUM(CASE WHEN n_imsis < 100 THEN 1 ELSE 0 END)*100/ COUNT(*) AS
    -- low_imsi_pct,
      MIN(CASE WHEN n_imsis >= 800 THEN date ELSE null END) AS  -- ignore tails
    start_date_major,
      MAX(CASE WHEN n_imsis >= 800 THEN date ELSE null END) AS
    end_date_major,
      MIN(CASE WHEN n_imsis >= 100 THEN date ELSE null END) AS  -- ignore tails
    start_date_effective,
      MAX(CASE WHEN n_imsis >= 100 THEN date ELSE null END) AS
    end_date_effective
    FROM (
      SELECT
      cell_id_rti,
      date,
        SUM(distinct_imsi) AS
      n_imsis
      FROM {daily_connections}
      WHERE cell_id_rti IN {cell_id_rti_clause}
      GROUP BY cell_id_rti, date
    ) AS daily_n_imsis
    GROUP BY cell_id_rti
    ORDER BY SUM(n_imsis), cell_id_rti
    """.format(cell_id_rti_clause=cell_id_rti_clause,
        daily_connections=conf.DAILY_CONNECTION_DATA)
    unused_con_gp, cur_gp = db.Pg.gp()
    cur_gp.execute(sql)
    data = cur_gp.fetchall()
    dribbler_cell_id_rtis = []
    bounds = []
    for row in data:
        (cell_id_rti, avg_imsis_ex_dribblers, #low_imsi_pct,
             start_date_major, end_date_major,
             start_date_effective, end_date_effective) = row
        if cell_id_rti == 12160:
            pass
        if debug and verbose:
            print(cell_id_rti, avg_imsis_ex_dribblers, #low_imsi_pct,
                start_date_major, end_date_major,
                start_date_effective, end_date_effective)
        dribbler = (avg_imsis_ex_dribblers < 500)
        if dribbler:
            dribbler_cell_id_rtis.append(cell_id_rti)
        else:
            bounds.append(
                {'cell_id_rti': cell_id_rti,
                 'start_date_major': start_date_major,
                 'end_date_major': end_date_major,
                 'start_date_effective': start_date_effective,
                 'end_date_effective': end_date_effective,
                })
    ## some cell_id_rti's have no connection data at all so will be lost. We want to count these as dribblers.
    bounds_cell_id_rtis = {dets['cell_id_rti'] for dets in bounds}
    missing_dribblers = (set(cell_id_rtis) - set(dribbler_cell_id_rtis)
        - bounds_cell_id_rtis)
    dribbler_cell_id_rtis += list(missing_dribblers)
    dribbler_cell_id_rtis.sort()
    bounds.sort(key=lambda d: d['cell_id_rti'])
    if debug:
        print(dribbler_cell_id_rtis)
        pp(bounds)
    return dribbler_cell_id_rtis, bounds

def print_additions(additions):
    print('..._additions = [')  ## add old versions of 4G alternatives
    for addition in additions:
        print('    ', addition, ',', sep='')
    print("]")

def print_replacements(replacements):
    print("...replacements = [")  ## replace records for new versions of alternative 4G cell_id_rti's with record which has corrected dates
    for replacement in replacements:
        cell_id_rti = replacement[0]
        print("        {'cell_id_rti': %s, 'replacement_rows': [%s]}," % (
            cell_id_rti, replacement))
    print("]")

def format_replacement_confs(replacement_confs):
    txt_lst = []
    txt_lst.append("...replacements = [")  ## replace records for new versions of alternative 4G cell_id_rti's with record which has corrected dates
    for replacement_conf in replacement_confs:
        txt_lst.append(str(replacement_conf) + ',')
    txt_lst.append("]")
    return "\n".join(txt_lst)

def _in_water(cur_local):
    debug = False
    sql = """\
    SELECT *
    FROM (
      SELECT
        id,
        cell_id_rti,
        tower_lat,
        tower_lon,
        effective_start,
        effective_end,
          MAX(contained::int) = 0 AS
        in_water
      FROM (
        SELECT
          id,
          cell_id_rti,
          tower_lat,
          tower_lon,
          effective_start,
          effective_end,
            ST_CONTAINS(
            region_geom,
            ST_GeomFromText(
              'POINT(' || tower_lon || ' ' || tower_lat || ')', {srid}
            )) AS
          contained
        FROM {mod_p_locations}
        CROSS JOIN
        (
          SELECT geom AS
            region_geom
          FROM {local_region}
        ) AS region_geoms
      ) AS src
      GROUP BY
        id,
        cell_id_rti,
        tower_lat,
        tower_lon,
        effective_start,
        effective_end
      ) AS src2
    WHERE in_water 
    """.format(srid=conf.WGS84_SRID, mod_p_locations=conf.MOD_P_LOCATIONS,
        local_region='region')
    cur_local.execute(sql)
    data = cur_local.fetchall()
    if debug: pp(data)
    return data

def handle_water_dets(cur_local):
    print("Checking for water details (may take a while)")
    water_dets = _in_water(cur_local)
    if water_dets:
        print("Add new records to mod_mod_extra.Config.custom_replacements with"
            " corrected lat/lons based on addresses and checking Google map for"
            " alleged but in-water coordinates")
        pp(water_dets)
        raise Exception(FRESH_RUN_MSG)
    input("Set check_water to False before continuing")


class Alt4G():

    @staticmethod
    def _make_switch_over_chart(cell_id_rti_a, cell_id_rti_b, new_data):
        fig = plt.figure(figsize=(150, 9))
        ax = fig.add_subplot(111)
        chart_title = ("Switchover between {} and {}?".format(cell_id_rti_a,
            cell_id_rti_b))
        x_a = [row['day'] for row in new_data
            if row['cell_id_rti'] == cell_id_rti_a]
        y_a = [row['imsi_n'] for row in new_data
            if row['cell_id_rti'] == cell_id_rti_a]
        x_b = [row['day'] for row in new_data
            if row['cell_id_rti'] == cell_id_rti_b]
        y_b = [row['imsi_n'] for row in new_data
            if row['cell_id_rti'] == cell_id_rti_b]
        mpl.Mpl.set_titles(ax, chart_title)
        mpl.Mpl.set_ticks(ax, inc_minor=False)
        mpl.Mpl.set_axis_labels(ax, y_label='N unique IMSIs')
        handles = []
        lbls = []
        if x_a:
            line_a, = plt.plot(x_a, y_a, 'red', linewidth=4)
            handles.append(line_a)
            lbls.append("cell_id_rti: {}".format(cell_id_rti_a))
        if x_b:
            line_b, = plt.plot(x_b, y_b, 'blue', linewidth=4)
            handles.append(line_b)
            lbls.append("cell_id_rti: {}".format(cell_id_rti_b))
        if not handles:
            return ('No chart for {} and {} - no data to display'
                .format(cell_id_rti_a, cell_id_rti_b))
        mpl.Mpl.set_legends(ax, handles, lbls, fontsize=30)
        #plt.show()
        html_rel_img_path = ('images/charts/{}_and_{}_switch_over.png'
            .format(cell_id_rti_a, cell_id_rti_b))
        fs_rel_img_path = 'csm/reports/' + html_rel_img_path
        try:
            plt.savefig(fs_rel_img_path, bbox_inches='tight')
        except Exception as e:
            print("Error making chart to compare {} and {}. Orig error:\n{}"
                .format(cell_id_rti_a, cell_id_rti_b, e))
            print('x_a', x_a)
            print('y_a', y_a)
            print('x_b', x_b)
            print('y_b', y_b)
            raise
        plt.close(fig)
        return html_rel_img_path

    @staticmethod
    def make_changeover_charts(alternative_4G_cell_id_rti_dets):
        """
        renamed_cell_id_rti_dets generated in find_original_name_cell_id_rtis()
        from source list of missing cell_id_rti's (in CSM but not mod_p).
        """
        debug = False
        cell_id_rti_pairs = [(d['csm_rti'], d['alt_mod_p_rti'])
            for d in alternative_4G_cell_id_rti_dets]
        unused_con_gp, cur_gp = db.Pg.gp()
        sql = """\
        SELECT
        cell_id_rti,
        date,
          SUM(distinct_imsi) AS
        imsi_n
        FROM {daily_connections}
        WHERE cell_id_rti IN(%s, %s)
        GROUP BY cell_id_rti, date
        ORDER BY date, cell_id_rti
        """.format(daily_connections=conf.DAILY_CONNECTION_DATA)
        title_main = "Switch over from old to new 4G cell_id_rti"
        html = [conf.HTML_START_TPL % {'title': title_main, 'more_styles': ''},
            "<p>Identify time to switch from one to the other</p>",
        ]
        for cell_id_rti_a, cell_id_rti_b in cell_id_rti_pairs:
            cur_gp.execute(sql, (cell_id_rti_a, cell_id_rti_b))
            data = cur_gp.fetchall()
            new_data = [
                {'cell_id_rti': cell_id_rti,
                 'day': datetime.datetime.strptime(date_str, '%Y-%m-%d'),
                 'imsi_n': int(imsi_n)}
                for cell_id_rti, date_str, imsi_n in data]
            if debug: pp(new_data)
            html_rel_img_path = Alt4G._make_switch_over_chart(cell_id_rti_a,
                cell_id_rti_b, new_data)
            html.append("<img width='3200px' padding=0 margin=0 src='{}'>".format(
                html_rel_img_path))
            #break
        ## assemble
        content = "\n".join(html)
        fpath = ("{}/reports/switch_over_from_old_to_new_4G_cell_id_rtis.html"
            .format(conf.CSM_ROOT))
        with open(fpath, 'w') as f:
            f.write(content)
        open_new_tab("file://{}".format(fpath))

    @staticmethod
    def find_alt_4G_rtis_dets(cur_local, csm_only_cell_id_rtis):
        """
        If you are looking for historical information on a person you may need
        to search for them using their maiden name rather than their married
        name. Similarly, if we are trying to get locations for historical
        connection data we will need to look for it under the cell_id_rti it was
        associated with at the time. Unfortunately, the Mod_P data records its
        whole history under the "married name"/most recent cell_id_rti. So we
        need to correct the cell_id_rti's for some records to be for the earlier
        cell_id_rti e.g.

        Mrs Smith    2015-01-01  2016-05-01  Auckland
        Mrs Smith    2016-07-01              Wellington

        needs correction to:

        Ms  Redrock  2015-01-01  2016-05-01  Auckland
        Mrs Smith    2016-07-01              Wellington

        so when we have data on Ms Redrock in 2015 we can find that she lived in
        Auckland. If we leave the data as it was there is no record for Ms
        Redrock and we won't be able to get a location.

        Similarly we might need to change
        290869       2015-01-01  2016-05-01  Auckland
        290869       2016-07-01              Wellington

        to

        151227189    2015-01-01  2016-05-01  Auckland
        290869       2016-07-01              Wellington
        """
        debug = False
        alt_4G_rtis_dets = []
        """
        We have a reference cell_id_rti from the CSM data and its cell_id e.g.
        151227189 and 53 (node id is 590731). We can use the tower data to make
        the match (csm.tower = mod_p.location_alpha). Another option that works
        is to use lat/lon (e.g. within, say, 100m?).
        """
        sql_alternatives_tpl = """\
        SELECT *
        FROM (
          SELECT DISTINCT
            csm_ref.cell_id_rti AS
          csm_rti,
            mod_p_src.cell_id_rti AS
          alt_mod_p_rti,
            mod_p_src.location_alpha AS
          mod_p_tower,
            csm_ref.tower AS
          csm_tower,
            %s AS
          csm_cell_id,  -- not from CSM but derived from cell_id_rti from CMS
            mod_p_src.cell_id AS
          mod_p_cell_id
          FROM {mod_p_locations_orig} AS mod_p_src, (
            SELECT DISTINCT ON (cell_id_rti)
            cell_id_rti,
            tower
            FROM {csm_history}
            WHERE cell_id_rti = %s
            ORDER BY cell_id_rti, cob_date DESC
          ) AS csm_ref
        ) AS inputs
        WHERE mod_p_tower = csm_tower
        AND mod_p_cell_id = csm_cell_id
        """.format(csm_history=conf.CSM_HISTORY,
            mod_p_locations_orig=conf.MOD_P_LOCATIONS_ORIG)
        if debug: unfound_cell_id_rtis = []
        for csm_only_cell_id_rti in csm_only_cell_id_rtis:
            unused_node_id, cell_id = extract_4g_node_and_cell_id(
                csm_only_cell_id_rti)
            cur_local.execute(sql_alternatives_tpl,
                (cell_id, csm_only_cell_id_rti))
            data = cur_local.fetchall()
            if not data:
                if debug: unfound_cell_id_rtis.append(csm_only_cell_id_rti)
                print("Unable to find alternative to {} in Mod_P data.".format(
                    csm_only_cell_id_rti))
            else:
                alt_4G_rtis_dets.extend([dict(row) for row in data])
        if debug:
            print(unfound_cell_id_rtis)
            pp(alt_4G_rtis_dets)
        return alt_4G_rtis_dets


class ApplyChanges(): 

    sql_insert_tpl = """\
    INSERT INTO {mod_p_locations}
    (cell_id_rti,
    cell_id,
    node_id,
    effective_start,
    effective_end,
    tower_lat,
    tower_lon,
    physical_address,
    description,
    sector,
    technology_class,
    sectortech_alpha,
    commission_date,
    decommission_date,
    location,
    sector_status,
    location_status,
    location_alpha,
    easting,
    northing)
    VALUES (
    %s, %s, %s, %s, %s,
    %s, %s, %s, %s, %s,
    %s, %s, %s, %s, %s,
    %s, %s, %s, %s, %s)
    """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
    
    sql_del_tpl = """
    DELETE FROM {mod_p_locations}
    WHERE cell_id_rti = %s
    """.format(mod_p_locations=conf.MOD_P_LOCATIONS)

    @staticmethod
    def replace_rti_rows(cur_local, replacements):
        """
        Delete all rows for each cell_id_rti and then add new rows. These will
        be modifications of existing rows and possibly the addition of
        completely new rows for that cell_id_rti as well.
    
        replacements -- e.g. [
        {
            'cell_id_rti': 385919,
            'replacement_rows': [
                (385919, 127, 1507, "2016-11-04", None,\
                -41.2710682648392, 173.283265495865,
                "110 Trafalgar Street Nelson", "Nelson City 2",
                156422, "L", "SNELL2", "2014-06-05", "",
                1564, "O", "O", "SNEL", 2533720, 5992816),
            ]
        }, ...
        ]
        """
        debug = False
        n_replacements = len(replacements)
        for n, cell_id_rti_dets in enumerate(replacements, 1):
            cur_local.execute(ApplyChanges.sql_del_tpl,
                (cell_id_rti_dets['cell_id_rti'], ))
            for row in cell_id_rti_dets['replacement_rows']:
                if debug: pp(row)
                try:
                    cur_local.execute(ApplyChanges.sql_insert_tpl, row)
                except pg.IntegrityError as e:
                    print(str(cur_local.query, encoding='utf-8'))
                    raise Exception("Unable to process replacement row {} - "
                        "orig error: {}".format(row, e))
                except (TypeError, IndexError) as e:
                    raise Exception("Unable to process replacement row {} - "
                        "orig error: {}".format(row, e))
            if n % 100 == 0:
                print(prog(n, tot=n_replacements))
        #con_local.commit()

    @staticmethod
    def add_rows_for_new_rtis(cur_local, rows_for_new_rtis):
        debug = False
        n_additions = len(rows_for_new_rtis)
        for n, row in enumerate(rows_for_new_rtis, 1):
            if debug: pp(row)
            try:
                cur_local.execute(ApplyChanges.sql_insert_tpl, row)
            except pg.DataError as e:
                print(str(cur_local.query, encoding='utf-8'))
                raise Exception("Unable to add new row. Orig error: {}"
                    .format(e))
            if n % 100 == 0:
                print(prog(n, tot=n_additions))
        #con_local.commit()

    @staticmethod
    def delete_rti_rows(cur_local, cell_id_rti, reason):
        cur_local.execute(ApplyChanges.sql_del_tpl, (cell_id_rti, ))
        #con_local.commit()
        print("Deleted cell_id_rti {} because {}".format(cell_id_rti, reason))


class FillMissing():
    """
        SwitchDets(
            15105823, None, datetime.date(2017, 7, 4),
            482847, datetime.date(2017, 6, 30), None
        ),
    """
    alt_4G_range_data = [  ## mod_p is assumed to be what is in mod_p from Daniel, CSM is what was in csm_history but not in mod_p except under an alternative 4G rti

    ]
    @staticmethod
    def get_csm_only_rtis(cur_local, use_stale_csm_only_rtis=False):
        """
        Identify CSM-only cell_id_rtis (a.k.a. the unfindables).
        """
        if use_stale_csm_only_rtis:
            csm_only_rtis = stale.Stale.csm_only_rtis
        else:
            sql_unfindables = """\
            -- unfindables
            SELECT DISTINCT cell_id_rti
            FROM {csm_history}
            LEFT JOIN
            {mod_p_locations_orig}
            USING(cell_id_rti)
            WHERE {mod_p_locations_orig} IS NULL
            ORDER BY cell_id_rti
            """.format(csm_history=conf.CSM_HISTORY,
                mod_p_locations_orig=conf.MOD_P_LOCATIONS_ORIG)
            cur_local.execute(sql_unfindables)
            csm_only_rtis = [row['cell_id_rti']
                for row in cur_local.fetchall()]
            print(csm_only_rtis)
            input("Set mod_mod_stale.Stale.csm_only_rtis to the above and set "
                "use_stale_csm_only_rtis to True")
        return csm_only_rtis

    @staticmethod
    def check_or_help_make_4G_conf(cur_local, csm_only_rtis,
            use_stale_4G_rtis_dets=False):
        """
        Some of the CSM-only cell_id_rti's do in fact appear in mod_p but under
        a different 4G alternative because the node_id has changed. E.g.
        151227189 is in CSM only but it is really the same as 290869 (different
        node_id). We need to add records to mod_p and change dates so that the
        new variant (usually shorter) ends at the time the other begins (albeit
        with small overlap because connection data overlaps slightly).
        """
        debug = False
        if use_stale_4G_rtis_dets:
            alt_4G_rtis_dets = stale.Stale.alt_4G_rtis_dets
        else:
            alt_4G_rtis_dets = Alt4G.find_alt_4G_rtis_dets(cur_local,
                csm_only_rtis)
            pp(alt_4G_rtis_dets)
            input("Set mod_mod_stale.Stale.alt_4G_rtis_dets to the above and "
                "set use_stale_4G_rtis_dets to True")
        alt_4G_rtis2add = [row['csm_rti'] for row in alt_4G_rtis_dets]
        ## a) check consistency with additions already configured above (ALL of
        ## the CSM-only 4G alternative cell_id_rtis must appear in additions -
        ## we only use replacements to modify the matching 4G alternative
        ## already in mod_p)
        all_alt_4G_additions_conf = (conf4G.Config.custom_alt_4G_additions
            + conf4G.Config.alt_4G_additions)
        csm_only_alt_4G_rtis_conf_adding = [addition_conf[0]
            for addition_conf in all_alt_4G_additions_conf]
        ## get discrepancies
        conf_only_rtis = sorted(list(
            set(csm_only_alt_4G_rtis_conf_adding) - set(alt_4G_rtis2add)))
        data_check_only_rtis = sorted(list(
            set(alt_4G_rtis2add) - set(csm_only_alt_4G_rtis_conf_adding)))  ## those required - those handled = those left to handle
        if conf_only_rtis:
            if debug: print(conf_only_rtis)
            print("Remove the following from mod_mod_4G_config.Config"
                ".custom_alt_4G_additions and mod_mod_4G_config.Config"
                ".alt_4G_additions before running this again. Next time the "
                "auto config will be supplied.:\n{}".format(conf_only_rtis))
            raise Exception(FRESH_RUN_MSG)
        if data_check_only_rtis:
            """
            Need to make two sets of tuples - one for old 4G alternative
            cell_id_rti's and one for the new 4G alternative in the pair.
            Each tuple must contain all the fields required to populate a
            record in mod_p. Also populate a list of tricky pairs that
            require custom handling.

            The data tups need to have modified effective start and ends and
            commission/decommission dates. The new alternative (already in
            mod_p) should keep its starts but end a week after the
            transition. The old alternative (in the CSM) should keep its end
            but start a week before the transition.

            Effective transition dates can be determined most safely by
            running comparison charts.

                    SwitchDets(
                        15105823, None, datetime.date(2017, 7, 4),
                        482847, datetime.date(2017, 6, 30), None
                    ),
            """
            if debug: print("data_check_only_rtis", data_check_only_rtis)
            if not FillMissing.alt_4G_range_data:
                cell_id_rti_dets_to_chart = [row
                    for row in alt_4G_rtis_dets if row['csm_rti']
                    in data_check_only_rtis]
                Alt4G.make_changeover_charts(cell_id_rti_dets_to_chart)
                print("Populate FillMissing.alt_4G_range_data (inside the "
                    "mod_mod.py script) based on chart results.\nFirst item is "
                    "from csm history (RED); second is the mod_p alternative "
                    "(BLUE).\nTypically same order as lines in charts.\n Rerun "
                    "and update config as instructed.\nThen comment out "
                    "alt_4G_range_data")
                raise Exception(FRESH_RUN_MSG)
            else:
                resp = input("If you haven't just refreshed "
                    "FillMissing.alt_4G_range_data inside mod_mod.py script "
                    "then stop and wipe it before proceeding. Just refreshed "
                    "it? (y)")
                if resp.lower() != 'y':
                    raise Exception("Wipe alt_4G_range_data inside mod_mod.py"
                        "script, set use_stale_4G_rtis_dets to True, and rerun")
                """
                Need to make simple data tups for all old alternative 4G
                cell_id_rti's and for new alternative 4G cell_id_rti's that
                only have one record in the mod_p data. For those that have
                multiple records in mod_p data, list them so custom data can
                be made.
                """
                sql_cell_id_rti_rows_tpl = """\
                SELECT *
                FROM {mod_p_locations_orig}
                WHERE cell_id_rti = %s
                ORDER BY effective_start
                """.format(mod_p_locations_orig=conf.MOD_P_LOCATIONS_ORIG)
                additions = []
                replacements = []
                range_data_to_custom_handle = []
                for range_data in FillMissing.alt_4G_range_data:  ## safe to add
                    ## get matching records from mod_p using newer alternative version of 4G cell_id_rti
                    cur_local.execute(sql_cell_id_rti_rows_tpl,
                        (range_data.mod_p_rti, ))
                    cell_id_rti_data = cur_local.fetchall()
                    n_rows = len(cell_id_rti_data)
                    if n_rows == 1:  ## a simple case so safe to automate
                        ## Change dates and make addition and replacement
                        row_list = list(cell_id_rti_data[0])
                        del row_list[0]
                        idx_cell_id_rti = 0  ## idxs given removal of id field
                        idx_start = 3
                        idx_end = 4
                        idx_commission = 12
                        idx_decommission = 13
                        ## e.g. 151227189 from CSM (only). Putting new end date on so stops around point of change to new.
                        addition = row_list.copy()  ## the old one needing to be added to the new mod_p data
                        addition[idx_cell_id_rti] = range_data.csm_rti
                        addition[idx_end] = range_data.csm_end
                        addition[idx_decommission] = range_data.csm_end
                        addition = tuple(addition)
                        if debug: print(addition)
                        additions.append(addition)
                        ## e.g. 290869 in mod_p at least. Putting new start date on so starts just before transition from old.
                        replacement = row_list.copy()  ## the old one needing to be added to the new mod_p data
                        replacement[idx_cell_id_rti] = range_data.mod_p_rti
                        replacement[idx_start] = range_data.mod_p_start
                        replacement[idx_commission] = range_data.mod_p_start
                        replacement = tuple(replacement)
                        if debug: print(replacement)
                        replacements.append(replacement)
                        ## mustn't create a contradiction
                        for data_row in [addition, replacement]:
                            if (data_row[idx_start]
                                    and data_row[idx_end]
                                    and data_row[idx_start]
                                    > data_row[idx_end]):
                                raise Exception("Contradictory date range "
                                    "created between start of '{}' and end "
                                    "of '{}' for {}".format(
                                    data_row[idx_start],
                                    data_row[idx_end],
                                    range_data))
                    elif n_rows > 1:
                        range_data_to_custom_handle.append(range_data)
                    else:
                        if debug: print(range_data)
                print("Add to mod_mod_4G_config.Config.alt_4G_additions and "
                    "mod_mod_4G_config.Config.alt_4G_replacements")
                print_additions(additions)
                print_replacements(replacements)
                print("range_data_to_custom_handle: {} - if not an empty list "
                    "then more than one record in mod_p_locations_orig under "
                    "new 4G alias (shorter). In which case set "
                    "custom_alt_4G_replacements to fix existing ones as "
                    "appropriate and custom_alt_4G_additions to add the new "
                    "versions: ".format(range_data_to_custom_handle))
                print("Either way, wipe FillMissing.alt_4G_range_data inside "
                    "mod_mod.py script")
                raise Exception(FRESH_RUN_MSG)
        return alt_4G_rtis2add, csm_only_alt_4G_rtis_conf_adding

    @staticmethod
    def check_or_help_make_dribblers_and_get_fresh_bounds(
            remaining_csm_only_rtis, use_stale_dribblers=False):
        """
        The focus is on dribblers but the process of identifying dribblers also
        generates fresh bounds data needed by later steps. So not the neatest
        separation of concerns but more efficient than if getting fresh_bounds
        again in an expensive process.
        """
        if use_stale_dribblers:
            actual_dribblers = stale.Stale.dribblers
            fresh_bounds = None
        else:
            actual_dribblers, fresh_bounds = get_dribblers_and_bounds(
                remaining_csm_only_rtis)
            print(actual_dribblers)
            input("Set mod_mod_stale.Stale.dribblers to the above and set "
                "use_stale_dribblers to True")
        if set(extra.Config.dribblers) != set(actual_dribblers):
            print("Reset mod_mod_extra.Config.dribblers to actual_dribblers")
            print('dribblers =', actual_dribblers)
            raise Exception(FRESH_RUN_MSG)
        return fresh_bounds

    @staticmethod
    def check_or_help_add_csm_derived_conf(cur_local, remaining_csm_only_rtis,
            fresh_bounds, use_stale_bounds=False):
        """
        Mop up all the remaining cell_id_rti's still missing from mod_p even
        after handling 4G alternatives and dribblers but found in the old CSM
        1.0 by bringing them across from CSM 1.0.
        """
        debug = False
        if use_stale_bounds:
            bounds = stale.Stale.bounds
        else:
            if fresh_bounds:
                bounds = fresh_bounds
            else:
                unused_dribblers, bounds = get_dribblers_and_bounds(
                    remaining_csm_only_rtis)
                pp(bounds)
                input("Add to the above to mod_mod_stale.Stale.bounds and set "
                    "use_stale_bounds to True")
        ## a) check consistency with additions already configured
        csm_derived_rtis2add = [dets['cell_id_rti'] for dets in bounds]
        rtis_requiring_fix = csm_derived_rtis2add
        conf_adding_rtis = [addition_conf[0]
            for addition_conf in extra.Config.csm_derived_additions]
        ## get discrepancies
        conf_only_rtis = sorted(list(
            set(conf_adding_rtis) - set(rtis_requiring_fix)))
        data_check_only_rtis = sorted(list(
            set(rtis_requiring_fix) - set(conf_adding_rtis)))
        if conf_only_rtis:
            if debug: print("conf_only_rtis", conf_only_rtis)
            print("Wipe mod_mod_extra.Config.csm_derived_additions and start "
                "again")
            raise Exception(FRESH_RUN_MSG)
        if data_check_only_rtis:
            if debug: print("data_check_only_rtis", data_check_only_rtis)
            fresh_bounds_dets = [dets for dets in bounds
                if dets['cell_id_rti']
                in data_check_only_rtis]
            fresh_csm_derived_additions = []
            sql_get_csm_fields_tpl = """\
            SELECT *
            FROM csm
            WHERE cob_date = (SELECT max(cob_date) FROM csm)
            AND cell_id_rti = %s
            """
            """
            Get additions to add from bounds details. Use data from CSM as
            best as possible to populate mod_p.
            """
            for n, dets in enumerate(fresh_bounds_dets, 1):
                cell_id_rti = dets['cell_id_rti']
                if debug: print(cell_id_rti)
                effective_start = dets['start_date_effective'] 
                effective_end = dets['end_date_effective']
                cur_local.execute(sql_get_csm_fields_tpl, (cell_id_rti, ))
                csm_fields_row = cur_local.fetchone()
                if csm_fields_row['band'] == '4g':
                    technology_class = 'L'
                elif csm_fields_row['band'] == '3g':
                    technology_class = 'U'
                else:
                    technology_class = None
                csm_derived_addition = (cell_id_rti, None, None,
                    effective_start, effective_end,
                    csm_fields_row['tower_latitude'],
                    csm_fields_row['tower_longitude'],
                    csm_fields_row['address'], csm_fields_row['address'],
                    csm_fields_row['cell_id'], technology_class, None,
                    effective_start, effective_end, None, None, None, None,
                    None, None)
                fresh_csm_derived_additions.append(csm_derived_addition)
                if n % 25 == 0:
                    print(prog(n, tot=len(fresh_bounds_dets)))
            fname = 'csm_derived_additions.txt'
            fpath = os.path.join(conf.CSM_ROOT, 'storage', fname)
            print("Open '{}' and add contents to "
                "mod_mod_extra.Config.csm_derived_additions".format(fpath))
            with open(fpath, 'w') as f:
                for addition in fresh_csm_derived_additions:
                    f.write('    {},\n'.format(addition))
            #print_additions(fresh_csm_derived_additions)
            raise Exception(FRESH_RUN_MSG)
        return csm_derived_rtis2add

    @staticmethod
    def audit(csm_only_rtis, alt_4G_rtis2add, csm_derived_rtis2add):
        n_csm_only = len(csm_only_rtis)
        n_alternative_4G = len(alt_4G_rtis2add)
        n_dribblers = len(extra.Config.dribblers)
        n_csm_derived = len(csm_derived_rtis2add)
        n_recovered = n_alternative_4G + n_dribblers + n_csm_derived
        print("{:<35}{:>4,}".format("N original CSM-only cell_id_rti's",
            n_csm_only))
        print("{:<35}{:>4,}".format("N recovered", n_recovered))
        print("-"*39)
        print("{:<35}{:>4,}".format("N alt 4G cell_id_rti's",
            n_alternative_4G))
        print("{:<35}{:>4,}".format("N dribbler cell_id_rti's", n_dribblers))
        print("{:<35}{:>4,}".format("N CSM-derived cell_id_rti's",
            n_csm_derived))


class TestConsolidation(unittest.TestCase):

    unused, cur_local = db.Pg.local()

    def test_overlapping(self):
        tests = [
            ([
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2017, 1, 1),
                 },   
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 6, 1),
                  'effective_end': datetime.date(2016, 7, 1),
                 },   
                 {'tower_lat': -41,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 2, 1),
                  'effective_end': datetime.date(2017, 6, 1),
                 },   
             ]
                ,
             [
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2017, 1, 1),
                  'commission_date': datetime.date(2016, 1, 1),
                  'decommission_date': datetime.date(2017, 1, 1),
                 },
                 {'tower_lat': -41,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 2, 1),
                  'effective_end': datetime.date(2017, 6, 1),
                  'commission_date': datetime.date(2017, 2, 1),
                  'decommission_date': datetime.date(2017, 6, 1),
                 },
             ]
            ),
            ([
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2017, 1, 1),
                 },   
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 6, 1),
                  'effective_end': datetime.date(2016, 7, 1),
                 },   
                 {'tower_lat': -40.0005,  ## within 500mm so considered the same
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 8, 1),  ## only a month apart so not big enough to keep separate
                  'effective_end': datetime.date(2017, 6, 1),
                 },   
             ]
                ,
             [
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2017, 6, 1),
                  'commission_date': datetime.date(2016, 1, 1),
                  'decommission_date': datetime.date(2017, 6, 1),
                 },
             ]
            ),
            ([
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2017, 1, 1),
                 },   
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 6, 1),
                  'effective_end': datetime.date(2016, 7, 1),
                 },   
                 {'tower_lat': -40.01,  ## more than 500m so should require a new range
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 2, 1),
                  'effective_end': datetime.date(2017, 6, 1),
                 },   
             ]
                ,
             [
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2017, 1, 1),
                  'commission_date': datetime.date(2016, 1, 1),
                  'decommission_date': datetime.date(2017, 1, 1),
                 },
                 {'tower_lat': -40.01,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 2, 1),
                  'effective_end': datetime.date(2017, 6, 1),
                  'commission_date': datetime.date(2017, 2, 1),
                  'decommission_date': datetime.date(2017, 6, 1),
                 },
             ]
            ),
            ([
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': None, 
                 },   
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 6, 1),
                  'effective_end': datetime.date(2016, 7, 1),
                 },   
             ]
                ,
             [
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': None,
                  'commission_date': datetime.date(2016, 1, 1),
                  'decommission_date': None,
                 },
             ]
            ),
            ([
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': None, 
                 },   
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 6, 1),
                  'effective_end': datetime.date(2016, 7, 1),
                 },
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 6, 2),
                  'effective_end': datetime.date(2016, 7, 1),
                 },
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 6, 11),
                  'effective_end': datetime.date(2016, 8, 1),
                 },
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 6, 11),
                  'effective_end': None, 
                 },
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 7, 12),
                  'effective_end': datetime.date(2016, 7, 1),
                 },
             ]
                ,
             [
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': None,
                  'commission_date': datetime.date(2016, 1, 1),
                  'decommission_date': None,
                 },
             ]
            ),
        ]
        for data, rti_new_rows in tests:
            self.assertEqual(
                Consolidation.consolidate_rti_data(
                    TestConsolidation.cur_local, data),
                rti_new_rows)

    def test_bad_overlapping(self):
        tests = [
            [
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2017, 1, 1),
                 },   
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 6, 1),
                  'effective_end': datetime.date(2016, 7, 1),
                 },   
                 {'tower_lat': -41,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 1, 1),  ## starting before range for other location is cleanly finished
                  'effective_end': datetime.date(2016, 7, 1),
                 },   
            ],
            [
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': None, ## will cause an overlap with the range for -41, 174
                 },   
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 6, 1),
                  'effective_end': datetime.date(2016, 7, 1),
                 },   
                 {'tower_lat': -41,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 2, 1),
                  'effective_end': datetime.date(2017, 6, 1),
                 },   
            ],
            [
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2017, 1, 1),
                 },   
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 1, 1),
                  'effective_end': None, ## can't be open-ended because the same rti has another range at another location starting afterwards
                 },   
                 {'tower_lat': -41,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 4, 1),
                  'effective_end': datetime.date(2017, 6, 1),
                 },   
             ],
        ]
        for data in tests:
            with self.assertRaises(Exception):
                Consolidation.consolidate_rti_data(TestConsolidation.cur_local,
                    data)

    def test_adjoining(self):
        tests = [
            ([
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2017, 1, 1),
                 },   
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 1, 1),
                  'effective_end': datetime.date(2017, 4, 1),
                 },   
                 {'tower_lat': -41,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 4, 2),
                  'effective_end': datetime.date(2017, 6, 1),
                 },   
             ]
                ,
             [
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2017, 4, 1),
                  'commission_date': datetime.date(2016, 1, 1),
                  'decommission_date': datetime.date(2017, 4, 1),
                 },
                 {'tower_lat': -41,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 4, 2),
                  'effective_end': datetime.date(2017, 6, 1),
                  'commission_date': datetime.date(2017, 4, 2),
                  'decommission_date': datetime.date(2017, 6, 1),
                 },
             ]
            ),
        ]
        for data, rti_new_rows in tests:
            self.assertEqual(Consolidation.consolidate_rti_data(
                TestConsolidation.cur_local, data), rti_new_rows)

    def test_bad_adjoining(self):
        tests = [
            [
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2017, 1, 1),
                 },   
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 1, 1),
                  'effective_end': datetime.date(2017, 4, 1),
                 },   
                 {'tower_lat': -41,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 4, 1),  ## needs to be after prev range - we want to remove overlaps in fixed versions 
                  'effective_end': datetime.date(2017, 6, 1),
                 },   
             ],
        ]
        for data in tests:
            with self.assertRaises(Exception):
                Consolidation.consolidate_rti_data(TestConsolidation.cur_local,
                    data)

    def test_gaps(self):
        tests = [
            ([
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2016, 5, 1),
                 },   
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 1, 1),  ## big enough gap to keep split
                  'effective_end': datetime.date(2017, 4, 1),
                 },   
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 4, 2),  ## small enough gap to combine
                  'effective_end': datetime.date(2017, 6, 1),
                 }, 
             ]
             ,
             [
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2016, 5, 1),
                  'commission_date': datetime.date(2016, 1, 1),
                  'decommission_date': datetime.date(2016, 5, 1),
                 },
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 1, 1),
                  'effective_end': datetime.date(2017, 6, 1),
                  'commission_date': datetime.date(2017, 1, 1),
                  'decommission_date': datetime.date(2017, 6, 1),
                 },
             ]),
            ([
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2016, 5, 1),
                 },   
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 7, 1),  ## small enough gap to combine
                  'effective_end': datetime.date(2017, 4, 1),
                 },   
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 4, 2),  ## small enough gap to combine
                  'effective_end': datetime.date(2017, 6, 1),
                 }, 
             ]
             ,
             [
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2017, 6, 1),
                  'commission_date': datetime.date(2016, 1, 1),
                  'decommission_date': datetime.date(2017, 6, 1),
                 },
             ]),
            ([
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2016, 5, 1),
                 },   
                 {'tower_lat': -45,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 7, 1),   ## small enough gap to combine but new location
                  'effective_end': datetime.date(2017, 4, 1),
                 },   
                 {'tower_lat': -45,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2017, 4, 2),  ## small enough gap to combine
                  'effective_end': datetime.date(2017, 6, 1),
                 }, 
             ]
             ,
             [
                 {'tower_lat': -40,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 1, 1),
                  'effective_end': datetime.date(2016, 5, 1),
                  'commission_date': datetime.date(2016, 1, 1),
                  'decommission_date': datetime.date(2016, 5, 1),
                 },
                 {'tower_lat': -45,
                  'tower_lon': 174,
                  'effective_start': datetime.date(2016, 7, 1),
                  'effective_end': datetime.date(2017, 6, 1),
                  'commission_date': datetime.date(2016, 7, 1),
                  'decommission_date': datetime.date(2017, 6, 1),
                 },
             ]),
        ]
        for data, rti_new_rows in tests:
            self.assertEqual(Consolidation.consolidate_rti_data(
                TestConsolidation.cur_local, data), rti_new_rows)


class Locator():
    """
    Based heavily on Daniel's code.
    """
    api_key = "AIzaSyBnLnNEuxl5vihsznzOR3s1kiTbmuTyzSg"
    url = "https://www.googleapis.com/geolocation/v1/geolocate?key=" + api_key
    carrier_name = "Spark NZ"
    mcc = 530
    mnc = 5

    @staticmethod
    def _handle_resp(obj, show_map=True):
        debug = False
        response = requests.post(Locator.url, json=obj)
        data = response.json()
        if "error" not in data:
            lat = data["location"]["lat"]
            lon = data["location"]["lng"]
            cell_id = obj["cellTowers"][0]["cellId"]
            if debug: print("{}, {}, {}".format(cell_id, lat, lon))
            if show_map:
                url = "https://www.google.co.nz/maps/place/{},{}".format(lat, lon)
                print(url)
                open_new_tab(url)
        else:
            if debug:
                print("Error: {}".format(
                    response.json()["error"]["errors"][0]["reason"]))
            lat, lon = None, None
        return lat, lon

    @staticmethod
    def get_location(rti, lac_code, show_map=True):
        tower_data = [
            {
                "cellId": rti,
                "locationAreaCode": lac_code,
                "mobileCountryCode": Locator.mcc,
                "mobileNetworkCode": Locator.mnc,
                "age": 0,
            }]
        post_data = {
            "homeMobileCountryCode": Locator.mcc,
            "homeMobileNetworkCode": Locator.mnc,
            "radioType": "lte",  ## should only use LTE because we won't find a match on 3G 2**16xRNC + cell_id
            "carrier": Locator.carrier_name,
            "cellTowers": tower_data,
            "considerIp": 'false',
        }
        lat, lon = Locator._handle_resp(post_data, show_map)
        return lat, lon


class Connections():

    EASY_CONFIG_TXT = 'easy_config.txt'

    @staticmethod
    def _get_connection_only_rti_dets(cur_local,
            use_stale_connection_dets=False, use_stale_csm2=False):
        """
        For each cell_id_rti look at n distinct imsis per day that are over 100
        (i.e. ignore the dribblers) and get the average of those days. Filter
        down to those with an average of more than some number e.g. 500. Then
        see if matching location data in CSM 2.0.
        """
        debug = False
        if use_stale_connection_dets:
            connection_only_rti_dets = stale.Stale.connection_only_rti_dets
        else:
            unused, cur_gp = db.Pg.gp()
            if not use_stale_csm2:
                print("Making csm table in Greenplum so it can be connected to "
                    "other Greenplum tables in next query step")
                SeedSource.overwrite_test_csm(cur_local)
            sql = """\
            SELECT *
            FROM (
              SELECT
                sig_rtis.cell_id_rti AS
              rti,
              avg_imsis,
              min_sig_date,
              max_sig_date,
                DATE_PART('day', max_sig_date::timestamp - min_sig_date::timestamp)::int AS
              sig_range
              FROM (
                SELECT
                cell_id_rti,
                  ROUND(AVG(n_imsis)::numeric)::int AS
                avg_imsis,
                  MIN(date) AS
                min_sig_date,
                  MAX(date) AS
                max_sig_date
                FROM (
                  SELECT cell_id_rti, date, SUM(distinct_imsi) AS n_imsis
                  FROM {daily_connections}
                  GROUP BY cell_id_rti, date
                  HAVING SUM(distinct_imsi) > 100
                ) AS rti_imsi_non_dribbing_freqs
                GROUP BY cell_id_rti
                HAVING AVG(n_imsis) > 500
              ) AS sig_rtis
              LEFT JOIN
              (
                SELECT DISTINCT cell_id_rti  -- no point fussing about whether we have dates matching at this point - how many have nothing there ever?
                FROM {csm2}
              ) AS csm2_rtis
              USING(cell_id_rti)
              WHERE csm2_rtis.cell_id_rti IS NULL
            ) AS key_data
            WHERE sig_range > 14
            ORDER BY rti -- avg_imsis DESC
            """.format(csm2=conf.CSM2_GREENPLUM,
                daily_connections=conf.DAILY_CONNECTION_DATA)
            cur_gp.execute(sql)
            if debug: print(str(cur_gp.query, encoding='utf-8'))
            connection_only_rti_dets = [dict(row) for row in cur_gp.fetchall()]
            print("[")
            for row in connection_only_rti_dets:
                print("    {},".format(row))
            print("]")
            input("Set mod_mod_stale.Stale.connection_only_rti_dets to the "
                "above and set use_stale_connection_dets to True (no impact "
                "unless use_stale_connections_only is False which it probably "
                "isn't ;-)")
        return connection_only_rti_dets

    @staticmethod
    def rti_locations_from_network_tbls(rti):
        db = '/home/gps/Documents/tourism/csm/storage/locations.db'
        unused, cur = Sqlite.get_con_cur(db)
        ## Dates are dd/mm/yy (blame Daniel ;-))
        ## see https://stackoverflow.com/questions/4428795/sqlite-convert-string-to-date
        sql = """\
        SELECT
          LATITUDE AS
        lat,
          LONGITUDE AS
        lon,
          '20'||substr(VERSION_DATE,7)||'-'||substr(VERSION_DATE,4,2)||'-'||substr(VERSION_DATE,1,2) AS
        date
        FROM loc_hist
        WHERE CELL_ID_RTI = ?
        ORDER BY substr(VERSION_DATE,7)||substr(VERSION_DATE,4,2)||substr(VERSION_DATE,1,2)
        """
        cur.execute(sql, (str(rti), ))
        data = cur.fetchall()
        main_data = [(float(row['lat']), float(row['lon']), row['date'])
            for row in data]
        return data, main_data

    @staticmethod
    def _extract_cell_id_node_ids(rti):
        candidate_node_id = int(rti/256)
        candidate_cell_id = rti - (256*candidate_node_id)
        match = isinstance(candidate_cell_id, int)
        if match:
            dets4G = {
                'rti': rti,
                'node_id': candidate_node_id,
                'cell_id': candidate_cell_id,
            }
        else:
            dets4G = None
        return dets4G

    @staticmethod
    def _get_missing_easy_additions(cur_local, skip_after_rti=None,
            use_stale_connection_dets=False, use_stale_csm2=False):
        """
        We know these ones matter but we need to figure out where they are using
        other data from mod_p data warehouse and, as a backup, the Google API.
        """
        debug = False
        easy_rtis_dets = Connections._get_connection_only_rti_dets(
            cur_local, use_stale_connection_dets, use_stale_csm2)
        ## only do those not already sorted out earlier
        handled_rtis = [t[0] for t in conconf.Config.connection_only_additions]
        easy_rtis = [d['rti'] for d in easy_rtis_dets]
        ## don't want to have anything in easy_rtis that shouldn't be in connections-only data (because it is in mod_p now, perhaps just added)
        rtis_not_connections_only_after_all = set(handled_rtis) - set(easy_rtis)
        if rtis_not_connections_only_after_all:
            print("Remove {} from mod_mod_connections_conf"
                ".Config.connection_only_additions"
                .format(sorted(list(rtis_not_connections_only_after_all))))
            raise Exception(FRESH_RUN_MSG)
        if debug: print(easy_rtis_dets)
        more_tricky = []
        skip = bool(skip_after_rti)
        input("Pause to allow you to make backup copy of 'easy_config.txt' "
            "before it is overwritten (if desired) ...")
        with open(Connections.EASY_CONFIG_TXT, 'w') as f:
            for n, rti_dets in enumerate(easy_rtis_dets, 1):
                rti = rti_dets['rti']
                if rti in handled_rtis:
                    continue
                if skip:
                    if rti == skip_after_rti:
                        skip = False
                    continue
                min_sig_date_str = rti_dets['min_sig_date']
                max_sig_date_str = rti_dets['max_sig_date']
                print("------------\n{} ({:,} of {:,}) for {} to {}".format(rti,
                    n, len(easy_rtis_dets),
                    min_sig_date_str, max_sig_date_str))
                google_lat, google_lon = Locator.get_location(rti, lac_code=1,
                    show_map=False)
                if google_lat and google_lon:
                    print("Google lat, lon:\n({}, {})".format(google_lat,
                        google_lon))
                data, main_data = Connections.rti_locations_from_network_tbls(
                    rti)
                if data:
                    print("Daniel's lat, lons (choose according to date)")
                if len(set([(lat, lon) for lat, lon, unused in main_data])) > 1:
                    print(main_data)
                else:
                    if main_data:
                        print(main_data[0][:2])
                resp = input("Enter lat, lon to use: ")
                if not resp:
                    more_tricky.append(rti)
                    continue
                elif resp == 'g':
                    lat, lon = google_lat, google_lon
                elif resp == 'd':
                    row2use = data[-1]
                    lat, lon = float(row2use['lat']), float(row2use['lon'])
                else:
                    raw_lat, raw_lon = resp.replace(' ', '').split(',')
                    lat, lon = float(raw_lat), float(raw_lon)
                dets4G = Connections._extract_cell_id_node_ids(rti)
                if dets4G:
                    cell_id = dets4G['cell_id']
                    node_id = dets4G['node_id']
                else:
                    cell_id = rti
                    node_id = None
                start_date = datetime.datetime.strptime(
                    min_sig_date_str, '%Y-%m-%d').date()
                if max_sig_date_str == '2017-06-01':
                    end_date = None
                else:
                    end_date = datetime.datetime.strptime(
                        max_sig_date_str, '%Y-%m-%d').date()
                addition_tup = (
                    rti, cell_id, node_id, start_date, end_date, lat, lon,
                    None, None, None, None, None,
                    start_date, end_date,
                    None, None, None, None, None, None)
                print("    {},".format(addition_tup))
                f.write("\n{},".format(addition_tup))
            print('more tricky:', more_tricky)

    @staticmethod
    def check_or_help_add_main_connection_additions(cur_local,
            use_stale_connections_only, use_stale_connection_dets,
            use_stale_csm2):
        if use_stale_connections_only:
            connections = stale.Stale.connection_only_additions
        else:
            Connections._get_missing_easy_additions(cur_local,
                skip_after_rti=None,
                use_stale_connection_dets=use_stale_connection_dets,
                use_stale_csm2=use_stale_csm2)
            print("Set mod_mod_stale.Stale.connection_only_additions to:")
            for row in conconf.Config.connection_only_additions:
                print("{},".format(row))
            print("Plus anything in LoQal/{}".format(
                Connections.EASY_CONFIG_TXT))
            print("Then set use_stale_connections_only to True")
            raise Exception(FRESH_RUN_MSG)
        ## a) check consistency with additions already configured
        data_check_only_rtis = [dets[0] for dets in connections]
        conf_adding_rtis = [addition_conf[0]
            for addition_conf in conconf.Config.connection_only_additions]
        ## get discrepancies
        conf_only_rtis = sorted(list(
            set(conf_adding_rtis) - set(data_check_only_rtis)))
        data_check_only_rtis = sorted(list(
            set(data_check_only_rtis) - set(conf_adding_rtis)))
        if conf_only_rtis:
            print("conf_only_rtis", conf_only_rtis)
            print("Wipe mod_mod_connections_conf.Config"
                ".connection_only_additions and start again")
            raise Exception(FRESH_RUN_MSG)
        if data_check_only_rtis:
            print("data_check_only_rtis", data_check_only_rtis)
            print("Add to mod_mod_connections_conf.Config"
                ".connection_only_additions from {}"
                .format(Connections.EASY_CONFIG_TXT))
            raise Exception(FRESH_RUN_MSG)
        

class Consolidation():

    BIG_GAP = 90

    @staticmethod
    def max_inc_null(vals):
        if None in vals:
            max_val = None
        else:
            max_val = max(vals)
        return max_val
        
    @staticmethod
    def consolidate_rti_data(cur_local, rti_data):
        """
        We need to cope with:
        * rti's where there is an overlap, or the effective start-end ranges
        adjoin e.g. one range ends on one day and the next range starts on the
        next day.
        * ranges where there are gaps between one range ending and another
        starting.

        In Waiuku to Pukekohe some cell_id_rti's vanished for about a month and
        reappeared at the same location. This will be a data artefact rather
        than a real phenomenon so we should remove the gap when no location
        change.

        e.g. rti, start,  end, lat, lon
               1,   Jan,  Mar,   3,   3
               1,   Jun, null,   3,   3
        -->
               1,   Jan,  null,   3,   3

        Note - raise Exception if overlaps occurs at same time as a relocation.

        We open a range with the earliest start and loop through the rows
        deciding whether to merge with the earlier one or close it off and start
        a new range. We close off whenever there is a change of location or the
        gap between ranges is over a certain threshold. We close off with the
        maximum previous end date.

        If not merging, the previous range must be closed off. But what date to
        use? The latest one possible (with None being the latest).

        Note - not starting from an aggregate query using min and max per
        location. I end up needing more sophistication eventually anyway and
        here I can get it (and test it). Sometimes I've looked at gaps etc and
        that makes for complex SQL but easy Python.
        """
        debug = False
        rti_new_rows = []
        range_row = None
        max_prev_end = ''  ## None is reserved for open dates (null)
        prev_end = ''
        prev_lat = None
        prev_lon = None
        for i, row in enumerate(rti_data):
            if not range_row:
                range_row = row.copy()
            lat = row['tower_lat']
            lon = row['tower_lon']
            start = row['effective_start']
            end = row['effective_end']
            if i > 0:
                gap_km = spatial.Pg.gap_km(cur_local, (lat, lon),
                    (prev_lat, prev_lon))
                new_location = (gap_km > 0.5)
                if start and prev_end:
                    gap = (start - prev_end).days
                    if debug: print(prev_end, start, gap)
                    big_gap = gap > Consolidation.BIG_GAP
                else:
                    big_gap = False
            else:
                new_location = False
                big_gap = False
            close_off = new_location or (not new_location and big_gap)
            if close_off:  ## close off old range_row and start new one 
                range_row['effective_end'] = max_prev_end
                range_row['commission_date'] = range_row['effective_start']
                range_row['decommission_date'] = range_row['effective_end']
                rti_new_rows.append(range_row)
                range_row = row.copy()
                if (max_prev_end is None
                        or range_row['effective_start'] <= max_prev_end):
                    data_str = "\n".join(["{} {} {} {} {}".format(
                        row['cell_id_rti'],
                        row['effective_start'], row['effective_end'],
                        row['tower_lat'], row['tower_lon'])
                        for row in rti_data])
                    raise Exception("Can't open a new range which overlaps an "
                        "old range. Rows:\n{}".format(data_str))
                max_prev_end = end
            else:
                if max_prev_end != '':
                    max_prev_end = Consolidation.max_inc_null(
                        [end, max_prev_end])
                else:
                    max_prev_end = end
            ## prepare for next loop
            prev_end = end
            prev_lat = lat
            prev_lon = lon
        ## close off last row
        if range_row:
            range_row['effective_end'] = max_prev_end
            range_row['commission_date'] = range_row['effective_start']
            range_row['decommission_date'] = range_row['effective_end']
            rti_new_rows.append(range_row)
        return rti_new_rows

    @staticmethod
    def get_overlapping_rtis(cur_local):
        debug = False
        sql_overlaps = """\
        SELECT
        id,
        cell_id_rti,
        effective_start,
        effective_end,
        tower_lat,
        tower_lon,
        physical_address
        FROM {mod_p_locations}  -- building on existing changes
        WHERE cell_id_rti IN (
            SELECT
            mod1.cell_id_rti
            FROM (
              SELECT
                id,
                cell_id_rti,
                effective_start,
                CASE WHEN effective_end IS NULL THEN '9999-01-01' ELSE effective_end END AS
              effective_end  -- Need a date so we can find overlaps between two rows where effective_end is null
              FROM {mod_p_locations}
            ) AS mod1
            INNER JOIN
            (
              SELECT
                id,
                cell_id_rti,
                effective_start,
                CASE WHEN effective_end IS NULL THEN '9999-01-01'
                  ELSE effective_end
                  END AS
              effective_end
              FROM {mod_p_locations}
            ) AS mod2
            ON (mod1.cell_id_rti = mod2.cell_id_rti AND mod1.id != mod2.id -- so not finding overlaps with own row
            AND (
                (mod1.effective_start, mod1.effective_end)
                OVERLAPS
                (mod2.effective_start - interval '1 second', mod2.effective_end) -- less a second so same date is considered an overlap even though inclusive starts and exclusive ends to ranges
              )
            )
        )
        ORDER BY cell_id_rti, effective_start, effective_end
        """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
        cur_local.execute(sql_overlaps)
        overlaps_data = cur_local.fetchall()
        if debug:
            print(overlaps_data)
        overlapping_rtis = {row['cell_id_rti'] for row in overlaps_data}
        return overlapping_rtis

    @staticmethod
    def get_consolidation_candidate_rtis(cur_local):
        """
        If any overlap, or of effective start-end ranges adjoin e.g. one range
        ends on one day and the next range starts on the next day, then we
        potentially need to consolidate. If overlaps and a relocation we are
        dealing with faulty data and will need to configure a custom solution
        depending on results of investigations of connection date etc).

        Actual consolidation code is reasonably sophisticated and runs
        line-by-line. Does what you would do by hand.
        """
        debug = False
        overlapping_rtis = Consolidation.get_overlapping_rtis(cur_local)
        sql_adjoining = """\
        SELECT extended_ends.cell_id_rti
        FROM
        (
          SELECT
          cell_id_rti,
            CASE WHEN effective_end IS NULL THEN null
            ELSE effective_end + interval '1 day'
            END AS
          extended_end
          FROM {mod_p_locations}
        ) AS extended_ends
        INNER JOIN
        (
          SELECT
          cell_id_rti,
          effective_start
          FROM {mod_p_locations}
        ) AS starts
        ON extended_ends.cell_id_rti = starts.cell_id_rti AND extended_end = effective_start
        ORDER BY cell_id_rti
        """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
        cur_local.execute(sql_adjoining)
        adjoining_data = cur_local.fetchall()
        if debug:
            print(adjoining_data)
        adjoining_rtis = {row['cell_id_rti'] for row in adjoining_data}
        candidate_rtis = sorted(list(overlapping_rtis.union(adjoining_rtis)))
        return candidate_rtis

    @staticmethod
    def _get_multi_open_rtis(cur_local):
        sql = """\
        SELECT DISTINCT cell_id_rti
        FROM {mod_p_locations_orig}
        INNER JOIN
        (
            SELECT cell_id_rti
            FROM {mod_p_locations_orig}
            GROUP BY cell_id_rti
            HAVING COUNT(
              CASE WHEN effective_end IS NULL THEN 1
              ELSE null
              END) > 1
        ) AS has_multiple_null_ends
        USING(cell_id_rti)
        ORDER BY cell_id_rti
        """.format(mod_p_locations_orig=conf.MOD_P_LOCATIONS_ORIG)
        cur_local.execute(sql)
        multi_open_cell_id_rtis = [row['cell_id_rti']
            for row in cur_local.fetchall()]
        return multi_open_cell_id_rtis

    @staticmethod
    def _get_start_after_open_rtis(cur_local):
        """
        cell_id_rti's and effective_starts for open rows
        joined to
        all_effective_starts
        where same cell_id_rti
        but not same id
        and effective start on or after open_effective_start
        """
        sql = """\
        SELECT *
        FROM {mod_p_locations_orig}
        WHERE cell_id_rti IN (
        SELECT DISTINCT open_rows.cell_id_rti FROM (
          SELECT
          id,
          cell_id_rti,
          effective_start
          FROM {mod_p_locations_orig}
          WHERE effective_end IS NULL
        ) AS open_rows
        INNER JOIN
        (
          SELECT
          id,
          cell_id_rti,
          effective_start
          FROM {mod_p_locations_orig}
        ) AS all_starts
        ON open_rows.cell_id_rti = all_starts.cell_id_rti
        AND open_rows.id != all_starts.id
        AND all_starts.effective_start >= open_rows.effective_start)
        ORDER BY cell_id_rti, effective_start
        """.format(mod_p_locations_orig=conf.MOD_P_LOCATIONS_ORIG)
        cur_local.execute(sql)
        start_after_open_cell_id_rtis = [row['cell_id_rti']
            for row in cur_local.fetchall()]
        return start_after_open_cell_id_rtis

    @staticmethod
    def check_or_help_add_overlap_consolidation_replacements(cur_local,
            rtis2consolidate):
        """
        The first consolidation step - turning redundant start-ends in the Mod_P
        data etc into streamlined start-ends.

        Identifies all cell_id_rti's where there is a start/end overlap issue
        (including coping with nulls start/ends).

        Separates out cases requiring custom config because of complexity and
        checks them against existing replacements config.

        Then identifies all remaining cell_id_rti's and checks them against the
        standard replacement config. If not there, generates new replacement
        config to consolidate. Consolidation must take into account location
        changes.
        """
        debug = False
        ## 1) "Open" overlaps - null dates are involved in overlap *************
        ## Custom fixes
        multi_open_rtis = Consolidation._get_multi_open_rtis(cur_local)
        start_after_open_rtis = Consolidation._get_start_after_open_rtis(
            cur_local)
        rtis_requiring_fix = sorted(list(
            set(multi_open_rtis
            + start_after_open_rtis)
        ))
        ## a) check consistency with replacements already configured
        conf_replacing_rtis = [replacement_conf['cell_id_rti']
            for replacement_conf
            in consol.Config.open_consolidation_replacements]
        ## get discrepancies
        conf_only_rtis = sorted(list(  ## conf = existing conf in extras.Config
            set(conf_replacing_rtis) - set(rtis_requiring_fix)))
        data_check_only_rtis = sorted(list(  ## fresh = in Mod_P but not already in config
            set(rtis_requiring_fix) - set(conf_replacing_rtis)))
        if conf_only_rtis:
            if debug: print('conf_only_rtis',  conf_only_rtis)
            print("Remove the following from mod_mod_consolidation_conf.Config."
                "open_consolidation_replacements: {}".format(conf_only_rtis))
            raise Exception(FRESH_RUN_MSG)
        if data_check_only_rtis:
            if debug: print("conf_only_rtis", conf_only_rtis)
            print("Copy the results of:\n"
                "SELECT DISTINCT ON (cell_id_rti) *\n"
                "FROM {mod_p_locations}\nWHERE cell_id_rti IN {rtis}\n"
                "ORDER BY cell_id_rti, effective_start;\n\n"
                "to mod_mod_consolidation_conf.Config."
                "open_consolidation_replacements".format(
                mod_p_locations=conf.MOD_P_LOCATIONS,
                rtis=db.Pg.nums2clause(data_check_only_rtis)))
            raise Exception(FRESH_RUN_MSG)
        ## 2) Standard overlaps ************************************************
        """
        Automatic fixing
        Types of "standard" overlap:
        - 1 ends in Feb but 2 starts in Jan and is unended --> 1 becomes unended
        """
        rtis_custom_consolidated = [row['cell_id_rti']
            for row in consol.Config.custom_consolidation_replacements]
        remaining_rtis = sorted(list(
            set(rtis2consolidate)
            - set(rtis_requiring_fix)
            - set(rtis_custom_consolidated)
        ))
        sql_rti_data_tpl = """\
        SELECT *
        FROM {mod_p_locations}
        WHERE cell_id_rti = %s
        ORDER BY effective_start, effective_end
        """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
        new_rows = []
        for rti in remaining_rtis: 
            cur_local.execute(sql_rti_data_tpl, (rti, ))
            data = cur_local.fetchall()
            try:
                rti_new_rows = Consolidation.consolidate_rti_data(cur_local,
                    data)
            except Exception as e:
                raise Exception("Add replacement conf for {} to "
                    "mod_mod_consolidation_conf"
                    ".Config.custom_consolidation_replacements. {}"
                    .format(rti, e))
            new_rows.extend(rti_new_rows)
        oth_overlapping_replacement_confs = []
        prev_cell_id_rti = None
        replacement_conf = None
        for i, row in enumerate(new_rows):
            cell_id_rti = row['cell_id_rti']
            new_cell_id_rti = (cell_id_rti != prev_cell_id_rti)
            replacement_row = (cell_id_rti, row['cell_id'], row['node_id'],
                row['effective_start'], row['effective_end'], row['tower_lat'],
                row['tower_lon'], row['physical_address'], row['description'],
                row['sector'], row['technology_class'], row['sectortech_alpha'],
                row['commission_date'], row['decommission_date'],
                row['location'], row['sector_status'], row['location_status'],
                row['location_alpha'], row['easting'], row['northing'])
            if new_cell_id_rti:
                if i != 0:
                    oth_overlapping_replacement_confs.append(replacement_conf)  ## add old one before starting new one
                    if debug: print(replacement_conf)
                replacement_conf = {
                    'cell_id_rti': cell_id_rti,
                    'replacement_rows': [
                        replacement_row,
                    ]
                }
            else:
                replacement_conf['replacement_rows'].append(replacement_row)
            prev_cell_id_rti = cell_id_rti
        oth_overlapping_replacement_confs.append(replacement_conf)
        ## a) check consistency with replacements already configured
        conf_replacing_rtis = [replacement_conf['cell_id_rti']
            for replacement_conf
            in consol_extra.Config.standard_consolidation_replacements]
        ## get discrepancies
        rtis_requiring_fix = remaining_rtis
        conf_only_rtis = sorted(list(
            set(conf_replacing_rtis) - set(rtis_requiring_fix)
        ))
        data_check_only_rtis = sorted(list(
            set(rtis_requiring_fix) - set(conf_replacing_rtis)
        ))
        if conf_only_rtis:
            if debug: print(conf_only_rtis)
            raise Exception("Remove the following from "
                "mod_mod_consolidation_extra."
                "Config.standard_consolidation_replacements: {}".format(
                conf_only_rtis))
        if data_check_only_rtis:
            if debug: print(data_check_only_rtis)
            fresh_confs = [
                conf for conf in oth_overlapping_replacement_confs
                if conf['cell_id_rti'] in data_check_only_rtis]
            fpath = os.path.join(conf.CSM_ROOT, 'storage',
                'oth_overlapping_replacement_confs.txt')
            with open(fpath, 'w') as f:
                f.write(format_replacement_confs(fresh_confs))
            raise Exception("Open '{}' and add replacement conf to "
                "mod_mod_consolidation_extra."
                "Config.standard_consolidation_replacements".format(fpath))
        # print("Potentially check to see if any more fake_gap_rtis in this group of large_gap_rtis: {}".format(fake_gap_rtis))

    @staticmethod
    def check_or_help_add_gap_consolidation_replacements(cur_local):
        """
        Only add to replacements if a change from existing.
        """
        debug = False
        sql_all = """\
        SELECT DISTINCT cell_id_rti
        FROM {mod_p_locations}
        ORDER BY cell_id_rti
        """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
        cur_local.execute(sql_all)
        all_rtis = [x['cell_id_rti'] for x in cur_local.fetchall()]
        new_rows = []
        sql_rti_data_tpl = """\
        SELECT *
        FROM {mod_p_locations}
        WHERE cell_id_rti = %s
        ORDER BY effective_start, effective_end
        """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
        for rti in all_rtis:
            cur_local.execute(sql_rti_data_tpl, (rti, ))
            data = cur_local.fetchall()
            rti_new_rows = Consolidation.consolidate_rti_data(cur_local, data)
            if len(data) == len(rti_new_rows):  ## don't look at specific changes because we often clean up decommission dates and that counts as a difference even though we don't care
                continue
            else:
                new_rows.extend(rti_new_rows)
        gap_closing_replacement_confs = []
        prev_cell_id_rti = None
        replacement_conf = None
        for i, row in enumerate(new_rows):
            cell_id_rti = row['cell_id_rti']
            new_cell_id_rti = (cell_id_rti != prev_cell_id_rti)
            replacement_row = (cell_id_rti, row['cell_id'], row['node_id'],
                row['effective_start'], row['effective_end'], row['tower_lat'],
                row['tower_lon'], row['physical_address'], row['description'],
                row['sector'], row['technology_class'], row['sectortech_alpha'],
                row['commission_date'], row['decommission_date'],
                row['location'], row['sector_status'], row['location_status'],
                row['location_alpha'], row['easting'], row['northing'])
            if new_cell_id_rti:
                if i != 0:
                    gap_closing_replacement_confs.append(replacement_conf)  ## add old replacement_conf before starting new one
                    if debug: print(replacement_conf)
                replacement_conf = {
                    'cell_id_rti': cell_id_rti,
                    'replacement_rows': [
                        replacement_row,
                    ]
                }
            else:
                replacement_conf['replacement_rows'].append(replacement_row)
            prev_cell_id_rti = cell_id_rti
        gap_closing_replacement_confs.append(replacement_conf)
        ## a) check consistency with replacements already configured
        conf_replacing_rtis = [replacement_conf['cell_id_rti']
            for replacement_conf in consol.Config.gap_closing_replacements]
        ## get discrepancies
        rtis_requiring_fix = [conf['cell_id_rti']
            for conf in gap_closing_replacement_confs]
        conf_only_rtis = sorted(list(
            set(conf_replacing_rtis) - set(rtis_requiring_fix)))
        data_check_only_rtis = sorted(list(
            set(rtis_requiring_fix) - set(conf_replacing_rtis)
        ))
        if conf_only_rtis:
            if debug: print(conf_only_rtis)
            print("Remove the following from mod_mod_consolidation_conf.Config."
                "gap_closing_replacements: {}".format(conf_only_rtis))
            raise Exception(FRESH_RUN_MSG)
        if data_check_only_rtis:
            if debug: print(data_check_only_rtis)
            fresh_confs = [
                conf for conf in gap_closing_replacement_confs
                if conf['cell_id_rti'] in data_check_only_rtis]
            fpath = os.path.join(conf.CSM_ROOT, 'storage',
                'gap_closing_replacement_confs.txt')
            with open(fpath, 'w') as f:
                f.write(format_replacement_confs(fresh_confs))
            print("Open '{}' and add replacement conf to "
                "mod_mod_consolidation_conf.Config"
                ".gap_closing_replacements".format(fpath))
            raise Exception(FRESH_RUN_MSG)

def make_local_csm_tbl(con_local, cur_local):
    """
    Goal - have configuration for all changes to transform mod_p_locations into
    mod_p_locations_orig. Think dev-ops for data cleaning ;-). Configuration
    consists of replacements, in which all data for a cell_id_rti is wiped and
    fresh records are added (some removing existing records, some modifying
    existing records, and others being completely new); and additions, which are
    completely new records to be appended. An addition might look like, for
    example:

    (38871, None, None, '2014-12-18', '2015-03-29', -38.0107, 175.325,
     'Walton Street | Te Awamutu', 'Walton Street | Te Awamutu', 'MTAMU1_1',
     'U', None, '2014-12-18', '2015-03-29', None, None, None, None, None, None),

    where the fields are:

    cell_id_rti,
    cell_id,
    node_id,
    effective_start,
    effective_end,
    tower_lat,
    tower_lon,
    physical_address,
    description,
    sector,
    technology_class,
    sectortech_alpha,
    commission_date,
    decommission_date,
    location,
    sector_status,
    location_status,
    location_alpha,
    easting,
    northing

    The config is hard-wired because some of it could not be automated. It was
    necessary to conduct some form of investigation and record the changes to be
    made as replacement config. So it is important to check that new CSM 1.0 and
    mod_p data doesn't change/increase what we need to hardwire. We do this by
    running the steps we can automate and ensuring no new ids are required to be
    added or modified (replacements).

    We need to hardwire replacements and additions for each step which affects
    additions and replacements.

    Happens in steps:

    A) Make source data (csm1 and new mod_p data)
    
    B) Custom corrections e.g. faulty locations in the water or far off-shore

    C) Handling rti's that are in CSM 1 but not Mod P
      i) 4G alternatives (via
         conf4G.Config.alt_4G_additions,
         conf4G.Config.alt_4G_replacements,
         conf4G.Config.custom_alt_4G_additions, and
         conf4G.Config.custom_alt_4G_replacements
         )
     ii) dribblers - simply ignored. Not added to Mod_P. Note - dribblers
         already in Mod P are not removed.
    iii) CSM1-derived - additions via
         extra.Config.csm_derived_additions
     iv) [audit all results]
      v) Apply all changes

    D) Consolidation where there is any overlap, or of effective start-end
        ranges adjoin e.g. one range ends on one day and the next range starts
        on the next day
      i) Consolidating the data as it is after transformation via
         consol.Config.custom_consolidation_replacements,
         consol.Config.open_consolidation_replacements, and
         consol_extra.Config.standard_consolidation_replacements
     ii) Apply all changes
    Note - the assumption is that consolidation additions/replacements won't
    touch the same cell_id_rti's as in additions/replacements from previous
    steps as the corrections won't be messed up enough to require consolidation
    ;-)

    E) Consolidating smaller gaps. Same as above about no repeat additions/
    consolidations.

    F) Handling rti's that are in the connection data (in a significant way) but
       not in (revised) Mod P
      i) Adding connection-only data where rti's are completely missing via
         conconf.Config.connection_only_additions
     ii) Add connection-only data where cell_id_rti's *are* present but
         significant date ranges are missing. Do this from
         conconf.Config.connection_only_additions_backfill. These gaps were
         identified in a semi-manual process and may conflict with other
         corrections. So need to ensure no overlap with anything else already
         added.
    iii) Apply all changes

    For each step (apart from F ii) because not worth overhead of updating) we
    initially run analyses of the latest data and see what configuration we
    need. This is compared to the configuration we have actually created. To
    speed things up, we can immediately store some of the live analyses under
    stale so we are caching that part. The goal of each step is to update all
    the related configuration. Once everything aligns we are able to move on to
    laster steps.

    After each step we actually apply the changes so further modifications are
    based on the existing corrections. This makes perfect sense. If an earlier
    correction step changes some dates we should be doing subsequent date-
    related checks on the changed data not the redundant original.

    Note -- the CSM 2 is uploaded to GreenPlum without the connections-only data
    (so we can join it to the connections data within GreenPlum), we then add
    the connections data to local copy, and upload finalised CSM 2.

    To speed up the process of developing the code for cleaning and extending
    the data (via config) the concept of stale data was used. Stale data is the
    data as it would have been received by the function calls which provide the
    source from which config is created. It is not the config itself.

    So when an updated version of the CSM 2 seed needs to be generated, it is
    important to update not only the configuration we use to generated the
    CSM 2, but also the stale data. And we must update all the different parts
    of the stale data so it is internally consistent otherwise the audit numbers
    will not necessarily align.
    """
    MOD_P_SOURCE_FNAME ='CELLID_SECTOR_TECHNOLOGY_20171016.dsv'
    sql_highest = "SELECT MAX(cob_date) AS highest FROM {csm}".format(
        csm=conf.CSM_HISTORY)
    cur_local.execute(sql_highest)
    highest = cur_local.fetchone()['highest']
    HIGHEST_COB_DATE_IN_EXISTING = highest

    make_fresh_mod_p_src = True
    make_fresh_csm_history_src = True

    check_water = True
    if not check_water:
        utils.warn("Not running water test - presumably already run once today "
            "in this cycle?")    
    ## Note -- it is assumed that the stale data has been kept up-to-date and all individual stale items are based on the same data reality.
    use_stale_csm_only_rtis = False

    use_stale_4G_rtis_dets = False

    use_stale_dribblers = False  ## one of my favourite variable names ;-)
    use_stale_bounds = False

    use_stale_connections_only = False  ## the 3 connection stale settings are in order - an earlier True setting means the rest are rendered irrelevant
    use_stale_connection_dets = False
    use_stale_csm2 = False  ## need the csm table populated in Greenplum based on our initial steps before the connections stage sp we can handle connections and repush csm2 to GP.
    if (not make_fresh_mod_p_src and not make_fresh_csm_history_src
            and not check_water and use_stale_csm_only_rtis
            and use_stale_4G_rtis_dets and use_stale_dribblers
            and use_stale_bounds and use_stale_connections_only):
        input("If you're starting a fresh run switch everything over before "
            "proceeding e.g. make_fresh_mod_p_src to correct T/F values. "
            "Ctrl-C to stop")
    some_stale_used = (use_stale_csm_only_rtis or use_stale_4G_rtis_dets
        or use_stale_dribblers or use_stale_bounds
        or use_stale_connections_only or use_stale_csm2
        or use_stale_connection_dets)
    if some_stale_used:
        warn("Using data which goes stale if mod_p and/or CSM data is "
            "updated. Must set all stale settings to False if any changes to "
            "source data. Stale is much faster and is useful when developing.")

    ## A) Update data **********************************************************
    freshen_src(con_local, cur_local, some_stale_used,
        make_fresh_mod_p_src=make_fresh_mod_p_src,
        make_fresh_csm_history_src=make_fresh_csm_history_src,
        mod_p_src_fname=MOD_P_SOURCE_FNAME,
        start_after_cob_date_str=HIGHEST_COB_DATE_IN_EXISTING)

    ## B) Custom corrections ***************************************************
    ## i) read custom config
    corr_replacements = extra.Config.custom_replacements
    ## ii) apply changes
    ApplyChanges.replace_rti_rows(cur_local, corr_replacements)
    con_local.commit()

    ## C) Check for any coordinates in water - stop to add custom correction
    ## and restart
    if check_water:
        handle_water_dets(cur_local)

    ## D) Identify CSM-only cell_id_rtis (a.k.a. the unfindables) **************
    ## carrying out a set of different sub-steps on these
    csm_only_rtis = FillMissing.get_csm_only_rtis(cur_local,
        use_stale_csm_only_rtis)
    ## i) Identify 4G alternatives
    (alt_4G_rtis2add,
     csm_only_alt_4G_rtis_conf_adding) = FillMissing.check_or_help_make_4G_conf(
        cur_local, csm_only_rtis, use_stale_4G_rtis_dets)
    ## ii) Dribblers
    remaining_csm_only_rtis = (set(csm_only_rtis)  ## only want to work with what we haven't already handled
        - set(csm_only_alt_4G_rtis_conf_adding))
    fresh_bounds = FillMissing.check_or_help_make_dribblers_and_get_fresh_bounds(
        remaining_csm_only_rtis, use_stale_dribblers)
    ## iii) Bring across effective bounds from CSM for CSM-only
    csm_derived_rtis2add = FillMissing.check_or_help_add_csm_derived_conf(
        cur_local, remaining_csm_only_rtis, fresh_bounds, use_stale_bounds)
    ## iv) Audit CSM-only cell_id_rti's - must all be accounted for
    FillMissing.audit(csm_only_rtis, alt_4G_rtis2add, csm_derived_rtis2add)
    ## v) Apply changes
    csm_replacements = (
          conf4G.Config.alt_4G_replacements
        + conf4G.Config.custom_alt_4G_replacements
    )
    csm_additions = (
          conf4G.Config.alt_4G_additions
        + conf4G.Config.custom_alt_4G_additions
        + extra.Config.csm_derived_additions
    )
    ApplyChanges.replace_rti_rows(cur_local, csm_replacements)
    ApplyChanges.add_rows_for_new_rtis(cur_local, csm_additions)
    con_local.commit()
    cur_local.execute("SELECT COUNT(*) FROM {mod_p_locations}".format(
        mod_p_locations=conf.MOD_P_LOCATIONS))
    print("{:,} records after adding rti's from CSM".format(
        cur_local.fetchone()[0]))

    ## E) Consolidation (overlaps) *********************************************
    ## i) Get consolidation config
    consolidation_candidate_rtis = Consolidation\
        .get_consolidation_candidate_rtis(cur_local)
    Consolidation.check_or_help_add_overlap_consolidation_replacements(
        cur_local, consolidation_candidate_rtis)
    ## ii) Apply changes
    cons_replacements = (
          consol.Config.custom_consolidation_replacements
        + consol.Config.open_consolidation_replacements
        + consol_extra.Config.standard_consolidation_replacements)
    ## quality check before applied
    cons_replacement_rtis = [x['cell_id_rti'] for x in cons_replacements]
    if len(set(cons_replacement_rtis)) != len(cons_replacement_rtis):
        cons_replacement_freqs = Counter(cons_replacement_rtis)
        cons_replacement_dupes = sorted([rti 
            for rti, freq in cons_replacement_freqs.items()
            if freq > 1])
        raise Exception("Duplicate rtis in replacements: {}".format(
            cons_replacement_dupes))
    ApplyChanges.replace_rti_rows(cur_local, cons_replacements)
    con_local.commit()
    cur_local.execute("SELECT COUNT(*) FROM {mod_p_locations}".format(
        mod_p_locations=conf.MOD_P_LOCATIONS))
    print("{:,} records after consolidating rti overlaps".format(
        cur_local.fetchone()[0]))

    ## F) Consolidation (small gaps) *******************************************
    ## i) Get consolidation config
    Consolidation.check_or_help_add_gap_consolidation_replacements(cur_local)
    ## ii) Apply changes
    E_replacements = consol.Config.gap_closing_replacements
    ApplyChanges.replace_rti_rows(cur_local, E_replacements)
    cur_local.execute("SELECT COUNT(*) FROM {mod_p_locations}".format(
        mod_p_locations=conf.MOD_P_LOCATIONS))
    print("{:,} records after consolidating rti gaps".format(
        cur_local.fetchone()[0]))

    ## G) Connection-only data *************************************************
    ## i) Get connection-only config
    Connections.check_or_help_add_main_connection_additions(cur_local,
        use_stale_connections_only, use_stale_connection_dets, use_stale_csm2)
    ## ii) Apply changes
    conn_additions = conconf.Config.connection_only_additions
    ApplyChanges.add_rows_for_new_rtis(cur_local, conn_additions)
    conn_additions_backfill = conconf.Config.connection_only_additions_backfill  ## calculated using FindRanges.fill_gaps()
    """
    Backfill a potential source of FAIL (but picked up in quality check at end)
    Sad story ;-). CSM located 382063 (73/83) as always being in Taupo Bay
    Northland even though it was earlier in Gore Southland. So CSM1 had a day
    trip flood of "tourists" from Southland to Northland in April 2016. Time-
    linking via CSM 2 to the rescue. But CSM 2 only achieved this by not knowing
    about the April activity of these cell_id_rti's. Oh well - still an
    improvement in the data even if not what it should be. Then I discover that
    there was significant activity in April for those towers and give those
    periods the Taupo Bay location as per the rest of the CSM 2. Boom! The
    Southland-Northland tourism flood returns. WAT? Ah! I dig down and realise
    that these towers were in fact in Gore in April and add a custom correction
    to the CSM seed process. Unfortunately I had no check for the backfill being
    in conflict with anything else and now there were two records for each of
    the three cell_id_rti's for the April date range - one in the correct
    Southland location and one in the incorrect Northland location. So the
    Southland-Northland tourism flood survived! So need to add new checks that
    the backfill never conflicts with any thing else. Also a check at the very
    end that there are no overlaps surviving.
    """
    ApplyChanges.add_rows_for_new_rtis(cur_local, conn_additions_backfill)
    cur_local.execute("SELECT COUNT(*) FROM {mod_p_locations}".format(
        mod_p_locations=conf.MOD_P_LOCATIONS))
    print("{:,} records after adding connections-only rti's".format(
        cur_local.fetchone()[0]))
    con_local.commit()

    ## H Remove bad cell_id_rtis
    sql_purge = """\
    DELETE FROM {mod_p_locations}
    WHERE cell_id_rti IN(0, 65335)
    """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
    cur_local.execute(sql_purge)
    con_local.commit()
    print("FINISHED making CSM 2.0 ({}) on local machine !!!".format(
        conf.MOD_P_LOCATIONS))

def _data2_coord_data(data):
    coord_data = [
        ((lat, lon),
         "N={} ({})".format(
            freq,
            ', '.join(str(rti) for rti in sorted(rtis))))
        for lat, lon, freq, rtis in data]
    return coord_data

def check_mod_p(cur_local):
    unused, cur_gp = db.Pg.gp()
    ## date range overlaps
    overlapping_rtis = sorted(list(
        Consolidation.get_overlapping_rtis(cur_local)))
    if overlapping_rtis:
        print("Search for these rti's and see if in more than one config - "
            "perhaps they stomped on each other's toes. "
            "mod_mod_connections_conf.Config.connection_only_additions_backfill"
            " is a likely candidate:\n\n{}".format(overlapping_rtis))
        raise Exception(FRESH_RUN_MSG)
    ## grid12 freqs
    sql = """\
    SELECT tower_lat, tower_lon
    FROM {mod_p_locations}
    WHERE effective_end IS NULL  -- i.e. active/open
    AND tower_lat IS NOT NULL AND tower_lon IS NOT NULL
    """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
    cur_local.execute(sql)
    data = cur_local.fetchall()
    grid12s = [
        spatial.Spatial.grid12(grids_regions.Grids.s2_id(lat, lon, level=12))
        for lat, lon in data]
    grid12_freqs = Counter(grid12s)
    for n, (grid12, freq) in enumerate(sorted(grid12_freqs.items()), 1):
        print("{:04} grid12: {} freq: {:>4}".format(n, grid12, str(freq)))
    most_common = grid12_freqs.most_common(n=50)
    print(most_common)
    sql_rect_tpl = """\
    SELECT rect
    FROM {grids}
    WHERE grid_id = %s
    """.format(grids=conf.GRIDS)
    rects_dets = []
    for grid12, freq in most_common:
        cur_gp.execute(sql_rect_tpl, (grid12, ))
        rect = cur_gp.fetchone()['rect']
        lbl = "N={:,}".format(freq)
        if freq > 99:
            colour = 'red'
        elif freq > 49:
            colour = 'orange'
        else:
            colour = 'black'
        rects_dets.append(
            {'rect': rect,
             'lbl': lbl,
             'colour': colour,
             'weight': 10,
             'opacity': 1,
        })
    folium_map.Map.show_rects_on_map(cur_local,
        title="Most active cell_id_rti's", rects_dets=rects_dets)
    ## map the results by coord
    sql_active = """\
    SELECT
      ROUND(tower_lat::numeric, 3),
      ROUND(tower_lon::numeric, 3),
        COUNT(*) AS
      freq,
        ARRAY_AGG(cell_id_rti) AS
      rtis
    FROM {cells}
    WHERE effective_end IS NULL
    GROUP BY ROUND(tower_lat::numeric, 3), ROUND(tower_lon::numeric, 3)
    """.format(cells=conf.MOD_P_LOCATIONS)
    sql_all = """\
    SELECT
      ROUND(tower_lat::numeric, 3),
      ROUND(tower_lon::numeric, 3),
        COUNT(*) AS
      freq,
        ARRAY_AGG(cell_id_rti) AS
      rtis
    FROM {cells}
    GROUP BY ROUND(tower_lat::numeric, 3), ROUND(tower_lon::numeric, 3)
    """.format(cells=conf.MOD_P_LOCATIONS)
    cur_local.execute(sql_active)
    coord_data_a = _data2_coord_data(cur_local.fetchall())
    cur_local.execute(sql_all)
    coord_data_b = _data2_coord_data(cur_local.fetchall())
    folium_map.Map.map_coord_data(title="Active cell_id_rti's",
        lbl_a="Geographical distribution of ACTIVE cell_id_rti's",
        coord_data_a=coord_data_a,
        lbl_b="Geographical distribution of ALL cell_id_rti's",
        coord_data_b=coord_data_b, is_n_val=False)

def main():
    con_local, cur_local = db.Pg.local() #@UnusedVariable
    
    #make_local_csm_tbl(con_local, cur_local)
    
    #check_mod_p(cur_local)
    
    #SeedSource.overwrite_cell_location_seed(cur_local, skip_csv=False)
    #SeedSource.make_empty_test_csm2(cur_local)
    SeedSource.overwrite_test_csm(cur_local, skip_csv=False)
    print("Finished!!")


if __name__ == '__main__':
    #unittest.main()
    main()
