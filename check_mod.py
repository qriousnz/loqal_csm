#! /usr/bin/env python3
from pprint import pprint as pp

import sys
sys.path.insert(0, '../voyager_shared')
import conf #@UnresolvedImport
import utils #@UnresolvedImport

import mod_mod_extra as extra

def no_missing_rtis(cur_local):
    """
    We don't want any cell_id_rti's that are handled in CSM 1 to be missing from
    CSM 2.
    """
    sql1 = """\
    SELECT DISTINCT cell_id_rti
    FROM {csm_history}
    """.format(csm_history=conf.CSM_HISTORY)
    sql2 = """\
    SELECT DISTINCT cell_id_rti
    FROM {mod_p_locations}
    """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
    cur_local.execute(sql1)
    rtis1 = {row['cell_id_rti'] for row in cur_local.fetchall()}
    cur_local.execute(sql2)
    rtis2 = {row['cell_id_rti'] for row in cur_local.fetchall()}
    rtis2 = rtis2.union(set(extra.Config.dribblers))
    if not rtis1.issubset(rtis2):
        missing_from_2 = sorted(list((rtis1 - rtis2)))
        raise Exception("CSM 1 should be a subset of CSM 2 and it isn't. {:,} "
            "cell_id_rti's are missing: {}".format(len(missing_from_2),
            ', '.join(str(x) for x in missing_from_2)))

def location_diffs(cur_local):
    """
    We don't assume that CSM 1 is accurate and CSM is not but we would expect
    them to generally be very close (< 0.5km).
    """
    debug = True
    sql = """\
    SELECT *
    FROM (
      SELECT
        modp.cell_id_rti,
        modp_loc,
        csm_loc,
        modp_lat,
        modp_lon,
        csm_lat,
        csm_lon,
          ROUND((ST_DISTANCE(
            ST_GeomFromText('POINT(' || modp_lon || ' ' || modp_lat || ')', 4326),
            ST_GeomFromText('POINT(' || csm_lon || ' ' || csm_lat || ')', 4326)
          )*{dist2km_scalar})::numeric, 1)::float AS
        dist_km
      FROM (
        SELECT DISTINCT ON (cell_id_rti)
          cell_id_rti,
            tower_lat AS
          modp_lat,
            tower_lon AS
          modp_lon,
            physical_address AS
          modp_loc
        FROM {mod_p_locations}
        ORDER BY cell_id_rti, effective_start DESC
      ) AS modp
      INNER JOIN
      (
        SELECT DISTINCT ON (cell_id_rti)
          cell_id_rti,
            tower_latitude AS
          csm_lat,
            tower_longitude AS
          csm_lon,
            address AS
          csm_loc
        FROM {csm_history}
        WHERE cob_date = (SELECT MAX(cob_date) FROM {csm_history})
        ORDER BY cell_id_rti, id DESC
      ) AS csm_latest
      USING(cell_id_rti)
    ) AS has_dist
    WHERE dist_km > 0.5
    ORDER BY dist_km DESC
    """.format(mod_p_locations=conf.MOD_P_LOCATIONS,
        csm_history=conf.CSM_HISTORY, dist2km_scalar=conf.ST_DISTANCE2KM_SCALAR)
    cur_local.execute(sql)
    locdiffs = cur_local.fetchall()
    print(len(locdiffs))
    if debug: pp(locdiffs, width=200)
    prev_coord1 = None
    unique_locdiffs = []
    for locdiff in locdiffs:
        current_coord1 = (locdiff['modp_lat'], locdiff['modp_lon'])
        if current_coord1 != prev_coord1:
            unique_locdiffs.append(locdiff)
        else:
            continue
        prev_coord1 = current_coord1
    pp(unique_locdiffs, width=200)
    return locdiffs, unique_locdiffs

def get_appearances(cur_local, cell_id_rti):
    """
    Display both CSM (1.0) and mod_p (CSM 2.0) settings. CSM settings do not
    show a range, only first date.
    """
    sql = """\
    SELECT * FROM (
      SELECT DISTINCT ON (cell_id_rti, tower_latitude, tower_longitude, address)
        'CSM'::text AS
      src,
      cell_id_rti,
        tower_latitude AS
      csm_lat,
        tower_longitude AS
      csm_lon,
      address,
        cob_date AS
      start,
      cob_date
      FROM {csm_history}
      WHERE cell_id_rti = {cell_id_rti}
    ORDER BY cell_id_rti, tower_latitude, tower_longitude, address, cob_date
    ) AS csm_src
    UNION ALL
    SELECT * FROM (
      SELECT DISTINCT ON (cell_id_rti, tower_lat, tower_lon, physical_address)
        'mod_p'::text AS
      src,
      cell_id_rti,
        tower_lat AS
      modp_lat,
        tower_lon AS
      modp_lon,
      physical_address,
        effective_start AS
      start,
      effective_end
      FROM {mod_p_locations}
      WHERE cell_id_rti = {cell_id_rti}
      ORDER BY cell_id_rti, tower_lat, tower_lon, physical_address, effective_start
    ) AS modp_src
    ORDER BY src, start
    """.format(cell_id_rti=cell_id_rti, csm_history=conf.CSM_HISTORY,
        mod_p_locations=conf.MOD_P_LOCATIONS)
    cur_local.execute(sql)
    print(str(cur_local.query, encoding='utf-8'))
    data = cur_local.fetchall()
    for row in data:
        print(row)

def csm_lost_relocation_errors(cur_local, cell_id_rti, span_desc):
    """
    Show any errors in case they explain why CSM (1.0) missed confirmed
    relocations that mod_p (CMS 2.0) picked up.
    """
    sql = """\
    SELECT *
    FROM {errors}
    WHERE cell_id_rti = %s
    """.format(errors=conf.REDROCK_ERROR_HISTORY)
    cur_local.execute(sql, (cell_id_rti, ))
    print("In the errors during this span? {}".format(span_desc))
    for row in cur_local.fetchall():
        print(row)

def csm_lost_relocations(cur_local, cell_id_rti, span_desc):
    get_appearances(cur_local, cell_id_rti)
    csm_lost_relocation_errors(cur_local, cell_id_rti, span_desc)
    ## check rr directly, not CSM?

def main():
    unused_con_local, cur_local, unused_cur_local2 = utils.Pg.get_local()
    #no_missing_rtis(cur_local)
    #location_diffs(cur_local); return
    missing_relocation_checks = [
        #(277103, "2017-03-28 onwards"),  ## some lost but only a few days at start of period
        #(52426, "2015, 12, 17 onwards"),  ## no explanation
        #(37571, "2016, 12, 16 to 2017, 1, 10"),  ## error at start but no explanation
        #(34265, "2017, 5, 9 on"),  ## error data not up-to-date enough to determine
        #(35521, "2015, 12, 17 to 2015, 12, 22"),  ## before redrock so should have been correct in csm
        #(43711, "2016, 11, 3 to 2016, 11, 13"),  ## no explanation
        #(51186, ""),
        #(430705, '2016-11-26 and 2017-03-30'),
        #(50574, ''),
        #(38478, ''),
        #(36964, ''),
        #(55555, ''),
        #(274053, ''),
    ]
    for cell_id_rti, span_desc in missing_relocation_checks:
        csm_lost_relocations(cur_local, cell_id_rti, span_desc)
    diff_dist_checks = [
        #(277103, ['2016-09-01', '2017-04-09'], None),
        '''CSM missed relocation''',
        #(52426, ['2015-05-01', '2016-01-10'], [52421,52422,52423,52424,52425]), 
        '''CSM missed relocation''',
        #(52541, ['2015-03-02', ], [52544, ])#'2016-05-01']), 
        '''Mod_p minor oddity - mod_p has relocation that didn't happen
         significantly - after first location stopped just dribbling connection
         data''',
        #(37571, ['2016-12-20', ], [37572,37574,37575,37577,37578]),
        '''CSM missed relocation - to Gisborne''',
        #(52871, ['2016-06-01', ], [52872, 52873]), 
        '''CSM wrong coords - both had same address but CSM had wrong coords
        (ChCh vs lower Southland)''',
        #(34265, ['2017-05-11'], [34261, 34262, 34264]),
        '''CSM wrong - in Hobsonville as per mod_p vs Gisborne for CSM''',
        #(35521, ['2015-12-20'], [35522, ]),
        ''' CSM missed relocation - in Gisborne like mod_p says.''',
        #(35423, ['2016-01-01', ], [35421, 35422]),
        '''CSM missed relocation - in Gisborne like mod_p says''',
        #(43711, ['2016-11-07'], [43701, 43702, 43703, 43704, 43705, 43707, 43708, 43712, 43714, 43715, 43717, 43718]),
        '''CSM missed relocation - in Christchurch as mod_p said not Twizel.
        Proof that Twizel can show up in data when actually there below:''',
        #(43711, ['2016-04-04'], [43701, 43702, 43703, 43704, 43705, 43707, 43708, 43712, 43714, 43715, 43717, 43718]),
        '''N/A - the setting above is not for a separate check - see previous
        note''',
        #(51186, ['2015-03-13', ], []),
        '''Irrelevant rti (or mod_p minor oddity) - only 3 days of activation
        ever! Mod_p said it was in Nelson before the data connection data so
        that may well be true. In the time period we have connection data for
        the only recorded location was Hokitika. The CSM got the location
        correct but the dates wrong (namely 2016-06-07 and 2016-12-12). Mod_p
        missed the Hokitika location altogether (IIRC).''',
        #(430705, ['2016-11-26', '2017-04-01', '2017-02-09', '2017-03-30'], []),
        '''Mod_p superior - but missed one 2-week patch. Got 2016-11-23 to
        2016-12-05 ChCh and 2017-02-09 to 13 Blenheim correct. Missed it being
        in Christchurch as per the connection data 2017-03-24 to 04-03. But much
        better than CSM which only recorded a 1-day activation. Extra note - the
        Clendon (Auckland) circle on the map looks fake as it is isolated (no
        surrounding rtis). The rti(s) that are located in Clendon are probably
        in Christchurch instead.''', 
        #(52473, ['2016-01-01', '2016-06-12'], []),
        '''CSM missed relocation''',
        #(50574, ['2015-12-31', ], [50571, ]), # '2015-05-01', '2015-12-21', '2016-12-12'], [50571, ]),
        '''CSM missed relocation - CSM missed relocation and asserted the rti
        was in Oamaru for one day when it wasn't (no connection data). Mod_p was
        a bit generous on the second time span (which is the best way to get it
        wrong) but did pick up Cardrona where it was for 2015-12-28 to
        2016-01-01 (vs 18th - 2nd) unlike CSM.
        ''',
        #(38478, ['2016-06-07', '2016-12-12', ], [38471, 38472, 38474, 38475, 38477]),
        '''CSM wrong - wasn't anywhere in two time periods asserted''',
        #(36964, ['2015-12-01', '2016-06-07', '2016-12-12', '2017-01-10', '2017-01-18', ], [36961,36962,36963,36965,36966]),
        '''
        CSM missed lots of data and was wrong - CSM missed all of 2015 data, and
        there was no connection traffic on the first two dates it asserted there
        would be. It was correct about 2017-01-18 but missed the 30 or so days
        around it that mod_p got right. Actual results from connections - near
        enough to Omaha, nothing, nothing, near enough to Omaha, near enough to
        Omaha.''',
        #(55555, ['2015-04-02', '2016-06-07', '2016-12-12', ], [55551, 55552, 55554]), ## 2015-04-01 missing data in fact_rti_summary even though in fact_cell_summary.
        '''
        CSM missed relocation - to Napier and was wrong about it being somewhere
        on two single-day ranges.''',
        #(274053, ['2015-06-06', '2015-10-06', '2016-06-06', '2016-10-10', '2016-12-12', '2017-05-01'], [274033, 274043, ]),
        #(274053, ['2015-10-06'], [274033, 274043, ]),
        '''CSM missed relocation and missed lots of data. Mod_p minor oddity.
        2016-05-26 to 2016-12-14 and 2017-04-10 to 2017-05-29 in connection
        data. ChCh, ChCh, ChCh, Hanmer Springs CSM didn't include Hanmer (the
        cell_id_rti ended up in the errors because of 'Matches multiple LTE
        Cells but SHMSA3_L(1),SSLBAD3_L(1) not marked as split-sector') and only
        had two one-day ranges in ChCh. The latter were correct but missed all
        the rest of 2016. Mod_p included 2015 when it shouldn't have so too
        generous (which is OK).''',
        ## stopping here - all other discrepancies are less than 100km and the pattern so far has been compellingly in favour of Mod_P over CSM 1
    ]
    for item in diff_dist_checks:
        try:
            cell_id_rti, date_strs, exclusions = item
        except ValueError:
            continue  ## comment only
        for date_str in date_strs:
            start_datetime_str = date_str + ' 00:00:00'
            end_datetime_str = date_str + ' 23:59:59'
            cell_id_rti = cell_id_rti
            utils.FindRelocations.get_likely_location_dts(cur_local,
                start_datetime_str, end_datetime_str, cell_id_rti,
                cell_id_rtis_to_exclude=exclusions)
    #utils.FindRelocations.get_daily_tots(rti=274053)
    ## Confirm relocations
    """
    SELECT *
    FROM mod_p_locations
    WHERE cell_id_rti IN (53712, 52541, 58161, 46192, 48311, 409987, 412015, 37581, 35421, 47785, 430703)
    ORDER BY cell_id_rti, effective_start
    """
    rti_relocs = []  #35421, 37581, 46192, 47785, 48311, 52541, 53712, 58161,
    #    409987, 412015, 430703]  ## hand selected
    sql_tpl = """\
    SELECT *
    FROM {mod_p_locations}
    WHERE cell_id_rti = %s
    ORDER BY effective_start
    """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
    for rti in rti_relocs:
        print("Details for configuring {}".format(rti))
        utils.FindRelocations.get_daily_tots(rti)
        cur_local.execute(sql_tpl, (rti, ))
        for row in cur_local.fetchall():
            print(row)
        if input("Proceed (y/n)? ").lower() == 'y':
            continue
    #return
    print("Don't forget to add corrections back to CSM2 maker code via custom "
        "config.")
    relocs_dets = [
#         (35421, [
#             ('2015-12-28', '2016-01-01'),  ## Waimata Valley Rd - in mod_p as '2015-12-17', '2016-01-11'
#             ('2017-02-04', None),  ## Western Springs - Guns'n'Roses - actually in mod_p as '2017-02-02', '2017-02-06'
#             ('2017-03-22', '2017-03-26'),  ## Western Springs - actually recorded as 27th
#         ]),
#         (37581, [
#             ('2016-12-16', '2017-01-10'),  ## Gisborne
#         ]),
#         (46192, [
#             ('2016-07-10', None),  ## Opunake South Taranaki
#         ]),
#         (47785, [
#             ('2016-11-17', '2016-11-20'),  ## Martinborough was 11-21 in mod-p
#             ('2017-03-13', '2016-03-18'),  ## Feilding was 03-20
#         ]),
#         (48311, [
#             ('2016-04-28', '2016-05-01'),  ## Invercargill was 05-02
#             ('2016-08-16', None),  ## Kaikoura
#         ]),
#         (52541, [
#             ('2015-01-01', '2015-04-13'),  ## started 2010-05-31 Winton
#             ('2016-03-19', '2016-03-20'),  ## ChCh was 2016-06-24. TODO - Fix CMS2
#         ]),
#         (53712, [
#             ('2015-02-18', '2015-02-19'),  ## ChCh was 02-17
#             ('2015-07-17', None),  ## ChCh was '2015-07-16', '2015-07-19' NB None means don't check end as it is the same date
#         ]),
#         (58161, [
#             ('2015-01-16', '2015-01-27'),  ## Wellington - missing from CSM
#             ('2015-06-30', '2015-10-28'),  ## Missing from mod_p and CSM. TODO - Fix CMS2
#             ('2016-06-15', '2016-09-30'),  ## Ohakune
#         ]),
#         (409987, [
#             ('2016-02-03', '2016-02-15'),  ## Gore - was '2016-04-19'. TODO - Fix CMS2
#             ('2016-09-29', None),  ## Ohoka
#         ]),
#         (412015, [
#             ('2016-03-23', '2016-03-28'),  ## Wanaka - was end '2016-04-01'  ## TODO - explore more - shifted from Wanak to Bulls on single day?
#             ('2016-11-23', None),  ## Bulls - was 2016-11-24. TODO - Fix CSM2
#         ]),
#         (430703, [  ## TODO - Fix CSM2.
#             ('2016-11-23', '2016-12-04'),  ## ChCh - was 2016-12-05
#             ('2017-02-09', '2017-02-13'),  ## Blenheim was till '2017-04-20'
#             ('2017-04-20', None),  ## Clendon was starting 2017-04-24
#         ]),
#         (430703, [  ## TODO - Fix CSM2.
#             #('2017-03-01', '2017-04-01'),  ## Blenheim/Clendon?
#             #('2017-04-15', '2017-04-18'),  ## Blenheim/Clendon?
#             #('2017-02-15', '2017-02-21'),  ## Blenheim/Clendon?
#             #('2017-02-10', '2017-02-13'),  ## Blenheim/Clendon?
#             #('2017-02-14', None),  ## Blenheim/Clendon?
#             ('2017-04-19', None),  ## Blenheim/Clendon?
#         ]),
    ]
    for rti, ranges_dets in relocs_dets:
        for start_date_str, end_date_str in ranges_dets:
            for date_str in [start_date_str, end_date_str]:
                if not date_str:
                    continue
                start_datetime_str = date_str + ' 00:00:00'
                end_datetime_str = date_str + ' 23:59:59'
                utils.FindRelocations.get_likely_location_dts(cur_local,
                    start_datetime_str, end_datetime_str, rti,
                    rtis_to_exclude=None)

if __name__ == '__main__':
    main()
