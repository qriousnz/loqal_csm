
"""
https://en.wikipedia.org/wiki/Mobile_country_code#Australia_-_AU
Handling the main MNC's of the Big Three (Telstra, Optus, and Vodafone)

Note - left joining to cell_locations from events is not enough to get
everything missing. Need a LJ to a query which has the IJ between the two in it.
Otherwise miss out on where there is a cell_id_rti but the date ranges don't
line up.
"""

from collections import defaultdict, Counter
import datetime
from functools import partial
import json

import requests

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import conf, db, folium_map, utils

CONSOLIDATED_ROAMERS = os.path.join(conf.CSM_ROOT, 'consolidated_roamers.json')
IMSI_FREQS = os.path.join(conf.CSM_ROOT, 'imsi_freqs.json')
ROAMING_EPISODES_ROOT = os.path.join(conf.CSM_ROOT,
    'reports/mass_reports/roaming_episodes/')
IMSI_ROAM_DATE_RANGES_FPATH = os.path.join(conf.CSM_ROOT,
        'reports/imsi_roam_date_ranges.json')

GOOGLE_API_KEY = "AIzaSyBnLnNEuxl5vihsznzOR3s1kiTbmuTyzSg"
GOOGLE_API_URL = ("https://www.googleapis.com/geolocation/v1/geolocate?key="
    + GOOGLE_API_KEY)
AUS_MCC = 505
TELSTRA_MNC = 1
OPTUS_MNC = 2
VODAFONE_MNC = 3
TELSTRA_CARRIER_NAME = 'Telstra'
OPTUS_CARRIER_NAME = 'Optus'
VODAFONE_CARRIER_NAME = 'Vodafone'

CARRIER_DETS = [
    (TELSTRA_MNC, TELSTRA_CARRIER_NAME),
    (OPTUS_MNC, OPTUS_CARRIER_NAME),
    (VODAFONE_MNC, VODAFONE_CARRIER_NAME),
]

TECH_LTE = 'lte'
TECH_GSM = 'gsm'
TECH_CDMA = 'cdma'
TECH_WCDMA = 'wcdma'

AUCK_AIRPORT_RTIS = (33071, 33072, 33073, 33074, 33075, 33076, 35621, 35622,
35623, 46021, 46022, 46023, 12801311, 12801312, 12801313, 33451, 33452, 33453,
33454, 33455, 33456, 35641, 35642, 35643, 46161, 46162, 46163, 12930079,
12930080, 12930081, 33651, 33652, 33653, 33654, 46291, 46292, 12930335,
12930336, 12930415, 12930425)

TAUPO_AIRPORT_RTIS = (25801, 25802, 25803, 25804, 25805, 25806, 25807, 25808,
25809, 331631, 331641, 331651)

NEW_PLYMOUTH_AIRPORT_RTIS = (45651, 45652, 45653, 45654, 45655, 45656, 472175,
472185, 472195)

PALMERSTON_NORTH_AIRPORT_RTIS = (46041, 46042, 46043, 46044, 46045, 46046,
56041, 56042, 56043, 56044, 56045, 56046, 344431, 344441, 344451)

WGN_AIRPORT_RTIS = (55311, 55312, 55313, 13187359, 13187360, 13187361, 55111,
55112, 55113, 13220383, 13220384, 13220385, 55211, 55212, 55213, 55214, 55215,
55216, 15131167, 15131168, 15131169)

NELSON_AIRPORT_RTIS = (41591, 41592, 41593, 41594, 41595, 41596, 41597, 41598,
41599, 51591, 51592, 51593, 51594, 51595, 51596, 385393, 385403, 385413, 44771,
44772, 44773, 44774, 44775, 44776, 44777, 44778, 44779, 52230, 52231, 52232,
52233, 52234, 52235, 52236, 54771, 54772, 54773, 54774, 54775, 54776, 54777,
54778, 54779, 315761, 315771, 315781)

CHCH_AIRPORT_RTIS = (40101, 40102, 40103, 40104, 40105, 40106, 40107, 40108,
40109, 40121, 40122, 40123, 50101, 50102, 50103, 50104, 50105, 422511, 422513,
422521, 422523, 422531, 422533, 13895199, 13895200, 13895201, 422769, 422789,
43023, 43063)

QT_AIRPORT_RTIS = (21651, 21652, 21653, 21654, 21655, 21656, 21657, 21658,
21659, 31651, 31652, 31653, 31654, 31655, 31656, 428399, 428401, 428405, 428406,
428407, 428409, 428411, 428415, 428416, 428417, 428419, 428421, 428425, 428426,
428427)

DUN_AIRPORT_RTIS = (57191, 57192, 57194, 57195, 57196, 13883680, 13883681,
50831, 50832, 50833, 50834, 50835, 51631, 51632, 51634, 51635, 51637, 51638)

TONGA_CELLS = (10007,10016,144081,144091,154093,154133,194077,20005,20007,20202,
21900,22402,3013,3014,3015,3023,3024,96637)

def get_rtis():
    unused_con_rem, cur_rem = db.Pg.get_rem()
    sql = """\
    SELECT DISTINCT cell_id_rti
    FROM {cells}
    ORDER BY cell_id_rti
    """.format(cells=conf.CELL_LOCATION)
    cur_rem.execute(sql)
    rtis = [row['cell_id_rti'] for row in cur_rem.fetchall()]
    return rtis

def get_location(config):
    """
    https://developers.google.com/maps/documentation/geolocation/intro
    """
    debug = True
    data = {
        "homeMobileCountryCode": config['mcc'],
        "homeMobileNetworkCode": config['mnc'],
        "radioType": config['tech_class'],  ## Supported values are lte, gsm, cdma, and wcdma
        "carrier": config['carrier_name'],  ## e.g. Vodafone
        "cellTowers": [
            {
                'cellId': config['rti'],
                'locationAreaCode': 1,  ## The Location Area Code (LAC) for GSM and WCDMA networks. The Network ID (NID) for CDMA networks. 2256 for 23333?
                'mobileCountryCode': config['mcc'],
                'mobileNetworkCode': config['mnc'],
                'age': 0,  ## 0 = current
            }],
        "considerIp": 'false',  ## Specifies whether to fall back to IP geolocation if wifi and cell tower signals are not available. Note that the IP address in the request header may not be the IP of the device. Defaults to true. Set considerIp to false to disable fall back. 
    }
    resp = requests.post(GOOGLE_API_URL, json=data)
    data = resp.json()
    if "error" not in data:
        lat = data["location"]["lat"]
        lon = data["location"]["lng"]
        cell_id = config['rti']
        if debug: print("{}, {}, {}".format(cell_id, lat, lon))
    else:
        if debug:
            print("Error: {}".format(
                resp.json()["error"]["errors"][0]["reason"]))
        lat, lon = None, None
    return lat, lon

def find_collisions():
    """
    Doesn't work - can't even find a single Australian cell_id_rti even though
    lots of them overlap. Experiment with one known cell_id_rti until that works
    and _then_ automate en masse.
    """
    rtis = get_rtis()
    configs = []
    for rti in rtis:
        for mnc, carrier_name in CARRIER_DETS:
            configs.append({
                'mcc': AUS_MCC, 'mnc': mnc, 'carrier_name': carrier_name,
                'tech_class': TECH_LTE, 'rti': rti, })
    
    for n, config in enumerate(configs, 1):
    #     if n < 53500:
    #         continue
        lat, lon = get_location(config)
        if lat and lon:
            print(config, lat, lon)
        if n % 100 == 0:
            print(utils.prog(n, "api configuration "
                "(currently rti: {} mnc: {} carrier_name: {} tech_class: {})"
                .format(config['rti'], config['mnc'], config['carrier_name'],
                TECH_LTE)))

def test_get_location():
    config = {
        'mcc': 530,
        'mnc': 5,
        'carrier_name': 'Spark',
        'tech_class': 'lte',
        'rti': 257055,
    }
    print(get_location(config))

def find_auck_airport_to_tonga_travellers(date_str):
    """
    Must connect to Auckland Airport AND to 10007 on same day. Look at days
    after that to see if clearly only connecting to Tongan cell_id_rti's before
    appearing at Auckland Airport again.
    """
    unused_con_rem, cur_rem = db.Pg.get_rem()
    auck_rti_clause = db.Pg.nums2clause(AUCK_AIRPORT_RTIS)
    sql = """\
    SELECT DISTINCT imsi_rti
    FROM (
      SELECT
        imsi_rti,
          (cell_id_rti IN {auck_rti_clause})::int AS
        in_auck_airport,
          (cell_id_rti = 10007)::int AS
        in_10007
      FROM {events}
      LEFT JOIN
      {locs} AS locs
      USING(cell_id_rti)
      WHERE date = '{date}'
      AND (
        locs.cell_id_rti IS NULL
        OR      
        (locs.start_date <= date
        AND (locs.end_date IS NULL OR locs.end_date >= date)
        )
      )
      AND cell_id_rti NOT IN (0, 65535)  -- fake rti's that can appear anywhere
    ) AS src
    GROUP BY imsi_rti
    HAVING MAX(in_auck_airport) = 1
    AND MAX(in_10007) = 1
    ORDER BY imsi_rti
    """.format(auck_rti_clause=auck_rti_clause, date=date_str,
        events=conf.CONNECTION_DATA, locs=conf.CELL_LOCATION)
    cur_rem.execute(sql)
    imsis = [row['imsi_rti'] for row in cur_rem.fetchall()]
    return imsis

def find_auck_airport_travellers(date_str):
    """
    Find people who only connect to Auckland Airport before 8am (trying to
    exclude as many workers and arrivals as possible). 8 onwards can't have Wgn,
    ChCh, or Dun airports and must have at least one location south of Taupo.
    Can then look at their connections after 8am. Is there Auckland Airport,
    then a gap, then rti's which are all in Sydney, at least for an hour or so?

    Note - many travellers will not connect to the Airport cell_id_rti's but
    will remain connected to the ones they passed on the way there.
    """
    unused_con_rem, cur_rem = db.Pg.get_rem()
    auck_rti_clause = db.Pg.nums2clause(AUCK_AIRPORT_RTIS)
    taupo_rti_clause = db.Pg.nums2clause(TAUPO_AIRPORT_RTIS)
    np_rti_clause = db.Pg.nums2clause(NEW_PLYMOUTH_AIRPORT_RTIS)
    pn_rti_clause = db.Pg.nums2clause(PALMERSTON_NORTH_AIRPORT_RTIS)
    wgn_rti_clause = db.Pg.nums2clause(WGN_AIRPORT_RTIS)
    nelson_rti_clause = db.Pg.nums2clause(NELSON_AIRPORT_RTIS)
    chch_rti_clause = db.Pg.nums2clause(CHCH_AIRPORT_RTIS)
    qt_rti_clause = db.Pg.nums2clause(QT_AIRPORT_RTIS)
    dun_rti_clause = db.Pg.nums2clause(DUN_AIRPORT_RTIS)
    sql_auck_airport_departures = """\
    SELECT imsi_rti
    FROM (
      SELECT
        imsi_rti,
          CASE
            WHEN cell_id_rti IN {auck_rti_clause} AND start_date_str < '{date} 08:00:00' THEN
            -1 -- 'early at Auckland Airport'
            WHEN cell_id_rti IN {auck_rti_clause} AND start_date_str >= '{date} 08:00:00' THEN
            1 -- 'late at Auckland Airport'
            ELSE 0  -- not at Auckland Airport
          END AS
        auckland_airport_status,
          cell_id_rti IN {np_rti_clause} AS
        at_np_airport,
          cell_id_rti IN {taupo_rti_clause} AS
        at_taupo_airport,
          cell_id_rti IN {pn_rti_clause} AS
        at_pn_airport,
          cell_id_rti IN {wgn_rti_clause} AS
        at_wgn_airport,
          cell_id_rti IN {nelson_rti_clause} AS
        at_nelson_airport,
          cell_id_rti IN {chch_rti_clause} AS
        at_chch_airport,
          cell_id_rti IN {qt_rti_clause} AS
        at_qt_airport,
          cell_id_rti IN {dun_rti_clause} AS
        at_dun_airport,
          tower_lat < -39 AS  -- South of Taupo
        south_of_taupo
      FROM {connections}
      LEFT JOIN
      {locs} AS locs
      USING(cell_id_rti)
      WHERE date = '{date}'
      AND locs.start_date <= date
      AND (locs.end_date IS NULL OR locs.end_date >= date)
      AND cell_id_rti NOT IN (0, 65535)  -- fake rti's that can appear anywhere
    ) AS src
    GROUP BY imsi_rti
    HAVING MIN(auckland_airport_status) = -1
    AND MAX(auckland_airport_status) < 1 -- OK if includes never-been-at-Auckland-Airport statuses but cannot include any onlylater airport statuses
    AND MAX(at_taupo_airport::int) = 0 -- never at Taupo Airport
    AND MAX(at_np_airport::int) = 0 -- never at New Plymouth Airport
    AND MAX(at_pn_airport::int) = 0 -- never at Palmerston North Airport
    AND MAX(at_wgn_airport::int) = 0 -- never at Wellington Airport
    AND MAX(at_nelson_airport::int) = 0 -- never at Nelson Airport
    AND MAX(at_chch_airport::int) = 0  -- never at Christchurch Airport
    AND MAX(at_qt_airport::int) = 0  -- never at Queenstown Airport
    AND MAX(at_dun_airport::int) = 0  -- never at Dunedin Airport
    AND MAX(south_of_taupo::int) = 1  -- at least one rti south of Taupo
    ORDER BY imsi_rti
    OFFSET 0 LIMIT 200
    """.format(auck_rti_clause=auck_rti_clause,
        taupo_rti_clause=taupo_rti_clause, np_rti_clause=np_rti_clause,
        pn_rti_clause=pn_rti_clause, wgn_rti_clause=wgn_rti_clause,
        nelson_rti_clause=nelson_rti_clause, chch_rti_clause=chch_rti_clause,
        qt_rti_clause=qt_rti_clause,
        dun_rti_clause=dun_rti_clause, date=date_str,
        connections=conf.CONNECTION_DATA, locs=conf.CELL_LOCATION)
    cur_rem.execute(sql_auck_airport_departures)
    imsis = [row['imsi_rti'] for row in cur_rem.fetchall()]
    return imsis

def imsi_report(imsi, date_str):
    unused_con_rem, cur_rem = db.Pg.get_rem()
    sql = """\
    SELECT
        events.imsi_rti AS
      rti,
      events.start_date_str,
      tower_lat,  -- position early so lat starts in same exact x-position for easy comparisons
      tower_lon,
      num_events,
      duration,
      tech_class,
      cell_id_rti,
      node_description,
      locs.start_date,
      locs.end_date
    FROM {connections} AS events
    LEFT JOIN
    {locs} AS locs
    USING(cell_id_rti)
    WHERE imsi_rti = %s
    AND date = %s
    AND locs.start_date <= date
    AND (locs.end_date IS NULL OR locs.end_date >= date)
    ORDER BY events.start_date_str, events.end_date_str
    """.format(connections=conf.CONNECTION_DATA, locs=conf.CELL_LOCATION)
    cur_rem.execute(sql, (imsi, date_str))
    data = cur_rem.fetchall()
    return data

def imsis_which_touched_rti(rti, date_str):
    unused_con_rem, cur_rem = db.Pg.get_rem()
    sql = """\
    SELECT DISTINCT imsi_rti
    FROM {events}
    WHERE date = %s
    AND cell_id_rti = %s
    ORDER BY imsi_rti
    """.format(events=conf.CONNECTION_DATA)
    cur_rem.execute(sql, (date_str, rti))
    imsis = [row['imsi_rti'] for row in cur_rem.fetchall()]
    return imsis

def context_report(imsi, start_date_str, end_date_str, fewer_fields=False):
    unused_con_rem, cur_rem = db.Pg.get_rem()
    if fewer_fields:
        fields = """\
        events.start_date_str,
        cell_id_rti,
        node_description,
        tower_lat,  -- position early so lat starts in same exact x-position for easy comparisons
        tower_lon
        """
        fields_str = '_few_fields'
    else:
        fields = """\
          events.imsi_rti AS
        imsi,
        events.start_date_str,
        tower_lat,  -- position early so lat starts in same exact x-position for easy comparisons
        tower_lon,
        num_events,
        duration,
        tech_class,
        cell_id_rti,
        node_description,
        locs.start_date,
        locs.end_date
        """
        fields_str = '_all_fields'
    sql = """\
    SELECT
      {fields}
    FROM {events} AS events
    LEFT JOIN
    {locs} AS locs
    USING(cell_id_rti)
    WHERE imsi_rti = %s
    AND date BETWEEN %s AND %s
    AND (locs.cell_id_rti IS NULL
      OR (locs.start_date <= date
        AND (locs.end_date IS NULL OR locs.end_date >= date)
      )
    )
    ORDER BY events.start_date_str, events.end_date_str
    """.format(fields=fields, events=conf.CONNECTION_DATA,
        locs=conf.CELL_LOCATION)
    cur_rem.execute(sql, (imsi, start_date_str, end_date_str))
    data = cur_rem.fetchall()
    report_name = 'reports/imsi_{}{}_{}_to_{}.txt'.format(imsi, fields_str,
        start_date_str, end_date_str)
    with open(report_name, 'w') as f:
        for row in data:
            f.write("\n{}".format(row))
    print("FINISHED! Made '{}'".format(report_name))

def dates_either_side(date_str):
    dt = datetime.datetime.strptime(date_str, '%Y-%m-%d')
    one_day = datetime.timedelta(days=1)
    prev_date_str = (dt - one_day).strftime('%Y-%m-%d')
    next_date_str = (dt + one_day).strftime('%Y-%m-%d')
    return prev_date_str, next_date_str

def unlocatable_rate(imsi, date_str):
    unused_con_rem, cur_rem = db.Pg.get_rem()
    sql = """\
    SELECT
        SUM(unlocatable)::float/COUNT(*)::float AS
      unlocatable_rate
    FROM (
      SELECT
        imsi_rti,
          CASE WHEN grid12 IS NULL THEN 1 ELSE 0 END AS
        unlocatable
      FROM {events} AS events
      LEFT JOIN
      {locs} AS locs
      USING(cell_id_rti)
      WHERE date = %s
      AND imsi_rti = %s
      AND (locs.cell_id_rti IS NULL  -- otherwise functions like an inner join when the time-linking occurs below
        OR (
          locs.start_date <= date
          AND (locs.end_date IS NULL OR locs.end_date >= date)
        )
      )
    ) AS src
    """.format(events=conf.CONNECTION_DATA, locs=conf.CELL_LOCATION)
    cur_rem.execute(sql, (date_str, imsi))
    unlocatable_rate = cur_rem.fetchone()['unlocatable_rate']
    return unlocatable_rate

def airport_then_more_unlocatables_travellers(date_str):
    prev_date_str, next_date_str = dates_either_side(date_str)
    imsis = find_auck_airport_travellers(date_str)
    n_imsis = len(imsis)
    print("Found {:,} IMSI's that were at the Airport".format(n_imsis))
    increased_unlocatables_travellers = []
    for n, imsi in enumerate(imsis, 1):
        prev_unlocatable_rate = unlocatable_rate(imsi, prev_date_str)  ## e.g. 0.1 (more likely 0.0 actually)
        next_unlocatable_rate = unlocatable_rate(imsi, next_date_str)  ## e.g. 0.3
        has_prev = (prev_unlocatable_rate not in (None, 0))
        has_next = (next_unlocatable_rate not in (None, 0))
        if has_next:
            if has_prev:
                sig_increased_rate = ((next_unlocatable_rate/
                                   prev_unlocatable_rate) > 3)
                suspicious = sig_increased_rate
            else:
                suspicious = (next_unlocatable_rate > 0.1) 
        else:
            suspicious = False  ## nothing missing on next day after travel - still could be a tourist but better fishing elsewhere
        if suspicious:
            print("IMSI {} looks suspicious :-)".format(imsi))
            increased_unlocatables_travellers.append(imsi)
        print(utils.prog(n, "IMSI", n_imsis))
    return increased_unlocatables_travellers

def get_likely_roamer():
    #date_str = '2017-01-04'
    #imsis = airport_then_more_unlocatables_travellers(date_str)
    '''
    #imsis = find_auck_airport_travellers(date_str)
    #imsis = find_auck_airport_to_tonga_travellers(date_str)
    coord_data_a = [
        #((39.171753, 115.735474), 900),
        #((41.086120605469, 121.10778808594), 901),
        #((36.839904785156, 120.52551269531), 902),
        ((28.682556152344, 115.83435058594), 903),
        #((39.916076660156, 116.54846191406), 904),
        #((23.041736, 112.558228), 905),
        #((34.439392089844, 116.97692871094), 906),
        #((39.608459472656, 122.71179199219), 907),
        
        #((36.834411621094, 114.49401855469), 2000),
        #((41.113586425781, 121.11877441406), 2001),
        #((32.126770019531, 118.78967285156), 2002),
        #((30.467834472656, 113.98864746094), 2003),
        ((28.561706542969, 115.88928222656), 2004),
        #((23.068542480469, 113.20861816406), 2005),
        #((30.753479003906, 108.37463378906), 2006),
        #((123.37097167969, 41.778259277344), 2007),
        
        ((30.537872, 114.384155), 5000),

        ((28.561706542969, 115.88928222656), 6000),


    ]
    folium_map.Map.map_coord_data(title='China traveller?', lbl_a='China', unique_a='China', coord_data_a=coord_data_a,
            lbl_b=None, unique_b=None, coord_data_b=None, marker_dets_fn=None,
            force_icon=False, zoom_start=5)
    return
    
    #print(imsis); return
    context_report(-8450883073529067843, '2017-01-04', '2017-01-30'); return
    #imsis = find_auck_airport_travellers(date_str)
    
    
    #rti = 10050
    #imsis = imsis_which_touched_rti(rti, date_str)
    imsis = [-8188957241004794980, -8126271826482331013, -6520727902840824931,
    -5317853291299584466, -4356600859227391677, -4180778953776262800,
    -1843167866294597348, 87129656696627872, 781393793564690307,
    2302475251509967831, 2687747413752961473, 3621622953077282718]
    for imsi in imsis:
        context_report(imsi, '2017-01-04', '2017-01-07', fewer_fields=True)
    return
    n_imsis = len(imsis)
    print(n_imsis)
    for n, imsi in enumerate(imsis, 1):
        if n != 1: input("Press any key to continue ...")
        data = imsi_report(imsi, date_str)
        print("\n\n IMSI {:,} of {:,} ".format(n, n_imsis) + "*"*100)
        for row in data:
            print(row)
#     imsi = -8251191066000937653
#     start_date_str = '2017-07-30'
#     end_date_str = '2017-08-08'
#     context_report(imsi, start_date_str, end_date_str)
    '''

class RoamHunter():
    """
    Detect every IMSI-day which has more than 10% missing locations
    OR at least 5.
    """

    ROOT = os.path.join(conf.CSM_ROOT, 'reports/mass_reports/roamers/')

    @staticmethod
    def roaming_imsis(date_str, last_imsi_done=None):
        """
        Pre-filter to those with any missing locations (ignoring rti's 0 and
        65535 - these two are always missing and do not indicate valid events).
        Chops down from 2,000K to 10K.
        
        Then, for those imsi's, look at rate of unlocatables. Should be half at
        least.
        """
        unused_con_rem, cur_rem = db.Pg.get_rem()
        if last_imsi_done is None:
            imsi_min_clause = ""
        else:
            imsi_min_clause = "AND imsi_rti > {}".format(last_imsi_done)
        ## IMSI's with rti's which lack locations (ignoring 0 and 65536) on day in question
        sql_imsis_with_unlocatables = """\
        SELECT DISTINCT imsi_rti
        FROM {events} AS events
        LEFT JOIN
        (
            SELECT events.cell_id_rti, locs.grid12
            FROM {events}
            INNER JOIN {locs} AS locs
            USING(cell_id_rti)
            WHERE date = %s
            {imsi_min_clause}
            AND locs.start_date <= date
            AND (locs.end_date IS NULL OR locs.end_date >= date)
        ) AS located_rtis
        USING(cell_id_rti)
        WHERE date = %s
        {imsi_min_clause}
        AND events.cell_id_rti NOT IN (0, 65535)
        AND located_rtis.cell_id_rti IS NULL
        """.format(events=conf.CONNECTION_DATA, locs=conf.CELL_LOCATION,
            imsi_min_clause=imsi_min_clause)
        sql_roaming = """\
        SELECT imsi_rti
        FROM (
          SELECT
            imsi_rti,
              CASE WHEN grid12 IS NULL THEN 1 ELSE 0 END AS
            unlocatable
          FROM {events} AS events
          LEFT JOIN
          (
              SELECT cell_id_rti, locs.grid12
              FROM {events}
              INNER JOIN {locs} AS locs
              USING(cell_id_rti)
              WHERE date = %s
              {imsi_min_clause}
              AND locs.start_date <= date
              AND (locs.end_date IS NULL OR locs.end_date >= date)
          ) AS located_rtis
          USING(cell_id_rti)
          WHERE date = %s
          AND imsi_rti IN ({crude_filter})
          AND events.cell_id_rti NOT IN (0, 65535)
        ) AS src
        GROUP BY imsi_rti
        HAVING (SUM(unlocatable)/COUNT(*))::float >= 0.5
        ORDER BY imsi_rti
        """.format(events=conf.CONNECTION_DATA, locs=conf.CELL_LOCATION,
            crude_filter=sql_imsis_with_unlocatables)
        cur_rem.execute(sql_roaming, (date_str, date_str))
        imsis = [row['imsi_rti'] for row in cur_rem.fetchall()]
        return imsis

    @staticmethod
    def get_date_strs(start_date_str='2015-01-01'):
        yesterday = ((datetime.datetime.today() - datetime.timedelta(days=1))
            .strftime('%Y-%m-%d'))
        if start_date_str > yesterday:
            raise Exception("The start date cannot be after yesterday")
        date_strs = [start_date_str, ]
        date_str = start_date_str
        while True:
            next_date_str = (datetime.datetime.strptime(date_str, '%Y-%m-%d')
                + datetime.timedelta(days=1)).strftime('%Y-%m-%d')
            date_strs.append(next_date_str)
            date_str = next_date_str
            if date_str == yesterday:
                break
        return date_strs

    @staticmethod
    def roam_hunter(date_strs):
        datetime_str = datetime.datetime.now().strftime('%Y-%m-%d_%H:%M:%S')
        for date_str in date_strs:
            fname = os.path.join(RoamHunter.ROOT, 'roamers_{}_for_{}.txt'
                .format(datetime_str, date_str))
            with open(fname, 'w') as f:
                roaming_imsis = RoamHunter.roaming_imsis(date_str)
                json.dump({'date': date_str, 'roaming_imsis': roaming_imsis}, f)
            print("Completed {}".format(date_str))

    @staticmethod
    def consolidate_roamers():
        """
        Store dict of imsi's with list of roaming date_strs for each imsi.
        """
        imsi_dets = defaultdict(list)
        fpaths = [os.path.join(RoamHunter.ROOT, fname)
            for fname in os.listdir(RoamHunter.ROOT)]
        for fpath in fpaths:
            with open(fpath, 'r') as f:
                try:
                    data = json.load(f)
                except json.decoder.JSONDecodeError:
                    print("Unable to decode '{}' - presumably empty?".format(
                        fpath))
                    continue
                for imsi in data['roaming_imsis']:
                    imsi_dets[imsi].append(data['date'])
        print(len(imsi_dets))
        with open(CONSOLIDATED_ROAMERS, 'w') as f:
            json.dump(imsi_dets, f)  #, cls=utils.Json.CustomEncoder)
        print("Finished consolidating roamers into '{}'"
            .format(CONSOLIDATED_ROAMERS))

    @staticmethod
    def _gap(date_a, date_b):
        diff = (date_b - date_a)
        return diff.days

    @staticmethod
    def dates2ranges(date_strs):
        dates = sorted([datetime.datetime.strptime(date_str, '%Y-%m-%d')
            for date_str in date_strs])
        ranges = []
        prev_date = None
        for n, date in enumerate(dates, 1):
            if not ranges:
                ranges.append({'start': date.strftime('%Y-%m-%d'), 'end': None})
            else:
                if date <= prev_date:
                    raise ValueError("prev_date '{}' is not before date"
                        " '{}'".format(prev_date, date))
                N_GAP = 5  ## consolidate if only a gap 4 days or less
                gap = RoamHunter._gap(prev_date, date)
                if gap in range(1, N_GAP + 1):
                    prev_date = date
                    continue
                elif gap > N_GAP:
                    ## close previous and, if not at end, open new
                    ranges[-1]['end'] = prev_date.strftime('%Y-%m-%d')
                    last_date = (n == len(dates))
                    if not last_date:
                        ranges.append({
                            'start': date.strftime('%Y-%m-%d'), 'end': None})
                else:
                    raise ValueError("Impossible gap value of {:,}".format(gap))
            prev_date = date
        ## close off anything left open
        if ranges[-1]['end'] is None:
            ranges[-1]['end'] = date.strftime('%Y-%m-%d')
        return ranges

    @staticmethod
    def expand_range(start_date_str, end_date_str, days=1):
        one_day = datetime.timedelta(days=days)
        prev_date = datetime.datetime.strptime(start_date_str, '%Y-%m-%d')
        post_date = datetime.datetime.strptime(end_date_str, '%Y-%m-%d')
        prev_date_str = (prev_date - one_day).strftime('%Y-%m-%d')
        post_date_str = (post_date + one_day).strftime('%Y-%m-%d')
        return prev_date_str, post_date_str

    @staticmethod
    def low_event_imsis(date_strs, imsi_dets_dict):
        debug = True
        verbose = False
        con_rem, cur_rem = db.Pg.get_rem()
        ## for each imsi create tuple of two dates so we can get max freqs for them later
        imsi_check_date_strs = {}
        for imsi, roam_date_strs in imsi_dets_dict.items():
            min_date_str = min(roam_date_strs)
            max_date_str = max(roam_date_strs)
            prev_date_str, post_date_str = RoamHunter.expand_range(min_date_str,
                max_date_str)
            imsi_check_date_strs[imsi] = (prev_date_str, post_date_str)
        for date_str in date_strs:
            if debug: print(date_str)
            imsis2filter = sorted([imsi
                for imsi, check_date_strs in imsi_check_date_strs.items()
                if date_str in check_date_strs])
            n_imsis = len(imsis2filter)
            if debug: print("{:,}".format(n_imsis))
            if not n_imsis:
                continue
            db.Pg.drop_tbl(con_rem, cur_rem, tbl=conf.TEMP_IMSIS)
            sql_make_tmp = """\
            CREATE TEMPORARY TABLE {temp_imsis} (
                imsi_rti bigint,
                PRIMARY KEY(imsi_rti)
            )
            """.format(temp_imsis=conf.TEMP_IMSIS)
            cur_rem.execute(sql_make_tmp)
            sql_insert = """\
            INSERT INTO {temp_imsis}
            (imsi_rti)
            VALUES
            """.format(temp_imsis=conf.TEMP_IMSIS)
            imsi_blocks2use = []
            i = 0
            while True:
                imsi_block2use = imsis2filter[i*1000: (i+1)*1000]
                if not imsi_block2use:
                    break
                imsi_blocks2use.append(imsi_block2use)
                i += 1
            for imsi_block2use in imsi_blocks2use:
                sql_insert_block = sql_insert + ",\n".join("({})".format(imsi)
                    for imsi in imsi_block2use)
                if debug and verbose: print(sql_insert_block)
                cur_rem.execute(sql_insert_block)
            sql_freq_tpl = """\
            SELECT
              imsi_rti,
                COUNT(*) AS
              freq
            FROM {events}
            INNER JOIN
            {temp_imsis}
            USING(imsi_rti)
            WHERE date = %s
            GROUP BY imsi_rti
            """.format(events=conf.CONNECTION_DATA, temp_imsis=conf.TEMP_IMSIS)
            cur_rem.execute(sql_freq_tpl, (date_str, ))
            if debug and verbose: print(str(cur_rem.query, encoding='utf-8'))
            data = cur_rem.fetchall()
            with open(IMSI_FREQS, 'a') as f:
                f.write("{}\n".format(json.dumps(data)))
        print("Finished making '{}'".format(IMSI_FREQS))

    @staticmethod
    def find_mis_locations(date_strs, imsi_dets_dict):
        """
        For each imsi we have dates we think roaming happened. 0.5M imsi's so
        querying each can be computationally expensive.
        
        Only include imsi's where there is likely to be enough event traffic to
        be a real imsi. 
        """
        ## filter out low-event imsi's based on a couple of days' data
        imsi_freqs = defaultdict(list)
        with open(IMSI_FREQS, 'r') as f:
            for line in f:
                data = json.loads(line)
                for imsi, freq in data:
                    imsi_freqs[imsi].append(freq)
        ## Note - if you made imsi_dict on a previous day from the other steps there may be imsis missing from it thus KeyError
        imsi_freqs = list(imsi_freqs.items()) #[:-2]
        imsi_dets = sorted([(imsi, imsi_dets_dict[str(imsi)])
            for imsi, freqs in imsi_freqs if max(freqs) > 10])
        susp_imsi_dets = []
        for imsi, roam_date_strs in imsi_dets:
            susp_imsi_dets.append(
                (imsi, RoamHunter.dates2ranges(roam_date_strs)))
        return susp_imsi_dets

    @staticmethod
    def context_report(imsi, start_date_str, end_date_str, fewer_fields=False,
            root=conf.CSM_ROOT):
        unused_con_rem, cur_rem = db.Pg.get_rem()
        if fewer_fields:
            fields = """\
            events.start_date_str,
            cell_id_rti,
            node_description,
            tower_lat,  -- position early so lat starts in same exact x-position for easy comparisons
            tower_lon
            """
            fields_str = '_few_fields'
        else:
            fields = """\
              events.imsi_rti AS
            imsi,
            events.start_date_str,
            tower_lat,  -- position early so lat starts in same exact x-position for easy comparisons
            tower_lon,
            num_events,
            duration,
            tech_class,
            cell_id_rti,
            node_description,
            locs.start_date,
            locs.end_date
            """
            fields_str = '_all_fields'
        sql = """\
        SELECT
          {fields}
        FROM {events} AS events
        LEFT JOIN
        {locs} AS locs
        USING(cell_id_rti)
        WHERE imsi_rti = %s
        AND date BETWEEN %s AND %s
        AND (locs.cell_id_rti IS NULL
          OR (locs.start_date <= date
            AND (locs.end_date IS NULL OR locs.end_date >= date)
          )
        )
        ORDER BY events.start_date_str, events.end_date_str
        """.format(fields=fields, events=conf.CONNECTION_DATA,
            locs=conf.CELL_LOCATION)
        cur_rem.execute(sql, (imsi, start_date_str, end_date_str))
        data = cur_rem.fetchall()
        report_name = '{}imsi_{}{}_{}_to_{}.txt'.format(root, imsi, fields_str,
            start_date_str, end_date_str)
        with open(report_name, 'w') as f:
            for row in data:
                f.write("\n{}".format(row))
        print("FINISHED! Made '{}'".format(report_name))

    @staticmethod
    def report_roamers(imsi_roam_date_ranges_fpath, do_all=False,
            min_imsi=None, root=ROAMING_EPISODES_ROOT):
        make_fresh_src = False
        make_fresh_consolidated_roamers = False
        make_fresh_low_event_imsis = False
        make_fresh_summary = False

        date_strs = RoamHunter.get_date_strs(start_date_str='2017-07-07')

        if make_fresh_src:
            RoamHunter.roam_hunter(date_strs)
        else:
            utils.warn("Not making fresh source")

        if make_fresh_consolidated_roamers:
            RoamHunter.consolidate_roamers()
        else:
            utils.warn("Not making fresh consolidated roamers")

        with open(CONSOLIDATED_ROAMERS, 'r') as f:
            imsi_dets_dict = json.load(f)  #, object_hook=utils.Json.decode_object)

        if make_fresh_low_event_imsis:
            RoamHunter.low_event_imsis(date_strs, imsi_dets_dict)
        else:
            utils.warn("Not making fresh low-event imsi's list")

        if make_fresh_summary:
            susp_imsi_dets = RoamHunter.find_mis_locations(date_strs,
                imsi_dets_dict)
            print("{:,}".format(len(susp_imsi_dets)))
            with open(imsi_roam_date_ranges_fpath, 'w') as f:
                json.dump(susp_imsi_dets, f)
        else:
            utils.warn("Not making fresh summary")

        with open(imsi_roam_date_ranges_fpath, 'r') as f:
            susp_imsi_dets = json.load(f)
            for imsi, roam_date_ranges in sorted(susp_imsi_dets):
                if min_imsi and imsi < min_imsi:
                    continue
                print("IMSI {}".format(imsi))
                for roam_date_range in roam_date_ranges:
                    start_date_str = roam_date_range['start']
                    end_date_str = roam_date_range['end']
                    print("Range is {} to {}".format(start_date_str,
                        end_date_str))
                    if do_all:
                        (report_start_date_str,
                         report_end_date_str) = RoamHunter.expand_range(
                            start_date_str, end_date_str, days=2)
                        RoamHunter.context_report(imsi, report_start_date_str,
                            report_end_date_str, fewer_fields=True, root=root)
                    else:
                        resp = input("Make report? y(es)/s(kip)/e(xit)").lower()
                        if resp in ('y', 'yes'):
                            (report_start_date_str,
                             report_end_date_str) = RoamHunter.expand_range(
                                start_date_str, end_date_str, days=2)
                            RoamHunter.context_report(imsi,
                                report_start_date_str, report_end_date_str,
                                fewer_fields=True, root=root)
                        elif resp in ('s', 'skip'):
                            print("Skipping {} for {} to {}".format(imsi,
                                start_date_str, end_date_str))
                            continue
                        elif resp in ('e', 'exit'):
                            print("Exiting ...")
                            return

    @staticmethod
    def count_roaming_episodes(imsi_roam_date_ranges_fpath):
        debug = False
        with open(imsi_roam_date_ranges_fpath, 'r') as f:
            susp_imsi_dets = json.load(f)
            n = 0
            for unused_imsi, roam_date_ranges in susp_imsi_dets:
                for unused_roam_date_range in roam_date_ranges:
                    n += 1
            if debug: print("{:,} roaming episodes".format(n))
        return n

    @staticmethod
    def _airport(lbl, rti):
        """
        Assumes no relocations for the rti's at the 3 international airports -
        Auckland, Wellington, and Christchurch.
        """
        if lbl is None:
            airport_lbl = False
        else:
            lbl = lbl.lower()
            airport_lbl = ('airport' in lbl or 'air nz' in lbl or 'terminal' in lbl)
        airport_rti = (rti in (AUCK_AIRPORT_RTIS + WGN_AIRPORT_RTIS
            + CHCH_AIRPORT_RTIS))
        return airport_lbl or airport_rti

    @staticmethod
    def _get_false_coords_from_roaming_episode(fpath):
        """
        Assumption - only ever start or end a block when 2 unlocatables (with
        valid cell_id_rti's) in a row. Use first of first pair for start and
        second of last pair for end. Occasionally fails but

        ## Sometimes unlocatables concentrate exclusively between international
        airport events

        ['2017-04-11 19:12:52', 40105, 'Christchurch Airport 2', -43.4814166435992, 172.551894149719]          AIRPORT - Departure
        ['2017-04-12 11:44:46', 28755, None, None, None]
        ...
        ['2017-04-18 15:37:38', 28752, None, None, None]
        ['2017-04-18 22:48:09', 43024, 'Christchurch Airport Carpark 1', -43.4889601873118, 172.540675249749]  AIRPORT - Returning

        ## Unlocatables don't necessarily happen where you expect them so can't
        start at first and end at last

        ['2017-03-19 21:25:48', 12930081, 'Auckland Int Airport 3', -37.0052929802122, 174.78475871054]        AIRPORT - Departure
        ['2017-03-20 01:12:27', 21944, None, None, None]  ROAMING
        ['2017-03-20 03:37:16', 21944, None, None, None]
        ['2017-03-20 03:37:16', 15850, None, None, None]
        ['2017-03-20 03:39:33', 32161, 'Ellerslie 1', -36.8990301774324, 174.806248951274]                     FAKE DOMESTIC
        ['2017-03-20 03:39:40', 146198531, None, None, None]
        ['2017-03-20 03:39:42', 32161, 'Ellerslie 1', -36.8990301774324, 174.806248951274]
        ['2017-03-20 03:40:36', 32164, 'Ellerslie 1', -36.8990301774324, 174.806248951274]
        ['2017-03-20 03:40:38', 146143233, None, None, None]
        ['2017-03-20 03:40:43', 32164, 'Ellerslie 1', -36.8990301774324, 174.806248951274]
        ['2017-03-20 03:43:33', 146198531, None, None, None]
        ...
        ['2017-03-22 22:46:07', 145945089, None, None, None]
        ['2017-03-22 22:48:15', 47706, 'Flaxmere relocation 3', -39.6275979466638, 176.786204708649]
        ['2017-03-22 23:04:29', 3754, None, None, None]
        ['2017-03-23 00:41:12', 12930081, 'Auckland Int Airport 3', -37.0052929802122, 174.78475871054]        AIRPORT - Returning
        ['2017-03-23 00:48:45', 12930079, 'Auckland Int Airport 1', -37.0052929802122, 174.78475871054]
        ...
        ['2017-03-23 02:50:46', 304505, 'Rataroa 2', -37.2383318104812, 175.300349722954]
        ['2017-03-23 02:50:47', 28732, 'Rataroa 2', -37.2383318104812, 175.300349722954]
        ['2017-03-23 02:50:50', 304505, 'Rataroa 2', -37.2383318104812, 175.300349722954]
        ['2017-03-23 02:52:26', 28732, 'Rataroa 2', -37.2383318104812, 175.300349722954]
        ['2017-03-23 02:53:38', 17192, None, None, None]                                                       MYSTERY UNLOCATABLE (one-sided international?) 
        ['2017-03-23 02:54:55', 28735, 'Rataroa 2', -37.2383318104812, 175.300349722954]
        ['2017-03-23 02:55:53', 291971, 'Kaihere 3', -37.3215137682836, 175.391918300807]
        ...
        ['2017-03-23 14:02:21', 146615808, None, None, None]                                                   MYSTERY UNLOCATABLE (one-sided international?) 
        ['2017-03-23 14:07:21', 358525, 'Opotiki Exchange 2', -38.0052609503098, 177.283947183626]
        ['2017-03-23 14:38:02', 8353, None, None, None]                                                        MYSTERY UNLOCATABLE (one-sided international?) 
        ['2017-03-23 14:42:21', 358525, 'Opotiki Exchange 2', -38.0052609503098, 177.283947183626]

        ## Don't always start or terminate immediately (or at all?) with an airport
        ...
        ['2016-05-10 15:33:17', 18971, None, None, None]
        ['2016-05-10 15:59:55', 15442, None, None, None]
        ['2016-05-11 00:30:45', 13930785, 'George Bellew Road | Yaldhurst | Christchurch 8042 | New Zealand', -43.502463, 172.536617]    AIRPORT - Returning
        ['2016-05-11 00:30:54', 12807456, 'PABX room situated in No 1 Hanger | Christchurch Airport', -43.481417, 172.551894]
        """
        debug = True
        verbose = False
        data = []
        EVIL_RTIS = (0, 65535)
        with open(fpath, 'r') as f:
            for i, line in enumerate(f, -1):
                if line not in ('', '\n'):
                    row = eval(line)
                    dt_str, rti, lbl, lat, lon = row
                    if debug and verbose: print(row)
                    event_date = datetime.datetime.strptime(dt_str,
                        '%Y-%m-%d %H:%M:%S').strftime('%Y-%m-%d')
                    coord = (lat, lon)
                    if debug and verbose: print(i, coord)
                    data.append((i, event_date, coord, rti, lbl))
        if not data:
            false_coords = []
        else:
            unlocatable_pairs = []
            for i, row in enumerate(data):
                try:
                    i_pair = (i, i+1)
                    unused, unused, coord1, rti1, unused = data[i_pair[0]]
                    unused, unused, coord2, rti2, unused = data[i_pair[1]]
                except IndexError:
                    break
                else:
                    both_valid_unlocatables = (
                        coord1 == (None, None)
                        and coord2 == (None, None)
                        and rti1 not in EVIL_RTIS
                        and rti2 not in EVIL_RTIS)
                    if both_valid_unlocatables:
                        unlocatable_pairs.append(i_pair)
            if not unlocatable_pairs:
                false_coords = []
            else:
                min_i = unlocatable_pairs[0][0]  ## first of first
                max_i = unlocatable_pairs[-1][1]  ## second of last
                false_coords_dates = set()
                prev_coord = None
                prev_lbl = None
                prev_rti = None
                for i, event_date, coord, rti, lbl in data:
                    if not (min_i <= i <= max_i):
                        prev_coord = coord
                        prev_lbl = lbl
                        prev_rti = rti
                        continue  ## skip all rows outside of roaming episode
                    prev_airport = RoamHunter._airport(prev_lbl, prev_rti)
                    apparent_start = prev_airport and coord == (None, None)  ## airport then unlocatable (roaming)
                    if apparent_start:
                        false_coords_dates = set()  ## start again because this is the real start - seems to have had two unlocatables too early
                    airport = RoamHunter._airport(lbl, rti)
                    apparent_end = (prev_coord == (None, None) and airport)  ## unlocatable (roaming) then airport
                    if apparent_end:
                        break  ## because sometimes two unlocatables after they have returned and it massively overcounts
                    false_coord = (coord != (None, None)
                        and rti not in EVIL_RTIS)
                    if false_coord:
                        false_coords_dates.add((coord, event_date))  ## note -- want to record coord again for each day it appears as this indicates potential impact on day trip data
                    prev_coord = coord
                    prev_lbl = lbl
                    prev_rti = rti
                false_coords = [coord
                    for coord, event_date in list(false_coords_dates)]
                if debug and verbose: print(false_coords)
        if debug:
            n = len(false_coords)
            if n > 5:
                print(os.path.split(fpath)[1], n)
        return false_coords

    @staticmethod
    def display_false_coords_from_roaming_episodes(folder):
        fnames = os.listdir(folder)
        n_sample_episodes = len(fnames)
        n_episodes = RoamHunter.count_roaming_episodes(
            IMSI_ROAM_DATE_RANGES_FPATH)
        print("{:,} episodes".format(len(fnames)))
        false_coords_sample = []
        for n, fname in enumerate(fnames, 1):
            fpath = os.path.join(folder, fname)
            false_coords_sample.extend(
                RoamHunter._get_false_coords_from_roaming_episode(fpath))
            if n % 250 == 0:
                print(utils.prog(n, 'episodes', n_sample_episodes))
        tot_false_coords_sample = len(false_coords_sample)
        tot_false_coords = int(round(
            (tot_false_coords_sample*n_episodes)/n_sample_episodes, -5))
        coord_freqs = Counter(false_coords_sample)
        coord_data = list(coord_freqs.items())
        fn = partial(folium_map.Map.standard_marker_dets_fn,
            black_threshold=5, red_threshold=2, orange_threshold=1)
        folium_map.Map.map_coord_data(
            title=('Fake domestic visits (actually roaming overseas) since Jan '
                '2015'),
            subtitle=('Distribution based on {:,} international roaming '
                'episodes sampled from {:,}<br>Sample N: {:,}, Total N (est): '
                '{:,}').format(n_sample_episodes, n_episodes,
                tot_false_coords_sample, tot_false_coords),
            lbl_a='Data as at {}'.format(
                datetime.datetime.today().strftime('%Y-%m-%d')),
            coord_data_a=coord_data, marker_dets_fn=fn)

def main():
    test_get_location()
    return

    folder = os.path.join(conf.CSM_ROOT,
        'reports/mass_reports/roaming_episodes/')
    RoamHunter.display_false_coords_from_roaming_episodes(folder)
    return

    RoamHunter.report_roamers(IMSI_ROAM_DATE_RANGES_FPATH, do_all=True,
        min_imsi=7000000000000000000); return

#     RoamHunter.context_report(imsi=-9198155971781032451,
#         start_date_str='2017-01-17', end_date_str='2017-03-11',
#         fewer_fields=True, root=conf.CSM_ROOT)

if __name__ == '__main__':
    main()
