#! /usr/bin/env python3

import getpass
from itertools import combinations, cycle
from pprint import pprint as pp
from webbrowser import open_new_tab

import folium
import psycopg2 as pg
import psycopg2.extras as pg_extras
#from pytz import timezone  ## working with naive dates from greenplum

import not4git

"""
Dependencies - python3, folium, psycopg2, postgreSQL, PostGIS extension

NB CREATE EXTENSION postgis; for the new database

NOTE - rounding is used to avoid reporting on jitter but side-effect is that
towers are not exactly plotted on map - may be off depending on effect of
rounding to 3dp. 

Fields in source table csm:

cob_date date,
cell_id text,
cell_id_rti integer,
tower text,
band text,
freq integer,
sector integer,
tower_latitude double precision,
tower_longitude double precision,
sector_latitude double precision,
sector_longitude double precision,
mb_code text,
prop_cell_is_of_mb double precision,
prop_mb_is_of_cell double precision,
au_code text,
prop_cell_is_of_au double precision,
prop_au_is_of_cell double precision,
unified_longitude double precision,
unified_latitude double precision,
address text,
suburb text,
city text,
region text,
postcode text,
au_name text,
region_nz text,
rto text,
community_id integer,
community text,
persona_id integer,
persona text

Using newlines inside html for annotations breaks the whole page. Use <br>.
"""

DIST2KM_SCALAR = 100
ROOT_PATH = "/home/gps/Documents/tourism/csm/"
SITE2COORDS = {
    'tower': ('tower_latitude', 'tower_longitude'),
    'cell_id_rti': ('sector_latitude', 'sector_longitude'),
}

def plot_set_of_cell_ids():
    """
    A tower has a specific location. Each of its sectors points outwards in a
    different direction e.g. 3 directions 120 deg apart. A given sector on a
    given tower can have more than cell_id e.g. tower AABE can have cell_ids
    AABEL1, AABEU1_1, and AABEW1_4. They all lie along the same radial line
    starting from the tower. Overlaps can occurs e.g. AABEL1 and  AABEU1_1 can
    be at the same location.

    The following cell_ids were read from:

    SELECT DISTINCT tower,
    sector,
    tower_latitude,
    tower_longitude,
    cell_id,
    sector_latitude,
    sector_longitude
    FROM csm
    WHERE tower = 'AABE' AND cob_date = '20160630'
    ORDER BY sector, cell_id;
    """
    map_osm = folium.Map(location=[-36.74358, 174.693391], zoom_start=16)
    cells_dets = [
        ('TOWER', -36.74358, 174.693391, 'black', 0),
        ("AABEL1", -36.7423863953775, 174.69820411322, 'red', 0),
        ("AABEU1_1", -36.7423863953775, 174.69820411322, 'green', 10),
        ("AABEW1_4", -36.7414308333171, 174.701455307164, 'blue', 20),
        ("AABEL2", -36.7499266214583, 174.685779443893, 'red', 0),
        ("AABEU2_1", -36.7499266214583, 174.685779443893, 'green', 10),
        ("AABEW2_4", -36.7496248901234, 174.686525913504, 'blue', 20),
        ("AABEL3", -36.7369493459899, 174.685539353715, 'red', 0),
        ("AABEU3_1", -36.7369493459899, 174.685539353715, 'green', 10),
        ("AABEW3_4", -36.7375997964099, 174.686282191916, 'blue', 20),
    ]
    for cell_id, lat, lon, colour, y_offset in cells_dets:
        html_pointer = ('<div style="font-size: 20pt; '
        'color: {}">x</div>'.format(colour))
        marker_icon_pointer = folium.features.DivIcon(
            icon_size=(450, 50),
            icon_anchor=(0, 0),  ## x, y from top left corner.
            html=html_pointer, )
        folium.map.Marker(  ## https://github.com/python-visualization/folium/issues/340#issuecomment-179673692
            (lat, lon),
            icon=marker_icon_pointer
        ).add_to(map_osm)
        html_text = ('<div style="font-size: 10pt; '
        'color: {}">{}</div>'.format(colour, cell_id))
        marker_icon = folium.features.DivIcon(
            icon_size=(450, 50),
            icon_anchor=(0, y_offset),  ## x, y from top left corner.
            html=html_text, )
        folium.map.Marker(  ## https://github.com/python-visualization/folium/issues/340#issuecomment-179673692
            (lat, lon),
            icon=marker_icon
        ).add_to(map_osm)
    fpath = "/home/gps/Documents/tourism/csm/tower_mapped.html"
    url = "file://{}".format(fpath)
    map_osm.save(fpath)
    open_new_tab(url)

def _big_date_gap(date1, date2):
    is_big = (date1 - date2).days > 30
    return is_big

def _get_max_km(cur, site_coords_data):
    """
    Assumption is that there will be a manageable number of combinations so not
    too expensive to get a max value.

    http://gis.stackexchange.com/questions/131363/choosing-srid-and-what-is-its-meaning
    a.k.a why the mysterious 4326 appears below
    """
    debug = False
    pg_coords = [(lon, lat) for lat, lon in site_coords_data.keys()]  ## yes - reversed
    comparisons = combinations(pg_coords, 2)
    comparisons_clause_lst = []
    for comparison in comparisons:
        pg_coord1, pg_coord2 = comparison
        comparisons_clause_lst.append("""
          SELECT
          ST_DISTANCE(
            ST_GeomFromText('POINT({} {})', 4326),
            ST_GeomFromText('POINT({} {})', 4326)
          ) AS dist""".format(*pg_coord1+pg_coord2))  ## lon then lat for POINT
    comparisons_clause = "\nUNION ALL\n".join(comparisons_clause_lst)
    sql = """\
    SELECT max(dist)
    FROM (
    {comparisons_clause}) AS comparisons
    """.format(comparisons_clause=comparisons_clause)
    if debug and len(comparisons_clause_lst) > 1:
        print(sql)
    cur.execute(sql)
    max_km = round(cur.fetchone()[0]*DIST2KM_SCALAR, 2)
    return max_km

def get_colours():
    colours = ['red', 'blue', 'green', 'brown', 'black', 'grey', 'pink']
    for colour in cycle(colours):
        yield colour

def _plot_circle(map_osm, location, radius, colour, popup=None):
    kwargs = {'location': location, 'radius': radius, 'color': colour}
    if popup:
        kwargs['popup'] = popup
    folium.CircleMarker(**kwargs).add_to(map_osm)

def _add_annotation(map_osm, location, text):
    html_text = '<div style="font-size: 9pt;">{}</div>'.format(text)
    marker_icon = folium.features.DivIcon(
        icon_size=(450, 50),
        icon_anchor=(0, 20),  ## x, y from top left corner.
        html=html_text, )
    folium.map.Marker(  ## https://github.com/python-visualization/folium/issues/340#issuecomment-179673692
        location,
        icon=marker_icon
    ).add_to(map_osm)

def plot_sites(cur, site_field='tower', min_shift_kms=4, include_all=True):
    """
    See if towers/sectors have shifted location. Need to round coords to 4dp
    because can be different even within a given cob_date. E.g.
    -36.74358, 174.693391 vs
    -36.7436 , 174.6934
    Sometimes it even needs to be 3dp.

    Note - tower names can change more than once e.g.:
    "SRCH";"Edgeware"
    "SRCH";"Shirley East"
    "SRCH";"Shirley West"

    "SRDA";"Harewood"
    "SRDA";"Russley"

    "SRED";"Belfast South"
    "SRED";"Redwood North"
    "SRED";"Styx Mill"

    "SRFT";"Reefton"

    "SRHT";"Marlborough Sounds Terrestrial"

    "SRIC";"Deans Bush"
    "SRIC";"Riccarton"
    "SRIC";"Riccarton West"

    "SRIV";"Centre Island"
    "SRIV";"Riverton East"
    """
    site_lat, site_lon = SITE2COORDS[site_field]
    debug = False
    sql = """\
    SELECT DISTINCT ON ({site_field}, cob_date)
      tower AS
    tower_name,
      {site_field} AS
    site,
    cob_date,
      round({site_lat}::numeric, 3) AS
    site_lat,
      round({site_lon}::numeric, 3) AS
    site_lon,
    au_name
    FROM csm
    WHERE {site_lat} IS NOT NULL
    AND {site_lon} IS NOT NULL
    ORDER BY {site_field}, cob_date, cell_id  /* cell_id provides order for DISTINCT ON when towers*/
    """.format(site_field=site_field, site_lat=site_lat, site_lon=site_lon)
    cur.execute(sql)
    sites_data = {}
    n = 0
    while True:
        row = cur.fetchone()
        if not row:
            break
        n += 1
        if debug: print(row)
        date = row['cob_date']
        site = row['site']
        coord = (row['site_lat'], row['site_lon'])
        if site not in sites_data:
            sites_data[site] = {
                'site_name': str(site),
                'coords': {coord: [[date, ],]},
                'max_km': 0,
                'first_lbl': row['au_name']}
        else:
            if coord not in sites_data[site]['coords']:
                sites_data[site]['coords'][coord] = [[date, ]]
            else:
                last_date_range = sites_data[site]['coords'][coord][-1]
                len_range = len(last_date_range)
                if len_range < 1:
                    raise Exception("No dates in date range")
                elif len_range == 1:
                    last_date_range.append(date)
                elif len_range == 2:
                    new_needed = (_big_date_gap(last_date_range[1], date))
                    if new_needed:
                        sites_data[site]['coords'][coord].append([date, ])
                    else:  ## update end of range
                        last_date_range[1] = date
                else:
                    raise Exception("Should be one or two dates in range - "
                        "not {}".format(len_range))
    #pp(sites_data)
    colours_gen = get_colours()
    odd_site_dets = []
    ## make detailed map
    map_osm_all = folium.Map(location=[-41.290658, 174.459096], zoom_start=7)
    for site_data in sites_data.values():
        odd_site = False
        site_name = site_data['site_name']
        coords = site_data['coords']
        if len(coords) > 1:
            max_km = _get_max_km(cur, site_data['coords'])
            site_data['max_km'] = max_km
            lbl = site_data['first_lbl']
            if max_km > min_shift_kms:
                odd_site = True
                radius = 200
                colour = next(colours_gen)
                for coord in coords.keys():
                    odd_site_dets.append((max_km, site_name, coord, colour,
                        lbl, coords))
                    if include_all:
                        popup = site_name
                        _plot_circle(map_osm_all, coord, radius, colour, popup)
                        text = "{}<br>{:,}km".format(site_name, max_km)
                        _add_annotation(map_osm_all, coord, text)
        if not odd_site:
            coord = list(coords.keys())[0]
            radius = 100
            colour = 'red'
            popup = site_name
            _plot_circle(map_osm_all, coord, radius, colour, popup)
    fpath_all = ROOT_PATH + "all_sites_mapped.html"
    map_osm_all.save(fpath_all)
    url_all = "file://{}".format(fpath_all)
    ## make odd map
    map_osm_odd = folium.Map(location=[-41.290658, 174.459096],
        tiles='Mapbox Bright', zoom_start=7)
    idx_max_kms = 0
    odd_site_dets.sort(key=lambda s: s[idx_max_kms], reverse=True)
    for (max_km, site_name, coord,
            colour, lbl, coords) in odd_site_dets:
        coord_dets = []
        date_ranges = coords[coord]
        for date_range in date_ranges:
            n_date_dets = len(date_range)
            if n_date_dets == 1:
                date_dets = "From {}".format(date_range[0])
            elif n_date_dets == 2:
                date_dets = "From {} to {}".format(*date_range)
            else:
                raise Exception("Unexpected number of datedets {:,}".format())
            coord_dets.append(date_dets)
        popup = site_name + " " + "<br>".join(coord_dets)
        _plot_circle(map_osm_odd, coord, radius, colour, popup)
        text = "{}<br>{:,}km".format(site_name, max_km)
        _add_annotation(map_osm_odd, coord, text)
    fpath_odd = ROOT_PATH + "odd_sites_mapped.html"
    map_osm_odd.save(fpath_odd)
    url_odd = "file://{}".format(fpath_odd)
    ## worst cases
    oddities = sorted(
        list(
            {(max_km, site_name)
             for max_km, site_name, unused, unused, unused, unused
             in odd_site_dets}
        ), key=lambda s: s[0], reverse=True)
    for max_km, site_name in oddities:
        print("{}km site {}".format(max_km, site_name))
    ## assemble
    if include_all:
        fpath_main = ROOT_PATH + 'sites_mapped.html'
        main_html = """
        "<iframe src='{all}' width=650px height=1100px></iframe>
        "<iframe src='{odd}' width=650px height=1100px></iframe>
        """.format(all=url_all, odd=url_odd)
        with open(fpath_main, 'w') as f:
            f.write(main_html)
        url_main = "file://{}".format(fpath_main)
    else:
        url_main = url_odd
    open_new_tab(url_main)

def main():
    pwd = not4git.access_local  #getpass.getpass('Enter postgres password: ')
    con = pg.connect("dbname='tourism' "
        "user='postgres' "
        "host='localhost' "
        "port=5433 "
        "password='{}'".format(pwd))
    cur = con.cursor(cursor_factory=pg_extras.DictCursor)
    cur2 = con.cursor(cursor_factory=pg_extras.DictCursor)  ## if I need to do something inside a loop of fetchmany() etc

    """
    tower_coords_data = {  ## tortuous route so travel distance (7.6km) longer than actual distance of 6.01km
        (-36.885906, 174.722768): 1,  ## Mt Albert
        (-36.844342, 174.766272): 1,  ## CBD
    }
    print(_get_max_km(cur, tower_coords_data))

    tower_coords_data = {  ## straight route so travel distance (13.8km) similar to actual distance of 12.65km
        (-36.68742, 174.7024): 1,  ## Redvale
        (-36.799471, 174.761183): 1,  ## Northcote Point
    }
    print(_get_max_km(cur, tower_coords_data))
    """

    #plot_set_of_cell_ids()
    #plot_sites(cur, site_field='tower', min_shift_kms=2, include_all=True)
    plot_sites(cur, site_field='cell_id_rti', min_shift_kms=30,
        include_all=False)

main()
