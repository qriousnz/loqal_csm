#! /usr/bin/env python3

import csv
from datetime import datetime
import os
import pickle
from pprint import pprint as pp

import matplotlib.pyplot as plt

from voyager_shared import (conf, connections, dates, db,
    find_relocations as findrel, location_sources as locsrcs, mpl,
    report_relocations as reprel, rti_mod, utils)

"""
* DONE Differing relocation checks ****

- Look at differences. Any relocations missed by Mod_P? Any extras in legacy?
And, if so, are they phantoms - i.e. unlikely to have happened given patterns in
connection data?

Results - Everything in Redrock data found in Mod_P unless a phantom relocation
i.e. not supported by connection data. And Mod_P had heaps more going back in
time.

 e.g. 320881 Te Puru --> Tauranga transition. Even though in
Redrock data not in mod_p and no support for relocation in connection data. A
phantom. 382063 was another phantom.


* DONE Help with time-linked fix validation for Westport ****

- Westport cell_id_rti's - any explanation for modest step-change downwards
surviving from about Oct 1 2015?
53191, 53192, 53193, 53194, 53195, 53196, 53197, 53198, 53199 on Mt Rochfort (to
east) started potentially cannibalising traffic from the cell_id_rti's actually
in Westport on 2015-10-01 when they came online.


* Differing cell_id_rti's ****

- Compare cell_id_rti's against all legacy data - anything missing from Mod_P?
And, if so, normal connection data against them?


* Connection checks ****

- Check that changes of location in mod_p are reflected in connection data.
- Check that recorded dates are close enough to reality.


* SKIP? Location checks ****

- For cell_id_rti's where we have data in both, is the latest data showing the
same location (< 1km different)? Can skip if Daniel checking against Google data.
"""

raw_lat_lon_swaps = """\
On row 8,500 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
On row 8,500 found swapped coordinates so successfully corrected them
On row 15,580 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
On row 15,580 found swapped coordinates so successfully corrected them
On row 20,377 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
On row 20,377 found swapped coordinates so successfully corrected them
On row 21,733 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
On row 21,733 found swapped coordinates so successfully corrected them
On row 27,541 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
On row 27,541 found swapped coordinates so successfully corrected them
On row 31,086 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
On row 31,086 found swapped coordinates so successfully corrected them
On row 33,531 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
On row 33,531 found swapped coordinates so successfully corrected them
On row 36,479 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
On row 36,479 found swapped coordinates so successfully corrected them
On row 38,654 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
On row 38,654 found swapped coordinates so successfully corrected them
On row 40,751 -51.4926371877991, -145.447506066183 not in credible NZ bounding box (Original easting: 5980910; northing: 2522359)
On row 40,751 found swapped coordinates so successfully corrected them
On row 42,354 -14.5661367723493, -143.880814346576 not in credible NZ bounding box (Original easting: 5832135; northing: 2344920)
On row 42,354 found swapped coordinates so successfully corrected them
On row 46,460 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
On row 46,460 found swapped coordinates so successfully corrected them
"""

renamed_cell_id_rti_dets = [
    {'alt_mod_p_cell_id_rti': 290869,
     'csm_cell_id': 53,
     'csm_cell_id_rti': 151227189,
     'csm_tower': 'MHNX',
     'mod_p_cell_id': 53,
     'mod_p_tower': 'MHNX'},
    {'alt_mod_p_cell_id_rti': 290868,
     'csm_cell_id': 52,
     'csm_cell_id_rti': 151227188,
     'csm_tower': 'MHNX',
     'mod_p_cell_id': 52,
     'mod_p_tower': 'MHNX'},
    {'alt_mod_p_cell_id_rti': 290867,
     'csm_cell_id': 51,
     'csm_cell_id_rti': 151227187,
     'csm_tower': 'MHNX',
     'mod_p_cell_id': 51,
     'mod_p_tower': 'MHNX'},
    {'alt_mod_p_cell_id_rti': 290829,
     'csm_cell_id': 13,
     'csm_cell_id_rti': 151227149,
     'csm_tower': 'MHNX',
     'mod_p_cell_id': 13,
     'mod_p_tower': 'MHNX'},
    {'alt_mod_p_cell_id_rti': 290828,
     'csm_cell_id': 12,
     'csm_cell_id_rti': 151227148,
     'csm_tower': 'MHNX',
     'mod_p_cell_id': 12,
     'mod_p_tower': 'MHNX'},
    {'alt_mod_p_cell_id_rti': 290827,
     'csm_cell_id': 11,
     'csm_cell_id_rti': 151227147,
     'csm_tower': 'MHNX',
     'mod_p_cell_id': 11,
     'mod_p_tower': 'MHNX'},
    {'alt_mod_p_cell_id_rti': 290869,
     'csm_cell_id': 53,
     'csm_cell_id_rti': 23227189,
     'csm_tower': 'MHNX',
     'mod_p_cell_id': 53,
     'mod_p_tower': 'MHNX'},
    {'alt_mod_p_cell_id_rti': 290868,
     'csm_cell_id': 52,
     'csm_cell_id_rti': 23227188,
     'csm_tower': 'MHNX',
     'mod_p_cell_id': 52,
     'mod_p_tower': 'MHNX'},
    {'alt_mod_p_cell_id_rti': 290867,
     'csm_cell_id': 51,
     'csm_cell_id_rti': 23227187,
     'csm_tower': 'MHNX',
     'mod_p_cell_id': 51,
     'mod_p_tower': 'MHNX'},
    {'alt_mod_p_cell_id_rti': 290829,
     'csm_cell_id': 13,
     'csm_cell_id_rti': 23227149,
     'csm_tower': 'MHNX',
     'mod_p_cell_id': 13,
     'mod_p_tower': 'MHNX'},
    {'alt_mod_p_cell_id_rti': 290828,
     'csm_cell_id': 12,
     'csm_cell_id_rti': 23227148,
     'csm_tower': 'MHNX',
     'mod_p_cell_id': 12,
     'mod_p_tower': 'MHNX'},
    {'alt_mod_p_cell_id_rti': 290827,
     'csm_cell_id': 11,
     'csm_cell_id_rti': 23227147,
     'csm_tower': 'MHNX',
     'mod_p_cell_id': 11,
     'mod_p_tower': 'MHNX'},
    {'alt_mod_p_cell_id_rti': 443271,
     'csm_cell_id': 135,
     'csm_cell_id_rti': 15168135,
     'csm_tower': 'CMSV',
     'mod_p_cell_id': 135,
     'mod_p_tower': 'CMSV'},
    {'alt_mod_p_cell_id_rti': 443261,
     'csm_cell_id': 125,
     'csm_cell_id_rti': 15168125,
     'csm_tower': 'CMSV',
     'mod_p_cell_id': 125,
     'mod_p_tower': 'CMSV'},
    {'alt_mod_p_cell_id_rti': 443251,
     'csm_cell_id': 115,
     'csm_cell_id_rti': 15168115,
     'csm_tower': 'CMSV',
     'mod_p_cell_id': 115,
     'mod_p_tower': 'CMSV'},
    {'alt_mod_p_cell_id_rti': 428675,
     'csm_cell_id': 131,
     'csm_cell_id_rti': 15143811,
     'csm_tower': 'SQTC',
     'mod_p_cell_id': 131,
     'mod_p_tower': 'SQTC'},
    {'alt_mod_p_cell_id_rti': 428665,
     'csm_cell_id': 121,
     'csm_cell_id_rti': 15143801,
     'csm_tower': 'SQTC',
     'mod_p_cell_id': 121,
     'mod_p_tower': 'SQTC'},
    {'alt_mod_p_cell_id_rti': 428655,
     'csm_cell_id': 111,
     'csm_cell_id_rti': 15143791,
     'csm_tower': 'SQTC',
     'mod_p_cell_id': 111,
     'mod_p_tower': 'SQTC'},
    {'alt_mod_p_cell_id_rti': 376711,
     'csm_cell_id': 135,
     'csm_cell_id_rti': 15133831,
     'csm_tower': 'SASH',
     'mod_p_cell_id': 135,
     'mod_p_tower': 'SASH'},
    {'alt_mod_p_cell_id_rti': 376701,
     'csm_cell_id': 125,
     'csm_cell_id_rti': 15133821,
     'csm_tower': 'SASH',
     'mod_p_cell_id': 125,
     'mod_p_tower': 'SASH'},
    {'alt_mod_p_cell_id_rti': 376691,
     'csm_cell_id': 115,
     'csm_cell_id_rti': 15133811,
     'csm_tower': 'SASH',
     'mod_p_cell_id': 115,
     'mod_p_tower': 'SASH'},
    {'alt_mod_p_cell_id_rti': 258593,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 15130401,
     'csm_tower': 'WHTT',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'WHTT'},
    {'alt_mod_p_cell_id_rti': 258592,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 15130400,
     'csm_tower': 'WHTT',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'WHTT'},
    {'alt_mod_p_cell_id_rti': 258591,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 15130399,
     'csm_tower': 'WHTT',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'WHTT'},
    {'alt_mod_p_cell_id_rti': 440865,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 15128609,
     'csm_tower': 'MROX',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'MROX'},
    {'alt_mod_p_cell_id_rti': 440864,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 15128608,
     'csm_tower': 'MROX',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'MROX'},
    {'alt_mod_p_cell_id_rti': 440863,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 15128607,
     'csm_tower': 'MROX',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'MROX'},
    {'alt_mod_p_cell_id_rti': 272673,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 15114529,
     'csm_tower': 'ALWC',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'ALWC'},
    {'alt_mod_p_cell_id_rti': 272672,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 15114528,
     'csm_tower': 'ALWC',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'ALWC'},
    {'alt_mod_p_cell_id_rti': 272671,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 15114527,
     'csm_tower': 'ALWC',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'ALWC'},
    {'alt_mod_p_cell_id_rti': 471585,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 15105313,
     'csm_tower': 'AABX',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'AABX'},
    {'alt_mod_p_cell_id_rti': 471584,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 15105312,
     'csm_tower': 'AABX',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'AABX'},
    {'alt_mod_p_cell_id_rti': 471583,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 15105311,
     'csm_tower': 'AABX',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'AABX'},
    {'alt_mod_p_cell_id_rti': 262433,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 13896737,
     'csm_tower': 'SOMU',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'SOMU'},
    {'alt_mod_p_cell_id_rti': 262432,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13896736,
     'csm_tower': 'SOMU',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'SOMU'},
    {'alt_mod_p_cell_id_rti': 262431,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13896735,
     'csm_tower': 'SOMU',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'SOMU'},
    {'alt_mod_p_cell_id_rti': 261665,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 13368097,
     'csm_tower': 'AMWD',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'AMWD'},
    {'alt_mod_p_cell_id_rti': 261664,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13368096,
     'csm_tower': 'AMWD',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'AMWD'},
    {'alt_mod_p_cell_id_rti': 261663,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13368095,
     'csm_tower': 'AMWD',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'AMWD'},
    {'alt_mod_p_cell_id_rti': 259361,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 13349921,
     'csm_tower': 'WNGS',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'WNGS'},
    {'alt_mod_p_cell_id_rti': 259360,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13349920,
     'csm_tower': 'WNGS',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'WNGS'},
    {'alt_mod_p_cell_id_rti': 259359,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13349919,
     'csm_tower': 'WNGS',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'WNGS'},
    {'alt_mod_p_cell_id_rti': 263969,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 13306145,
     'csm_tower': 'AGLM',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'AGLM'},
    {'alt_mod_p_cell_id_rti': 263968,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13306144,
     'csm_tower': 'AGLM',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'AGLM'},
    {'alt_mod_p_cell_id_rti': 263967,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13306143,
     'csm_tower': 'AGLM',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'AGLM'},
    {'alt_mod_p_cell_id_rti': 277537,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 13254689,
     'csm_tower': 'AWHR',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'AWHR'},
    {'alt_mod_p_cell_id_rti': 277536,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13254688,
     'csm_tower': 'AWHR',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'AWHR'},
    {'alt_mod_p_cell_id_rti': 277535,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13254687,
     'csm_tower': 'AWHR',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'AWHR'},
    {'alt_mod_p_cell_id_rti': 257313,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 13217569,
     'csm_tower': 'WMTC',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'WMTC'},
    {'alt_mod_p_cell_id_rti': 257312,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13217568,
     'csm_tower': 'WMTC',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'WMTC'},
    {'alt_mod_p_cell_id_rti': 257311,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13217567,
     'csm_tower': 'WMTC',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'WMTC'},
    {'alt_mod_p_cell_id_rti': 363808,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13209120,
     'csm_tower': 'ALBC',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'ALBC'},
    {'alt_mod_p_cell_id_rti': 363807,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13209119,
     'csm_tower': 'ALBC',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'ALBC'},
    {'alt_mod_p_cell_id_rti': 471329,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 13152033,
     'csm_tower': 'ASLK',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'ASLK'},
    {'alt_mod_p_cell_id_rti': 471328,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13152032,
     'csm_tower': 'ASLK',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'ASLK'},
    {'alt_mod_p_cell_id_rti': 471327,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13152031,
     'csm_tower': 'ASLK',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'ASLK'},
    {'alt_mod_p_cell_id_rti': 266528,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13147936,
     'csm_tower': 'ATAM',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'ATAM'},
    {'alt_mod_p_cell_id_rti': 266527,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13147935,
     'csm_tower': 'ATAM',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'ATAM'},
    {'alt_mod_p_cell_id_rti': 322849,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 13147681,
     'csm_tower': 'AETX',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'AETX'},
    {'alt_mod_p_cell_id_rti': 322848,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13147680,
     'csm_tower': 'AETX',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'AETX'},
    {'alt_mod_p_cell_id_rti': 322847,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13147679,
     'csm_tower': 'AETX',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'AETX'},
    {'alt_mod_p_cell_id_rti': 275488,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13120288,
     'csm_tower': 'AUNI',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'AUNI'},
    {'alt_mod_p_cell_id_rti': 275487,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13120287,
     'csm_tower': 'AUNI',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'AUNI'},
    {'alt_mod_p_cell_id_rti': 261152,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13103648,
     'csm_tower': 'ATRN',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'ATRN'},
    {'alt_mod_p_cell_id_rti': 261151,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13103647,
     'csm_tower': 'ATRN',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'ATRN'},
    {'alt_mod_p_cell_id_rti': 259105,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 13103393,
     'csm_tower': 'ATAS',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'ATAS'},
    {'alt_mod_p_cell_id_rti': 259104,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13103392,
     'csm_tower': 'ATAS',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'ATAS'},
    {'alt_mod_p_cell_id_rti': 259103,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13103391,
     'csm_tower': 'ATAS',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'ATAS'},
    {'alt_mod_p_cell_id_rti': 268321,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 13087521,
     'csm_tower': 'ATID',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'ATID'},
    {'alt_mod_p_cell_id_rti': 268320,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13087520,
     'csm_tower': 'ATID',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'ATID'},
    {'alt_mod_p_cell_id_rti': 268319,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13087519,
     'csm_tower': 'ATID',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'ATID'},
    {'alt_mod_p_cell_id_rti': 268065,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 13071905,
     'csm_tower': 'AWIR',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'AWIR'},
    {'alt_mod_p_cell_id_rti': 268064,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13071904,
     'csm_tower': 'AWIR',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'AWIR'},
    {'alt_mod_p_cell_id_rti': 268063,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13071903,
     'csm_tower': 'AWIR',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'AWIR'},
    {'alt_mod_p_cell_id_rti': 263457,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 13071393,
     'csm_tower': 'AMRI',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'AMRI'},
    {'alt_mod_p_cell_id_rti': 263456,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13071392,
     'csm_tower': 'AMRI',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'AMRI'},
    {'alt_mod_p_cell_id_rti': 263455,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13071391,
     'csm_tower': 'AMRI',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'AMRI'},
    {'alt_mod_p_cell_id_rti': 261921,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 13064737,
     'csm_tower': 'AVIX',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'AVIX'},
    {'alt_mod_p_cell_id_rti': 261920,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13064736,
     'csm_tower': 'AVIX',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'AVIX'},
    {'alt_mod_p_cell_id_rti': 261919,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13064735,
     'csm_tower': 'AVIX',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'AVIX'},
    {'alt_mod_p_cell_id_rti': 258335,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13051679,
     'csm_tower': 'AMBY',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'AMBY'},
    {'alt_mod_p_cell_id_rti': 264993,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 13044769,
     'csm_tower': 'APBC',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'APBC'},
    {'alt_mod_p_cell_id_rti': 264992,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13044768,
     'csm_tower': 'APBC',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'APBC'},
    {'alt_mod_p_cell_id_rti': 264991,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13044767,
     'csm_tower': 'APBC',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'APBC'},
    {'alt_mod_p_cell_id_rti': 301345,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 13025057,
     'csm_tower': 'MRAP',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'MRAP'},
    {'alt_mod_p_cell_id_rti': 301344,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 13025056,
     'csm_tower': 'MRAP',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'MRAP'},
    {'alt_mod_p_cell_id_rti': 301343,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 13025055,
     'csm_tower': 'MRAP',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'MRAP'},
    {'alt_mod_p_cell_id_rti': 260897,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 12975137,
     'csm_tower': 'APNR',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'APNR'},
    {'alt_mod_p_cell_id_rti': 260896,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 12975136,
     'csm_tower': 'APNR',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'APNR'},
    {'alt_mod_p_cell_id_rti': 260895,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 12975135,
     'csm_tower': 'APNR',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'APNR'},
    {'alt_mod_p_cell_id_rti': 482593,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 12948769,
     'csm_tower': 'AHDN',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'AHDN'},
    {'alt_mod_p_cell_id_rti': 482592,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 12948768,
     'csm_tower': 'AHDN',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'AHDN'},
    {'alt_mod_p_cell_id_rti': 482591,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 12948767,
     'csm_tower': 'AHDN',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'AHDN'},
    {'alt_mod_p_cell_id_rti': 261409,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 12936225,
     'csm_tower': 'AHRR',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'AHRR'},
    {'alt_mod_p_cell_id_rti': 261408,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 12936224,
     'csm_tower': 'AHRR',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'AHRR'},
    {'alt_mod_p_cell_id_rti': 261407,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 12936223,
     'csm_tower': 'AHRR',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'AHRR'},
    {'alt_mod_p_cell_id_rti': 263713,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 12929569,
     'csm_tower': 'AELR',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'AELR'},
    {'alt_mod_p_cell_id_rti': 263712,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 12929568,
     'csm_tower': 'AELR',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'AELR'},
    {'alt_mod_p_cell_id_rti': 263711,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 12929567,
     'csm_tower': 'AELR',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'AELR'},
    {'alt_mod_p_cell_id_rti': 436749,
     'csm_cell_id': 13,
     'csm_cell_id_rti': 12922125,
     'csm_tower': 'MTAH',
     'mod_p_cell_id': 13,
     'mod_p_tower': 'MTAH'},
    {'alt_mod_p_cell_id_rti': 436748,
     'csm_cell_id': 12,
     'csm_cell_id_rti': 12922124,
     'csm_tower': 'MTAH',
     'mod_p_cell_id': 12,
     'mod_p_tower': 'MTAH'},
    {'alt_mod_p_cell_id_rti': 436747,
     'csm_cell_id': 11,
     'csm_cell_id_rti': 12922123,
     'csm_tower': 'MTAH',
     'mod_p_cell_id': 11,
     'mod_p_tower': 'MTAH'},
    {'alt_mod_p_cell_id_rti': 268577,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 12882721,
     'csm_tower': 'WALI',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'WALI'},
    {'alt_mod_p_cell_id_rti': 268576,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 12882720,
     'csm_tower': 'WALI',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'WALI'},
    {'alt_mod_p_cell_id_rti': 268575,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 12882719,
     'csm_tower': 'WALI',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'WALI'},
    {'alt_mod_p_cell_id_rti': 433697,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 12871713,
     'csm_tower': 'ABLC',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'ABLC'},
    {'alt_mod_p_cell_id_rti': 433696,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 12871712,
     'csm_tower': 'ABLC',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'ABLC'},
    {'alt_mod_p_cell_id_rti': 433695,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 12871711,
     'csm_tower': 'ABLC',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'ABLC'},
    {'alt_mod_p_cell_id_rti': 396832,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 12828192,
     'csm_tower': 'ANSH',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'ANSH'},
    {'alt_mod_p_cell_id_rti': 396831,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 12828191,
     'csm_tower': 'ANSH',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'ANSH'},
    {'alt_mod_p_cell_id_rti': 485409,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 12824097,
     'csm_tower': 'APWT',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'APWT'},
    {'alt_mod_p_cell_id_rti': 485408,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 12824096,
     'csm_tower': 'APWT',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'APWT'},
    {'alt_mod_p_cell_id_rti': 485407,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 12824095,
     'csm_tower': 'APWT',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'APWT'},
    {'alt_mod_p_cell_id_rti': 493345,
     'csm_cell_id': 33,
     'csm_cell_id_rti': 12822817,
     'csm_tower': 'APTK',
     'mod_p_cell_id': 33,
     'mod_p_tower': 'APTK'},
    {'alt_mod_p_cell_id_rti': 493344,
     'csm_cell_id': 32,
     'csm_cell_id_rti': 12822816,
     'csm_tower': 'APTK',
     'mod_p_cell_id': 32,
     'mod_p_tower': 'APTK'},
    {'alt_mod_p_cell_id_rti': 493343,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 12822815,
     'csm_tower': 'APTK',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'APTK'},
    {'alt_mod_p_cell_id_rti': 277023,
     'csm_cell_id': 31,
     'csm_cell_id_rti': 12812063,
     'csm_tower': 'AGRF',
     'mod_p_cell_id': 31,
     'mod_p_tower': 'AGRF'},
    ]

raw_data2_consolidate = """

    ## consolidate all

    31083;"2009-05-25";"2010-11-04";-36.7883988456741;174.771375373102;"NZI Building Cnr Lake Road and Hurton Road Takapuna"
    31083;"2010-11-05";"2010-11-22";-36.7883988456741;174.771375373102;"NZI Building Cnr Lake Road and Hurton Road Takapuna"
    31083;"2010-11-23";"2011-04-17";-36.7883988456741;174.771375373102;"NZI Building Cnr Lake Road and Hurton Road Takapuna"
    31083;"2011-04-17";"2014-03-12";-36.7883988456741;174.771375373102;"NZI Building Cnr Lake Road and Hurton Road Takapuna"
    31083;"2014-03-12";"2014-05-22";-36.7883988456741;174.771375373102;"NZI Building Cnr Lake Road and Hurton Road Takapuna"
    31083;"2014-05-22";"2014-09-26";-36.7883988456741;174.771375373102;"NZI Building Cnr Lake Road and Hurton Road Takapuna"
    31083;"2014-09-11";"2016-09-06";-36.7883988456741;174.771375373102;"NZI Building Cnr Lake Road and Hurton Road Takapuna"
    31083;"2016-09-06";"";-36.7883988456741;174.771375373102;"NZI Building Cnr Lake Road and Hurton Road Takapuna"

    31236;"2009-01-23";"2010-09-28";-36.8542893133162;174.763988849045;"31 Airedale Street Auckland CBD Auckland"
    31236;"2010-09-27";"2011-04-17";-36.8542893133162;174.763988849045;"31 Airedale Street Auckland CBD Auckland"
    31236;"2011-04-17";"2012-02-28";-36.8542893133162;174.763988849045;"31 Airedale Street Auckland CBD Auckland"
    31236;"2012-02-28";"2016-08-25";-36.8542893133162;174.763988849045;"31 Airedale Street Auckland CBD Auckland"
    31236;"2016-08-26";"";-36.8542893133162;174.763988849045;"31 Airedale Street Auckland CBD Auckland"

    32804;"2009-05-25";"2010-11-04";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32804;"2010-11-05";"2010-12-02";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32804;"2010-12-03";"2011-04-17";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32804;"2011-04-17";"2011-08-04";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32804;"2011-08-05";"2011-08-22";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32804;"2011-08-23";"2014-03-12";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32804;"2014-03-12";"2014-05-22";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32804;"2014-05-22";"2016-07-20";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32804;"2016-07-07";"2016-08-30";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32804;"2016-08-30";"";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"

    32805;"2009-05-25";"2010-11-04";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32805;"2010-11-05";"2010-12-02";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32805;"2010-12-03";"2011-04-17";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32805;"2011-04-17";"2011-08-04";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32805;"2011-08-05";"2011-08-22";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32805;"2011-08-23";"2014-03-12";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32805;"2014-03-12";"2014-05-22";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32805;"2014-05-22";"2016-07-20";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32805;"2016-07-07";"2016-08-30";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32805;"2016-08-30";"";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"

    32806;"2009-05-25";"2010-11-04";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32806;"2010-11-05";"2010-12-02";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32806;"2010-12-03";"2011-04-17";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32806;"2011-04-17";"2011-08-04";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32806;"2011-08-05";"2011-08-22";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32806;"2011-08-23";"2014-03-12";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32806;"2014-03-12";"2014-05-22";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32806;"2014-05-22";"2016-07-20";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32806;"2016-07-07";"2016-08-30";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"
    32806;"2016-08-30";"";-36.8721057272134;174.669874499954;"658 Rosebank Road Avondale"

    36381;"2008-10-01";"2010-03-31";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36381;"2010-04-01";"2011-05-01";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36381;"2011-05-01";"2011-05-16";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36381;"2011-05-17";"2012-08-08";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36381;"2012-08-08";"2012-08-31";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36381;"2012-08-31";"2015-03-09";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36381;"2014-12-03";"2015-03-09";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36381;"2015-03-09";"2015-03-29";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36381;"2015-03-10";"2015-03-29";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"

    36382;"2008-10-01";"2010-03-31";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36382;"2010-04-01";"2011-05-01";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36382;"2011-05-01";"2011-05-16";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36382;"2011-05-17";"2012-08-08";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36382;"2012-08-08";"2012-08-31";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36382;"2012-08-31";"2015-03-09";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36382;"2014-12-03";"2015-03-09";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36382;"2015-03-09";"2015-03-29";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36382;"2015-03-10";"2015-03-29";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"

    36383;"2008-10-01";"2010-03-31";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36383;"2010-04-01";"2011-05-01";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36383;"2011-05-01";"2011-05-16";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36383;"2011-05-17";"2012-08-08";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36383;"2012-08-08";"2012-08-31";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36383;"2012-08-31";"2015-03-09";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36383;"2014-12-03";"2015-03-09";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36383;"2015-03-09";"2015-03-29";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36383;"2015-03-10";"2015-03-29";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"

    36384;"2011-05-24";"2012-08-08";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36384;"2012-08-08";"2012-08-31";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36384;"2012-08-31";"2015-03-09";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36384;"2014-12-03";"2015-03-09";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36384;"2015-03-09";"2015-03-29";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36384;"2015-03-10";"2015-03-29";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"

    36385;"2011-05-24";"2012-08-08";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36385;"2012-08-08";"2012-08-31";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36385;"2012-08-31";"2015-03-09";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36385;"2014-12-03";"2015-03-09";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36385;"2015-03-09";"2015-03-29";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36385;"2015-03-10";"2015-03-29";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"

    36386;"2011-05-24";"2012-08-08";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36386;"2012-08-08";"2012-08-31";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36386;"2012-08-31";"2015-03-09";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36386;"2014-12-03";"2015-03-09";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36386;"2015-03-09";"2015-03-29";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"
    36386;"2015-03-10";"2015-03-29";-38.010715861341;175.325211797546;"Walton Street Te Awamutu"

    50161;"2010-05-20";"2010-11-15";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50161;"2010-11-14";"2011-08-14";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50161;"2011-08-14";"2015-06-03";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50161;"2015-06-04";"";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"

    50162;"2010-05-20";"2010-11-15";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50162;"2010-11-14";"2011-08-14";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50162;"2011-08-14";"2015-06-03";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50162;"2015-06-04";"";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"

    50163;"2010-05-20";"2010-11-15";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50163;"2010-11-14";"2011-08-14";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50163;"2011-08-14";"2015-06-03";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50163;"2015-06-04";"";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"

    50164;"2010-05-20";"2010-11-15";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50164;"2010-11-14";"2011-08-14";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50164;"2011-08-14";"2015-06-03";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50164;"2015-06-04";"2016-10-19";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50164;"2016-10-19";"";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"

    50165;"2010-05-20";"2010-11-15";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50165;"2010-11-14";"2011-08-14";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50165;"2011-08-14";"2015-06-03";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50165;"2015-06-04";"2016-10-19";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50165;"2016-10-19";"";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"

    50166;"2010-05-20";"2010-11-15";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50166;"2010-11-14";"2011-08-14";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50166;"2011-08-14";"2015-06-03";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50166;"2015-06-04";"2016-10-19";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"
    50166;"2016-10-19";"";-41.2913204492517;174.794547780131;"236 Oriental Parade Wellington"

    50251;"2010-06-25";"2010-11-17";-41.2606302353593;174.910230863127;"249 Marine Drive Lowry Bay Lower Hutt"
    50251;"2010-11-16";"2011-08-14";-41.2606302353593;174.910230863127;"249 Marine Drive Lowry Bay Lower Hutt"
    50251;"2011-08-14";"2011-08-25";-41.2606302353593;174.910230863127;"249 Marine Drive Lowry Bay Lower Hutt"
    50251;"2011-08-26";"2013-05-15";-41.2606302353593;174.910230863127;"249 Marine Drive Lowry Bay Lower Hutt"
    50251;"2013-05-15";"2013-07-15";-41.2606302353593;174.910230863127;"249 Marine Drive Lowry Bay Lower Hutt"
    50251;"2013-07-15";"";-41.2606302353593;174.910230863127;"249 Marine Drive Lowry Bay Lower Hutt"

    50261;"2010-06-30";"2010-11-11";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50261;"2010-11-10";"2011-08-14";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50261;"2011-08-14";"2011-08-25";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50261;"2011-08-26";"2013-05-15";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50261;"2013-05-15";"2013-07-15";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50261;"2013-07-15";"2014-04-02";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50261;"2014-04-02";"2014-05-27";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50261;"2014-05-27";"2015-09-29";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50261;"2015-09-30";"2015-11-11";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"

    50262;"2010-06-30";"2010-11-11";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50262;"2010-11-10";"2011-08-14";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50262;"2011-08-14";"2011-08-25";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50262;"2011-08-26";"2013-05-15";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50262;"2013-05-15";"2013-07-15";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50262;"2013-07-15";"2014-04-02";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50262;"2014-04-02";"2014-05-27";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50262;"2014-05-27";"2015-09-29";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50262;"2015-09-30";"2015-11-11";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"

    50263;"2010-06-30";"2010-11-11";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50263;"2010-11-10";"2011-08-14";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50263;"2011-08-14";"2011-08-25";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50263;"2011-08-26";"2013-05-15";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50263;"2013-05-15";"2013-07-15";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50263;"2013-07-15";"2014-04-02";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50263;"2014-04-02";"2014-05-27";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50263;"2014-05-27";"2015-09-29";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"
    50263;"2015-09-30";"2015-11-11";-41.1083000835582;174.837109267793;"Niblick Lane Titahi Bay Wellington"

    50440;"2008-10-01";"2010-02-13";-43.7941646982584;172.97726485156;"Long Bay Road Akaroa Banks Peninsula"
    50440;"2010-02-14";"2010-10-03";-43.7941646982584;172.97726485156;"Long Bay Road Akaroa Banks Peninsula"
    50440;"2010-10-04";"2010-11-23";-43.7941646982584;172.97726485156;"Long Bay Road Akaroa Banks Peninsula"
    50440;"2010-11-22";"2011-04-18";-43.7941646982584;172.97726485156;"Long Bay Road Akaroa Banks Peninsula"
    50440;"2011-04-19";"2011-05-29";-43.7941646982584;172.97726485156;"Long Bay Road Akaroa Banks Peninsula"
    50440;"2011-05-29";"2011-11-24";-43.7941646982584;172.97726485156;"Long Bay Road Akaroa Banks Peninsula"

    50491;"2008-10-01";"2010-02-13";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50491;"2010-02-14";"2010-10-03";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50491;"2010-10-04";"2010-11-24";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50491;"2010-11-23";"2011-04-18";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50491;"2011-04-19";"2011-05-29";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50491;"2011-05-29";"2012-08-09";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50491;"2012-08-09";"2013-03-08";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50491;"2013-03-08";"2014-02-11";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50491;"2014-02-11";"2014-03-18";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50491;"2014-03-18";"2015-09-16";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50491;"2015-09-17";"2015-09-17";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"

    50492;"2008-10-01";"2010-02-13";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50492;"2010-02-14";"2010-10-03";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50492;"2010-10-04";"2010-11-24";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50492;"2010-11-23";"2011-04-18";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50492;"2011-04-19";"2011-05-29";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50492;"2011-05-29";"2012-08-09";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50492;"2012-08-09";"2013-03-08";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50492;"2013-03-08";"2014-02-11";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50492;"2014-02-11";"2014-03-18";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50492;"2014-03-18";"2015-09-16";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50492;"2015-09-17";"2015-09-17";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"

    50493;"2008-10-01";"2010-02-13";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50493;"2010-02-14";"2010-10-03";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50493;"2010-10-04";"2010-11-24";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50493;"2010-11-23";"2011-04-18";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50493;"2011-04-19";"2011-05-29";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50493;"2011-05-29";"2012-08-09";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50493;"2012-08-09";"2013-03-08";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50493;"2013-03-08";"2014-02-11";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50493;"2014-02-11";"2014-03-18";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50493;"2014-03-18";"2015-09-16";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"
    50493;"2015-09-17";"2015-09-17";-43.9027408784249;171.751858108706;"159 or 163 Wills Street Ashburton"

    50512;"2008-10-01";"2010-02-13";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50512;"2010-02-14";"2010-10-03";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50512;"2010-10-04";"2010-11-24";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50512;"2010-11-23";"2011-04-18";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50512;"2011-04-19";"2011-05-29";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50512;"2011-05-29";"2012-08-09";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50512;"2012-08-09";"2013-03-08";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50512;"2013-03-08";"2014-02-11";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50512;"2014-02-11";"2014-03-18";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"

    50513;"2008-10-01";"2010-02-13";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50513;"2010-02-14";"2010-10-03";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50513;"2010-10-04";"2010-11-24";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50513;"2010-11-23";"2011-04-18";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50513;"2011-04-19";"2011-05-29";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50513;"2011-05-29";"2012-08-09";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50513;"2012-08-09";"2013-03-08";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50513;"2013-03-08";"2014-02-11";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50513;"2014-02-11";"2014-03-18";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"

    50651;"2008-10-01";"2010-10-11";-41.3795592686254;173.117297449019;"Factory Road Brightwater"
    50651;"2010-10-12";"2010-11-03";-41.3795592686254;173.117297449019;"Factory Road Brightwater"
    50651;"2010-11-02";"2011-05-29";-41.3795592686254;173.117297449019;"Factory Road Brightwater"
    50651;"2011-05-29";"2013-11-28";-41.3795592686254;173.117297449019;"Factory Road Brightwater"
    50651;"2013-11-28";"2014-03-26";-41.3795592686254;173.117297449019;"Factory Road Brightwater"
    50651;"2014-03-26";"2015-05-18";-41.3795592686254;173.117297449019;"Factory Road Brightwater"
    50651;"2015-05-18";"2015-12-10";-41.3795592686254;173.117297449019;"Factory Road Brightwater"

    50652;"2008-10-01";"2010-10-11";-41.3795592686254;173.117297449019;"Factory Road Brightwater"
    50652;"2010-10-12";"2010-11-03";-41.3795592686254;173.117297449019;"Factory Road Brightwater"
    50652;"2010-11-02";"2011-05-29";-41.3795592686254;173.117297449019;"Factory Road Brightwater"
    50652;"2011-05-29";"2013-11-28";-41.3795592686254;173.117297449019;"Factory Road Brightwater"
    50652;"2013-11-28";"2014-03-26";-41.3795592686254;173.117297449019;"Factory Road Brightwater"
    50652;"2014-03-26";"2015-05-18";-41.3795592686254;173.117297449019;"Factory Road Brightwater"
    50652;"2015-05-18";"2015-12-10";-41.3795592686254;173.117297449019;"Factory Road Brightwater"

    50690;"2009-03-04";"2010-02-13";-44.2076574451273;171.38463351802;"Rolleston Road Clandeboye Timaru District"
    50690;"2010-02-14";"2010-10-03";-44.2076574451273;171.38463351802;"Rolleston Road Clandeboye Timaru District"
    50690;"2010-10-04";"2010-11-24";-44.2076574451273;171.38463351802;"Rolleston Road Clandeboye Timaru District"
    50690;"2010-11-23";"2011-04-18";-44.2076574451273;171.38463351802;"Rolleston Road Clandeboye Timaru District"
    50690;"2011-04-19";"2011-05-29";-44.2076574451273;171.38463351802;"Rolleston Road Clandeboye Timaru District"
    50690;"2011-05-29";"2012-08-09";-44.2076574451273;171.38463351802;"Rolleston Road Clandeboye Timaru District"
    50690;"2012-08-09";"2013-03-08";-44.2076574451273;171.38463351802;"Rolleston Road Clandeboye Timaru District"
    50690;"2013-03-08";"2014-02-11";-44.2076574451273;171.38463351802;"Rolleston Road Clandeboye Timaru District"
    50690;"2014-02-11";"2014-03-18";-44.2076574451273;171.38463351802;"Rolleston Road Clandeboye Timaru District"
    50690;"2014-03-18";"2015-05-25";-44.2076574451273;171.38463351802;"Rolleston Road Clandeboye Timaru District"

    50730;"2008-10-01";"2010-10-11";-42.1575274996992;173.936835096228;"Woodbank Clarence Bridge RD1 Kaikoura"
    50730;"2010-10-12";"2010-11-23";-42.1575274996992;173.936835096228;"Woodbank Clarence Bridge RD1 Kaikoura"
    50730;"2010-11-22";"2011-05-29";-42.1575274996992;173.936835096228;"Woodbank Clarence Bridge RD1 Kaikoura"
    50730;"2011-05-29";"2013-11-28";-42.1575274996992;173.936835096228;"Woodbank Clarence Bridge RD1 Kaikoura"
    50730;"2013-11-28";"2016-12-09";-42.1575274996992;173.936835096228;"Woodbank Clarence Bridge RD1 Kaikoura"

    50800;"2008-10-01";"2010-10-11";-42.8178294187601;173.299124820484;"Camels Hump Jedburg Farm McQueen Road Cheviot"
    50800;"2010-10-12";"2010-11-23";-42.8178294187601;173.299124820484;"Camels Hump Jedburg Farm McQueen Road Cheviot"
    50800;"2010-11-22";"2011-05-29";-42.8178294187601;173.299124820484;"Camels Hump Jedburg Farm McQueen Road Cheviot"
    50800;"2011-05-29";"2013-11-28";-42.8178294187601;173.299124820484;"Camels Hump Jedburg Farm McQueen Road Cheviot"
    50800;"2013-11-28";"2016-12-14";-42.8178294187601;173.299124820484;"Camels Hump Jedburg Farm McQueen Road Cheviot"

    50880;"2008-10-01";"2010-02-13";-43.4882694788121;172.112476909712;"28-32 Railway Terrace North Darfield"
    50880;"2010-02-14";"2010-10-03";-43.4882694788121;172.112476909712;"28-32 Railway Terrace North Darfield"
    50880;"2010-10-04";"2010-11-23";-43.4882694788121;172.112476909712;"28-32 Railway Terrace North Darfield"
    50880;"2010-11-22";"2011-04-18";-43.4882694788121;172.112476909712;"28-32 Railway Terrace North Darfield"
    50880;"2011-04-19";"2011-05-19";-43.4882694788121;172.112476909712;"28-32 Railway Terrace North Darfield"

    50891;"2010-10-01";"2010-10-03";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50891;"2010-10-04";"2010-11-24";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50891;"2010-11-23";"2011-04-18";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50891;"2011-04-19";"2011-05-29";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50891;"2011-05-29";"2012-08-09";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50891;"2012-08-09";"2013-03-08";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50891;"2013-03-08";"2014-02-11";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50891;"2014-02-11";"2014-03-18";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50891;"2014-03-18";"2015-09-24";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50891;"2015-09-25";"2016-07-03";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"

    50892;"2010-10-01";"2010-10-03";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50892;"2010-10-04";"2010-11-24";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50892;"2010-11-23";"2011-04-18";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50892;"2011-04-19";"2011-05-29";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50892;"2011-05-29";"2012-08-09";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50892;"2012-08-09";"2013-03-08";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50892;"2013-03-08";"2014-02-11";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50892;"2014-02-11";"2014-03-18";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50892;"2014-03-18";"2015-09-24";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50892;"2015-09-25";"2016-07-03";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"

    50893;"2010-10-01";"2010-10-03";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50893;"2010-10-04";"2010-11-24";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50893;"2010-11-23";"2011-04-18";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50893;"2011-04-19";"2011-05-29";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50893;"2011-05-29";"2012-08-09";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50893;"2012-08-09";"2013-03-08";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50893;"2013-03-08";"2014-02-11";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50893;"2014-02-11";"2014-03-18";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50893;"2014-03-18";"2015-09-24";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"
    50893;"2015-09-25";"2016-07-03";-43.8411925177468;171.845007643944;"Dromore Station Road Dromore Mid-Canterbury"

    50920;"2008-10-01";"2010-02-13";-44.0466374316819;170.872992681127;"Fairlie Timaru"
    50920;"2010-02-14";"2010-10-03";-44.0466374316819;170.872992681127;"Fairlie Timaru"
    50920;"2010-10-04";"2010-11-24";-44.0466374316819;170.872992681127;"Fairlie Timaru"
    50920;"2010-11-23";"2011-04-18";-44.0466374316819;170.872992681127;"Fairlie Timaru"
    50920;"2011-04-19";"2011-05-29";-44.0466374316819;170.872992681127;"Fairlie Timaru"
    50920;"2011-05-29";"2012-08-09";-44.0466374316819;170.872992681127;"Fairlie Timaru"
    50920;"2012-08-09";"2013-03-08";-44.0466374316819;170.872992681127;"Fairlie Timaru"
    50920;"2013-03-08";"2014-02-11";-44.0466374316819;170.872992681127;"Fairlie Timaru"
    50920;"2014-02-11";"2014-03-18";-44.0466374316819;170.872992681127;"Fairlie Timaru"
    50920;"2014-03-18";"2016-09-12";-44.0466374316819;170.872992681127;"Fairlie Timaru"

    50941;"2008-10-01";"2010-10-11";-41.3102425587593;173.31714667662;"Tantragee Road Nelson"
    50941;"2010-10-12";"2010-11-03";-41.3102425587593;173.31714667662;"Tantragee Road Nelson"
    50941;"2010-11-02";"2011-05-29";-41.3102425587593;173.31714667662;"Tantragee Road Nelson"
    50941;"2011-05-29";"2013-11-28";-41.3102425587593;173.31714667662;"Tantragee Road Nelson"
    50941;"2013-11-28";"2014-03-26";-41.3102425587593;173.31714667662;"Tantragee Road Nelson"
    50941;"2014-03-26";"2015-05-18";-41.3102425587593;173.31714667662;"Tantragee Road Nelson"
    50941;"2015-05-18";"";-41.3102425587593;173.31714667662;"Tantragee Road Nelson"

    50991;"2010-06-17";"2010-10-03";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50991;"2010-10-04";"2010-11-24";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50991;"2010-11-23";"2011-04-18";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50991;"2011-04-19";"2011-05-29";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50991;"2011-05-29";"2012-08-09";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50991;"2012-08-09";"2013-03-08";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50991;"2013-03-08";"2014-02-11";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50991;"2014-02-11";"2014-03-18";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50991;"2014-03-18";"2015-05-25";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"

    50992;"2010-06-17";"2010-10-03";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50992;"2010-10-04";"2010-11-24";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50992;"2010-11-23";"2011-04-18";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50992;"2011-04-19";"2011-05-29";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50992;"2011-05-29";"2012-08-09";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50992;"2012-08-09";"2013-03-08";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50992;"2013-03-08";"2014-02-11";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50992;"2014-02-11";"2014-03-18";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50992;"2014-03-18";"2015-05-25";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"

    50993;"2010-06-17";"2010-10-03";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50993;"2010-10-04";"2010-11-24";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50993;"2010-11-23";"2011-04-18";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50993;"2011-04-19";"2011-05-29";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50993;"2011-05-29";"2012-08-09";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50993;"2012-08-09";"2013-03-08";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50993;"2013-03-08";"2014-02-11";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50993;"2014-02-11";"2014-03-18";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"
    50993;"2014-03-18";"2015-05-25";-44.1120427817308;171.246007144127;"Talbot Street Geraldine"

    51031;"2008-10-01";"2010-02-13";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51031;"2010-02-14";"2010-10-03";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51031;"2010-10-04";"2010-11-24";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51031;"2010-11-23";"2011-04-18";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51031;"2011-04-19";"2011-05-29";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51031;"2011-05-29";"2012-08-09";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51031;"2012-08-09";"2013-03-08";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51031;"2013-03-08";"2014-02-11";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51031;"2014-02-11";"2014-03-18";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51031;"2014-03-18";"2015-05-26";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"

    51032;"2008-10-01";"2010-02-13";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51032;"2010-02-14";"2010-10-03";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51032;"2010-10-04";"2010-11-24";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51032;"2010-11-23";"2011-04-18";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51032;"2011-04-19";"2011-05-29";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51032;"2011-05-29";"2012-08-09";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51032;"2012-08-09";"2013-03-08";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51032;"2013-03-08";"2014-02-11";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51032;"2014-02-11";"2014-03-18";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51032;"2014-03-18";"2015-05-26";-44.3732492478546;171.230732294723;"80 Old North Road Oceanview Timaru District"
    51042;"2008-10-01";"2010-02-13";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51042;"2010-02-14";"2010-10-03";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51042;"2010-10-04";"2010-11-24";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51042;"2010-11-23";"2011-04-18";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51042;"2011-04-19";"2011-05-29";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51042;"2011-05-29";"2012-08-09";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51042;"2012-08-09";"2013-03-08";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51042;"2013-03-08";"2014-02-11";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51042;"2014-02-11";"2014-03-18";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51042;"2014-03-18";"2014-07-01";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51042;"2014-07-02";"2014-07-03";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"

    51043;"2008-10-01";"2010-02-13";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51043;"2010-02-14";"2010-10-03";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51043;"2010-10-04";"2010-11-24";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51043;"2010-11-23";"2011-04-18";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51043;"2011-04-19";"2011-05-29";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51043;"2011-05-29";"2012-08-09";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51043;"2012-08-09";"2013-03-08";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51043;"2013-03-08";"2014-02-11";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51043;"2014-02-11";"2014-03-18";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51043;"2014-03-18";"2014-07-01";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51043;"2014-07-02";"2014-07-03";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"

    51060;"2008-10-01";"2010-10-11";-42.5154107797914;172.819248527681;"Holmes Peak Clarence Valley Road Hanmer"
    51060;"2010-10-12";"2010-11-23";-42.5154107797914;172.819248527681;"Holmes Peak Clarence Valley Road Hanmer"
    51060;"2010-11-22";"2011-05-29";-42.5154107797914;172.819248527681;"Holmes Peak Clarence Valley Road Hanmer"
    51060;"2011-05-29";"2012-06-28";-42.5154107797914;172.819248527681;"Holmes Peak Clarence Valley Road Hanmer"

    51070;"2008-10-01";"2010-10-11";-41.2957349954776;173.748997155858;"Takorika Trig Havelock"
    51070;"2010-10-12";"2010-11-04";-41.2957349954776;173.748997155858;"Takorika Trig Havelock"
    51070;"2010-11-03";"2011-05-29";-41.2957349954776;173.748997155858;"Takorika Trig Havelock"
    51070;"2011-05-29";"2013-08-29";-41.2957349954776;173.748997155858;"Takorika Trig Havelock"

    51081;"2008-10-01";"2010-02-13";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51081;"2010-02-14";"2010-10-03";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51081;"2010-10-04";"2010-11-24";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51081;"2010-11-23";"2011-04-18";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51081;"2011-04-19";"2011-05-29";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51081;"2011-05-29";"2012-08-09";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51081;"2012-08-09";"2013-03-08";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51081;"2013-03-08";"2014-02-11";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51081;"2014-02-11";"2014-03-18";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51081;"2014-03-18";"2015-05-26";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"

    51082;"2008-10-01";"2010-02-13";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51082;"2010-02-14";"2010-10-03";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51082;"2010-10-04";"2010-11-24";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51082;"2010-11-23";"2011-04-18";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51082;"2011-04-19";"2011-05-29";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51082;"2011-05-29";"2012-08-09";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51082;"2012-08-09";"2013-03-08";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51082;"2013-03-08";"2014-02-11";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51082;"2014-02-11";"2014-03-18";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51082;"2014-03-18";"2015-05-26";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"

    51083;"2008-10-01";"2010-02-13";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51083;"2010-02-14";"2010-10-03";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51083;"2010-10-04";"2010-11-24";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51083;"2010-11-23";"2011-04-18";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51083;"2011-04-19";"2011-05-29";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51083;"2011-05-29";"2012-08-09";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51083;"2012-08-09";"2013-03-08";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51083;"2013-03-08";"2014-02-11";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51083;"2014-02-11";"2014-03-18";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"
    51083;"2014-03-18";"2015-05-26";-44.4064500807749;171.081195870205;"Mt Horrible Road from Whalebone Corner Claremont Timaru"

    51190;"2008-10-01";"2010-02-13";-43.5412285922609;171.976241629362;"Plantation Road Hororata Mid Canterbury"
    51190;"2010-02-14";"2010-10-03";-43.5412285922609;171.976241629362;"Plantation Road Hororata Mid Canterbury"
    51190;"2010-10-04";"2010-11-23";-43.5412285922609;171.976241629362;"Plantation Road Hororata Mid Canterbury"
    51190;"2010-11-22";"2011-04-18";-43.5412285922609;171.976241629362;"Plantation Road Hororata Mid Canterbury"
    51190;"2011-04-19";"2011-05-29";-43.5412285922609;171.976241629362;"Plantation Road Hororata Mid Canterbury"
    51190;"2011-05-29";"2012-04-30";-43.5412285922609;171.976241629362;"Plantation Road Hororata Mid Canterbury"

    51200;"2008-10-01";"2010-02-13";-43.7373973403351;172.87598481079;"Summit Road Akaroa Harbour"
    51200;"2010-02-14";"2010-10-03";-43.7373973403351;172.87598481079;"Summit Road Akaroa Harbour"
    51200;"2010-10-04";"2010-11-23";-43.7373973403351;172.87598481079;"Summit Road Akaroa Harbour"
    51200;"2010-11-22";"2011-04-18";-43.7373973403351;172.87598481079;"Summit Road Akaroa Harbour"
    51200;"2011-04-19";"2011-05-29";-43.7373973403351;172.87598481079;"Summit Road Akaroa Harbour"
    51200;"2011-05-29";"2012-01-31";-43.7373973403351;172.87598481079;"Summit Road Akaroa Harbour"

    51210;"2008-10-01";"2010-10-11";-42.5562489904431;173.423718477151;"Watherston property 661 Hundalee Road (SH1) Conway Flat Huranui"
    51210;"2010-10-12";"2010-11-23";-42.5562489904431;173.423718477151;"Watherston property 661 Hundalee Road (SH1) Conway Flat Huranui"
    51210;"2010-11-22";"2011-05-29";-42.5562489904431;173.423718477151;"Watherston property 661 Hundalee Road (SH1) Conway Flat Huranui"
    51210;"2011-05-29";"2013-11-28";-42.5562489904431;173.423718477151;"Watherston property 661 Hundalee Road (SH1) Conway Flat Huranui"
    51210;"2013-11-28";"2016-12-13";-42.5562489904431;173.423718477151;"Watherston property 661 Hundalee Road (SH1) Conway Flat Huranui"

    51240;"2008-10-01";"2010-10-11";-41.0307715890158;173.964445897103;"Pelorous Sound Malborough Sound"
    51240;"2010-10-12";"2010-11-04";-41.0307715890158;173.964445897103;"Pelorous Sound Malborough Sound"
    51240;"2010-11-03";"2011-05-29";-41.0307715890158;173.964445897103;"Pelorous Sound Malborough Sound"
    51240;"2011-05-29";"2013-11-28";-41.0307715890158;173.964445897103;"Pelorous Sound Malborough Sound"
    51240;"2013-11-28";"2015-11-28";-41.0307715890158;173.964445897103;"Pelorous Sound Malborough Sound"

    51271;"2008-10-01";"2010-10-11";-41.4409924758257;173.807664079207;"Stoney Creek Road Kaituna Marlborough"
    51271;"2010-10-12";"2010-11-04";-41.4409924758257;173.807664079207;"Stoney Creek Road Kaituna Marlborough"
    51271;"2010-11-03";"2011-05-29";-41.4409924758257;173.807664079207;"Stoney Creek Road Kaituna Marlborough"
    51271;"2011-05-29";"2013-11-28";-41.4409924758257;173.807664079207;"Stoney Creek Road Kaituna Marlborough"
    51271;"2013-11-28";"2015-10-10";-41.4409924758257;173.807664079207;"Stoney Creek Road Kaituna Marlborough"

    51272;"2008-10-01";"2010-10-11";-41.4409924758257;173.807664079207;"Stoney Creek Road Kaituna Marlborough"
    51272;"2010-10-12";"2010-11-04";-41.4409924758257;173.807664079207;"Stoney Creek Road Kaituna Marlborough"
    51272;"2010-11-03";"2011-05-29";-41.4409924758257;173.807664079207;"Stoney Creek Road Kaituna Marlborough"
    51272;"2011-05-29";"2013-11-28";-41.4409924758257;173.807664079207;"Stoney Creek Road Kaituna Marlborough"
    51272;"2013-11-28";"2015-10-10";-41.4409924758257;173.807664079207;"Stoney Creek Road Kaituna Marlborough"

    51273;"2008-10-01";"2010-10-11";-41.4409924758257;173.807664079207;"Stoney Creek Road Kaituna Marlborough"
    51273;"2010-10-12";"2010-11-04";-41.4409924758257;173.807664079207;"Stoney Creek Road Kaituna Marlborough"
    51273;"2010-11-03";"2011-05-29";-41.4409924758257;173.807664079207;"Stoney Creek Road Kaituna Marlborough"
    51273;"2011-05-29";"2013-11-28";-41.4409924758257;173.807664079207;"Stoney Creek Road Kaituna Marlborough"
    51273;"2013-11-28";"2015-10-10";-41.4409924758257;173.807664079207;"Stoney Creek Road Kaituna Marlborough"

    51280;"2008-10-01";"2010-10-11";-42.4187062681852;173.692773943333;"Maui Street Kaikoura"
    51280;"2010-10-12";"2010-11-23";-42.4187062681852;173.692773943333;"Maui Street Kaikoura"
    51280;"2010-11-22";"2011-05-29";-42.4187062681852;173.692773943333;"Maui Street Kaikoura"
    51280;"2011-05-29";"2010-12-04";-42.4187062681852;173.692773943333;"Maui Street Kaikoura"

    51301;"2008-10-01";"2010-10-11";-41.1669404058282;173.982918574831;"Crail Bay Road Nopera Kenepuru Sound Blenheim"
    51301;"2010-10-12";"2010-11-04";-41.1669404058282;173.982918574831;"Crail Bay Road Nopera Kenepuru Sound Blenheim"
    51301;"2010-11-03";"2011-05-29";-41.1669404058282;173.982918574831;"Crail Bay Road Nopera Kenepuru Sound Blenheim"
    51301;"2011-05-29";"2013-11-28";-41.1669404058282;173.982918574831;"Crail Bay Road Nopera Kenepuru Sound Blenheim"
    51301;"2013-11-28";"2015-12-03";-41.1669404058282;173.982918574831;"Crail Bay Road Nopera Kenepuru Sound Blenheim"

    51302;"2008-10-01";"2010-10-11";-41.1669404058282;173.982918574831;"Crail Bay Road Nopera Kenepuru Sound Blenheim"
    51302;"2010-10-12";"2010-11-04";-41.1669404058282;173.982918574831;"Crail Bay Road Nopera Kenepuru Sound Blenheim"
    51302;"2010-11-03";"2011-05-29";-41.1669404058282;173.982918574831;"Crail Bay Road Nopera Kenepuru Sound Blenheim"
    51302;"2011-05-29";"2013-11-28";-41.1669404058282;173.982918574831;"Crail Bay Road Nopera Kenepuru Sound Blenheim"
    51302;"2013-11-28";"2015-12-03";-41.1669404058282;173.982918574831;"Crail Bay Road Nopera Kenepuru Sound Blenheim"

    51391;"2008-10-01";"2010-02-13";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51391;"2010-02-14";"2010-10-03";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51391;"2010-10-04";"2010-11-24";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51391;"2010-11-23";"2011-04-18";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51391;"2011-04-19";"2011-05-29";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51391;"2011-05-29";"2012-08-09";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51391;"2012-08-09";"2013-03-08";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51391;"2013-03-08";"2014-02-11";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51391;"2014-02-11";"2014-03-18";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51391;"2014-03-18";"2015-09-24";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51391;"2015-09-25";"2016-11-07";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51392;"2008-10-01";"2010-02-13";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51392;"2010-02-14";"2010-10-03";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51392;"2010-10-04";"2010-11-24";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51392;"2010-11-23";"2011-04-18";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51392;"2011-04-19";"2011-05-29";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51392;"2011-05-29";"2012-08-09";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51392;"2012-08-09";"2013-03-08";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51392;"2013-03-08";"2014-02-11";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51392;"2014-02-11";"2014-03-18";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51392;"2014-03-18";"2015-09-24";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51392;"2015-09-25";"2016-11-07";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"

    51393;"2008-10-01";"2010-02-13";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51393;"2010-02-14";"2010-10-03";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51393;"2010-10-04";"2010-11-24";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51393;"2010-11-23";"2011-04-18";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51393;"2011-04-19";"2011-05-29";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51393;"2011-05-29";"2012-08-09";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51393;"2012-08-09";"2013-03-08";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51393;"2013-03-08";"2014-02-11";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51393;"2014-02-11";"2014-03-18";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51393;"2014-03-18";"2015-09-24";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"
    51393;"2015-09-25";"2016-11-07";-43.7818991735288;171.356532237557;"654 Lower Downs Road  Anama  Mayfield"

    51401;"2008-10-01";"2010-02-13";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51401;"2010-02-14";"2010-10-03";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51401;"2010-10-04";"2010-11-24";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51401;"2010-11-23";"2011-04-18";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51401;"2011-04-19";"2011-05-29";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51401;"2011-05-29";"2012-08-09";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51401;"2012-08-09";"2013-03-08";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51401;"2013-03-08";"2014-02-11";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51401;"2014-02-11";"2014-03-18";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51401;"2014-03-18";"2015-09-24";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51401;"2015-09-25";"2016-11-07";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"

    51402;"2008-10-01";"2010-02-13";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51402;"2010-02-14";"2010-10-03";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51402;"2010-10-04";"2010-11-24";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51402;"2010-11-23";"2011-04-18";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51402;"2011-04-19";"2011-05-29";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51402;"2011-05-29";"2012-08-09";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51402;"2012-08-09";"2013-03-08";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51402;"2013-03-08";"2014-02-11";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51402;"2014-02-11";"2014-03-18";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51402;"2014-03-18";"2015-09-24";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51402;"2015-09-25";"2016-11-07";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"

    51403;"2008-10-01";"2010-02-13";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51403;"2010-02-14";"2010-10-03";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51403;"2010-10-04";"2010-11-24";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51403;"2010-11-23";"2011-04-18";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51403;"2011-04-19";"2011-05-29";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51403;"2011-05-29";"2012-08-09";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51403;"2012-08-09";"2013-03-08";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51403;"2013-03-08";"2014-02-11";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51403;"2014-02-11";"2014-03-18";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51403;"2014-03-18";"2015-09-24";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"
    51403;"2015-09-25";"2016-11-07";-43.5891208874696;171.482726036533;"Cnr Springburn Road and Stavely Road Methven"

    51511;"2008-10-01";"2010-10-11";-41.1139211547727;173.013289896342;"High Street Motueka"
    51511;"2010-10-12";"2010-11-03";-41.1139211547727;173.013289896342;"High Street Motueka"
    51511;"2010-11-02";"2011-05-29";-41.1139211547727;173.013289896342;"High Street Motueka"
    51511;"2011-05-29";"2013-11-28";-41.1139211547727;173.013289896342;"High Street Motueka"
    51511;"2013-11-28";"2014-03-26";-41.1139211547727;173.013289896342;"High Street Motueka"
    51511;"2014-03-26";"2015-05-18";-41.1139211547727;173.013289896342;"High Street Motueka"
    51511;"2015-05-18";"2015-06-03";-41.1139211547727;173.013289896342;"High Street Motueka"
    51512;"2008-10-01";"2010-10-11";-41.1139211547727;173.013289896342;"High Street Motueka"
    51512;"2010-10-12";"2010-11-03";-41.1139211547727;173.013289896342;"High Street Motueka"
    51512;"2010-11-02";"2011-05-29";-41.1139211547727;173.013289896342;"High Street Motueka"
    51512;"2011-05-29";"2013-11-28";-41.1139211547727;173.013289896342;"High Street Motueka"
    51512;"2013-11-28";"2014-03-26";-41.1139211547727;173.013289896342;"High Street Motueka"
    51512;"2014-03-26";"2015-05-18";-41.1139211547727;173.013289896342;"High Street Motueka"
    51512;"2015-05-18";"2015-06-03";-41.1139211547727;173.013289896342;"High Street Motueka"
    51513;"2008-10-01";"2010-10-11";-41.1139211547727;173.013289896342;"High Street Motueka"
    51513;"2010-10-12";"2010-11-03";-41.1139211547727;173.013289896342;"High Street Motueka"
    51513;"2010-11-02";"2011-05-29";-41.1139211547727;173.013289896342;"High Street Motueka"
    51513;"2011-05-29";"2013-11-28";-41.1139211547727;173.013289896342;"High Street Motueka"
    51513;"2013-11-28";"2014-03-26";-41.1139211547727;173.013289896342;"High Street Motueka"
    51513;"2014-03-26";"2015-05-18";-41.1139211547727;173.013289896342;"High Street Motueka"
    51513;"2015-05-18";"2015-06-03";-41.1139211547727;173.013289896342;"High Street Motueka"

    51531;"2008-10-01";"2010-10-11";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51531;"2010-10-12";"2010-11-03";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51531;"2010-11-02";"2011-05-29";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51531;"2011-05-29";"2013-11-28";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51531;"2013-11-28";"2014-03-26";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51531;"2014-03-26";"2015-05-18";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51531;"2015-05-18";"2015-12-19";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"

    51532;"2008-10-01";"2010-10-11";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51532;"2010-10-12";"2010-11-03";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51532;"2010-11-02";"2011-05-29";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51532;"2011-05-29";"2013-11-28";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51532;"2013-11-28";"2014-03-26";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51532;"2014-03-26";"2015-05-18";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51532;"2015-05-18";"2015-12-19";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"

    51533;"2008-10-01";"2010-10-11";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51533;"2010-10-12";"2010-11-03";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51533;"2010-11-02";"2011-05-29";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51533;"2011-05-29";"2013-11-28";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51533;"2013-11-28";"2014-03-26";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51533;"2014-03-26";"2015-05-18";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51533;"2015-05-18";"2015-12-19";-41.2477545831928;173.090430433473;"Telecom Exchange 149 Aranui Road Mapua"
    51562;"2008-10-01";"2010-10-11";-41.1237104100916;172.852900609185;"Mariri Loop Road SH61 Motueka River Valley Nelson"
    51562;"2010-10-12";"2010-11-03";-41.1237104100916;172.852900609185;"Mariri Loop Road SH61 Motueka River Valley Nelson"
    51562;"2010-11-02";"2011-05-29";-41.1237104100916;172.852900609185;"Mariri Loop Road SH61 Motueka River Valley Nelson"
    51562;"2011-05-29";"2013-11-28";-41.1237104100916;172.852900609185;"Mariri Loop Road SH61 Motueka River Valley Nelson"
    51562;"2013-11-28";"2013-12-02";-41.1237104100916;172.852900609185;"Mariri Loop Road SH61 Motueka River Valley Nelson"
    51562;"2013-12-03";"2013-12-11";-41.1237104100916;172.852900609185;"Mariri Loop Road SH61 Motueka River Valley Nelson"

    51571;"2008-10-01";"2010-10-11";-41.2955304329565;173.976980943276;"Mt Freeth  Picton  Marlborugh"
    51571;"2010-10-12";"2010-11-04";-41.2955304329565;173.976980943276;"Mt Freeth  Picton  Marlborugh"
    51571;"2010-11-03";"2011-05-29";-41.2955304329565;173.976980943276;"Mt Freeth  Picton  Marlborugh"
    51571;"2011-05-29";"2013-11-28";-41.2955304329565;173.976980943276;"Mt Freeth  Picton  Marlborugh"
    51571;"2013-11-28";"2015-06-14";-41.2955304329565;173.976980943276;"Mt Freeth  Picton  Marlborugh"

    51572;"2008-10-01";"2010-10-11";-41.2955304329565;173.976980943276;"Mt Freeth  Picton  Marlborugh"
    51572;"2010-10-12";"2010-11-04";-41.2955304329565;173.976980943276;"Mt Freeth  Picton  Marlborugh"
    51572;"2010-11-03";"2011-05-29";-41.2955304329565;173.976980943276;"Mt Freeth  Picton  Marlborugh"
    51572;"2011-05-29";"2013-11-28";-41.2955304329565;173.976980943276;"Mt Freeth  Picton  Marlborugh"
    51572;"2013-11-28";"2015-06-14";-41.2955304329565;173.976980943276;"Mt Freeth  Picton  Marlborugh"

    51573;"2008-10-01";"2010-10-11";-41.2955304329565;173.976980943276;"Mt Freeth  Picton  Marlborugh"
    51573;"2010-10-12";"2010-11-04";-41.2955304329565;173.976980943276;"Mt Freeth  Picton  Marlborugh"
    51573;"2010-11-03";"2011-05-29";-41.2955304329565;173.976980943276;"Mt Freeth  Picton  Marlborugh"
    51573;"2011-05-29";"2013-11-28";-41.2955304329565;173.976980943276;"Mt Freeth  Picton  Marlborugh"
    51573;"2013-11-28";"2015-06-14";-41.2955304329565;173.976980943276;"Mt Freeth  Picton  Marlborugh"

    51581;"2010-06-30";"2010-10-03";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51581;"2010-10-04";"2010-11-24";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51581;"2010-11-23";"2011-04-18";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51581;"2011-04-19";"2011-05-29";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51581;"2011-05-29";"2012-08-09";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51581;"2012-08-09";"2013-03-08";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51581;"2013-03-08";"2014-02-11";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51581;"2014-02-11";"2014-03-18";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51581;"2014-03-18";"2015-07-01";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51581;"2015-07-02";"2015-07-02";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"

    51582;"2010-06-30";"2010-10-03";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51582;"2010-10-04";"2010-11-24";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51582;"2010-11-23";"2011-04-18";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51582;"2011-04-19";"2011-05-29";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51582;"2011-05-29";"2012-08-09";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51582;"2012-08-09";"2013-03-08";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51582;"2013-03-08";"2014-02-11";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51582;"2014-02-11";"2014-03-18";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51582;"2014-03-18";"2015-07-01";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51582;"2015-07-02";"2015-07-02";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"

    51583;"2010-06-30";"2010-10-03";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51583;"2010-10-04";"2010-11-24";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51583;"2010-11-23";"2011-04-18";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51583;"2011-04-19";"2011-05-29";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51583;"2011-05-29";"2012-08-09";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51583;"2012-08-09";"2013-03-08";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51583;"2013-03-08";"2014-02-11";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51583;"2014-02-11";"2014-03-18";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51583;"2014-03-18";"2015-07-01";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"
    51583;"2015-07-02";"2015-07-02";-43.6372860591743;171.655821745159;"Line Road Methven Mid Canterbury"

    51591;"2008-10-01";"2010-10-11";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51591;"2010-10-12";"2010-11-03";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51591;"2010-11-02";"2011-05-29";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51591;"2011-05-29";"2013-11-28";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51591;"2013-11-28";"2014-03-26";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51591;"2014-03-26";"2015-05-18";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51591;"2015-05-18";"2015-11-24";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"

    51592;"2008-10-01";"2010-10-11";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51592;"2010-10-12";"2010-11-03";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51592;"2010-11-02";"2011-05-29";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51592;"2011-05-29";"2013-11-28";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51592;"2013-11-28";"2014-03-26";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51592;"2014-03-26";"2015-05-18";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51592;"2015-05-18";"2015-11-24";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"

    51593;"2008-10-01";"2010-10-11";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51593;"2010-10-12";"2010-11-03";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51593;"2010-11-02";"2011-05-29";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51593;"2011-05-29";"2013-11-28";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51593;"2013-11-28";"2014-03-26";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51593;"2014-03-26";"2015-05-18";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51593;"2015-05-18";"2015-11-24";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"

    51594;"2009-06-14";"2010-10-11";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51594;"2010-10-12";"2010-11-03";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51594;"2010-11-02";"2011-05-29";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51594;"2011-05-29";"2013-11-28";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51594;"2013-11-28";"2014-03-26";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51594;"2014-03-26";"2015-05-18";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51594;"2015-05-18";"2015-11-24";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"

    51595;"2009-06-14";"2010-10-11";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51595;"2010-10-12";"2010-11-03";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51595;"2010-11-02";"2011-05-29";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51595;"2011-05-29";"2013-11-28";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51595;"2013-11-28";"2014-03-26";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51595;"2014-03-26";"2015-05-18";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51595;"2015-05-18";"2015-11-24";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"

    51596;"2009-06-14";"2010-10-11";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51596;"2010-10-12";"2010-11-03";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51596;"2010-11-02";"2011-05-29";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51596;"2011-05-29";"2013-11-28";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51596;"2013-11-28";"2014-03-26";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51596;"2014-03-26";"2015-05-18";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"
    51596;"2015-05-18";"2015-11-24";-41.3021667181408;173.227417557693;"230 Seaview Road Nayland Nelson"

    51633;"2008-10-01";"2010-10-11";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51633;"2010-10-12";"2010-11-03";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51633;"2010-11-02";"2011-05-29";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51633;"2011-05-29";"2013-11-28";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51633;"2013-11-28";"2014-03-26";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51633;"2014-03-26";"2014-04-11";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51633;"2014-04-12";"2014-04-13";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"

    51641;"2008-10-01";"2010-10-11";-41.2622740936241;173.270339378699;"Port Nelson Terminal Port Road  NELSON"
    51641;"2010-10-12";"2010-11-03";-41.2622740936241;173.270339378699;"Port Nelson Terminal Port Road  NELSON"
    51641;"2010-11-02";"2011-05-29";-41.2622740936241;173.270339378699;"Port Nelson Terminal Port Road  NELSON"
    51641;"2011-05-29";"2013-11-28";-41.2622740936241;173.270339378699;"Port Nelson Terminal Port Road  NELSON"
    51641;"2013-11-28";"2014-03-26";-41.2622740936241;173.270339378699;"Port Nelson Terminal Port Road  NELSON"
    51641;"2014-03-26";"2015-05-18";-41.2622740936241;173.270339378699;"Port Nelson Terminal Port Road  NELSON"
    51641;"2015-05-18";"2015-11-24";-41.2622740936241;173.270339378699;"Port Nelson Terminal Port Road  NELSON"

    51642;"2008-10-01";"2010-10-11";-41.2622740936241;173.270339378699;"Port Nelson Terminal Port Road  NELSON"
    51642;"2010-10-12";"2010-11-03";-41.2622740936241;173.270339378699;"Port Nelson Terminal Port Road  NELSON"
    51642;"2010-11-02";"2011-05-29";-41.2622740936241;173.270339378699;"Port Nelson Terminal Port Road  NELSON"
    51642;"2011-05-29";"2013-11-28";-41.2622740936241;173.270339378699;"Port Nelson Terminal Port Road  NELSON"
    51642;"2013-11-28";"2014-03-26";-41.2622740936241;173.270339378699;"Port Nelson Terminal Port Road  NELSON"
    51642;"2014-03-26";"2015-05-18";-41.2622740936241;173.270339378699;"Port Nelson Terminal Port Road  NELSON"
    51642;"2015-05-18";"2015-11-24";-41.2622740936241;173.270339378699;"Port Nelson Terminal Port Road  NELSON"
    51651;"2008-10-01";"2010-10-11";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51651;"2010-10-12";"2010-11-03";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51651;"2010-11-02";"2011-05-29";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51651;"2011-05-29";"2013-11-28";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51651;"2013-11-28";"2014-03-26";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51651;"2014-03-26";"2015-05-18";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51651;"2015-05-18";"2015-11-24";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"

    51652;"2008-10-01";"2010-10-11";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51652;"2010-10-12";"2010-11-03";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51652;"2010-11-02";"2011-05-29";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51652;"2011-05-29";"2013-11-28";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51652;"2013-11-28";"2014-03-26";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51652;"2014-03-26";"2015-05-18";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51652;"2015-05-18";"2015-11-24";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"

    51653;"2008-10-01";"2010-10-11";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51653;"2010-10-12";"2010-11-03";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51653;"2010-11-02";"2011-05-29";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51653;"2011-05-29";"2013-11-28";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51653;"2013-11-28";"2014-03-26";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51653;"2014-03-26";"2015-05-18";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"
    51653;"2015-05-18";"2015-11-24";-41.2999405591788;173.26283830553;"337 Waimea Road Enner Glynn Nelson"

    51780;"2008-10-01";"2010-10-11";-42.6559672621977;173.304059978213;"Harris property 12 Mendip Road access via Wilding property SH1 Parnasus North Canterbury"
    51780;"2010-10-12";"2010-11-23";-42.6559672621977;173.304059978213;"Harris property 12 Mendip Road access via Wilding property SH1 Parnasus North Canterbury"
    51780;"2010-11-22";"2011-05-29";-42.6559672621977;173.304059978213;"Harris property 12 Mendip Road access via Wilding property SH1 Parnasus North Canterbury"
    51780;"2011-05-29";"2013-11-28";-42.6559672621977;173.304059978213;"Harris property 12 Mendip Road access via Wilding property SH1 Parnasus North Canterbury"
    51780;"2013-11-28";"2016-12-14";-42.6559672621977;173.304059978213;"Harris property 12 Mendip Road access via Wilding property SH1 Parnasus North Canterbury"

    51840;"2008-10-01";"2010-02-13";-43.3378133720177;171.911230903892;"SH 73 Malvern Selwyn District"
    51840;"2010-02-14";"2010-10-03";-43.3378133720177;171.911230903892;"SH 73 Malvern Selwyn District"
    51840;"2010-10-04";"2010-11-23";-43.3378133720177;171.911230903892;"SH 73 Malvern Selwyn District"
    51840;"2010-11-22";"2011-04-18";-43.3378133720177;171.911230903892;"SH 73 Malvern Selwyn District"
    51840;"2011-04-19";"2011-05-29";-43.3378133720177;171.911230903892;"SH 73 Malvern Selwyn District"
    51840;"2011-05-29";"2011-08-03";-43.3378133720177;171.911230903892;"SH 73 Malvern Selwyn District"

    51860;"2008-10-01";"2010-02-13";-44.2437742394975;171.113999239918;"Tengawai Road Timaru"
    51860;"2010-02-14";"2010-10-03";-44.2437742394975;171.113999239918;"Tengawai Road Timaru"
    51860;"2010-10-04";"2010-11-24";-44.2437742394975;171.113999239918;"Tengawai Road Timaru"
    51860;"2010-11-23";"2011-04-18";-44.2437742394975;171.113999239918;"Tengawai Road Timaru"
    51860;"2011-04-19";"2011-05-29";-44.2437742394975;171.113999239918;"Tengawai Road Timaru"
    51860;"2011-05-29";"2011-11-23";-44.2437742394975;171.113999239918;"Tengawai Road Timaru"

    51881;"2008-10-01";"2010-10-11";-41.0366760884552;172.856308583452;"Takaka Hill Motueka Nelson"
    51881;"2010-10-12";"2010-11-03";-41.0366760884552;172.856308583452;"Takaka Hill Motueka Nelson"
    51881;"2010-11-02";"2011-05-29";-41.0366760884552;172.856308583452;"Takaka Hill Motueka Nelson"
    51881;"2011-05-29";"2013-11-28";-41.0366760884552;172.856308583452;"Takaka Hill Motueka Nelson"
    51881;"2013-11-28";"2014-03-26";-41.0366760884552;172.856308583452;"Takaka Hill Motueka Nelson"
    51881;"2014-03-26";"2015-05-18";-41.0366760884552;172.856308583452;"Takaka Hill Motueka Nelson"
    51881;"2015-05-18";"2015-12-20";-41.0366760884552;172.856308583452;"Takaka Hill Motueka Nelson"

    51882;"2008-10-01";"2010-10-11";-41.0366760884552;172.856308583452;"Takaka Hill Motueka Nelson"
    51882;"2010-10-12";"2010-11-03";-41.0366760884552;172.856308583452;"Takaka Hill Motueka Nelson"
    51882;"2010-11-02";"2011-05-29";-41.0366760884552;172.856308583452;"Takaka Hill Motueka Nelson"
    51882;"2011-05-29";"2013-11-28";-41.0366760884552;172.856308583452;"Takaka Hill Motueka Nelson"
    51882;"2013-11-28";"2014-03-26";-41.0366760884552;172.856308583452;"Takaka Hill Motueka Nelson"
    51882;"2014-03-26";"2015-05-18";-41.0366760884552;172.856308583452;"Takaka Hill Motueka Nelson"
    51882;"2015-05-18";"2015-12-20";-41.0366760884552;172.856308583452;"Takaka Hill Motueka Nelson"

    51921;"2008-10-01";"2010-02-13";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51921;"2010-02-14";"2010-10-03";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51921;"2010-10-04";"2010-11-24";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51921;"2010-11-23";"2011-04-18";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51921;"2011-04-19";"2011-05-29";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51921;"2011-05-29";"2012-08-09";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51921;"2012-08-09";"2013-03-08";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51921;"2013-03-08";"2014-02-11";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51921;"2014-02-11";"2014-03-18";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51921;"2014-03-18";"2015-09-24";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51921;"2015-09-25";"2016-01-21";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"

    51922;"2008-10-01";"2010-02-13";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51922;"2010-02-14";"2010-10-03";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51922;"2010-10-04";"2010-11-24";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51922;"2010-11-23";"2011-04-18";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51922;"2011-04-19";"2011-05-29";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51922;"2011-05-29";"2012-08-09";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51922;"2012-08-09";"2013-03-08";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51922;"2013-03-08";"2014-02-11";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51922;"2014-02-11";"2014-03-18";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51922;"2014-03-18";"2015-09-24";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51922;"2015-09-25";"2016-01-21";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"

    51923;"2008-10-01";"2010-02-13";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51923;"2010-02-14";"2010-10-03";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51923;"2010-10-04";"2010-11-24";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51923;"2010-11-23";"2011-04-18";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51923;"2011-04-19";"2011-05-29";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51923;"2011-05-29";"2012-08-09";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51923;"2012-08-09";"2013-03-08";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51923;"2013-03-08";"2014-02-11";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51923;"2014-02-11";"2014-03-18";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51923;"2014-03-18";"2015-09-24";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"
    51923;"2015-09-25";"2016-01-21";-43.7696181966141;171.998012146893;"James and Andrea Liemburg SH1 Rakaia Canterbury"

    51980;"2008-10-01";"2010-10-11";-41.2742623130839;174.166240317543;"Port Underwood Hitaua Bay Marlborough Sounds"
    51980;"2010-10-12";"2010-11-04";-41.2742623130839;174.166240317543;"Port Underwood Hitaua Bay Marlborough Sounds"
    51980;"2010-11-03";"2011-05-29";-41.2742623130839;174.166240317543;"Port Underwood Hitaua Bay Marlborough Sounds"
    51980;"2011-05-29";"2013-11-28";-41.2742623130839;174.166240317543;"Port Underwood Hitaua Bay Marlborough Sounds"
    51980;"2013-11-28";"2014-12-11";-41.2742623130839;174.166240317543;"Port Underwood Hitaua Bay Marlborough Sounds"
    51980;"2014-12-12";"2014-12-13";-41.2742623130839;174.166240317543;"Port Underwood Hitaua Bay Marlborough Sounds"

    52060;"2008-10-01";"2010-02-13";-44.0468776923614;171.408909647029;"Whithells Road Ealing Mid Canterbury"
    52060;"2010-02-14";"2010-10-03";-44.0468776923614;171.408909647029;"Whithells Road Ealing Mid Canterbury"
    52060;"2010-10-04";"2010-11-24";-44.0468776923614;171.408909647029;"Whithells Road Ealing Mid Canterbury"
    52060;"2010-11-23";"2011-04-18";-44.0468776923614;171.408909647029;"Whithells Road Ealing Mid Canterbury"
    52060;"2011-04-19";"2011-05-29";-44.0468776923614;171.408909647029;"Whithells Road Ealing Mid Canterbury"
    52060;"2011-05-29";"2012-01-26";-44.0468776923614;171.408909647029;"Whithells Road Ealing Mid Canterbury"

    52080;"2008-10-01";"2010-10-11";-41.6899594924891;174.049376337695;"Star Hill Marama Road Seddon"
    52080;"2010-10-12";"2010-11-04";-41.6899594924891;174.049376337695;"Star Hill Marama Road Seddon"
    52080;"2010-11-03";"2011-05-29";-41.6899594924891;174.049376337695;"Star Hill Marama Road Seddon"
    52080;"2011-05-29";"2013-11-28";-41.6899594924891;174.049376337695;"Star Hill Marama Road Seddon"
    52080;"2014-08-22";"2016-12-13";-41.6899594924891;174.049376337695;"Star Hill Marama Road Seddon"

    52161;"2008-10-01";"2010-10-11";-41.3193847596887;173.240531245507;"Songer St Stoke Nelson"
    52161;"2010-10-12";"2010-11-03";-41.3193847596887;173.240531245507;"Songer St Stoke Nelson"
    52161;"2010-11-02";"2011-05-29";-41.3193847596887;173.240531245507;"Songer St Stoke Nelson"
    52161;"2011-05-29";"2013-11-28";-41.3193847596887;173.240531245507;"Songer St Stoke Nelson"
    52161;"2013-11-28";"2014-03-26";-41.3193847596887;173.240531245507;"Songer St Stoke Nelson"
    52161;"2014-03-26";"2015-05-18";-41.3193847596887;173.240531245507;"Songer St Stoke Nelson"
    52161;"2015-05-18";"2015-11-24";-41.3193847596887;173.240531245507;"Songer St Stoke Nelson"

    52162;"2008-10-01";"2010-10-11";-41.3193847596887;173.240531245507;"Songer St Stoke Nelson"
    52162;"2010-10-12";"2010-11-03";-41.3193847596887;173.240531245507;"Songer St Stoke Nelson"
    52162;"2010-11-02";"2011-05-29";-41.3193847596887;173.240531245507;"Songer St Stoke Nelson"
    52162;"2011-05-29";"2013-11-28";-41.3193847596887;173.240531245507;"Songer St Stoke Nelson"
    52162;"2013-11-28";"2014-03-26";-41.3193847596887;173.240531245507;"Songer St Stoke Nelson"
    52162;"2014-03-26";"2015-05-18";-41.3193847596887;173.240531245507;"Songer St Stoke Nelson"
    52162;"2015-05-18";"2015-11-24";-41.3193847596887;173.240531245507;"Songer St Stoke Nelson"

    52221;"2010-04-29";"2010-10-03";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52221;"2010-10-04";"2010-11-24";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52221;"2010-11-23";"2011-04-18";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52221;"2011-04-19";"2011-05-29";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52221;"2011-05-29";"2012-08-09";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52221;"2012-08-09";"2013-03-08";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52221;"2013-03-08";"2014-02-11";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52221;"2014-02-11";"2014-03-18";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52221;"2014-03-18";"2015-05-25";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"

    52222;"2010-04-29";"2010-10-03";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52222;"2010-10-04";"2010-11-24";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52222;"2010-11-23";"2011-04-18";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52222;"2011-04-19";"2011-05-29";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52222;"2011-05-29";"2012-08-09";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52222;"2012-08-09";"2013-03-08";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52222;"2013-03-08";"2014-02-11";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52222;"2014-02-11";"2014-03-18";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52222;"2014-03-18";"2015-05-25";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"

    52223;"2010-04-29";"2010-10-03";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52223;"2010-10-04";"2010-11-24";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52223;"2010-11-23";"2011-04-18";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52223;"2011-04-19";"2011-05-29";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52223;"2011-05-29";"2012-08-09";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52223;"2012-08-09";"2013-03-08";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52223;"2013-03-08";"2014-02-11";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52223;"2014-02-11";"2014-03-18";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"
    52223;"2014-03-18";"2015-05-25";-44.2462707796449;171.277088782394;"21 Fraser Street Temuka"

    52231;"2010-02-17";"2010-10-11";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52231;"2010-10-12";"2010-11-03";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52231;"2010-11-02";"2011-05-29";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52231;"2011-05-29";"2013-11-28";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52231;"2013-11-28";"2014-03-26";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52231;"2014-03-26";"2014-11-26";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52231;"2014-11-27";"2014-11-29";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"

    52232;"2010-02-17";"2010-10-11";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52232;"2010-10-12";"2010-11-03";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52232;"2010-11-02";"2011-05-29";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52232;"2011-05-29";"2013-11-28";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52232;"2013-11-28";"2014-03-26";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52232;"2014-03-26";"2014-11-26";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52232;"2014-11-27";"2014-11-29";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"

    52233;"2010-02-17";"2010-10-11";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52233;"2010-10-12";"2010-11-03";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52233;"2010-11-02";"2011-05-29";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52233;"2011-05-29";"2013-11-28";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52233;"2013-11-28";"2014-03-26";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52233;"2014-03-26";"2014-11-26";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"
    52233;"2014-11-27";"2014-11-29";-41.2912989310819;173.240465952571;"50 Beatty Street Tahunanui Nelson"

    52241;"2008-10-01";"2010-02-13";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52241;"2010-02-14";"2010-10-03";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52241;"2010-10-04";"2010-11-24";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52241;"2010-11-23";"2011-05-29";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52241;"2011-05-29";"2012-08-09";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52241;"2012-08-09";"2013-03-08";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52241;"2013-03-08";"2014-02-11";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52241;"2014-02-11";"2014-03-18";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52241;"2014-03-18";"2015-04-09";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"

    52242;"2008-10-01";"2010-02-13";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52242;"2010-02-14";"2010-10-03";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52242;"2010-10-04";"2010-11-24";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52242;"2010-11-23";"2011-05-29";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52242;"2011-05-29";"2012-08-09";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52242;"2012-08-09";"2013-03-08";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52242;"2013-03-08";"2014-02-11";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52242;"2014-02-11";"2014-03-18";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"

    52243;"2008-10-01";"2010-02-13";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52243;"2010-02-14";"2010-10-03";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52243;"2010-10-04";"2010-11-24";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52243;"2010-11-23";"2011-05-29";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52243;"2011-05-29";"2012-08-09";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52243;"2012-08-09";"2013-03-08";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52243;"2013-03-08";"2014-02-11";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"
    52243;"2014-02-11";"2014-03-18";-44.3973266719533;171.254479229521;"Tower Corporation Building 28 George St Timaru"

    52251;"2008-10-01";"2010-02-13";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52251;"2010-02-14";"2010-10-03";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52251;"2010-10-04";"2010-11-24";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52251;"2010-11-23";"2011-04-18";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52251;"2011-04-19";"2011-05-29";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52251;"2011-05-29";"2012-08-09";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52251;"2012-08-09";"2013-03-08";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52251;"2013-03-08";"2014-02-11";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52251;"2014-02-11";"2014-03-18";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52251;"2014-03-18";"2015-09-24";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52251;"2015-09-25";"2016-07-03";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"

    52252;"2008-10-01";"2010-02-13";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52252;"2010-02-14";"2010-10-03";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52252;"2010-10-04";"2010-11-24";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52252;"2010-11-23";"2011-04-18";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52252;"2011-04-19";"2011-05-29";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52252;"2011-05-29";"2012-08-09";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52252;"2012-08-09";"2013-03-08";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52252;"2013-03-08";"2014-02-11";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52252;"2014-02-11";"2014-03-18";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52252;"2014-03-18";"2015-09-24";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"
    52252;"2015-09-25";"2016-07-03";-43.9207762994937;171.7210004542;"114 McMurdo Street Tinwald Ashburton"

    52261;"2008-10-01";"2010-10-11";-41.4007320808298;173.937856486386;"Windermere Forest Parkes Road Tuamarina"
    52261;"2010-10-12";"2010-11-04";-41.4007320808298;173.937856486386;"Windermere Forest Parkes Road Tuamarina"
    52261;"2010-11-03";"2011-05-29";-41.4007320808298;173.937856486386;"Windermere Forest Parkes Road Tuamarina"
    52261;"2011-05-29";"2013-11-28";-41.4007320808298;173.937856486386;"Windermere Forest Parkes Road Tuamarina"
    52261;"2013-11-28";"";-41.4007320808298;173.937856486386;"Windermere Forest Parkes Road Tuamarina"

    52262;"2008-10-01";"2010-10-11";-41.4007320808298;173.937856486386;"Windermere Forest Parkes Road Tuamarina"
    52262;"2010-10-12";"2010-11-04";-41.4007320808298;173.937856486386;"Windermere Forest Parkes Road Tuamarina"
    52262;"2010-11-03";"2011-05-29";-41.4007320808298;173.937856486386;"Windermere Forest Parkes Road Tuamarina"
    52262;"2011-05-29";"2013-11-28";-41.4007320808298;173.937856486386;"Windermere Forest Parkes Road Tuamarina"
    52262;"2013-11-28";"";-41.4007320808298;173.937856486386;"Windermere Forest Parkes Road Tuamarina"

    52281;"2008-10-01";"2010-02-13";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"
    52281;"2010-02-14";"2010-10-03";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"
    52281;"2010-10-04";"2010-11-24";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"
    52281;"2010-11-23";"2011-05-29";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"
    52281;"2011-05-29";"2012-08-09";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"
    52281;"2012-08-09";"2013-03-08";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"
    52281;"2013-03-08";"2014-02-11";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"
    52281;"2014-02-11";"2014-03-18";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"
    52281;"2014-03-18";"2015-05-26";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"

    52282;"2008-10-01";"2010-02-13";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"
    52282;"2010-02-14";"2010-10-03";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"
    52282;"2010-10-04";"2010-11-24";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"
    52282;"2010-11-23";"2011-05-29";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"
    52282;"2011-05-29";"2012-08-09";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"
    52282;"2012-08-09";"2013-03-08";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"
    52282;"2013-03-08";"2014-02-11";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"
    52282;"2014-02-11";"2014-03-18";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"
    52282;"2014-03-18";"2015-05-26";-44.4257873487652;171.244116320585;"SHW1 Scarborough Timaru"

    52291;"2008-10-01";"2010-10-11";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52291;"2010-10-12";"2010-11-03";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52291;"2010-11-02";"2011-05-29";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52291;"2011-05-29";"2013-11-28";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52291;"2013-11-28";"2014-03-26";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52291;"2014-03-26";"2015-05-18";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52291;"2015-05-18";"2015-11-24";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"

    52292;"2008-10-01";"2010-10-11";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52292;"2010-10-12";"2010-11-03";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52292;"2010-11-02";"2011-05-29";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52292;"2011-05-29";"2013-11-28";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52292;"2013-11-28";"2014-03-26";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52292;"2014-03-26";"2015-05-18";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52292;"2015-05-18";"2015-11-24";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"

    52293;"2008-10-01";"2010-10-11";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52293;"2010-10-12";"2010-11-03";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52293;"2010-11-02";"2011-05-29";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52293;"2011-05-29";"2013-11-28";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52293;"2013-11-28";"2014-03-26";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52293;"2014-03-26";"2015-05-18";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"
    52293;"2015-05-18";"2015-11-24";-41.2860385476684;173.273969639637;"Cnr Franklyn St and Waimea Road Nelson"

    52341;"2008-10-01";"2010-10-11";-41.2979406734297;172.994354506486;"Eggers Road Upper Moutere Nelson District"
    52341;"2010-10-12";"2010-11-03";-41.2979406734297;172.994354506486;"Eggers Road Upper Moutere Nelson District"
    52341;"2010-11-02";"2011-05-29";-41.2979406734297;172.994354506486;"Eggers Road Upper Moutere Nelson District"
    52341;"2011-05-29";"2013-11-28";-41.2979406734297;172.994354506486;"Eggers Road Upper Moutere Nelson District"
    52341;"2013-11-28";"2014-03-26";-41.2979406734297;172.994354506486;"Eggers Road Upper Moutere Nelson District"
    52341;"2014-03-26";"2015-05-18";-41.2979406734297;172.994354506486;"Eggers Road Upper Moutere Nelson District"
    52341;"2015-05-18";"2015-12-19";-41.2979406734297;172.994354506486;"Eggers Road Upper Moutere Nelson District"

    52351;"2008-10-01";"2010-10-11";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52351;"2010-10-12";"2010-11-04";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52351;"2010-11-03";"2011-05-29";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52351;"2011-05-29";"2013-11-28";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52351;"2013-11-28";"2014-02-28";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52351;"2014-03-01";"2014-03-03";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52351;"2014-07-11";"2015-05-27";-45.1169386387291;170.967719326512;"Bushey Beach Road Cape Wanbrow Oamaru"
    52351;"2015-05-27";"";-45.1169386387291;170.967719326512;"Bushey Beach Road Cape Wanbrow Oamaru"

    52352;"2008-10-01";"2010-10-11";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52352;"2010-10-12";"2010-11-04";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52352;"2010-11-03";"2011-05-29";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52352;"2011-05-29";"2013-11-28";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52352;"2013-11-28";"2014-02-28";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52352;"2014-03-01";"2014-03-03";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52352;"2014-07-11";"2015-05-27";-45.1169386387291;170.967719326512;"Bushey Beach Road Cape Wanbrow Oamaru"
    52352;"2015-05-27";"";-45.1169386387291;170.967719326512;"Bushey Beach Road Cape Wanbrow Oamaru"
    35147;"2014-01-23";"2014-05-23";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"
    35147;"2014-05-22";"2014-07-02";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"

    52371;"2010-05-04";"2010-10-03";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52371;"2010-10-04";"2010-11-24";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52371;"2010-11-23";"2011-04-18";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52371;"2011-04-19";"2011-05-29";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52371;"2011-05-29";"2012-08-09";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52371;"2012-08-09";"2013-03-08";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52371;"2013-03-08";"2014-02-11";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52371;"2014-02-11";"2014-03-18";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52371;"2014-03-18";"2015-05-26";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"

    52372;"2010-05-04";"2010-10-03";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52372;"2010-10-04";"2010-11-24";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52372;"2010-11-23";"2011-04-18";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52372;"2011-04-19";"2011-05-29";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52372;"2011-05-29";"2012-08-09";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52372;"2012-08-09";"2013-03-08";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52372;"2013-03-08";"2014-02-11";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52372;"2014-02-11";"2014-03-18";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52372;"2014-03-18";"2015-05-26";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"

    52373;"2010-05-04";"2010-10-03";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52373;"2010-10-04";"2010-11-24";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52373;"2010-11-23";"2011-04-18";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52373;"2011-04-19";"2011-05-29";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52373;"2011-05-29";"2012-08-09";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52373;"2012-08-09";"2013-03-08";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52373;"2013-03-08";"2014-02-11";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52373;"2014-02-11";"2014-03-18";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"
    52373;"2014-03-18";"2015-05-26";-44.3547639674168;171.23756332169;"8 Racecource road Washdyke South Canterbury"

    52390;"2008-10-01";"2010-10-11";-41.3967257043519;173.037002990639;"Nelson"
    52390;"2010-10-12";"2010-11-03";-41.3967257043519;173.037002990639;"Nelson"
    52390;"2010-11-02";"2011-05-29";-41.3967257043519;173.037002990639;"Nelson"
    52390;"2011-05-29";"2011-03-10";-41.3967257043519;173.037002990639;"Nelson"

    52440;"2008-10-01";"2010-10-11";-41.8485480979163;174.171068964991;"Clermont Street (Ward Beach Road) Ward"
    52440;"2010-10-12";"2010-11-04";-41.8485480979163;174.171068964991;"Clermont Street (Ward Beach Road) Ward"
    52440;"2010-11-03";"2011-05-29";-41.8485480979163;174.171068964991;"Clermont Street (Ward Beach Road) Ward"
    52440;"2011-05-29";"2013-11-28";-41.8485480979163;174.171068964991;"Clermont Street (Ward Beach Road) Ward"
    52440;"2013-11-28";"";-41.8485480979163;174.171068964991;"Clermont Street (Ward Beach Road) Ward"

    52450;"2008-10-01";"2010-10-11";-41.5958161108226;174.042985031547;"Kemp property SH1 Blenheim / Dashwood"
    52450;"2010-10-12";"2010-11-04";-41.5958161108226;174.042985031547;"Kemp property SH1 Blenheim / Dashwood"
    52450;"2010-11-03";"2011-05-29";-41.5958161108226;174.042985031547;"Kemp property SH1 Blenheim / Dashwood"
    52450;"2011-05-29";"2013-11-28";-41.5958161108226;174.042985031547;"Kemp property SH1 Blenheim / Dashwood"
    52450;"2013-11-28";"";-41.5958161108226;174.042985031547;"Kemp property SH1 Blenheim / Dashwood"
    55031;"2008-10-01";"2010-03-08";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55031;"2010-03-09";"2010-11-15";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55031;"2010-11-14";"2011-08-14";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55031;"2011-08-14";"2015-06-03";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55031;"2015-06-04";"";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"

    55032;"2008-10-01";"2010-03-08";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55032;"2010-03-09";"2010-11-15";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55032;"2010-11-14";"2011-08-14";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55032;"2011-08-14";"2015-06-03";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55032;"2015-06-04";"";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"

    55033;"2008-10-01";"2010-03-08";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55033;"2010-03-09";"2010-11-15";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55033;"2010-11-14";"2011-08-14";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55033;"2011-08-14";"2015-06-03";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55033;"2015-06-04";"";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"

    55034;"2009-05-25";"2010-03-08";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55034;"2010-03-09";"2010-11-15";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55034;"2010-11-14";"2011-08-14";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55034;"2011-08-14";"2015-04-08";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55034;"2015-04-09";"2015-06-03";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55034;"2015-06-04";"";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"

    55035;"2009-05-25";"2010-03-08";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55035;"2010-03-09";"2010-11-15";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55035;"2010-11-14";"2011-08-14";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55035;"2011-08-14";"2015-04-08";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55035;"2015-04-09";"2015-06-03";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55035;"2015-06-04";"";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"

    55036;"2009-05-25";"2010-03-08";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55036;"2010-03-09";"2010-11-15";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55036;"2010-11-14";"2011-08-14";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55036;"2011-08-14";"2015-04-08";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55036;"2015-04-09";"2015-06-03";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"
    55036;"2015-06-04";"";-41.3048343671898;174.778517002082;"102 Adelaide Road Newtown Wellington"

    55041;"2008-10-01";"2010-03-10";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55041;"2010-03-11";"2010-11-17";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55041;"2010-11-16";"2011-08-14";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55041;"2011-08-14";"2011-08-25";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55041;"2011-08-26";"2013-05-15";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55041;"2013-05-15";"2013-07-15";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55041;"2013-07-15";"";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"

    55042;"2008-10-01";"2010-03-10";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55042;"2010-03-11";"2010-11-17";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55042;"2010-11-16";"2011-08-14";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55042;"2011-08-14";"2011-08-25";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55042;"2011-08-26";"2013-05-15";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55042;"2013-05-15";"2013-07-15";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55042;"2013-07-15";"";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"

    55043;"2008-10-01";"2010-03-10";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55043;"2010-03-11";"2010-11-17";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55043;"2010-11-16";"2011-08-14";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55043;"2011-08-14";"2011-08-25";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55043;"2011-08-26";"2013-05-15";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55043;"2013-05-15";"2013-07-15";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55043;"2013-07-15";"";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"

    55044;"2009-05-15";"2010-03-10";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55044;"2010-03-11";"2010-11-17";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55044;"2010-11-16";"2011-08-14";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55044;"2011-08-14";"2011-08-25";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55044;"2011-08-26";"2013-05-15";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55044;"2013-05-15";"2013-07-15";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55044;"2013-07-15";"2016-10-03";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55044;"2016-10-05";"";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"

    55045;"2009-05-15";"2010-03-10";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55045;"2010-03-11";"2010-11-17";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55045;"2010-11-16";"2011-08-14";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55045;"2011-08-14";"2011-08-25";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55045;"2011-08-26";"2013-05-15";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55045;"2013-05-15";"2013-07-15";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55045;"2013-07-15";"2016-10-03";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55045;"2016-10-05";"";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"

    55046;"2009-05-15";"2010-03-10";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55046;"2010-03-11";"2010-11-17";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55046;"2010-11-16";"2011-08-14";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55046;"2011-08-14";"2011-08-25";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55046;"2011-08-26";"2013-05-15";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55046;"2013-05-15";"2013-07-15";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55046;"2013-07-15";"2016-10-03";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"
    55046;"2016-10-05";"";-41.2173940702674;174.881283141152;"Cnr Percy Avenue and Main Hutt Road Lower Hutt - Lot 1 Percy Avenue next to car sales yard"

    55051;"2009-01-30";"2010-03-09";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"
    55051;"2010-03-10";"2010-11-14";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"
    55051;"2010-11-13";"2011-07-04";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"
    55051;"2011-07-05";"2011-08-14";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"
    55051;"2011-08-14";"2015-06-03";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"
    55051;"2015-06-04";"";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"

    55052;"2009-01-30";"2010-03-09";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"
    55052;"2010-03-10";"2010-11-14";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"
    55052;"2010-11-13";"2011-07-04";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"
    55052;"2011-07-05";"";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"

    55053;"2009-05-19";"2010-03-09";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"
    55053;"2010-03-10";"2010-11-14";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"
    55053;"2010-11-13";"2011-07-04";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"

    55054;"2009-05-19";"2010-03-09";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"
    55054;"2010-03-10";"2010-11-14";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"
    55054;"2010-11-13";"2011-07-04";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"
    55054;"2011-07-05";"2011-08-14";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"
    55054;"2011-08-14";"2015-06-03";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"
    55054;"2015-06-04";"";-41.2740470818115;174.785721059542;"147 Waterloo Quay Pipitea Wellington"

    55061;"2008-10-01";"2010-03-10";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55061;"2010-03-11";"2010-11-17";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55061;"2010-11-16";"2011-08-14";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55061;"2011-08-14";"2011-08-25";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55061;"2011-08-26";"2013-05-15";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55061;"2013-05-15";"2013-07-15";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55061;"2013-07-15";"";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"

    55062;"2008-10-01";"2010-03-10";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55062;"2010-03-11";"2010-11-17";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55062;"2010-11-16";"2011-08-14";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55062;"2011-08-14";"2011-08-25";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55062;"2011-08-26";"2013-05-15";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55062;"2013-05-15";"2013-07-15";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55062;"2013-07-15";"";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"

    55063;"2008-10-01";"2010-03-10";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55063;"2010-03-11";"2010-11-17";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55063;"2010-11-16";"2011-08-14";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55063;"2011-08-14";"2011-08-25";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55063;"2011-08-26";"2013-05-15";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55063;"2013-05-15";"2013-07-15";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"
    55063;"2013-07-15";"";-41.2133743633909;174.921471403254;"44D Oxford Terrance Waterloo Lower Hutt"

    55071;"2008-10-01";"2010-03-09";-41.2804477509078;174.773023786907;"SW end Bolton Street Bridge Wellington"
    55071;"2010-03-10";"2010-11-14";-41.2804477509078;174.773023786907;"SW end Bolton Street Bridge Wellington"
    55071;"2010-11-13";"2011-08-14";-41.2804477509078;174.773023786907;"SW end Bolton Street Bridge Wellington"
    55071;"2011-08-14";"2015-06-03";-41.2804477509078;174.773023786907;"SW end Bolton Street Bridge Wellington"
    55071;"2015-06-04";"";-41.2804477509078;174.773023786907;"SW end Bolton Street Bridge Wellington"

    55091;"2008-10-01";"2010-03-09";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55091;"2010-03-10";"2010-11-15";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55091;"2010-11-14";"2011-08-14";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55091;"2011-08-14";"2015-06-03";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55091;"2015-06-04";"";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"

    55092;"2008-10-01";"2010-03-09";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55092;"2010-03-10";"2010-11-15";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55092;"2010-11-14";"2011-08-14";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55092;"2011-08-14";"2015-06-03";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55092;"2015-06-04";"";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"

    55093;"2008-10-01";"2010-03-09";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55093;"2010-03-10";"2010-11-15";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55093;"2010-11-14";"2011-08-14";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55093;"2011-08-14";"2015-06-03";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55093;"2015-06-04";"";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55094;"2009-04-13";"2010-03-09";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55094;"2010-03-10";"2010-11-15";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55094;"2010-11-14";"2011-07-17";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55094;"2011-07-18";"2011-08-14";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55094;"2011-08-14";"2015-06-03";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55094;"2015-06-04";"";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55095;"2009-04-13";"2010-03-09";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55095;"2010-03-10";"2010-11-15";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55095;"2010-11-14";"2011-07-17";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55095;"2011-07-18";"2011-08-14";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55095;"2011-08-14";"2015-06-03";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55095;"2015-06-04";"";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55096;"2009-04-13";"2010-03-09";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55096;"2010-03-10";"2010-11-15";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55096;"2010-11-14";"2011-07-17";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55096;"2011-07-18";"2011-08-14";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55096;"2011-08-14";"2015-06-03";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55096;"2015-06-04";"";-41.2922696528975;174.7832411694;"Polo House Cnr Wakefield-Chaffers Street Wellington"
    55101;"2008-10-01";"2010-03-09";-41.2947443318538;174.782794606266;"25-27 Cambridge Terrace Wellington"
    55101;"2010-03-10";"2010-11-15";-41.2947443318538;174.782794606266;"25-27 Cambridge Terrace Wellington"
    55101;"2010-11-14";"2011-08-14";-41.2947443318538;174.782794606266;"25-27 Cambridge Terrace Wellington"
    55101;"2011-08-14";"2015-06-03";-41.2947443318538;174.782794606266;"25-27 Cambridge Terrace Wellington"
    55101;"2015-06-04";"";-41.2947443318538;174.782794606266;"25-27 Cambridge Terrace Wellington"
    55102;"2008-10-01";"2010-03-09";-41.2947443318538;174.782794606266;"25-27 Cambridge Terrace Wellington"
    55102;"2010-03-10";"2010-11-15";-41.2947443318538;174.782794606266;"25-27 Cambridge Terrace Wellington"
    55102;"2010-11-14";"2011-08-14";-41.2947443318538;174.782794606266;"25-27 Cambridge Terrace Wellington"
    55102;"2011-08-14";"2015-06-03";-41.2947443318538;174.782794606266;"25-27 Cambridge Terrace Wellington"
    55102;"2015-06-04";"";-41.2947443318538;174.782794606266;"25-27 Cambridge Terrace Wellington"
    55103;"2008-10-01";"2010-03-09";-41.2947443318538;174.782794606266;"25-27 Cambridge Terrace Wellington"
    55103;"2010-03-10";"2010-11-15";-41.2947443318538;174.782794606266;"25-27 Cambridge Terrace Wellington"
    55103;"2010-11-14";"2011-08-14";-41.2947443318538;174.782794606266;"25-27 Cambridge Terrace Wellington"
    55103;"2011-08-14";"2015-04-07";-41.2947443318538;174.782794606266;"25-27 Cambridge Terrace Wellington"
    55111;"2008-12-17";"2010-03-08";-41.3185593673927;174.805933769542;"1 Jean Batten Street Rongotai Wellington"
    55111;"2010-03-09";"2010-11-16";-41.3185593673927;174.805933769542;"1 Jean Batten Street Rongotai Wellington"
    55111;"2010-11-15";"2011-08-14";-41.3185593673927;174.805933769542;"1 Jean Batten Street Rongotai Wellington"
    55111;"2011-08-14";"2015-06-03";-41.3185593673927;174.805933769542;"1 Jean Batten Street Rongotai Wellington"
    55111;"2015-06-04";"";-41.3185593673927;174.805933769542;"1 Jean Batten Street Rongotai Wellington"
    55112;"2008-12-17";"2010-03-08";-41.3185593673927;174.805933769542;"1 Jean Batten Street Rongotai Wellington"
    55112;"2010-03-09";"2010-11-16";-41.3185593673927;174.805933769542;"1 Jean Batten Street Rongotai Wellington"
    55112;"2010-11-15";"2011-08-14";-41.3185593673927;174.805933769542;"1 Jean Batten Street Rongotai Wellington"
    55112;"2011-08-14";"2015-06-03";-41.3185593673927;174.805933769542;"1 Jean Batten Street Rongotai Wellington"
    55112;"2015-06-04";"";-41.3185593673927;174.805933769542;"1 Jean Batten Street Rongotai Wellington"
    55113;"2008-12-17";"2010-03-08";-41.3185593673927;174.805933769542;"1 Jean Batten Street Rongotai Wellington"
    55113;"2010-03-09";"2010-11-16";-41.3185593673927;174.805933769542;"1 Jean Batten Street Rongotai Wellington"
    55113;"2010-11-15";"2011-08-14";-41.3185593673927;174.805933769542;"1 Jean Batten Street Rongotai Wellington"
    55113;"2011-08-14";"2015-06-03";-41.3185593673927;174.805933769542;"1 Jean Batten Street Rongotai Wellington"
    55113;"2015-06-04";"";-41.3185593673927;174.805933769542;"1 Jean Batten Street Rongotai Wellington"
    55121;"2008-10-01";"2010-03-09";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55121;"2010-03-10";"2010-11-15";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55121;"2010-11-14";"2011-08-14";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55121;"2011-08-14";"2015-06-03";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55121;"2015-06-04";"";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55122;"2008-10-01";"2010-03-09";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55122;"2010-03-10";"2010-11-15";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55122;"2010-11-14";"2011-08-14";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55122;"2011-08-14";"2015-06-03";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55122;"2015-06-04";"";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55123;"2008-10-01";"2010-03-09";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55123;"2010-03-10";"2010-11-15";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55123;"2010-11-14";"2011-08-14";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55123;"2011-08-14";"2015-06-03";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55123;"2015-06-04";"";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55124;"2009-05-24";"2010-03-09";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55124;"2010-03-10";"2010-11-15";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55124;"2010-11-14";"2011-08-14";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55124;"2011-08-14";"2015-06-03";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55124;"2015-06-04";"";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55125;"2009-05-24";"2010-03-09";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55125;"2010-03-10";"2010-11-15";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55125;"2010-11-14";"2011-08-14";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55125;"2011-08-14";"2015-06-03";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55125;"2015-06-04";"";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55126;"2009-05-24";"2010-03-09";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55126;"2010-03-10";"2010-11-15";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55126;"2010-11-14";"2011-08-14";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55126;"2011-08-14";"2015-06-03";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55126;"2015-06-04";"";-41.2935666150425;174.776206774953;"39-41 Ghuznee Street Te Aro Wellington"
    55131;"2008-10-01";"2010-03-10";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55131;"2010-03-11";"2010-11-17";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55131;"2010-11-16";"2011-08-14";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55131;"2011-08-14";"2011-08-25";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55131;"2011-08-26";"2013-05-15";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55131;"2013-05-15";"2013-07-15";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55131;"2013-07-15";"";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55132;"2008-10-01";"2010-03-10";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55132;"2010-03-11";"2010-11-17";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55132;"2010-11-16";"2011-08-14";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55132;"2011-08-14";"2011-08-25";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55132;"2011-08-26";"2013-05-15";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55132;"2013-05-15";"2013-07-15";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55132;"2013-07-15";"";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55133;"2008-10-01";"2010-03-10";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55133;"2010-03-11";"2010-11-17";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55133;"2010-11-16";"2011-08-14";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55133;"2011-08-14";"2011-08-25";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55133;"2011-08-26";"2013-05-15";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55133;"2013-05-15";"2013-07-15";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55133;"2013-07-15";"";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55134;"2009-05-18";"2010-03-10";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55134;"2010-03-11";"2010-11-17";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55134;"2010-11-16";"2011-08-14";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55134;"2011-08-14";"2011-08-25";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55134;"2011-08-26";"2013-05-15";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55134;"2013-05-15";"2013-07-15";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55134;"2013-07-15";"2016-10-05";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55134;"2016-10-05";"";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55135;"2009-05-18";"2010-03-10";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55135;"2010-03-11";"2010-11-17";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55135;"2010-11-16";"2011-08-14";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55135;"2011-08-14";"2011-08-25";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55135;"2011-08-26";"2013-05-15";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55135;"2013-05-15";"2013-07-15";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55135;"2013-07-15";"2016-10-05";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55135;"2016-10-05";"";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55136;"2009-05-18";"2010-03-10";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55136;"2010-03-11";"2010-11-17";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55136;"2010-11-16";"2011-08-14";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55136;"2011-08-14";"2011-08-25";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55136;"2011-08-26";"2013-05-15";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55136;"2013-05-15";"2013-07-15";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55136;"2013-07-15";"2016-10-05";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55136;"2016-10-05";"";-41.2368524067273;174.913492218506;"Hutt Park Roa Gracefield"
    55151;"2008-12-17";"2010-03-08";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55151;"2010-03-09";"2010-11-16";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55151;"2010-11-15";"2011-08-14";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55151;"2011-08-14";"2015-06-03";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55151;"2015-06-04";"";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55152;"2008-12-17";"2010-03-08";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55152;"2010-03-09";"2010-11-16";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55152;"2010-11-15";"2011-08-14";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55152;"2011-08-14";"2015-06-03";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55152;"2015-06-04";"";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55153;"2008-12-17";"2010-03-08";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55153;"2010-03-09";"2010-11-16";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55153;"2010-11-15";"2011-08-14";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55153;"2011-08-14";"2015-06-03";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55153;"2015-06-04";"";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55154;"2009-05-22";"2010-03-08";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55154;"2010-03-09";"2010-11-16";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55154;"2010-11-15";"2011-08-14";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55154;"2011-08-14";"2015-06-03";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55154;"2015-06-04";"";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55155;"2009-05-22";"2010-03-08";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55155;"2010-03-09";"2010-11-16";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55155;"2010-11-15";"2011-08-14";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55155;"2011-08-14";"2015-06-03";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55155;"2015-06-04";"";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55156;"2009-05-22";"2010-03-08";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55156;"2010-03-09";"2010-11-16";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55156;"2010-11-15";"2011-08-14";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55156;"2011-08-14";"2015-06-03";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55156;"2015-06-04";"";-41.3038904113868;174.794173408539;"32-34 Waitoa Road Wellington"
    55161;"2008-12-17";"2010-03-07";-41.2834154878434;174.768602263791;"30 Salamanca Road Kelburn Wellington"
    55161;"2010-03-08";"2010-11-14";-41.2834154878434;174.768602263791;"30 Salamanca Road Kelburn Wellington"
    55161;"2010-11-13";"2011-08-14";-41.2834154878434;174.768602263791;"30 Salamanca Road Kelburn Wellington"
    55161;"2011-08-14";"2015-06-03";-41.2834154878434;174.768602263791;"30 Salamanca Road Kelburn Wellington"
    55161;"2015-06-04";"";-41.2834154878434;174.768602263791;"30 Salamanca Road Kelburn Wellington"
    55162;"2009-05-25";"2010-03-07";-41.2834154878434;174.768602263791;"30 Salamanca Road Kelburn Wellington"
    55162;"2010-03-08";"2010-11-14";-41.2834154878434;174.768602263791;"30 Salamanca Road Kelburn Wellington"
    55162;"2010-11-13";"2011-08-14";-41.2834154878434;174.768602263791;"30 Salamanca Road Kelburn Wellington"
    55162;"2011-08-14";"2015-06-03";-41.2834154878434;174.768602263791;"30 Salamanca Road Kelburn Wellington"
    55162;"2015-06-04";"";-41.2834154878434;174.768602263791;"30 Salamanca Road Kelburn Wellington"
    55171;"2008-10-01";"2010-03-08";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55171;"2010-03-09";"2010-11-16";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55171;"2010-11-15";"2011-08-14";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55171;"2011-08-14";"2015-06-03";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55171;"2015-06-04";"2016-02-11";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55171;"2016-02-12";"";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55172;"2008-10-01";"2010-03-08";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55172;"2010-03-09";"2010-11-16";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55172;"2010-11-15";"2011-08-14";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55172;"2011-08-14";"2015-06-03";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55172;"2015-06-04";"2016-02-11";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55172;"2016-02-12";"";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55173;"2008-10-01";"2010-03-08";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55173;"2010-03-09";"2010-11-16";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55173;"2010-11-15";"2011-08-14";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55173;"2011-08-14";"2015-06-03";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55173;"2015-06-04";"2016-02-11";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55173;"2016-02-12";"";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55174;"2009-05-20";"2010-03-08";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55174;"2010-03-09";"2010-11-16";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55174;"2010-11-15";"2011-08-14";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55174;"2011-08-14";"2014-02-20";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55174;"2014-02-21";"2014-02-25";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55174;"2016-02-12";"";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55175;"2009-05-20";"2010-03-08";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55175;"2010-03-09";"2010-11-16";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55175;"2010-11-15";"2011-08-14";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55175;"2014-02-21";"2014-02-25";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55175;"2016-02-12";"";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55176;"2009-05-20";"2010-03-08";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55176;"2010-03-09";"2010-11-16";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55176;"2010-11-15";"2011-08-14";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55176;"2011-08-14";"2014-02-20";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55176;"2014-02-21";"2014-02-25";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55176;"2016-02-12";"";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"
    55201;"2009-01-22";"2010-03-09";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55201;"2010-03-10";"2010-11-14";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55201;"2010-11-13";"2011-08-14";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55201;"2011-08-14";"2015-06-03";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55201;"2015-06-04";"";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"

    55202;"2009-01-22";"2010-03-09";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55202;"2010-03-10";"2010-11-14";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55202;"2010-11-13";"2011-08-14";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55202;"2011-08-14";"2015-06-03";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55202;"2015-06-04";"";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"

    55203;"2009-01-22";"2010-03-09";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55203;"2010-03-10";"2010-11-14";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55203;"2010-11-13";"2011-08-14";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55203;"2011-08-14";"2015-06-03";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55203;"2015-06-04";"";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"

    55204;"2009-05-25";"2010-03-09";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55204;"2010-03-10";"2010-11-14";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55204;"2010-11-13";"2011-08-14";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55204;"2011-08-14";"2011-11-19";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55204;"2011-11-20";"2015-06-03";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55204;"2015-06-04";"";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55205;"2009-05-25";"2010-03-09";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55205;"2010-03-10";"2010-11-14";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55205;"2010-11-13";"2011-08-14";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55205;"2011-08-14";"2011-11-19";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55205;"2011-11-20";"2015-06-03";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55205;"2015-06-04";"";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55206;"2009-05-25";"2010-03-09";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55206;"2010-03-10";"2010-11-14";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55206;"2010-11-13";"2011-08-14";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55206;"2011-08-14";"2011-11-19";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55206;"2011-11-20";"2015-06-03";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55206;"2015-06-04";"";-41.2828676039497;174.777327462817;"131 Featherston Street Wellington"
    55211;"2008-12-20";"2010-03-08";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55211;"2010-03-09";"2010-11-16";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55211;"2010-11-15";"2011-08-14";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55211;"2011-08-14";"2015-06-03";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55211;"2015-06-04";"";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55212;"2008-12-20";"2010-03-08";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55212;"2010-03-09";"2010-11-16";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55212;"2010-11-15";"2011-08-14";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55212;"2011-08-14";"2015-06-03";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55212;"2015-06-04";"";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55213;"2008-12-20";"2010-03-08";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55213;"2010-03-09";"2010-11-16";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55213;"2010-11-15";"2011-08-14";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55213;"2011-08-14";"2015-06-03";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55213;"2015-06-04";"";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55214;"2009-05-20";"2010-03-08";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55214;"2010-03-09";"2010-11-16";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55214;"2010-11-15";"2011-08-14";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55214;"2011-08-14";"2015-06-03";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55214;"2015-06-04";"";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55215;"2009-05-20";"2010-03-08";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55215;"2010-03-09";"2010-11-16";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55215;"2010-11-15";"2011-08-14";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55215;"2011-08-14";"2015-06-03";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55215;"2015-06-04";"";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55216;"2009-05-20";"2010-03-08";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55216;"2010-03-09";"2010-11-16";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55216;"2010-11-15";"2011-08-14";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55216;"2011-08-14";"2015-06-03";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55216;"2015-06-04";"";-41.3251138017861;174.812421730737;"383 Broadway Miramar Wellington"
    55221;"2008-10-01";"2010-03-10";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55221;"2010-03-11";"2010-11-17";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55221;"2010-11-16";"2011-08-14";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55221;"2011-08-14";"2011-08-25";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55221;"2011-08-26";"2013-05-15";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55221;"2013-05-15";"2013-07-15";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55221;"2013-07-15";"2016-10-12";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55222;"2008-10-01";"2010-03-10";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55222;"2010-03-11";"2010-11-17";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55222;"2010-11-16";"2011-08-14";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55222;"2011-08-14";"2011-08-25";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55222;"2011-08-26";"2013-05-15";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55222;"2013-05-15";"2013-07-15";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55222;"2013-07-15";"2016-10-12";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55223;"2008-10-01";"2010-03-10";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55223;"2010-03-11";"2010-11-17";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55223;"2010-11-16";"2011-08-14";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55223;"2011-08-14";"2011-08-25";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55223;"2011-08-26";"2013-05-15";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55223;"2013-05-15";"2013-07-15";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55223;"2013-07-15";"2016-10-12";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55224;"2009-05-18";"2010-03-10";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55224;"2010-03-11";"2010-11-17";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55224;"2010-11-16";"2011-08-14";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55224;"2011-08-14";"2011-08-25";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55224;"2011-08-26";"2013-05-15";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55224;"2013-05-15";"2013-07-15";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55224;"2013-07-15";"2014-10-16";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55224;"2014-10-17";"2016-10-12";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55225;"2009-05-18";"2010-03-10";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55225;"2010-03-11";"2010-11-17";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55225;"2010-11-16";"2011-08-14";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55225;"2011-08-14";"2011-08-25";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55225;"2011-08-26";"2013-05-15";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55225;"2013-05-15";"2013-07-15";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55225;"2013-07-15";"2014-10-16";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55225;"2014-10-17";"2016-10-12";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55226;"2009-05-18";"2010-03-10";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55226;"2010-03-11";"2010-11-17";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55226;"2010-11-16";"2011-08-14";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55226;"2011-08-14";"2011-08-25";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55226;"2011-08-26";"2013-05-15";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55226;"2013-05-15";"2013-07-15";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55226;"2013-07-15";"2014-10-16";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55226;"2014-10-17";"2016-10-12";-41.2051205759833;174.910796153995;"467 High Street (opp Downer Street) Lower Hutt"
    55234;"2011-03-10";"2011-08-14";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55234;"2011-08-14";"2013-09-29";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55234;"2011-09-30";"2013-09-29";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55235;"2011-03-10";"2011-08-14";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55235;"2011-08-14";"2013-09-29";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55235;"2011-09-30";"2013-09-29";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55236;"2011-03-10";"2011-08-14";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55236;"2011-08-14";"2013-09-29";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55236;"2011-09-30";"2013-09-29";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55241;"2008-10-01";"2010-03-08";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55241;"2010-03-09";"2010-11-16";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55241;"2010-11-15";"2011-08-14";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55241;"2011-08-14";"2015-06-03";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55241;"2015-06-04";"";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55242;"2008-10-01";"2010-03-08";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55242;"2010-03-09";"2010-11-16";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55242;"2010-11-15";"2011-08-14";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55242;"2011-08-14";"2015-06-03";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55242;"2015-06-04";"";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55243;"2008-10-01";"2010-03-08";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55243;"2010-03-09";"2010-11-16";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55243;"2010-11-15";"2011-08-14";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55243;"2011-08-14";"2015-06-03";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55243;"2015-06-04";"";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55244;"2009-04-13";"2010-03-08";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55244;"2010-03-09";"2010-11-16";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55244;"2010-11-15";"2011-08-14";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55244;"2011-08-14";"2015-06-03";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55244;"2015-06-04";"";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55245;"2009-04-13";"2010-03-08";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55245;"2010-03-09";"2010-11-16";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55245;"2010-11-15";"2011-08-14";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55245;"2011-08-14";"2015-06-03";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55245;"2015-06-04";"";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55246;"2009-04-13";"2010-03-08";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55246;"2010-03-09";"2010-11-16";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55246;"2010-11-15";"2011-08-14";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55246;"2011-08-14";"2015-06-03";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55246;"2015-06-04";"";-41.3085174294663;174.822583854488;"148 Park Road Miramar Wellington"
    55261;"2009-04-13";"2010-03-09";-41.3004418860482;174.774230381963;"302 Taranaki Street Wellington"
    55261;"2010-03-10";"2010-11-15";-41.3004418860482;174.774230381963;"302 Taranaki Street Wellington"
    55261;"2010-11-14";"2011-08-14";-41.3004418860482;174.774230381963;"302 Taranaki Street Wellington"
    55261;"2011-08-14";"2015-06-03";-41.3004418860482;174.774230381963;"302 Taranaki Street Wellington"
    55261;"2015-06-04";"";-41.3004418860482;174.774230381963;"302 Taranaki Street Wellington"
    55262;"2009-04-13";"2010-03-09";-41.3004418860482;174.774230381963;"302 Taranaki Street Wellington"
    55262;"2010-03-10";"2010-11-15";-41.3004418860482;174.774230381963;"302 Taranaki Street Wellington"
    55262;"2010-11-14";"2011-08-14";-41.3004418860482;174.774230381963;"302 Taranaki Street Wellington"
    55262;"2011-08-14";"2015-06-03";-41.3004418860482;174.774230381963;"302 Taranaki Street Wellington"
    55262;"2015-06-04";"";-41.3004418860482;174.774230381963;"302 Taranaki Street Wellington"
    55263;"2009-04-13";"2010-03-09";-41.3004418860482;174.774230381963;"302 Taranaki Street Wellington"
    55263;"2010-03-10";"2010-11-15";-41.3004418860482;174.774230381963;"302 Taranaki Street Wellington"
    55263;"2010-11-14";"2011-08-14";-41.3004418860482;174.774230381963;"302 Taranaki Street Wellington"
    55263;"2011-08-14";"2015-06-03";-41.3004418860482;174.774230381963;"302 Taranaki Street Wellington"
    55263;"2015-06-04";"";-41.3004418860482;174.774230381963;"302 Taranaki Street Wellington"
    55271;"2008-12-20";"2010-03-08";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55271;"2010-03-09";"2010-11-16";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55271;"2010-11-15";"2011-08-14";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55271;"2011-08-14";"2015-06-03";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55271;"2015-06-04";"";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55272;"2008-12-20";"2010-03-08";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55272;"2010-03-09";"2010-11-16";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55272;"2010-11-15";"2011-08-14";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55272;"2011-08-14";"2015-06-03";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55272;"2015-06-04";"";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55273;"2008-12-20";"2010-03-08";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55273;"2010-03-09";"2010-11-16";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55273;"2010-11-15";"2011-08-14";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55273;"2011-08-14";"2015-06-03";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55273;"2015-06-04";"";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55274;"2009-05-25";"2010-03-08";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55274;"2010-03-09";"2010-11-16";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55274;"2010-11-15";"2011-08-14";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55274;"2011-08-14";"2014-02-26";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55274;"2014-02-27";"2014-03-10";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55274;"2014-03-11";"2016-04-20";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55274;"2015-03-04";"2015-06-03";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55274;"2015-06-04";"";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55275;"2009-05-25";"2010-03-08";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55275;"2010-03-09";"2010-11-16";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55275;"2010-11-15";"2011-08-14";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55275;"2011-08-14";"2014-02-26";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55275;"2014-02-27";"2014-03-10";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55275;"2014-03-11";"2016-04-20";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55275;"2015-03-04";"2015-06-03";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55275;"2015-06-04";"";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55276;"2009-05-25";"2010-03-08";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55276;"2010-03-09";"2010-11-16";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55276;"2010-11-15";"2011-08-14";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55276;"2011-08-14";"2014-02-26";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55276;"2014-02-27";"2014-03-10";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55276;"2014-03-11";"2016-04-20";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55276;"2015-03-04";"2015-06-03";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55276;"2015-06-04";"";-41.3171903625697;174.778575810611;"container on roof Wakefield Hospital 33 Rintoul Street Newtown Wellington"
    55291;"2009-05-01";"2010-03-10";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55291;"2010-03-11";"2010-11-17";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55291;"2010-11-16";"2011-08-14";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55291;"2011-08-14";"2011-08-25";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55291;"2011-08-26";"2013-05-15";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55291;"2013-05-15";"2013-07-15";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55291;"2013-07-15";"";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55292;"2009-05-01";"2010-03-10";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55292;"2010-03-11";"2010-11-17";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55292;"2010-11-16";"2011-08-14";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55292;"2011-08-14";"2011-08-25";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55292;"2011-08-26";"2013-05-15";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55292;"2013-05-15";"2013-07-15";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55292;"2013-07-15";"";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55293;"2008-11-01";"2010-03-10";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55293;"2010-03-11";"2010-11-17";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55293;"2010-11-16";"2011-08-14";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55293;"2011-08-14";"2011-08-25";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55293;"2011-08-26";"2013-05-15";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55293;"2013-05-15";"2013-07-15";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55293;"2013-07-15";"2016-10-03";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55294;"2008-11-01";"2010-03-10";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55294;"2010-03-11";"2010-11-17";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55294;"2010-11-16";"2011-08-14";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55294;"2011-08-14";"2011-08-25";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55294;"2011-08-26";"2013-05-15";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55294;"2013-05-15";"2013-07-15";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55294;"2013-07-15";"2016-10-03";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55294;"2016-10-04";"";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55295;"2008-11-01";"2010-03-10";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55295;"2010-03-11";"2010-11-17";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55295;"2010-11-16";"2011-08-14";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55295;"2011-08-14";"2011-08-25";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55295;"2011-08-26";"2013-05-15";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55295;"2013-05-15";"2013-07-15";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55295;"2013-07-15";"2016-10-03";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55295;"2016-10-04";"";-41.2246068577668;174.864131332841;"Kerslake House Cnr The Esplanade and Hutt Road Petone"
    55301;"2008-10-01";"2010-03-10";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55301;"2010-03-11";"2010-11-17";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55301;"2010-11-16";"2011-08-14";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55301;"2011-08-14";"2011-08-25";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55301;"2011-08-26";"2013-05-15";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55301;"2013-05-15";"2013-07-15";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55301;"2013-07-15";"";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55302;"2008-10-01";"2010-03-10";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55302;"2010-03-11";"2010-11-17";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55302;"2010-11-16";"2011-08-14";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55302;"2011-08-14";"2011-08-25";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55302;"2011-08-26";"2013-05-15";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55302;"2013-05-15";"2013-07-15";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55302;"2013-07-15";"";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55303;"2008-10-01";"2010-03-10";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55303;"2010-03-11";"2010-11-17";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55303;"2010-11-16";"2011-08-14";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55303;"2011-08-14";"2011-08-25";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55303;"2011-08-26";"2013-05-15";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55303;"2013-05-15";"2013-07-15";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55303;"2013-07-15";"";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55304;"2009-04-29";"2010-03-10";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55304;"2010-03-11";"2010-11-17";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55304;"2010-11-16";"2011-08-14";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55304;"2011-08-14";"2011-08-25";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55304;"2011-08-26";"2013-05-15";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55304;"2013-05-15";"2013-07-15";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55304;"2013-07-15";"2016-10-04";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55304;"2016-10-05";"";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55305;"2009-04-29";"2010-03-10";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55305;"2010-03-11";"2010-11-17";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55305;"2010-11-16";"2011-08-14";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55305;"2011-08-14";"2011-08-25";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55305;"2011-08-26";"2013-05-15";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55305;"2013-05-15";"2013-07-15";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55305;"2013-07-15";"2016-10-04";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55305;"2016-10-05";"";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55306;"2009-04-29";"2010-03-10";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55306;"2010-03-11";"2010-11-17";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55306;"2010-11-16";"2011-08-14";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55306;"2011-08-14";"2011-08-25";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55306;"2011-08-26";"2013-05-15";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55306;"2013-05-15";"2013-07-15";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55306;"2013-07-15";"2016-10-04";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55306;"2016-10-05";"";-41.2256830593961;174.884442023982;"41a Cuba Street Petone"
    55311;"2009-04-13";"2010-03-08";-41.3271745855297;174.800984619298;"7 McGregor Street Rongotai Wellington"
    55311;"2010-03-09";"2010-11-16";-41.3271745855297;174.800984619298;"7 McGregor Street Rongotai Wellington"
    55311;"2010-11-15";"2011-08-14";-41.3271745855297;174.800984619298;"7 McGregor Street Rongotai Wellington"
    55311;"2011-08-14";"2015-06-03";-41.3271745855297;174.800984619298;"7 McGregor Street Rongotai Wellington"
    55311;"2015-06-04";"";-41.3271745855297;174.800984619298;"7 McGregor Street Rongotai Wellington"
    55312;"2009-04-13";"2010-03-08";-41.3271745855297;174.800984619298;"7 McGregor Street Rongotai Wellington"
    55312;"2010-03-09";"2010-11-16";-41.3271745855297;174.800984619298;"7 McGregor Street Rongotai Wellington"
    55312;"2010-11-15";"2011-08-14";-41.3271745855297;174.800984619298;"7 McGregor Street Rongotai Wellington"
    55312;"2011-08-14";"2015-06-03";-41.3271745855297;174.800984619298;"7 McGregor Street Rongotai Wellington"
    55312;"2015-06-04";"";-41.3271745855297;174.800984619298;"7 McGregor Street Rongotai Wellington"
    55313;"2009-04-13";"2010-03-08";-41.3271745855297;174.800984619298;"7 McGregor Street Rongotai Wellington"
    55313;"2010-03-09";"2010-11-16";-41.3271745855297;174.800984619298;"7 McGregor Street Rongotai Wellington"
    55313;"2010-11-15";"2011-08-14";-41.3271745855297;174.800984619298;"7 McGregor Street Rongotai Wellington"
    55313;"2011-08-14";"2015-06-03";-41.3271745855297;174.800984619298;"7 McGregor Street Rongotai Wellington"
    55313;"2015-06-04";"";-41.3271745855297;174.800984619298;"7 McGregor Street Rongotai Wellington"
    55321;"2008-12-16";"2010-03-08";-41.298069712417;174.820873773587;"Shelly Bay Road Wellington"
    55321;"2010-03-09";"2010-11-16";-41.298069712417;174.820873773587;"Shelly Bay Road Wellington"
    55321;"2010-11-15";"2011-08-14";-41.298069712417;174.820873773587;"Shelly Bay Road Wellington"
    55321;"2011-08-14";"2015-06-03";-41.298069712417;174.820873773587;"Shelly Bay Road Wellington"
    55321;"2015-06-04";"";-41.298069712417;174.820873773587;"Shelly Bay Road Wellington"
    55322;"2009-05-20";"2010-03-08";-41.298069712417;174.820873773587;"Shelly Bay Road Wellington"
    55322;"2010-03-09";"2010-11-16";-41.298069712417;174.820873773587;"Shelly Bay Road Wellington"
    55322;"2010-11-15";"2011-08-14";-41.298069712417;174.820873773587;"Shelly Bay Road Wellington"
    55322;"2011-08-14";"2015-06-03";-41.298069712417;174.820873773587;"Shelly Bay Road Wellington"
    55322;"2015-06-04";"2016-11-30";-41.298069712417;174.820873773587;"Shelly Bay Road Wellington"
    55331;"2008-10-01";"2010-03-09";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55331;"2010-03-10";"2010-11-14";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55331;"2010-11-13";"2011-08-14";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55331;"2011-08-14";"2015-06-03";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55331;"2015-06-04";"";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55332;"2008-10-01";"2010-03-09";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55332;"2010-03-10";"2010-11-14";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55332;"2010-11-13";"2011-08-14";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55332;"2011-08-14";"";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55333;"2008-10-01";"2010-03-09";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55333;"2010-03-10";"2010-11-14";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55333;"2010-11-13";"2011-08-14";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55333;"2011-08-14";"2015-06-03";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55333;"2015-06-04";"";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55334;"2009-04-13";"2010-03-09";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55334;"2010-03-10";"2010-10-12";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55334;"2010-10-11";"2010-11-14";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55334;"2010-11-13";"2011-01-26";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55334;"2011-01-27";"2011-08-14";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55334;"2011-08-14";"2015-06-03";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55334;"2015-06-04";"";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55335;"2009-04-13";"2010-03-09";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55335;"2010-03-10";"2010-10-12";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55335;"2010-10-11";"2010-11-14";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55335;"2010-11-13";"2011-01-26";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55335;"2011-01-27";"2011-08-14";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55335;"2011-08-14";"";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55336;"2009-04-13";"2010-03-09";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55336;"2010-03-10";"2010-10-12";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55336;"2010-10-11";"2010-11-14";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55336;"2010-11-13";"2011-01-26";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55336;"2011-01-27";"2011-08-14";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55336;"2011-08-14";"2015-06-03";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55336;"2015-06-04";"";-41.2720947282561;174.786169591761;"Wellington Westpac Stadium Aotea Quay Wellington"
    55341;"2008-10-01";"2010-03-10";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55341;"2010-03-11";"2010-11-17";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55341;"2010-11-16";"2011-08-14";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55341;"2011-08-14";"2011-08-25";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55341;"2011-08-26";"2013-05-15";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55341;"2013-05-15";"2013-07-15";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55341;"2013-07-15";"";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55342;"2008-10-01";"2010-03-10";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55342;"2010-03-11";"2010-11-17";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55342;"2010-11-16";"2011-08-14";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55342;"2011-08-14";"2011-08-25";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55342;"2011-08-26";"2013-05-15";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55342;"2013-05-15";"2013-07-15";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55342;"2013-07-15";"";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"

    55343;"2009-05-01";"2010-03-10";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55343;"2010-03-11";"2010-11-17";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55343;"2010-11-16";"2011-08-14";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55343;"2011-08-14";"2011-08-25";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55343;"2011-08-26";"2013-05-15";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55343;"2013-05-15";"2013-07-15";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55343;"2013-07-15";"2016-10-03";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"

    55344;"2009-05-01";"2010-03-10";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55344;"2010-03-11";"2010-11-17";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55344;"2010-11-16";"2011-08-14";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55344;"2011-08-14";"2011-08-25";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55344;"2011-08-26";"2013-05-15";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55344;"2013-05-15";"2013-07-15";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55344;"2013-07-15";"2016-10-03";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    55344;"2016-10-04";"";-41.2350370216578;174.897928880652;"38 Marine Parade Petone"
    
    55361;"2009-01-21";"2010-03-09";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55361;"2010-03-10";"2010-11-14";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55361;"2010-11-13";"2011-08-14";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55361;"2011-08-14";"2013-10-02";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55361;"2013-10-03";"2015-06-03";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55361;"2015-06-04";"";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55362;"2009-01-21";"2010-03-09";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55362;"2010-03-10";"2010-11-14";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55362;"2010-11-13";"2011-08-14";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55362;"2011-08-14";"2013-10-02";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55362;"2013-10-03";"2015-06-03";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55362;"2015-06-04";"";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55363;"2009-01-21";"2010-03-09";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55363;"2010-03-10";"2010-11-14";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55363;"2010-11-13";"2011-08-14";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55363;"2011-08-14";"2013-10-02";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55363;"2013-10-03";"2015-06-03";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55363;"2015-06-04";"";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55364;"2009-04-13";"2010-03-09";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55364;"2010-03-10";"2010-11-14";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55364;"2010-11-13";"2011-08-14";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55364;"2011-08-14";"2012-02-19";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55364;"2012-02-20";"2013-10-02";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55364;"2013-10-03";"2015-06-03";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55364;"2015-06-04";"";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55365;"2009-04-13";"2010-03-09";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55365;"2010-03-10";"2010-11-14";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55365;"2010-11-13";"2011-08-14";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55365;"2011-08-14";"2012-02-19";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55365;"2012-02-20";"2013-10-02";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55365;"2013-10-03";"2015-06-03";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55365;"2015-06-04";"";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55366;"2009-04-13";"2010-03-09";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55366;"2010-03-10";"2010-11-14";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55366;"2010-11-13";"2011-08-14";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55366;"2011-08-14";"2012-02-19";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55366;"2012-02-20";"2013-10-02";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55366;"2013-10-03";"2015-06-03";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55366;"2015-06-04";"";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    55371;"2009-01-28";"2010-03-09";-41.2729739960874;174.777609996041;"133 Molesworth Street Thorndon Wellington"
    55371;"2010-03-10";"2010-11-14";-41.2729739960874;174.777609996041;"133 Molesworth Street Thorndon Wellington"
    55371;"2010-11-13";"2011-02-13";-41.2729739960874;174.777609996041;"133 Molesworth Street Thorndon Wellington"
    55371;"2011-07-14";"2011-08-14";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55371;"2011-08-14";"2014-02-23";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55372;"2009-01-28";"2010-03-09";-41.2729739960874;174.777609996041;"133 Molesworth Street Thorndon Wellington"
    55372;"2010-03-10";"2010-11-14";-41.2729739960874;174.777609996041;"133 Molesworth Street Thorndon Wellington"
    55372;"2010-11-13";"2011-02-13";-41.2729739960874;174.777609996041;"133 Molesworth Street Thorndon Wellington"
    55372;"2011-07-14";"2011-08-14";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55372;"2011-08-14";"2014-02-23";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55373;"2009-01-28";"2010-03-09";-41.2729739960874;174.777609996041;"133 Molesworth Street Thorndon Wellington"
    55373;"2010-03-10";"2010-11-14";-41.2729739960874;174.777609996041;"133 Molesworth Street Thorndon Wellington"
    55373;"2010-11-13";"2011-02-13";-41.2729739960874;174.777609996041;"133 Molesworth Street Thorndon Wellington"
    55374;"2010-08-05";"2010-11-14";-41.2729739960874;174.777609996041;"133 Molesworth Street Thorndon Wellington"
    55374;"2010-11-13";"2011-02-13";-41.2729739960874;174.777609996041;"133 Molesworth Street Thorndon Wellington"
    55374;"2011-07-14";"2011-08-14";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55374;"2011-08-14";"2014-02-23";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55375;"2010-08-05";"2010-11-14";-41.2729739960874;174.777609996041;"133 Molesworth Street Thorndon Wellington"
    55375;"2010-11-13";"2011-02-13";-41.2729739960874;174.777609996041;"133 Molesworth Street Thorndon Wellington"
    55375;"2011-07-14";"2011-08-14";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55375;"2011-08-14";"2014-02-23";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55376;"2010-08-05";"2010-11-14";-41.2729739960874;174.777609996041;"133 Molesworth Street Thorndon Wellington"
    55376;"2010-11-13";"2011-02-13";-41.2729739960874;174.777609996041;"133 Molesworth Street Thorndon Wellington"
    55391;"2009-01-28";"2010-03-09";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55391;"2010-03-10";"2010-11-15";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55391;"2010-11-14";"2011-08-14";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55391;"2011-08-14";"2013-11-12";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55391;"2014-11-12";"2014-01-26";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55392;"2009-01-28";"2010-03-09";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55392;"2010-03-10";"2010-11-15";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55392;"2010-11-14";"2011-08-14";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55392;"2011-08-14";"2013-11-12";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55392;"2014-11-12";"2014-01-26";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55393;"2009-01-28";"2010-03-09";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55393;"2010-03-10";"2010-11-15";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55393;"2010-11-14";"2011-08-14";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55393;"2011-08-14";"2013-11-12";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55393;"2014-11-12";"2014-01-26";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55394;"2010-08-05";"2010-11-15";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55394;"2010-11-14";"2011-07-14";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55394;"2011-07-15";"2011-08-14";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55394;"2011-08-14";"2013-11-12";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55394;"2014-11-12";"2014-01-26";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55395;"2010-08-05";"2010-11-15";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55395;"2010-11-14";"2011-07-14";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55395;"2011-07-15";"2011-08-14";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55395;"2011-08-14";"2013-11-12";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55395;"2014-11-12";"2014-01-26";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55396;"2010-08-05";"2010-11-15";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55396;"2010-11-14";"2011-07-14";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55396;"2011-07-15";"2011-08-14";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55396;"2011-08-14";"2013-11-12";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55396;"2014-11-12";"2014-01-26";-41.294330930728;174.781565360852;"49-61 Tory Street Te Aro Wellington"
    55411;"2008-10-01";"2010-03-09";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55411;"2010-03-10";"2010-11-15";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55411;"2010-11-14";"2011-08-14";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55411;"2011-08-14";"2015-06-03";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55411;"2015-06-04";"";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55412;"2008-10-01";"2010-03-09";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55412;"2010-03-10";"2010-11-15";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55412;"2010-11-14";"2011-08-14";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55412;"2011-08-14";"2015-06-03";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55412;"2015-06-04";"";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55413;"2008-10-01";"2010-03-09";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55413;"2010-03-10";"2010-11-15";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55413;"2010-11-14";"2011-08-14";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55413;"2011-08-14";"2015-06-03";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55413;"2015-06-04";"";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55414;"2009-05-25";"2010-03-09";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55414;"2010-03-10";"2010-11-15";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55414;"2010-11-14";"2011-08-14";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55414;"2011-08-14";"2015-06-03";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55414;"2015-06-04";"";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55415;"2009-05-25";"2010-03-09";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55415;"2010-03-10";"2010-11-15";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55415;"2010-11-14";"2011-08-14";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55415;"2011-08-14";"2015-06-03";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55415;"2015-06-04";"";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55416;"2009-05-25";"2010-03-09";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55416;"2010-03-10";"2010-11-15";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55416;"2010-11-14";"2011-08-14";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55416;"2011-08-14";"2015-06-03";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55416;"2015-06-04";"";-41.2925137604669;174.772619846198;"Trekkers Hotel 36 Wigan Street Wellington"
    55421;"2009-04-13";"2010-03-09";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55421;"2010-03-10";"2010-11-15";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55421;"2010-11-14";"2011-08-14";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55421;"2011-08-14";"2015-06-03";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55421;"2015-06-04";"2015-07-08";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55421;"2015-07-09";"2015-07-11";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55422;"2009-04-13";"2010-03-09";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55422;"2010-03-10";"2010-11-15";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55422;"2010-11-14";"2011-08-14";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55422;"2011-08-14";"2015-06-03";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55422;"2015-06-04";"2015-07-08";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55422;"2015-07-09";"2015-07-11";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55423;"2009-04-13";"2010-03-09";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55423;"2010-03-10";"2010-11-15";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55423;"2010-11-14";"2011-08-14";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55423;"2011-08-14";"2015-06-03";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55423;"2015-06-04";"2015-07-08";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55423;"2015-07-09";"2015-07-11";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    55431;"2008-10-01";"2010-03-09";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55431;"2010-03-10";"2010-11-15";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55431;"2010-11-14";"2011-08-14";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55431;"2011-08-14";"2015-06-03";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55431;"2015-06-04";"";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55432;"2008-10-01";"2010-03-09";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55432;"2010-03-10";"2010-11-15";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55432;"2010-11-14";"2011-08-14";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55432;"2011-08-14";"2015-06-03";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55432;"2015-06-04";"";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55433;"2008-10-01";"2010-03-09";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55433;"2010-03-10";"2010-11-15";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55433;"2010-11-14";"2011-08-14";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55433;"2011-08-14";"2015-06-03";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55433;"2015-06-04";"";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55434;"2009-05-21";"2010-03-09";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55434;"2010-03-10";"2010-11-15";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55434;"2010-11-14";"2011-08-14";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55434;"2011-08-14";"2015-06-03";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55434;"2015-06-04";"";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55435;"2009-05-21";"2010-03-09";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55435;"2010-03-10";"2010-11-15";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55435;"2010-11-14";"2011-08-14";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55435;"2011-08-14";"2015-06-03";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55435;"2015-06-04";"";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55436;"2009-05-21";"2010-03-09";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55436;"2010-03-10";"2010-11-15";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55436;"2010-11-14";"2011-08-14";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55436;"2011-08-14";"2015-06-03";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55436;"2015-06-04";"";-41.2966864850599;174.770677987524;"355 Willis Street Wellington"
    55451;"2008-10-01";"2010-03-09";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55451;"2010-03-10";"2010-11-14";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55451;"2010-11-13";"2011-08-14";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55451;"2011-08-14";"2015-06-03";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55451;"2015-06-04";"";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55452;"2008-10-01";"2010-03-09";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55452;"2010-03-10";"2010-11-14";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55452;"2010-11-13";"2011-08-14";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55452;"2011-08-14";"2015-06-03";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55452;"2015-06-04";"";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55453;"2008-10-01";"2010-03-09";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55453;"2010-03-10";"2010-11-14";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55453;"2010-11-13";"2011-08-14";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55453;"2011-08-14";"2015-06-03";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55453;"2015-06-04";"";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55454;"2009-04-13";"2010-03-09";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55454;"2010-03-10";"2010-11-14";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55454;"2010-11-13";"2011-08-14";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55454;"2011-08-14";"2011-12-10";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55454;"2011-12-11";"2015-06-03";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55454;"2015-06-04";"";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55455;"2009-04-13";"2010-03-09";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55455;"2010-03-10";"2010-11-14";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55455;"2010-11-13";"2011-08-14";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55455;"2011-08-14";"2011-12-10";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55455;"2011-12-11";"2015-06-03";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55455;"2015-06-04";"";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55456;"2009-04-13";"2010-03-09";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55456;"2010-03-10";"2010-11-14";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55456;"2010-11-13";"2011-08-14";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55456;"2011-08-14";"2011-12-10";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55456;"2011-12-11";"2015-06-03";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55456;"2015-06-04";"";-41.2866930661675;174.776893258557;"9-21 Willeston Street Wellington"
    55461;"2008-10-01";"2010-03-09";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55461;"2010-03-10";"2010-11-14";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55461;"2010-11-13";"2011-08-14";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55461;"2011-08-14";"2015-06-03";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55461;"2015-06-04";"";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55462;"2008-10-01";"2010-03-09";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55462;"2010-03-10";"2010-11-14";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55462;"2010-11-13";"2011-08-14";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55462;"2011-08-14";"2015-06-03";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55462;"2015-06-04";"";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55463;"2008-10-01";"2010-03-09";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55463;"2010-03-10";"2010-11-14";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55463;"2010-11-13";"2011-08-14";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55463;"2011-08-14";"2015-06-03";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55463;"2015-06-04";"";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55464;"2009-04-13";"2010-03-09";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55464;"2010-03-10";"2010-11-14";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55464;"2010-11-13";"2011-06-27";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55464;"2011-06-28";"2011-08-14";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55464;"2011-08-14";"2015-06-03";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55464;"2015-06-04";"";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55465;"2009-04-13";"2010-03-09";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55465;"2010-03-10";"2010-11-14";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55465;"2010-11-13";"2011-06-27";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55465;"2011-06-28";"2011-08-14";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55465;"2011-08-14";"2015-06-03";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55465;"2015-06-04";"";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55466;"2009-04-13";"2010-03-09";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55466;"2010-03-10";"2010-11-14";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55466;"2010-11-13";"2011-06-27";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55466;"2011-06-28";"2011-08-14";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55466;"2011-08-14";"2015-06-03";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55466;"2015-06-04";"";-41.2806744318976;174.778211561768;"70 Featherston Street Wellington"
    55471;"2008-10-01";"2010-03-10";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55471;"2010-03-11";"2010-11-17";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55471;"2010-11-16";"2011-08-14";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55471;"2011-08-14";"2011-08-25";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55471;"2011-08-26";"2013-05-15";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55471;"2013-05-15";"2013-07-15";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55471;"2013-07-15";"";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55472;"2008-10-01";"2010-03-10";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55472;"2010-03-11";"2010-11-17";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55472;"2010-11-16";"2011-08-14";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55472;"2011-08-14";"2011-08-25";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55472;"2011-08-26";"2013-05-15";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55472;"2013-05-15";"2013-07-15";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55472;"2013-07-15";"";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55473;"2008-10-01";"2010-03-10";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55473;"2010-03-11";"2010-11-17";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55473;"2010-11-16";"2011-08-14";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55473;"2011-08-14";"2011-08-25";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55473;"2011-08-26";"2013-05-15";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55473;"2013-05-15";"2013-07-15";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55473;"2013-07-15";"";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55474;"2009-05-01";"2010-03-10";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55474;"2010-03-11";"2010-11-17";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55474;"2010-11-16";"2011-08-14";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55474;"2011-08-14";"2011-08-25";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55474;"2011-08-26";"2013-05-15";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55474;"2013-05-15";"2013-07-15";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55474;"2013-07-15";"2016-10-05";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55474;"2016-10-06";"";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55475;"2009-05-01";"2010-03-10";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55475;"2010-03-11";"2010-11-17";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55475;"2010-11-16";"2011-08-14";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55475;"2011-08-14";"2011-08-25";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55475;"2011-08-26";"2013-05-15";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55475;"2013-05-15";"2013-07-15";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55475;"2013-07-15";"2016-10-05";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55475;"2016-10-06";"";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55476;"2009-05-01";"2010-03-10";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55476;"2010-03-11";"2010-11-17";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55476;"2010-11-16";"2011-08-14";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55476;"2011-08-14";"2011-08-25";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55476;"2011-08-26";"2013-05-15";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55476;"2013-05-15";"2013-07-15";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55476;"2013-07-15";"2016-10-05";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55476;"2016-10-06";"";-41.222503466548;174.911370611867;"66 Whites Line East Behind Woburn Pharmacy Waiwhetu Lower Hutt"
    55481;"2008-10-01";"2010-03-09";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55481;"2010-03-10";"2010-11-14";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55481;"2010-11-13";"2011-08-14";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55481;"2011-08-14";"2015-06-03";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55481;"2015-06-04";"";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55482;"2008-10-01";"2010-03-09";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55482;"2010-03-10";"2010-11-14";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55482;"2010-11-13";"2011-08-14";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55482;"2011-08-14";"2015-06-03";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55482;"2015-06-04";"";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55483;"2009-04-13";"2010-03-09";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55483;"2010-03-10";"2010-11-14";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55483;"2010-11-13";"2011-07-12";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55484;"2009-04-13";"2010-03-09";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55484;"2010-03-10";"2010-11-14";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55484;"2010-11-13";"2011-07-12";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55484;"2011-07-13";"2011-08-14";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55484;"2011-08-14";"2015-06-03";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55484;"2015-06-04";"";-41.2842540566127;174.774427593051;"1. TAB Building 298-310 Lambton Quay Wellington 2. James Cook Hotel 143-151 The Terrace Wellington"
    55501;"2009-01-30";"2010-03-09";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55501;"2010-03-10";"2010-11-14";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55501;"2010-11-13";"2011-08-14";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55501;"2011-08-14";"2015-06-03";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55501;"2015-06-04";"";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55502;"2009-01-30";"2010-03-09";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55502;"2010-03-10";"2010-11-14";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55502;"2010-11-13";"2011-08-14";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55502;"2011-08-14";"2015-06-03";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55502;"2015-06-04";"";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55503;"2009-01-30";"2010-03-09";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55503;"2010-03-10";"2010-11-14";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55503;"2010-11-13";"2011-08-14";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55503;"2011-08-14";"2015-06-03";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55503;"2015-06-04";"";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55504;"2009-05-22";"2010-03-09";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55504;"2010-03-10";"2010-11-14";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55504;"2010-11-13";"2011-08-14";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55504;"2011-08-15";"2011-08-24";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55504;"2011-08-25";"2015-06-03";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55504;"2015-06-04";"";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55505;"2009-05-22";"2010-03-09";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55505;"2010-03-10";"2010-11-14";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55505;"2010-11-13";"2011-08-14";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55505;"2011-08-15";"2011-08-24";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55505;"2011-08-25";"2015-06-03";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55505;"2015-06-04";"";-41.28442712299;174.774300910005;"147 The Terrace Wellington"

    55506;"2009-05-22";"2010-03-09";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55506;"2010-03-10";"2010-11-14";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55506;"2010-11-13";"2011-08-14";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55506;"2011-08-15";"2011-08-24";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55506;"2011-08-25";"2015-06-03";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    55506;"2015-06-04";"";-41.28442712299;174.774300910005;"147 The Terrace Wellington"
    
    55561;"2008-10-01";"2010-03-02";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55561;"2010-03-03";"2010-10-05";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55561;"2010-10-06";"2010-11-08";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55561;"2010-11-07";"2011-08-21";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55561;"2011-08-21";"2015-05-12";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55561;"2015-05-12";"2015-05-12";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55561;"2015-05-13";"2015-09-13";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    
    55562;"2008-10-01";"2010-03-02";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55562;"2010-03-03";"2010-10-05";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55562;"2010-10-06";"2010-11-08";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55562;"2010-11-07";"2011-08-21";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55562;"2011-08-21";"2015-05-12";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55562;"2015-05-12";"2015-05-12";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55562;"2015-05-13";"2015-09-13";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    
    55563;"2008-10-01";"2010-03-02";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55563;"2010-03-03";"2010-10-05";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55563;"2010-10-06";"2010-11-08";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55563;"2010-11-07";"2011-08-21";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55563;"2011-08-21";"2015-05-12";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55563;"2015-05-12";"2015-05-12";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    55563;"2015-05-13";"2015-09-13";-39.9270349809862;175.069379873269;"Bastia Hill Wanganui"
    
    55610;"2008-10-01";"2010-02-26";-40.75236022677;175.609255312232;"Off SH2 near Mt Bruce Wairarapa"
    55610;"2010-02-27";"2010-10-05";-40.75236022677;175.609255312232;"Off SH2 near Mt Bruce Wairarapa"
    55610;"2010-10-06";"2010-11-01";-40.75236022677;175.609255312232;"Off SH2 near Mt Bruce Wairarapa"
    55610;"2010-10-31";"2011-08-21";-40.75236022677;175.609255312232;"Off SH2 near Mt Bruce Wairarapa"
    55610;"2011-08-21";"2013-05-15";-40.75236022677;175.609255312232;"Off SH2 near Mt Bruce Wairarapa"
    55610;"2013-05-15";"2013-07-15";-40.75236022677;175.609255312232;"Off SH2 near Mt Bruce Wairarapa"
    55610;"2013-07-15";"2014-04-02";-40.75236022677;175.609255312232;"Off SH2 near Mt Bruce Wairarapa"
    55610;"2014-04-02";"2014-05-27";-40.75236022677;175.609255312232;"Off SH2 near Mt Bruce Wairarapa"
    55610;"2014-05-27";"2015-09-29";-40.75236022677;175.609255312232;"Off SH2 near Mt Bruce Wairarapa"
    55610;"2015-09-30";"2017-03-30";-40.75236022677;175.609255312232;"Off SH2 near Mt Bruce Wairarapa"
    
    55540;"2008-10-01";"2010-03-02";-39.9534061569606;175.492584190705;"Mt Curl Road Mt Ashcroft"
    55540;"2010-03-03";"2010-10-05";-39.9534061569606;175.492584190705;"Mt Curl Road Mt Ashcroft"
    55540;"2010-10-06";"2010-11-08";-39.9534061569606;175.492584190705;"Mt Curl Road Mt Ashcroft"
    55540;"2010-11-07";"2011-08-21";-39.9534061569606;175.492584190705;"Mt Curl Road Mt Ashcroft"
    55540;"2011-08-21";"2011-12-06";-39.9534061569606;175.492584190705;"Mt Curl Road Mt Ashcroft"
    
    55631;"2010-04-21";"2010-10-05";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    55631;"2010-10-06";"2010-11-08";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    55631;"2010-11-07";"2011-08-21";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    55631;"2011-08-21";"2015-05-12";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    55631;"2015-05-12";"2015-05-12";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    55631;"2015-05-13";"2015-09-14";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    
    55632;"2010-04-21";"2010-10-05";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    55632;"2010-10-06";"2010-11-08";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    55632;"2010-11-07";"2011-08-21";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    55632;"2011-08-21";"2015-05-12";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    55632;"2015-05-12";"2015-05-12";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    55632;"2015-05-13";"2015-09-14";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    
    55633;"2010-04-21";"2010-10-05";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    55633;"2010-10-06";"2010-11-08";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    55633;"2010-11-07";"2011-08-21";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    55633;"2011-08-21";"2015-05-12";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    55633;"2015-05-12";"2015-05-12";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    55633;"2015-05-13";"2015-09-14";-39.9516192835406;175.027962140063;"Wanganui Freezing Works Beach Road Wanganui"
    
    55670;"2008-10-01";"2010-02-26";-40.6508167317246;175.720930197459;"Off Alfredton Road Eketahuna"
    55670;"2010-02-27";"2010-10-05";-40.6508167317246;175.720930197459;"Off Alfredton Road Eketahuna"
    55670;"2010-10-06";"2010-11-01";-40.6508167317246;175.720930197459;"Off Alfredton Road Eketahuna"
    55670;"2010-10-31";"2011-08-21";-40.6508167317246;175.720930197459;"Off Alfredton Road Eketahuna"
    55670;"2011-08-21";"2013-05-15";-40.6508167317246;175.720930197459;"Off Alfredton Road Eketahuna"
    55670;"2013-05-15";"2013-07-15";-40.6508167317246;175.720930197459;"Off Alfredton Road Eketahuna"
    55670;"2013-07-15";"2014-04-02";-40.6508167317246;175.720930197459;"Off Alfredton Road Eketahuna"
    55670;"2014-04-02";"2014-05-27";-40.6508167317246;175.720930197459;"Off Alfredton Road Eketahuna"
    55670;"2014-05-27";"2015-09-29";-40.6508167317246;175.720930197459;"Off Alfredton Road Eketahuna"
    55670;"2015-09-30";"2017-03-08";-40.6508167317246;175.720930197459;"Off Alfredton Road Eketahuna"
    
    55711;"2010-10-14";"2010-10-30";-41.103081535611;175.311205822304;"Hill behind Featherston"
    55711;"2010-10-29";"2011-08-21";-41.103081535611;175.311205822304;"Hill behind Featherston"
    55711;"2011-08-21";"2013-05-15";-41.103081535611;175.311205822304;"Hill behind Featherston"
    55711;"2013-05-15";"2013-07-15";-41.103081535611;175.311205822304;"Hill behind Featherston"
    55711;"2013-07-15";"2014-04-02";-41.103081535611;175.311205822304;"Hill behind Featherston"
    55711;"2014-04-02";"2014-05-27";-41.103081535611;175.311205822304;"Hill behind Featherston"
    55711;"2014-05-27";"2015-09-29";-41.103081535611;175.311205822304;"Hill behind Featherston"
    55711;"2015-09-30";"2015-12-13";-41.103081535611;175.311205822304;"Hill behind Featherston"
    
    55712;"2010-10-14";"2010-10-30";-41.103081535611;175.311205822304;"Hill behind Featherston"
    55712;"2010-10-29";"2011-08-21";-41.103081535611;175.311205822304;"Hill behind Featherston"
    55712;"2011-08-21";"2013-05-15";-41.103081535611;175.311205822304;"Hill behind Featherston"
    55712;"2013-05-15";"2013-07-15";-41.103081535611;175.311205822304;"Hill behind Featherston"
    55712;"2013-07-15";"2014-04-02";-41.103081535611;175.311205822304;"Hill behind Featherston"
    55712;"2014-04-02";"2014-05-27";-41.103081535611;175.311205822304;"Hill behind Featherston"
    55712;"2014-05-27";"2015-09-29";-41.103081535611;175.311205822304;"Hill behind Featherston"
    55712;"2015-09-30";"2015-12-13";-41.103081535611;175.311205822304;"Hill behind Featherston"
    
    55731;"2010-03-29";"2010-10-05";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    55731;"2010-10-06";"2010-11-06";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    55731;"2010-11-05";"2011-08-21";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    55731;"2011-08-21";"2015-05-12";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    55731;"2015-05-12";"2015-05-12";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    55731;"2015-05-13";"2016-02-18";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    
    55732;"2010-03-29";"2010-10-05";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    55732;"2010-10-06";"2010-11-06";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    55732;"2010-11-05";"2011-08-21";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    55732;"2011-08-21";"2015-05-12";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    55732;"2015-05-12";"2015-05-12";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    55732;"2015-05-13";"2016-02-18";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    
    55733;"2010-03-29";"2010-10-05";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    55733;"2010-10-06";"2010-11-06";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    55733;"2010-11-05";"2011-08-21";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    55733;"2011-08-21";"2015-05-12";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    55733;"2015-05-12";"2015-05-12";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    55733;"2015-05-13";"2016-02-18";-40.2147773891443;175.552808962562;"Highfield Road Feilding"
    
    55763;"2008-10-01";"2010-03-01";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55763;"2010-03-02";"2010-10-05";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55763;"2010-10-06";"2010-11-09";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55763;"2010-11-08";"2011-08-21";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55763;"2011-08-21";"2013-12-05";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55763;"2013-12-06";"2013-12-08";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    
    55791;"2008-10-01";"2010-03-01";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55791;"2010-03-02";"2010-10-05";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55791;"2010-10-06";"2010-11-06";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55791;"2010-11-05";"2011-08-21";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55791;"2011-08-21";"2015-05-12";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55791;"2015-05-12";"2015-05-12";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55791;"2015-05-13";"2015-06-26";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55792;"2008-10-01";"2010-03-01";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55792;"2010-03-02";"2010-10-05";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55792;"2010-10-06";"2010-11-06";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55792;"2010-11-05";"2011-08-21";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55792;"2011-08-21";"2015-05-12";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55792;"2015-05-12";"2015-05-12";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55792;"2015-05-13";"2015-06-26";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55793;"2008-10-01";"2010-03-01";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55793;"2010-03-02";"2010-10-05";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55793;"2010-10-06";"2010-11-06";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55793;"2010-11-05";"2011-08-21";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55793;"2011-08-21";"2015-05-12";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55793;"2015-05-12";"2015-05-12";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    55793;"2015-05-13";"2015-06-26";-40.3676134496931;175.632798189008;"Manawatu Golf Course  Hokowhitu Palmerston North"
    
    55811;"2008-10-01";"2010-03-01";-40.5796142551751;175.401790837333;"Heights Road Shannon"
    55811;"2010-03-02";"2010-10-05";-40.5796142551751;175.401790837333;"Heights Road Shannon"
    55811;"2010-10-06";"2010-11-09";-40.5796142551751;175.401790837333;"Heights Road Shannon"
    55811;"2010-11-08";"2011-08-21";-40.5796142551751;175.401790837333;"Heights Road Shannon"
    55811;"2011-08-21";"2015-05-12";-40.5796142551751;175.401790837333;"Heights Road Shannon"
    55811;"2015-05-12";"2015-05-12";-40.5796142551751;175.401790837333;"Heights Road Shannon"
    55811;"2015-05-13";"2015-10-12";-40.5796142551751;175.401790837333;"Heights Road Shannon"
    
    55812;"2008-10-01";"2010-03-01";-40.5796142551751;175.401790837333;"Heights Road Shannon"
    55812;"2010-03-02";"2010-10-05";-40.5796142551751;175.401790837333;"Heights Road Shannon"
    55812;"2010-10-06";"2010-11-09";-40.5796142551751;175.401790837333;"Heights Road Shannon"
    55812;"2010-11-08";"2011-08-21";-40.5796142551751;175.401790837333;"Heights Road Shannon"
    55812;"2011-08-21";"2015-05-12";-40.5796142551751;175.401790837333;"Heights Road Shannon"
    55812;"2015-05-12";"2015-05-12";-40.5796142551751;175.401790837333;"Heights Road Shannon"
    55812;"2015-05-13";"2015-10-12";-40.5796142551751;175.401790837333;"Heights Road Shannon"
    
    55840;"2008-10-01";"2010-03-02";-39.9363187144078;175.610528027729;"Off Rangatira Road 4 Km east of Hunterville and 2 km south west of Vinegar Hill"
    55840;"2010-03-03";"2010-10-05";-39.9363187144078;175.610528027729;"Off Rangatira Road 4 Km east of Hunterville and 2 km south west of Vinegar Hill"
    55840;"2010-10-06";"2010-11-08";-39.9363187144078;175.610528027729;"Off Rangatira Road 4 Km east of Hunterville and 2 km south west of Vinegar Hill"
    55840;"2010-11-07";"2011-08-21";-39.9363187144078;175.610528027729;"Off Rangatira Road 4 Km east of Hunterville and 2 km south west of Vinegar Hill"
    55840;"2011-08-21";"2011-11-02";-39.9363187144078;175.610528027729;"Off Rangatira Road 4 Km east of Hunterville and 2 km south west of Vinegar Hill"
    
    55891;"2008-10-01";"2010-03-01";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55891;"2010-03-02";"2010-10-05";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55891;"2010-10-06";"2010-11-06";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55891;"2010-11-05";"2011-08-21";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55891;"2011-08-21";"2015-05-12";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55891;"2015-05-12";"2015-05-12";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55891;"2015-05-13";"2015-06-26";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    
    55892;"2008-10-01";"2010-03-01";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55892;"2010-03-02";"2010-10-05";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55892;"2010-10-06";"2010-11-06";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55892;"2010-11-05";"2011-08-21";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55892;"2011-08-21";"2015-05-12";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55892;"2015-05-12";"2015-05-12";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55892;"2015-05-13";"2015-06-26";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    
    55893;"2008-10-01";"2010-03-01";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55893;"2010-03-02";"2010-10-05";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55893;"2010-10-06";"2010-11-06";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55893;"2010-11-05";"2011-08-21";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55893;"2011-08-21";"2015-05-12";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55893;"2015-05-12";"2015-05-12";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    55893;"2015-05-13";"2015-06-26";-40.34054222338;175.600855004792;"21 Jasper Place Palmerston North"
    
    55920;"2008-10-01";"2010-03-02";-39.8490147008262;174.943448660274;"Hill on State H/Way No 3 Kai-Iwi"
    55920;"2010-03-03";"2010-10-05";-39.8490147008262;174.943448660274;"Hill on State H/Way No 3 Kai-Iwi"
    55920;"2010-10-06";"2010-11-08";-39.8490147008262;174.943448660274;"Hill on State H/Way No 3 Kai-Iwi"
    55920;"2010-11-07";"2011-08-21";-39.8490147008262;174.943448660274;"Hill on State H/Way No 3 Kai-Iwi"
    55920;"2011-08-21";"2012-02-28";-39.8490147008262;174.943448660274;"Hill on State H/Way No 3 Kai-Iwi"
    
    55961;"2010-03-31";"2010-10-05";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    55961;"2010-10-06";"2010-11-09";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    55961;"2010-11-08";"2011-08-21";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    55961;"2011-08-21";"2015-05-12";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    55961;"2015-05-12";"2015-05-12";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    55961;"2015-05-13";"2015-09-24";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    
    55962;"2010-03-31";"2010-10-05";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    55962;"2010-10-06";"2010-11-09";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    55962;"2010-11-08";"2011-08-21";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    55962;"2011-08-21";"2015-05-12";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    55962;"2015-05-12";"2015-05-12";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    55962;"2015-05-13";"2015-09-24";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    
    55963;"2010-03-31";"2010-10-05";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    55963;"2010-10-06";"2010-11-09";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    55963;"2010-11-08";"2011-08-21";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    55963;"2011-08-21";"2015-05-12";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    55963;"2015-05-12";"2015-05-12";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"
    55963;"2015-05-13";"2015-09-24";-40.6184651416665;175.288679303046;"8 - 10 Devon Street Levin"

    
    55991;"2010-05-29";"2010-10-05";-40.0678184155671;175.380145419086;"Telephone Exchange 10 Hammond Street Marton"
    55991;"2010-10-06";"2010-11-08";-40.0678184155671;175.380145419086;"Telephone Exchange 10 Hammond Street Marton"
    55991;"2010-11-07";"2011-08-21";-40.0678184155671;175.380145419086;"Telephone Exchange 10 Hammond Street Marton"
    55991;"2011-08-21";"2015-03-10";-40.0678184155671;175.380145419086;"Telephone Exchange 10 Hammond Street Marton"
    55991;"2015-03-11";"2015-03-26";-40.0678184155671;175.380145419086;"Telephone Exchange 10 Hammond Street Marton"
    
    55992;"2010-05-29";"2010-10-05";-40.0678184155671;175.380145419086;"Telephone Exchange 10 Hammond Street Marton"
    55992;"2010-10-06";"2010-11-08";-40.0678184155671;175.380145419086;"Telephone Exchange 10 Hammond Street Marton"
    55992;"2010-11-07";"2011-08-21";-40.0678184155671;175.380145419086;"Telephone Exchange 10 Hammond Street Marton"
    55992;"2011-08-21";"2015-03-10";-40.0678184155671;175.380145419086;"Telephone Exchange 10 Hammond Street Marton"
    55992;"2015-03-11";"2015-03-26";-40.0678184155671;175.380145419086;"Telephone Exchange 10 Hammond Street Marton"
    
    55993;"2010-05-29";"2010-10-05";-40.0678184155671;175.380145419086;"Telephone Exchange 10 Hammond Street Marton"
    55993;"2010-10-06";"2010-11-08";-40.0678184155671;175.380145419086;"Telephone Exchange 10 Hammond Street Marton"
    55993;"2010-11-07";"2011-08-21";-40.0678184155671;175.380145419086;"Telephone Exchange 10 Hammond Street Marton"
    55993;"2011-08-21";"2015-03-10";-40.0678184155671;175.380145419086;"Telephone Exchange 10 Hammond Street Marton"
    55993;"2015-03-11";"2015-03-26";-40.0678184155671;175.380145419086;"Telephone Exchange 10 Hammond Street Marton"
    
    56001;"2008-10-01";"2010-03-01";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56001;"2010-03-02";"2010-10-05";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56001;"2010-10-06";"2010-11-06";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56001;"2010-11-05";"2011-08-21";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56001;"2011-08-21";"2015-05-12";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56001;"2015-05-12";"2015-05-12";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56001;"2015-05-13";"2015-06-24";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    
    56002;"2008-10-01";"2010-03-01";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56002;"2010-03-02";"2010-10-05";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56002;"2010-10-06";"2010-11-06";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56002;"2010-11-05";"2011-08-21";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56002;"2011-08-21";"2015-05-12";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56002;"2015-05-12";"2015-05-12";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56002;"2015-05-13";"2015-06-24";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    
    56003;"2008-10-01";"2010-03-01";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56003;"2010-03-02";"2010-10-05";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56003;"2010-10-06";"2010-11-06";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56003;"2010-11-05";"2011-08-21";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56003;"2011-08-21";"2015-05-12";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56003;"2015-05-12";"2015-05-12";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    56003;"2015-05-13";"2015-06-24";-40.3782590616441;175.614799191811;"Batchelar Road adjacent to Tennent Drive Palmerston North (South)"
    
    56011;"2010-05-31";"2010-10-05";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56011;"2010-10-06";"2010-10-30";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56011;"2010-10-29";"2011-08-21";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56011;"2011-08-21";"2013-05-15";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56011;"2013-05-15";"2013-07-15";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56011;"2013-07-15";"2014-04-02";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56011;"2014-04-02";"2014-05-27";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56011;"2014-05-27";"2015-09-29";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56011;"2015-09-30";"2015-12-13";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56012;"2010-05-31";"2010-10-05";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56012;"2010-10-06";"2010-10-30";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56012;"2010-10-29";"2011-08-21";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56012;"2011-08-21";"2013-05-15";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56012;"2013-05-15";"2013-07-15";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56012;"2013-07-15";"2014-04-02";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56012;"2014-04-02";"2014-05-27";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56012;"2014-05-27";"2015-09-29";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56012;"2015-09-30";"2015-12-13";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56013;"2010-05-31";"2010-10-05";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56013;"2010-10-06";"2010-10-30";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56013;"2010-10-29";"2011-08-21";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56013;"2011-08-21";"2013-05-15";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56013;"2013-05-15";"2013-07-15";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56013;"2013-07-15";"2014-04-02";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56013;"2014-04-02";"2014-05-27";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56013;"2014-05-27";"2015-09-29";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    56013;"2015-09-30";"2015-12-13";-41.1820798543089;175.485589057821;"Riverside Road Martinborough"
    
    56021;"2008-10-01";"2010-03-05";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56021;"2010-03-06";"2010-10-05";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56021;"2010-10-08";"2010-11-05";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56021;"2010-11-04";"2011-08-21";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56021;"2011-08-21";"2015-05-12";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56021;"2015-05-12";"2015-05-12";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56021;"2015-05-13";"";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    
    56022;"2008-10-01";"2010-03-05";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56022;"2010-03-06";"2010-10-05";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56022;"2010-10-08";"2010-11-05";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56022;"2010-11-04";"2011-08-21";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56022;"2011-08-21";"2015-05-12";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56022;"2015-05-12";"2015-05-12";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56022;"2015-05-13";"";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    
    56023;"2008-10-01";"2010-03-05";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56023;"2010-03-06";"2010-10-05";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56023;"2010-10-08";"2010-11-05";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56023;"2010-11-04";"2011-08-21";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56023;"2011-08-21";"2015-05-12";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56023;"2015-05-12";"2015-05-12";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    56023;"2015-05-13";"";-40.5597657299387;176.037570612536;"Mt Butters Road Mt Butters Tararua"
    
    56041;"2008-10-01";"2010-03-01";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56041;"2010-03-02";"2010-10-05";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56041;"2010-10-06";"2010-11-06";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56041;"2010-11-05";"2011-08-21";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56041;"2011-08-21";"2015-05-12";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56041;"2015-05-12";"2015-05-12";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56041;"2015-05-13";"2015-06-26";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    
    56042;"2008-10-01";"2010-03-01";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56042;"2010-03-02";"2010-10-05";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56042;"2010-10-06";"2010-11-06";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56042;"2010-11-05";"2011-08-21";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56042;"2011-08-21";"2015-05-12";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56042;"2015-05-12";"2015-05-12";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56042;"2015-05-13";"2015-06-26";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    
    56043;"2008-10-01";"2010-03-01";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56043;"2010-03-02";"2010-10-05";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56043;"2010-10-06";"2010-11-06";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56043;"2010-11-05";"2011-08-21";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56043;"2011-08-21";"2015-05-12";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56043;"2015-05-12";"2015-05-12";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    56043;"2015-05-13";"2015-06-26";-40.3313057120473;175.621161557518;"Malden Street Palmerston North"
    
    56090;"2008-10-01";"2010-03-02";-39.827750554985;175.77123831744;"Off SH 1 south of Mangaweka"
    56090;"2010-03-03";"2010-10-05";-39.827750554985;175.77123831744;"Off SH 1 south of Mangaweka"
    56090;"2010-10-06";"2010-11-08";-39.827750554985;175.77123831744;"Off SH 1 south of Mangaweka"
    56090;"2010-11-07";"2011-08-21";-39.827750554985;175.77123831744;"Off SH 1 south of Mangaweka"
    56090;"2011-08-21";"2015-05-12";-39.827750554985;175.77123831744;"Off SH 1 south of Mangaweka"
    56090;"2015-05-12";"2015-05-12";-39.827750554985;175.77123831744;"Off SH 1 south of Mangaweka"
    56090;"2015-05-13";"";-39.827750554985;175.77123831744;"Off SH 1 south of Mangaweka"
    
    56131;"2008-10-01";"2010-02-26";-40.1020404380646;176.183245971981;"Blairgowrie Road Norsewood"
    56131;"2010-02-27";"2010-10-05";-40.1020404380646;176.183245971981;"Blairgowrie Road Norsewood"
    56131;"2010-10-06";"2010-11-05";-40.1020404380646;176.183245971981;"Blairgowrie Road Norsewood"
    56131;"2010-11-04";"2011-08-21";-40.1020404380646;176.183245971981;"Blairgowrie Road Norsewood"
    56131;"2011-08-21";"2015-05-12";-40.1020404380646;176.183245971981;"Blairgowrie Road Norsewood"
    56131;"2015-05-12";"2015-05-12";-40.1020404380646;176.183245971981;"Blairgowrie Road Norsewood"
    56131;"2015-05-13";"";-40.1020404380646;176.183245971981;"Blairgowrie Road Norsewood"
    
    56132;"2008-10-01";"2010-02-26";-40.1020404380646;176.183245971981;"Blairgowrie Road Norsewood"
    56132;"2010-02-27";"2010-10-05";-40.1020404380646;176.183245971981;"Blairgowrie Road Norsewood"
    56132;"2010-10-06";"2010-11-05";-40.1020404380646;176.183245971981;"Blairgowrie Road Norsewood"
    56132;"2010-11-04";"2011-08-21";-40.1020404380646;176.183245971981;"Blairgowrie Road Norsewood"
    56132;"2011-08-21";"2015-05-12";-40.1020404380646;176.183245971981;"Blairgowrie Road Norsewood"
    56132;"2015-05-12";"2015-05-12";-40.1020404380646;176.183245971981;"Blairgowrie Road Norsewood"
    56132;"2015-05-13";"";-40.1020404380646;176.183245971981;"Blairgowrie Road Norsewood"
    
    56151;"2008-10-01";"2010-03-01";-40.1968387760145;175.378886595572;"Ohakea Water Tower Hone Heke St Ohakea Airbase"
    56151;"2010-03-02";"2010-10-05";-40.1968387760145;175.378886595572;"Ohakea Water Tower Hone Heke St Ohakea Airbase"
    56151;"2010-10-06";"2010-11-08";-40.1968387760145;175.378886595572;"Ohakea Water Tower Hone Heke St Ohakea Airbase"
    56151;"2010-11-07";"2011-08-21";-40.1968387760145;175.378886595572;"Ohakea Water Tower Hone Heke St Ohakea Airbase"
    56151;"2011-08-21";"2015-05-12";-40.1968387760145;175.378886595572;"Ohakea Water Tower Hone Heke St Ohakea Airbase"
    56151;"2015-05-12";"2015-05-12";-40.1968387760145;175.378886595572;"Ohakea Water Tower Hone Heke St Ohakea Airbase"
    56151;"2015-05-13";"2017-02-18";-40.1968387760145;175.378886595572;"Ohakea Water Tower Hone Heke St Ohakea Airbase"
    56152;"2008-10-01";"2010-03-01";-40.1968387760145;175.378886595572;"Ohakea Water Tower Hone Heke St Ohakea Airbase"
    56152;"2010-03-02";"2010-10-05";-40.1968387760145;175.378886595572;"Ohakea Water Tower Hone Heke St Ohakea Airbase"
    56152;"2010-10-06";"2010-11-08";-40.1968387760145;175.378886595572;"Ohakea Water Tower Hone Heke St Ohakea Airbase"
    56152;"2010-11-07";"2011-08-21";-40.1968387760145;175.378886595572;"Ohakea Water Tower Hone Heke St Ohakea Airbase"
    56152;"2011-08-21";"2015-05-12";-40.1968387760145;175.378886595572;"Ohakea Water Tower Hone Heke St Ohakea Airbase"
    56152;"2015-05-12";"2015-05-12";-40.1968387760145;175.378886595572;"Ohakea Water Tower Hone Heke St Ohakea Airbase"
    56152;"2015-05-13";"2017-02-18";-40.1968387760145;175.378886595572;"Ohakea Water Tower Hone Heke St Ohakea Airbase"
    
    56181;"2008-10-01";"2010-02-26";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56181;"2010-02-27";"2010-10-05";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56181;"2010-10-06";"2010-11-05";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56181;"2010-11-04";"2011-08-21";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56181;"2011-08-21";"2015-05-12";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56181;"2015-05-12";"2015-05-12";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56181;"2015-05-13";"";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56182;"2008-10-01";"2010-02-26";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56182;"2010-02-27";"2010-10-05";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56182;"2010-10-06";"2010-11-05";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56182;"2010-11-04";"2011-08-21";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56182;"2011-08-21";"2015-05-12";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56182;"2015-05-12";"2015-05-12";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56182;"2015-05-13";"";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56183;"2008-10-01";"2010-02-26";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56183;"2010-02-27";"2010-10-05";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56183;"2010-10-06";"2010-11-05";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56183;"2010-11-04";"2011-08-21";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56183;"2011-08-21";"2015-05-12";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56183;"2015-05-12";"2015-05-12";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"
    56183;"2015-05-13";"";-39.9145654552703;176.45700818558;"Plantation Road Ongaonga Central Hawkes Bay"


    56201;"2008-10-01";"2010-03-01";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56201;"2010-03-02";"2010-10-05";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56201;"2010-10-06";"2010-11-09";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56201;"2010-11-08";"2011-08-21";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56201;"2011-08-21";"2015-05-12";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56201;"2015-05-12";"2015-05-12";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56201;"2015-05-13";"2017-02-19";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    
    56202;"2008-10-01";"2010-03-01";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56202;"2010-03-02";"2010-10-05";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56202;"2010-10-06";"2010-11-09";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56202;"2010-11-08";"2011-08-21";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56202;"2011-08-21";"2015-05-12";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56202;"2015-05-12";"2015-05-12";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56202;"2015-05-13";"2017-02-19";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    
    56203;"2008-10-01";"2010-03-01";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56203;"2010-03-02";"2010-10-05";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56203;"2010-10-06";"2010-11-09";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56203;"2010-11-08";"2011-08-21";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56203;"2011-08-21";"2015-05-12";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56203;"2015-05-12";"2015-05-12";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    56203;"2015-05-13";"2017-02-19";-40.3549772637311;175.348987103691;"Taikorea Road Oroua Downs-Waitohi Manawatu District"
    
    56210;"2008-10-01";"2010-02-26";-40.4256115630704;175.852605427881;"Hill north of Pahiatua (just off) State Highway 2"
    56210;"2010-02-27";"2010-10-05";-40.4256115630704;175.852605427881;"Hill north of Pahiatua (just off) State Highway 2"
    56210;"2010-10-06";"2010-11-05";-40.4256115630704;175.852605427881;"Hill north of Pahiatua (just off) State Highway 2"
    56210;"2010-11-04";"2011-05-29";-40.4256115630704;175.852605427881;"Hill north of Pahiatua (just off) State Highway 2"
    
    56221;"2010-07-08";"2010-10-05";-40.742338041889;175.18973378445;"Off Waitohu Valley Road Pukehou"
    56221;"2010-10-06";"2010-11-09";-40.742338041889;175.18973378445;"Off Waitohu Valley Road Pukehou"
    56221;"2010-11-08";"2011-08-21";-40.742338041889;175.18973378445;"Off Waitohu Valley Road Pukehou"
    56221;"2011-08-21";"2015-05-12";-40.742338041889;175.18973378445;"Off Waitohu Valley Road Pukehou"
    56221;"2015-05-12";"2015-05-12";-40.742338041889;175.18973378445;"Off Waitohu Valley Road Pukehou"
    56221;"2015-05-13";"2017-03-16";-40.742338041889;175.18973378445;"Off Waitohu Valley Road Pukehou"
    
    56222;"2010-07-08";"2010-10-05";-40.742338041889;175.18973378445;"Off Waitohu Valley Road Pukehou"
    56222;"2010-10-06";"2010-11-09";-40.742338041889;175.18973378445;"Off Waitohu Valley Road Pukehou"
    56222;"2010-11-08";"2011-08-21";-40.742338041889;175.18973378445;"Off Waitohu Valley Road Pukehou"
    56222;"2011-08-21";"2015-05-12";-40.742338041889;175.18973378445;"Off Waitohu Valley Road Pukehou"
    56222;"2015-05-12";"2015-05-12";-40.742338041889;175.18973378445;"Off Waitohu Valley Road Pukehou"
    56222;"2015-05-13";"2017-03-16";-40.742338041889;175.18973378445;"Off Waitohu Valley Road Pukehou"
    
    56241;"2008-10-01";"2010-03-01";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56241;"2010-03-02";"2010-10-05";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56241;"2010-10-06";"2010-11-06";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56241;"2010-11-05";"2011-08-21";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56241;"2011-08-21";"2015-05-12";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56241;"2015-05-12";"2015-05-12";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56241;"2015-05-13";"2015-06-26";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    
    56242;"2008-10-01";"2010-03-01";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56242;"2010-03-02";"2010-10-05";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56242;"2010-10-06";"2010-11-06";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56242;"2010-11-05";"2011-08-21";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56242;"2011-08-21";"2015-05-12";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56242;"2015-05-12";"2015-05-12";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56242;"2015-05-13";"2015-06-26";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    
    56243;"2008-10-01";"2010-03-01";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56243;"2010-03-02";"2010-10-05";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56243;"2010-10-06";"2010-11-06";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56243;"2010-11-05";"2011-08-21";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56243;"2011-08-21";"2015-05-12";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56243;"2015-05-12";"2015-05-12";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    56243;"2015-05-13";"2015-06-26";-40.3554294039957;175.614691075595;"486-498 Main Street Palmerston North"
    
    56261;"2008-10-01";"2010-03-01";-40.3319283469566;175.791083797208;"North Range Road Microwave Tower Palmerston North"
    56261;"2010-03-02";"2010-10-05";-40.3319283469566;175.791083797208;"North Range Road Microwave Tower Palmerston North"
    56261;"2010-10-06";"2010-11-06";-40.3319283469566;175.791083797208;"North Range Road Microwave Tower Palmerston North"
    56261;"2010-11-05";"2011-08-21";-40.3319283469566;175.791083797208;"North Range Road Microwave Tower Palmerston North"
    56261;"2011-08-21";"2015-05-12";-40.3319283469566;175.791083797208;"North Range Road Microwave Tower Palmerston North"
    56261;"2015-05-12";"2015-05-12";-40.3319283469566;175.791083797208;"North Range Road Microwave Tower Palmerston North"
    56261;"2015-05-13";"";-40.3319283469566;175.791083797208;"North Range Road Microwave Tower Palmerston North"
    
    56262;"2008-10-01";"2010-03-01";-40.3319283469566;175.791083797208;"North Range Road Microwave Tower Palmerston North"
    56262;"2010-03-02";"2010-10-05";-40.3319283469566;175.791083797208;"North Range Road Microwave Tower Palmerston North"
    56262;"2010-10-06";"2010-11-06";-40.3319283469566;175.791083797208;"North Range Road Microwave Tower Palmerston North"
    56262;"2010-11-05";"2011-08-21";-40.3319283469566;175.791083797208;"North Range Road Microwave Tower Palmerston North"
    56262;"2011-08-21";"2015-05-12";-40.3319283469566;175.791083797208;"North Range Road Microwave Tower Palmerston North"
    56262;"2015-05-12";"2015-05-12";-40.3319283469566;175.791083797208;"North Range Road Microwave Tower Palmerston North"
    56262;"2015-05-13";"";-40.3319283469566;175.791083797208;"North Range Road Microwave Tower Palmerston North"
    
    56280;"2008-10-01";"2010-03-05";-41.052187535442;176.075165951933;"Near View Trig Riversdale Masterton"
    56280;"2010-03-06";"2010-10-05";-41.052187535442;176.075165951933;"Near View Trig Riversdale Masterton"
    56280;"2010-10-06";"2010-11-01";-41.052187535442;176.075165951933;"Near View Trig Riversdale Masterton"
    56280;"2010-10-31";"2011-08-21";-41.052187535442;176.075165951933;"Near View Trig Riversdale Masterton"
    56280;"2011-08-21";"2013-06-18";-41.052187535442;176.075165951933;"Near View Trig Riversdale Masterton"
    56280;"2013-06-18";"2015-05-12";-41.052187535442;176.075165951933;"Near View Trig Riversdale Masterton"
    56280;"2015-05-12";"2015-05-12";-41.052187535442;176.075165951933;"Near View Trig Riversdale Masterton"
    56280;"2015-05-13";"2016-12-20";-41.052187535442;176.075165951933;"Near View Trig Riversdale Masterton"
    
    56301;"2008-10-01";"2010-02-25";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56301;"2010-02-26";"2010-10-05";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56301;"2010-10-06";"2010-11-01";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56301;"2010-10-31";"2011-08-21";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56301;"2011-08-21";"2013-05-15";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56301;"2013-05-15";"2013-07-15";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56301;"2013-07-15";"2014-04-02";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56301;"2014-04-02";"2014-05-27";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56301;"2014-05-27";"2015-09-29";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56301;"2015-09-30";"2017-02-11";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    
    56302;"2008-10-01";"2010-02-25";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56302;"2010-02-26";"2010-10-05";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56302;"2010-10-06";"2010-11-01";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56302;"2010-10-31";"2011-08-21";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56302;"2011-08-21";"2013-05-15";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56302;"2013-05-15";"2013-07-15";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56302;"2013-07-15";"2014-04-02";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56302;"2014-04-02";"2014-05-27";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56302;"2014-05-27";"2015-09-29";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56302;"2015-09-30";"2017-02-11";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    
    56303;"2008-10-01";"2010-02-25";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56303;"2010-02-26";"2010-10-05";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56303;"2010-10-06";"2010-11-01";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56303;"2010-10-31";"2011-08-21";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56303;"2011-08-21";"2013-05-15";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56303;"2013-05-15";"2013-07-15";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56303;"2013-07-15";"2014-04-02";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56303;"2014-04-02";"2014-05-27";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56303;"2014-05-27";"2015-09-29";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    56303;"2015-09-30";"2017-02-11";-40.9586271447646;175.621220038758;"Solway Hotel Main Road Masterton"
    
    56311;"2008-10-01";"2010-03-01";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56311;"2010-03-02";"2010-10-05";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56311;"2010-10-06";"2010-11-06";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56311;"2010-11-05";"2011-08-21";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56311;"2011-08-21";"2015-05-12";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56311;"2015-05-12";"2015-05-12";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56311;"2015-05-13";"2015-06-26";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    
    56312;"2008-10-01";"2010-03-01";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56312;"2010-03-02";"2010-10-05";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56312;"2010-10-06";"2010-11-06";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56312;"2010-11-05";"2011-08-21";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56312;"2011-08-21";"2015-05-12";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56312;"2015-05-12";"2015-05-12";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56312;"2015-05-13";"2015-06-26";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    
    56313;"2008-10-01";"2010-03-01";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56313;"2010-03-02";"2010-10-05";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56313;"2010-10-06";"2010-11-06";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56313;"2010-11-05";"2011-08-21";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56313;"2011-08-21";"2015-05-12";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56313;"2015-05-12";"2015-05-12";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    56313;"2015-05-13";"2015-06-26";-40.3471571685818;175.628184007464;"St Peters Church Ruahine Street Palmerston North (intersection of Broadway Ave)"
    
    56331;"2008-10-01";"2010-03-01";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56331;"2010-03-02";"2010-10-05";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56331;"2010-10-06";"2010-11-06";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56331;"2010-11-05";"2011-08-21";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56331;"2011-08-21";"2015-05-12";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56331;"2015-05-12";"2015-05-12";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56331;"2015-05-13";"2015-06-26";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56332;"2008-10-01";"2010-03-01";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56332;"2010-03-02";"2010-10-05";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56332;"2010-10-06";"2010-11-06";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56332;"2010-11-05";"2011-08-21";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56332;"2011-08-21";"2015-05-12";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56332;"2015-05-12";"2015-05-12";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56332;"2015-05-13";"2015-06-26";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56333;"2008-10-01";"2010-03-01";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56333;"2010-03-02";"2010-10-05";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56333;"2010-10-06";"2010-11-06";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56333;"2010-11-05";"2011-08-21";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56333;"2011-08-21";"2015-05-12";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56333;"2015-05-12";"2015-05-12";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56333;"2015-05-13";"2015-06-26";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56334;"2010-10-28";"2010-11-06";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56334;"2010-11-05";"2011-07-06";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56334;"2011-07-07";"2011-08-21";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56334;"2011-08-21";"2015-05-12";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56334;"2015-05-12";"2015-05-12";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56334;"2015-05-13";"2015-06-26";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56335;"2010-10-28";"2010-11-06";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56335;"2010-11-05";"2011-07-06";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56335;"2011-07-07";"2011-08-21";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56335;"2011-08-21";"2015-05-12";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56335;"2015-05-12";"2015-05-12";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56335;"2015-05-13";"2015-06-26";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56336;"2010-10-28";"2010-11-06";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56336;"2010-11-05";"2011-07-06";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56336;"2011-07-07";"2011-08-21";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56336;"2011-08-21";"2015-05-12";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56336;"2015-05-12";"2015-05-12";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    56336;"2015-05-13";"2015-06-26";-40.3559182117642;175.6014599521;"Palmerston North Speedway Cuba St Palmerston North"
    
    56351;"2008-10-01";"2010-03-01";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56351;"2010-03-02";"2010-10-05";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56351;"2010-10-06";"2010-11-06";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56351;"2010-11-05";"2011-08-21";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56351;"2011-08-21";"2015-05-12";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56351;"2015-05-12";"2015-05-12";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56351;"2015-05-13";"2015-06-26";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56352;"2008-10-01";"2010-03-01";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56352;"2010-03-02";"2010-10-05";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56352;"2010-10-06";"2010-11-06";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56352;"2010-11-05";"2011-08-21";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56352;"2011-08-21";"2015-05-12";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56352;"2015-05-12";"2015-05-12";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56352;"2015-05-13";"2015-06-26";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56353;"2008-10-01";"2010-03-01";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56353;"2010-03-02";"2010-10-05";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56353;"2010-10-06";"2010-11-06";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56353;"2010-11-05";"2011-08-21";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56353;"2011-08-21";"2015-05-12";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56353;"2015-05-12";"2015-05-12";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    56353;"2015-05-13";"2015-06-26";-40.3383400232321;175.642971190423;"Mihaere Drive (Terrace end) Palmerston North"
    
    56361;"2008-10-01";"2010-03-01";-40.8444503977746;175.102853635874;"Hadfield Road Forest Heights Te Horo"
    56361;"2010-03-02";"2010-10-05";-40.8444503977746;175.102853635874;"Hadfield Road Forest Heights Te Horo"
    56361;"2010-10-06";"2010-11-09";-40.8444503977746;175.102853635874;"Hadfield Road Forest Heights Te Horo"
    56361;"2010-11-08";"2011-05-23";-40.8444503977746;175.102853635874;"Hadfield Road Forest Heights Te Horo"
    56361;"2011-05-24";"2011-08-21";-40.8444503977746;175.102853635874;"Hadfield Road Forest Heights Te Horo"
    56361;"2011-08-21";"2015-05-12";-40.8444503977746;175.102853635874;"Hadfield Road Forest Heights Te Horo"
    56361;"2015-05-12";"2015-05-12";-40.8444503977746;175.102853635874;"Hadfield Road Forest Heights Te Horo"
    56361;"2015-05-13";"2015-09-29";-40.8444503977746;175.102853635874;"Hadfield Road Forest Heights Te Horo"
    
    56491;"2008-10-01";"2010-03-01";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56491;"2010-03-02";"2010-10-05";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56491;"2010-10-06";"2010-11-06";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56491;"2010-11-05";"2011-08-21";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56491;"2011-08-21";"2015-05-12";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56491;"2015-05-12";"2015-05-12";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56491;"2015-05-13";"2015-06-24";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56492;"2008-10-01";"2010-03-01";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56492;"2010-03-02";"2010-10-05";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56492;"2010-10-06";"2010-11-06";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56492;"2010-11-05";"2011-08-21";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56492;"2011-08-21";"2015-05-12";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56492;"2015-05-12";"2015-05-12";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56492;"2015-05-13";"2015-06-24";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56493;"2008-10-01";"2010-03-01";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56493;"2010-03-02";"2010-10-05";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56493;"2010-10-06";"2010-11-06";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56493;"2010-11-05";"2011-08-21";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56493;"2011-08-21";"2015-05-12";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56493;"2015-05-12";"2015-05-12";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    56493;"2015-05-13";"2015-06-24";-40.3685057960119;175.572566562939;"Manawatu Trotting Club Pioneer Highway Palmerston North"
    
    56501;"2008-10-01";"2010-02-26";-40.2883191493708;175.971597910657;"State Highway 2 Waiaruhe Dannevirke"
    56501;"2010-02-27";"2010-10-05";-40.2883191493708;175.971597910657;"State Highway 2 Waiaruhe Dannevirke"
    56501;"2010-10-06";"2010-11-05";-40.2883191493708;175.971597910657;"State Highway 2 Waiaruhe Dannevirke"
    56501;"2010-11-04";"2011-08-21";-40.2883191493708;175.971597910657;"State Highway 2 Waiaruhe Dannevirke"
    56501;"2011-08-21";"2015-05-12";-40.2883191493708;175.971597910657;"State Highway 2 Waiaruhe Dannevirke"
    56501;"2015-05-12";"2015-05-12";-40.2883191493708;175.971597910657;"State Highway 2 Waiaruhe Dannevirke"
    56501;"2015-05-13";"";-40.2883191493708;175.971597910657;"State Highway 2 Waiaruhe Dannevirke"
    
    56502;"2008-10-01";"2010-02-26";-40.2883191493708;175.971597910657;"State Highway 2 Waiaruhe Dannevirke"
    56502;"2010-02-27";"2010-10-05";-40.2883191493708;175.971597910657;"State Highway 2 Waiaruhe Dannevirke"
    56502;"2010-10-06";"2010-11-05";-40.2883191493708;175.971597910657;"State Highway 2 Waiaruhe Dannevirke"
    56502;"2010-11-04";"2011-08-21";-40.2883191493708;175.971597910657;"State Highway 2 Waiaruhe Dannevirke"
    56502;"2011-08-21";"2015-05-12";-40.2883191493708;175.971597910657;"State Highway 2 Waiaruhe Dannevirke"
    56502;"2015-05-12";"2015-05-12";-40.2883191493708;175.971597910657;"State Highway 2 Waiaruhe Dannevirke"
    56502;"2015-05-13";"";-40.2883191493708;175.971597910657;"State Highway 2 Waiaruhe Dannevirke"
    
    56511;"2008-10-01";"2010-03-01";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    56511;"2010-03-02";"2010-10-05";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    56511;"2010-10-06";"2010-11-09";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    56511;"2010-11-08";"2011-08-21";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    56511;"2011-08-21";"2015-05-12";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    56511;"2015-05-12";"2015-05-12";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    
    56512;"2008-10-01";"2010-03-01";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    56512;"2010-03-02";"2010-10-05";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    56512;"2010-10-06";"2010-11-09";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    56512;"2010-11-08";"2011-08-21";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    56512;"2011-08-21";"2015-05-12";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    56512;"2015-05-12";"2015-05-12";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    
    56611;"2009-03-04";"2010-03-07";-41.2619945697589;174.775869892289;"77 Wadestown Road Wadestown Wellington"
    56611;"2010-03-08";"2010-11-12";-41.2619945697589;174.775869892289;"77 Wadestown Road Wadestown Wellington"
    56611;"2010-11-11";"2011-08-14";-41.2619945697589;174.775869892289;"77 Wadestown Road Wadestown Wellington"
    56611;"2011-08-14";"2015-06-03";-41.2619945697589;174.775869892289;"77 Wadestown Road Wadestown Wellington"
    56611;"2015-06-04";"";-41.2619945697589;174.775869892289;"77 Wadestown Road Wadestown Wellington"
    
    56612;"2009-03-04";"2010-03-07";-41.2619945697589;174.775869892289;"77 Wadestown Road Wadestown Wellington"
    56612;"2010-03-08";"2010-11-12";-41.2619945697589;174.775869892289;"77 Wadestown Road Wadestown Wellington"
    56612;"2010-11-11";"2011-08-14";-41.2619945697589;174.775869892289;"77 Wadestown Road Wadestown Wellington"
    56612;"2011-08-14";"2015-06-03";-41.2619945697589;174.775869892289;"77 Wadestown Road Wadestown Wellington"
    56612;"2015-06-04";"";-41.2619945697589;174.775869892289;"77 Wadestown Road Wadestown Wellington"
    
    56613;"2009-03-04";"2010-03-07";-41.2619945697589;174.775869892289;"77 Wadestown Road Wadestown Wellington"
    56613;"2010-03-08";"2010-11-12";-41.2619945697589;174.775869892289;"77 Wadestown Road Wadestown Wellington"
    56613;"2010-11-11";"2011-08-14";-41.2619945697589;174.775869892289;"77 Wadestown Road Wadestown Wellington"
    56613;"2011-08-14";"2015-06-03";-41.2619945697589;174.775869892289;"77 Wadestown Road Wadestown Wellington"
    56613;"2015-06-04";"";-41.2619945697589;174.775869892289;"77 Wadestown Road Wadestown Wellington"
    
    56621;"2009-02-25";"2010-03-07";-41.2867650898477;174.754447177115;"4 Birdwood Street Karori Wellington"
    56621;"2010-03-08";"2010-11-12";-41.2867650898477;174.754447177115;"4 Birdwood Street Karori Wellington"
    56621;"2010-11-11";"2011-08-14";-41.2867650898477;174.754447177115;"4 Birdwood Street Karori Wellington"
    56621;"2011-08-14";"2015-06-03";-41.2867650898477;174.754447177115;"4 Birdwood Street Karori Wellington"
    56621;"2015-06-04";"";-41.2867650898477;174.754447177115;"4 Birdwood Street Karori Wellington"
    
    56622;"2009-02-25";"2010-03-07";-41.2867650898477;174.754447177115;"4 Birdwood Street Karori Wellington"
    56622;"2010-03-08";"2010-11-12";-41.2867650898477;174.754447177115;"4 Birdwood Street Karori Wellington"
    56622;"2010-11-11";"2011-08-14";-41.2867650898477;174.754447177115;"4 Birdwood Street Karori Wellington"
    56622;"2011-08-14";"2015-06-03";-41.2867650898477;174.754447177115;"4 Birdwood Street Karori Wellington"
    56622;"2015-06-04";"";-41.2867650898477;174.754447177115;"4 Birdwood Street Karori Wellington"
    
    56623;"2009-02-25";"2010-03-07";-41.2867650898477;174.754447177115;"4 Birdwood Street Karori Wellington"
    56623;"2010-03-08";"2010-11-12";-41.2867650898477;174.754447177115;"4 Birdwood Street Karori Wellington"
    56623;"2010-11-11";"2011-08-14";-41.2867650898477;174.754447177115;"4 Birdwood Street Karori Wellington"
    56623;"2011-08-14";"2015-06-03";-41.2867650898477;174.754447177115;"4 Birdwood Street Karori Wellington"
    56623;"2015-06-04";"";-41.2867650898477;174.754447177115;"4 Birdwood Street Karori Wellington"
    
    56420;"2008-10-01";"2010-03-02";-40.0287014680415;175.193836245752;"Glasgow Property SH3 Turakina Wanganui"
    56420;"2010-03-03";"2010-10-05";-40.0287014680415;175.193836245752;"Glasgow Property SH3 Turakina Wanganui"
    56420;"2010-10-06";"2010-11-08";-40.0287014680415;175.193836245752;"Glasgow Property SH3 Turakina Wanganui"
    56420;"2010-11-07";"2011-08-21";-40.0287014680415;175.193836245752;"Glasgow Property SH3 Turakina Wanganui"
    56420;"2011-08-21";"2011-11-08";-40.0287014680415;175.193836245752;"Glasgow Property SH3 Turakina Wanganui"
    
    56451;"2010-05-31";"2010-10-05";-39.9930466244354;176.594924840429;"Located off Mangatarata Road east of Waipukurau and SH 2"
    56451;"2010-10-06";"2010-11-05";-39.9930466244354;176.594924840429;"Located off Mangatarata Road east of Waipukurau and SH 2"
    56451;"2010-11-04";"2011-08-21";-39.9930466244354;176.594924840429;"Located off Mangatarata Road east of Waipukurau and SH 2"
    56451;"2011-08-21";"2014-04-10";-39.9930466244354;176.594924840429;"Located off Mangatarata Road east of Waipukurau and SH 2"
    56451;"2014-04-11";"2014-05-03";-39.9930466244354;176.594924840429;"Located off Mangatarata Road east of Waipukurau and SH 2"
    
    56452;"2010-05-31";"2010-10-05";-39.9930466244354;176.594924840429;"Located off Mangatarata Road east of Waipukurau and SH 2"
    56452;"2010-10-06";"2010-11-05";-39.9930466244354;176.594924840429;"Located off Mangatarata Road east of Waipukurau and SH 2"
    56452;"2010-11-04";"2011-08-21";-39.9930466244354;176.594924840429;"Located off Mangatarata Road east of Waipukurau and SH 2"
    56452;"2011-08-21";"2014-04-10";-39.9930466244354;176.594924840429;"Located off Mangatarata Road east of Waipukurau and SH 2"
    56452;"2014-04-11";"2014-05-03";-39.9930466244354;176.594924840429;"Located off Mangatarata Road east of Waipukurau and SH 2"
    
    56453;"2010-05-31";"2010-10-05";-39.9930466244354;176.594924840429;"Located off Mangatarata Road east of Waipukurau and SH 2"
    56453;"2010-10-06";"2010-11-05";-39.9930466244354;176.594924840429;"Located off Mangatarata Road east of Waipukurau and SH 2"
    56453;"2010-11-04";"2011-08-21";-39.9930466244354;176.594924840429;"Located off Mangatarata Road east of Waipukurau and SH 2"
    56453;"2011-08-21";"2014-04-10";-39.9930466244354;176.594924840429;"Located off Mangatarata Road east of Waipukurau and SH 2"
    56453;"2014-04-11";"2014-05-03";-39.9930466244354;176.594924840429;"Located off Mangatarata Road east of Waipukurau and SH 2"    
    
    56651;"2008-10-01";"2010-03-08";-41.3157263049759;174.880935545437;"Breaker Bay Network Site Camp Bay Eastbourne Wellington"
    56651;"2010-03-09";"2010-11-16";-41.3157263049759;174.880935545437;"Breaker Bay Network Site Camp Bay Eastbourne Wellington"
    56651;"2010-11-15";"2011-08-14";-41.3157263049759;174.880935545437;"Breaker Bay Network Site Camp Bay Eastbourne Wellington"
    56651;"2011-08-14";"2013-10-31";-41.3157263049759;174.880935545437;"Breaker Bay Network Site Camp Bay Eastbourne Wellington"
    56651;"2013-11-01";"2014-02-16";-41.3157263049759;174.880935545437;"Breaker Bay Network Site Camp Bay Eastbourne Wellington"
    
    56661;"2008-10-01";"2010-03-07";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56661;"2010-03-08";"2010-11-11";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56661;"2010-11-10";"2011-08-14";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56661;"2011-08-14";"2011-08-25";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56661;"2011-08-26";"2013-05-15";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56661;"2013-05-15";"2013-07-15";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56661;"2013-07-15";"2014-04-02";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56661;"2014-04-02";"2014-05-27";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56661;"2014-05-27";"2015-09-29";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56661;"2015-09-30";"2016-12-08";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    
    56662;"2008-10-01";"2010-03-07";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56662;"2010-03-08";"2010-11-11";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56662;"2010-11-10";"2011-08-14";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56662;"2011-08-14";"2011-08-25";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56662;"2011-08-26";"2013-05-15";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56662;"2013-05-15";"2013-07-15";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56662;"2013-07-15";"2014-04-02";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56662;"2014-04-02";"2014-05-27";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56662;"2014-05-27";"2015-09-29";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56662;"2015-09-30";"2016-12-08";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    
    56663;"2008-10-01";"2010-03-07";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56663;"2010-03-08";"2010-11-11";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56663;"2010-11-10";"2011-08-14";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56663;"2011-08-14";"2011-08-25";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56663;"2011-08-26";"2013-05-15";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56663;"2013-05-15";"2013-07-15";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56663;"2013-07-15";"2014-04-02";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56663;"2014-04-02";"2014-05-27";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56663;"2014-05-27";"2015-09-29";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    56663;"2015-09-30";"2016-12-08";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    
    56671;"2008-10-01";"2010-03-07";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56671;"2010-03-08";"2010-11-11";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56671;"2010-11-10";"2011-08-14";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56671;"2011-08-14";"2011-08-25";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56671;"2011-08-26";"2013-05-15";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56671;"2013-05-15";"2013-07-15";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56671;"2013-07-15";"";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    
    56672;"2008-10-01";"2010-03-07";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56672;"2010-03-08";"2010-11-11";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56672;"2010-11-10";"2011-08-14";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56672;"2011-08-14";"2011-08-25";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56672;"2011-08-26";"2013-05-15";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56672;"2013-05-15";"2013-07-15";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56672;"2013-07-15";"";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    
    56673;"2008-10-01";"2010-03-07";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56673;"2010-03-08";"2010-11-11";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56673;"2010-11-10";"2011-08-14";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56673;"2011-08-14";"2011-08-25";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56673;"2011-08-26";"2013-05-15";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56673;"2013-05-15";"2013-07-15";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    56673;"2013-07-15";"";-41.2027260517101;174.820761336172;"4 Westchester Drive ChurtonPark(motorway off ramp) Wellington"
    
    56681;"2008-10-01";"2010-03-08";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56681;"2010-03-09";"2010-11-12";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56681;"2010-11-11";"2011-08-14";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56681;"2011-08-14";"2011-08-25";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56681;"2011-08-26";"2013-05-15";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56681;"2013-05-15";"2013-07-15";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56681;"2013-07-15";"";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    
    56682;"2008-10-01";"2010-03-08";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56682;"2010-03-09";"2010-11-12";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56682;"2010-11-11";"2011-08-14";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56682;"2011-08-14";"2011-08-25";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56682;"2011-08-26";"2013-05-15";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56682;"2013-05-15";"2013-07-15";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56682;"2013-07-15";"";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    
    56683;"2009-08-07";"2010-03-08";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56683;"2010-03-09";"2010-11-12";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56683;"2010-11-11";"2011-08-14";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56683;"2011-08-14";"2011-08-25";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56683;"2011-08-26";"2013-05-15";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56683;"2013-05-15";"2013-07-15";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    56683;"2013-07-15";"";-41.2426088647154;174.811539626966;"Tyres Road Ngauranga Gorge Wellington"
    
    56691;"2008-10-01";"2010-02-15";-41.3384344431123;174.756277758007;"169 Happy Valley Road Wellington"
    56691;"2010-02-16";"2010-11-07";-41.3384344431123;174.756277758007;"169 Happy Valley Road Wellington"
    56691;"2010-11-06";"2011-08-14";-41.3384344431123;174.756277758007;"169 Happy Valley Road Wellington"
    56691;"2011-08-14";"2015-06-03";-41.3384344431123;174.756277758007;"169 Happy Valley Road Wellington"
    56691;"2015-06-04";"";-41.3384344431123;174.756277758007;"169 Happy Valley Road Wellington"
    
    56692;"2008-10-01";"2010-02-15";-41.3384344431123;174.756277758007;"169 Happy Valley Road Wellington"
    56692;"2010-02-16";"2010-11-07";-41.3384344431123;174.756277758007;"169 Happy Valley Road Wellington"
    56692;"2010-11-06";"2011-08-14";-41.3384344431123;174.756277758007;"169 Happy Valley Road Wellington"
    56692;"2011-08-14";"2015-06-03";-41.3384344431123;174.756277758007;"169 Happy Valley Road Wellington"
    56692;"2015-06-04";"";-41.3384344431123;174.756277758007;"169 Happy Valley Road Wellington"
    
    56701;"2008-10-01";"2010-03-10";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56701;"2010-03-11";"2010-11-17";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56701;"2010-11-16";"2011-08-14";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56701;"2011-08-14";"2011-08-25";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56701;"2011-08-26";"2013-05-15";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56701;"2013-05-15";"2013-07-15";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56701;"2013-07-15";"2014-04-02";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56701;"2014-04-02";"2014-05-27";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56701;"2014-05-27";"";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    
    56702;"2008-10-01";"2010-03-10";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56702;"2010-03-11";"2010-11-17";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56702;"2010-11-16";"2011-08-14";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56702;"2011-08-14";"2011-08-25";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56702;"2011-08-26";"2013-05-15";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56702;"2013-05-15";"2013-07-15";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56702;"2013-07-15";"2014-04-02";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56702;"2014-04-02";"2014-05-27";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56702;"2014-05-27";"";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    
    56703;"2008-10-01";"2010-03-10";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56703;"2010-03-11";"2010-11-17";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56703;"2010-11-16";"2011-08-14";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56703;"2011-08-14";"2011-08-25";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56703;"2011-08-26";"2013-05-15";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56703;"2013-05-15";"2013-07-15";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56703;"2013-07-15";"2014-04-02";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56703;"2014-04-02";"2014-05-27";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    56703;"2014-05-27";"";-41.1541154392724;174.982467760531;"Manor Park Golf Club Manor Park Lower Hutt"
    
    56710;"2008-10-01";"2010-03-08";-41.3383321526384;174.774223538012;"Cnr Mersey Street and Clyde Street Island Bay Wellington"
    56710;"2010-03-09";"2010-11-16";-41.3383321526384;174.774223538012;"Cnr Mersey Street and Clyde Street Island Bay Wellington"
    56710;"2010-11-15";"2011-08-14";-41.3383321526384;174.774223538012;"Cnr Mersey Street and Clyde Street Island Bay Wellington"
    56710;"2011-08-14";"2015-06-03";-41.3383321526384;174.774223538012;"Cnr Mersey Street and Clyde Street Island Bay Wellington"
    56710;"2015-06-04";"";-41.3383321526384;174.774223538012;"Cnr Mersey Street and Clyde Street Island Bay Wellington"
    56720;"2008-10-01";"2010-03-07";-41.1138859544908;174.961004559463;"Moonshine Road Porirua Wellington  (off State highway 58)"
    56720;"2010-03-08";"2010-11-11";-41.1138859544908;174.961004559463;"Moonshine Road Porirua Wellington  (off State highway 58)"
    56720;"2010-11-10";"2011-08-14";-41.1138859544908;174.961004559463;"Moonshine Road Porirua Wellington  (off State highway 58)"
    56720;"2011-08-14";"2011-08-25";-41.1138859544908;174.961004559463;"Moonshine Road Porirua Wellington  (off State highway 58)"
    56720;"2011-08-26";"2013-05-15";-41.1138859544908;174.961004559463;"Moonshine Road Porirua Wellington  (off State highway 58)"
    56720;"2013-05-15";"2013-07-15";-41.1138859544908;174.961004559463;"Moonshine Road Porirua Wellington  (off State highway 58)"
    56720;"2013-07-15";"2014-04-02";-41.1138859544908;174.961004559463;"Moonshine Road Porirua Wellington  (off State highway 58)"
    56720;"2014-04-02";"2014-05-27";-41.1138859544908;174.961004559463;"Moonshine Road Porirua Wellington  (off State highway 58)"
    56720;"2014-05-27";"2015-09-29";-41.1138859544908;174.961004559463;"Moonshine Road Porirua Wellington  (off State highway 58)"
    56720;"2015-09-30";"";-41.1138859544908;174.961004559463;"Moonshine Road Porirua Wellington  (off State highway 58)"
    
    56731;"2008-10-01";"2010-03-07";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56731;"2010-03-08";"2010-11-12";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56731;"2010-11-11";"2011-08-14";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56731;"2011-08-14";"2011-08-25";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56731;"2011-08-26";"2013-05-15";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56731;"2013-05-15";"2013-07-15";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56731;"2013-07-15";"";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    
    56732;"2008-10-01";"2010-03-07";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56732;"2010-03-08";"2010-11-12";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56732;"2010-11-11";"2011-08-14";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56732;"2011-08-14";"2011-08-25";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56732;"2011-08-26";"2013-05-15";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56732;"2013-05-15";"2013-07-15";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56732;"2013-07-15";"";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    
    56733;"2008-10-01";"2010-03-07";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56733;"2010-03-08";"2010-11-12";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56733;"2010-11-11";"2011-08-14";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56733;"2011-08-14";"2011-08-25";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56733;"2011-08-26";"2013-05-15";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56733;"2013-05-15";"2013-07-15";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    56733;"2013-07-15";"";-41.2226690357991;174.808582946986;"1 Norman Lane Johnsonville"
    
    56740;"2008-10-01";"2010-03-10";-41.0583912238275;175.205653661319;"Pakuratahi Forks Kaitoke Regional Park Kaitoke Valley Wellington Region"
    56740;"2010-03-11";"2010-10-05";-41.0583912238275;175.205653661319;"Pakuratahi Forks Kaitoke Regional Park Kaitoke Valley Wellington Region"
    56740;"2010-10-06";"2010-11-18";-41.0583912238275;175.205653661319;"Pakuratahi Forks Kaitoke Regional Park Kaitoke Valley Wellington Region"
    56740;"2010-11-17";"2011-08-21";-41.0583912238275;175.205653661319;"Pakuratahi Forks Kaitoke Regional Park Kaitoke Valley Wellington Region"
    56740;"2011-08-21";"2013-05-15";-41.0583912238275;175.205653661319;"Pakuratahi Forks Kaitoke Regional Park Kaitoke Valley Wellington Region"
    56740;"2013-05-15";"2013-07-15";-41.0583912238275;175.205653661319;"Pakuratahi Forks Kaitoke Regional Park Kaitoke Valley Wellington Region"
    56740;"2013-07-15";"2014-04-02";-41.0583912238275;175.205653661319;"Pakuratahi Forks Kaitoke Regional Park Kaitoke Valley Wellington Region"
    56740;"2014-04-02";"2014-05-27";-41.0583912238275;175.205653661319;"Pakuratahi Forks Kaitoke Regional Park Kaitoke Valley Wellington Region"
    56740;"2014-05-27";"2015-09-29";-41.0583912238275;175.205653661319;"Pakuratahi Forks Kaitoke Regional Park Kaitoke Valley Wellington Region"
    56740;"2015-09-30";"2017-03-08";-41.0583912238275;175.205653661319;"Pakuratahi Forks Kaitoke Regional Park Kaitoke Valley Wellington Region"
    
    56751;"2008-10-01";"2010-03-07";-41.2472369706628;174.791952713974;"Khandallah Presbyterian Church 31 Ganges Road Khandallah Wellington"
    56751;"2010-03-08";"2010-11-12";-41.2472369706628;174.791952713974;"Khandallah Presbyterian Church 31 Ganges Road Khandallah Wellington"
    56751;"2010-11-11";"2011-08-14";-41.2472369706628;174.791952713974;"Khandallah Presbyterian Church 31 Ganges Road Khandallah Wellington"
    56751;"2011-08-14";"2015-06-03";-41.2472369706628;174.791952713974;"Khandallah Presbyterian Church 31 Ganges Road Khandallah Wellington"
    56751;"2015-06-04";"";-41.2472369706628;174.791952713974;"Khandallah Presbyterian Church 31 Ganges Road Khandallah Wellington"
    
    56752;"2008-10-01";"2010-03-07";-41.2472369706628;174.791952713974;"Khandallah Presbyterian Church 31 Ganges Road Khandallah Wellington"
    56752;"2010-03-08";"2010-11-12";-41.2472369706628;174.791952713974;"Khandallah Presbyterian Church 31 Ganges Road Khandallah Wellington"
    56752;"2010-11-11";"2011-08-14";-41.2472369706628;174.791952713974;"Khandallah Presbyterian Church 31 Ganges Road Khandallah Wellington"
    56752;"2011-08-14";"2015-06-03";-41.2472369706628;174.791952713974;"Khandallah Presbyterian Church 31 Ganges Road Khandallah Wellington"
    56752;"2015-06-04";"";-41.2472369706628;174.791952713974;"Khandallah Presbyterian Church 31 Ganges Road Khandallah Wellington"
    
    56753;"2008-10-01";"2010-03-07";-41.2472369706628;174.791952713974;"Khandallah Presbyterian Church 31 Ganges Road Khandallah Wellington"
    56753;"2010-03-08";"2010-11-12";-41.2472369706628;174.791952713974;"Khandallah Presbyterian Church 31 Ganges Road Khandallah Wellington"
    56753;"2010-11-11";"2011-08-14";-41.2472369706628;174.791952713974;"Khandallah Presbyterian Church 31 Ganges Road Khandallah Wellington"
    56753;"2011-08-14";"2015-06-03";-41.2472369706628;174.791952713974;"Khandallah Presbyterian Church 31 Ganges Road Khandallah Wellington"
    56753;"2015-06-04";"";-41.2472369706628;174.791952713974;"Khandallah Presbyterian Church 31 Ganges Road Khandallah Wellington"
    
    56761;"2008-10-01";"2010-03-02";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56761;"2010-03-03";"2010-10-05";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56761;"2010-10-06";"2010-11-09";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56761;"2010-11-08";"2011-08-21";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56761;"2011-08-21";"2013-06-18";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56761;"2013-06-18";"2014-05-16";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56761;"2014-05-16";"2015-09-29";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56761;"2015-09-30";"";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    
    56762;"2008-10-01";"2010-03-02";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56762;"2010-03-03";"2010-10-05";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56762;"2010-10-06";"2010-11-09";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56762;"2010-11-08";"2011-08-21";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56762;"2011-08-21";"2013-06-18";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56762;"2013-06-18";"2014-05-16";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56762;"2014-05-16";"2015-09-29";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56762;"2015-09-30";"";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    
    56763;"2008-10-01";"2010-03-02";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56763;"2010-03-03";"2010-10-05";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56763;"2010-10-06";"2010-11-09";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56763;"2010-11-08";"2011-08-21";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56763;"2011-08-21";"2013-06-18";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56763;"2013-06-18";"2014-05-16";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56763;"2014-05-16";"2015-09-29";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    56763;"2015-09-30";"";-40.867692926821;175.028737021002;"70-74 Te Moana Road Waikanae Beach Waikanae"
    
    56770;"2008-10-01";"2010-03-07";-41.2856691566094;174.741128555245;"Donald Street Karori Wellington"
    56770;"2010-03-08";"2010-11-12";-41.2856691566094;174.741128555245;"Donald Street Karori Wellington"
    56770;"2010-11-11";"2011-01-16";-41.2856691566094;174.741128555245;"Donald Street Karori Wellington"
    
    56781;"2008-10-01";"2010-03-08";-41.2602741718771;174.788749941078;"School Road Kaiwharawhara"
    56781;"2010-03-09";"2010-11-12";-41.2602741718771;174.788749941078;"School Road Kaiwharawhara"
    56781;"2010-11-11";"2011-08-14";-41.2602741718771;174.788749941078;"School Road Kaiwharawhara"
    56781;"2011-08-14";"2012-10-30";-41.2602741718771;174.788749941078;"School Road Kaiwharawhara"
    56781;"2012-10-31";"2013-05-15";-41.2602741718771;174.788749941078;"School Road Kaiwharawhara"
    56781;"2013-05-15";"2013-07-15";-41.2602741718771;174.788749941078;"School Road Kaiwharawhara"
    56781;"2013-07-15";"";-41.2602741718771;174.788749941078;"School Road Kaiwharawhara"
    
    56782;"2008-10-01";"2010-03-08";-41.2602741718771;174.788749941078;"School Road Kaiwharawhara"
    56782;"2010-03-09";"2010-11-12";-41.2602741718771;174.788749941078;"School Road Kaiwharawhara"
    56782;"2010-11-11";"2011-08-14";-41.2602741718771;174.788749941078;"School Road Kaiwharawhara"
    56782;"2011-08-14";"2012-10-30";-41.2602741718771;174.788749941078;"School Road Kaiwharawhara"
    56782;"2012-10-31";"2013-05-15";-41.2602741718771;174.788749941078;"School Road Kaiwharawhara"
    56782;"2013-05-15";"2013-07-15";-41.2602741718771;174.788749941078;"School Road Kaiwharawhara"
    56782;"2013-07-15";"";-41.2602741718771;174.788749941078;"School Road Kaiwharawhara"
    
    56791;"2008-10-01";"2010-03-07";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56791;"2010-03-08";"2010-11-12";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56791;"2010-11-11";"2011-08-14";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56791;"2011-08-14";"2011-08-25";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56791;"2011-08-26";"2013-05-15";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56791;"2013-05-15";"2013-07-15";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56791;"2013-07-15";"";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    
    56792;"2008-10-01";"2010-03-07";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56792;"2010-03-08";"2010-11-12";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56792;"2010-11-11";"2011-08-14";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56792;"2011-08-14";"2011-08-25";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56792;"2011-08-26";"2013-05-15";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56792;"2013-05-15";"2013-07-15";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56792;"2013-07-15";"";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    
    56793;"2008-10-01";"2010-03-07";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56793;"2010-03-08";"2010-11-12";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56793;"2010-11-11";"2011-08-14";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56793;"2011-08-14";"2011-08-25";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56793;"2011-08-26";"2013-05-15";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56793;"2013-05-15";"2013-07-15";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    56793;"2013-07-15";"";-41.2318988972187;174.810446405943;"Kiwi Storage off Newlands Road above Newlands interchange Wellington"
    
    56801;"2008-10-01";"2010-03-08";-41.2491708994236;174.810060951026;"31 Jarden Mile Ngauranga Wellington"
    56801;"2010-03-09";"2010-11-12";-41.2491708994236;174.810060951026;"31 Jarden Mile Ngauranga Wellington"
    56801;"2010-11-11";"2011-08-14";-41.2491708994236;174.810060951026;"31 Jarden Mile Ngauranga Wellington"
    56801;"2011-08-14";"2011-08-25";-41.2491708994236;174.810060951026;"31 Jarden Mile Ngauranga Wellington"
    56801;"2011-08-26";"2013-05-15";-41.2491708994236;174.810060951026;"31 Jarden Mile Ngauranga Wellington"
    56801;"2013-05-15";"2013-07-15";-41.2491708994236;174.810060951026;"31 Jarden Mile Ngauranga Wellington"
    56801;"2013-07-15";"";-41.2491708994236;174.810060951026;"31 Jarden Mile Ngauranga Wellington"
    
    56802;"2008-10-01";"2010-03-08";-41.2491708994236;174.810060951026;"31 Jarden Mile Ngauranga Wellington"
    56802;"2010-03-09";"2010-11-12";-41.2491708994236;174.810060951026;"31 Jarden Mile Ngauranga Wellington"
    56802;"2010-11-11";"2011-08-14";-41.2491708994236;174.810060951026;"31 Jarden Mile Ngauranga Wellington"
    56802;"2011-08-14";"2011-08-25";-41.2491708994236;174.810060951026;"31 Jarden Mile Ngauranga Wellington"
    56802;"2011-08-26";"2013-05-15";-41.2491708994236;174.810060951026;"31 Jarden Mile Ngauranga Wellington"
    56802;"2013-05-15";"2013-07-15";-41.2491708994236;174.810060951026;"31 Jarden Mile Ngauranga Wellington"
    56802;"2013-07-15";"";-41.2491708994236;174.810060951026;"31 Jarden Mile Ngauranga Wellington"
    
    56810;"2008-10-01";"2010-03-07";-41.2509652622344;174.773854565865;"All Saints Church 1 - 9 Abbott Street Ngaio"
    56810;"2010-03-08";"2010-11-12";-41.2509652622344;174.773854565865;"All Saints Church 1 - 9 Abbott Street Ngaio"
    56810;"2010-11-11";"2011-08-14";-41.2509652622344;174.773854565865;"All Saints Church 1 - 9 Abbott Street Ngaio"
    56810;"2011-08-14";"2013-11-18";-41.2509652622344;174.773854565865;"All Saints Church 1 - 9 Abbott Street Ngaio"
    
    56821;"2008-10-01";"2010-03-07";-41.2214128064533;174.820918643257;"27 Trebann Street Newlands Wellington"
    56821;"2010-03-08";"2010-11-12";-41.2214128064533;174.820918643257;"27 Trebann Street Newlands Wellington"
    56821;"2010-11-11";"2011-08-14";-41.2214128064533;174.820918643257;"27 Trebann Street Newlands Wellington"
    56821;"2011-08-14";"2011-08-25";-41.2214128064533;174.820918643257;"27 Trebann Street Newlands Wellington"
    56821;"2011-08-26";"2013-05-15";-41.2214128064533;174.820918643257;"27 Trebann Street Newlands Wellington"
    56821;"2013-05-15";"2013-07-15";-41.2214128064533;174.820918643257;"27 Trebann Street Newlands Wellington"
    56821;"2013-07-15";"";-41.2214128064533;174.820918643257;"27 Trebann Street Newlands Wellington"
    
    56831;"2008-10-01";"2010-03-10";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56831;"2010-03-11";"2010-11-17";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56831;"2010-11-16";"2011-08-14";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56831;"2011-08-14";"2011-08-25";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56831;"2011-08-26";"2013-05-15";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56831;"2013-05-15";"2013-07-15";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56831;"2013-07-15";"";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    
    56832;"2008-10-01";"2010-03-10";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56832;"2010-03-11";"2010-11-17";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56832;"2010-11-16";"2011-08-14";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56832;"2011-08-14";"2011-08-25";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56832;"2011-08-26";"2013-05-15";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56832;"2013-05-15";"2013-07-15";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56832;"2013-07-15";"";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    
    56833;"2008-10-01";"2010-03-10";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56833;"2010-03-11";"2010-11-17";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56833;"2010-11-16";"2011-08-14";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56833;"2011-08-14";"2011-08-25";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56833;"2011-08-26";"2013-05-15";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56833;"2013-05-15";"2013-07-15";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    56833;"2013-07-15";"";-41.2017943760316;174.946631118706;"Resene Paints - Vogel St Naenae Lower Hutt"
    
    56841;"2010-09-27";"2010-11-14";-41.2744882993026;174.75865708363;"Mobil Garage 107 Pembroke Road Northland Wellington"
    56841;"2010-11-13";"2011-08-14";-41.2744882993026;174.75865708363;"Mobil Garage 107 Pembroke Road Northland Wellington"
    56841;"2011-08-14";"2015-06-03";-41.2744882993026;174.75865708363;"Mobil Garage 107 Pembroke Road Northland Wellington"
    56841;"2015-06-04";"";-41.2744882993026;174.75865708363;"Mobil Garage 107 Pembroke Road Northland Wellington"
    
    56842;"2010-09-27";"2010-11-14";-41.2744882993026;174.75865708363;"Mobil Garage 107 Pembroke Road Northland Wellington"
    56842;"2010-11-13";"2011-08-14";-41.2744882993026;174.75865708363;"Mobil Garage 107 Pembroke Road Northland Wellington"
    56842;"2011-08-14";"2015-06-03";-41.2744882993026;174.75865708363;"Mobil Garage 107 Pembroke Road Northland Wellington"
    56842;"2015-06-04";"";-41.2744882993026;174.75865708363;"Mobil Garage 107 Pembroke Road Northland Wellington"
    
    56843;"2010-09-27";"2010-11-14";-41.2744882993026;174.75865708363;"Mobil Garage 107 Pembroke Road Northland Wellington"
    56843;"2010-11-13";"2011-08-14";-41.2744882993026;174.75865708363;"Mobil Garage 107 Pembroke Road Northland Wellington"
    56843;"2011-08-14";"2015-06-03";-41.2744882993026;174.75865708363;"Mobil Garage 107 Pembroke Road Northland Wellington"
    56843;"2015-06-04";"";-41.2744882993026;174.75865708363;"Mobil Garage 107 Pembroke Road Northland Wellington"
    
    56851;"2008-10-01";"2010-03-08";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56851;"2010-03-09";"2010-11-12";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56851;"2010-11-11";"2011-08-14";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56851;"2011-08-14";"2011-08-25";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56851;"2011-08-26";"2013-05-15";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56851;"2013-05-15";"2013-07-15";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56851;"2013-07-15";"";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    
    56852;"2008-10-01";"2010-03-08";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56852;"2010-03-09";"2010-11-12";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56852;"2010-11-11";"2011-08-14";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56852;"2011-08-14";"2011-08-25";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56852;"2011-08-26";"2013-05-15";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56852;"2013-05-15";"2013-07-15";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56852;"2013-07-15";"";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    
    56853;"2008-10-01";"2010-03-08";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56853;"2010-03-09";"2010-11-12";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56853;"2010-11-11";"2011-08-14";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56853;"2011-08-14";"2011-08-25";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56853;"2011-08-26";"2013-05-15";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56853;"2013-05-15";"2013-07-15";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    56853;"2013-07-15";"";-41.2385596980644;174.804949530434;"Adjacent to State Highway 1 Tyres Road Ngauranga Wellington"
    
    56861;"2008-10-01";"2010-03-10";-41.2576563514747;174.955117539129;"Wright Street Wainuiomata"
    56861;"2010-03-11";"2010-11-18";-41.2576563514747;174.955117539129;"Wright Street Wainuiomata"
    56861;"2010-11-17";"2011-08-14";-41.2576563514747;174.955117539129;"Wright Street Wainuiomata"
    56861;"2011-08-14";"2011-08-25";-41.2576563514747;174.955117539129;"Wright Street Wainuiomata"
    56861;"2011-08-26";"2013-05-15";-41.2576563514747;174.955117539129;"Wright Street Wainuiomata"
    56861;"2013-05-15";"2013-07-15";-41.2576563514747;174.955117539129;"Wright Street Wainuiomata"
    56861;"2013-07-15";"2016-10-12";-41.2576563514747;174.955117539129;"Wright Street Wainuiomata"
    56862;"2008-10-01";"2010-03-10";-41.2576563514747;174.955117539129;"Wright Street Wainuiomata"
    56862;"2010-03-11";"2010-11-18";-41.2576563514747;174.955117539129;"Wright Street Wainuiomata"
    56862;"2010-11-17";"2011-08-14";-41.2576563514747;174.955117539129;"Wright Street Wainuiomata"
    56862;"2011-08-14";"2011-08-25";-41.2576563514747;174.955117539129;"Wright Street Wainuiomata"
    56862;"2011-08-26";"2013-05-15";-41.2576563514747;174.955117539129;"Wright Street Wainuiomata"
    56862;"2013-05-15";"2013-07-15";-41.2576563514747;174.955117539129;"Wright Street Wainuiomata"
    56862;"2013-07-15";"2016-10-12";-41.2576563514747;174.955117539129;"Wright Street Wainuiomata"
    
    56871;"2008-10-01";"2010-03-07";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56871;"2010-03-08";"2010-11-11";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56871;"2010-11-10";"2011-08-14";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56871;"2011-08-14";"2011-08-25";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56871;"2011-08-26";"2013-05-15";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56871;"2013-05-15";"2013-07-15";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56871;"2013-07-15";"";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    
    56872;"2008-10-01";"2010-03-07";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56872;"2010-03-08";"2010-11-11";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56872;"2010-11-10";"2011-08-14";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56872;"2011-08-14";"2011-08-25";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56872;"2011-08-26";"2013-05-15";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56872;"2013-05-15";"2013-07-15";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56872;"2013-07-15";"";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    
    56873;"2008-10-01";"2010-03-07";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56873;"2010-03-08";"2010-11-11";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56873;"2010-11-10";"2011-08-14";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56873;"2011-08-14";"2011-08-25";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56873;"2011-08-26";"2013-05-15";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56873;"2013-05-15";"2013-07-15";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    56873;"2013-07-15";"";-41.2067019424792;174.78700083657;"598 Ohariu Valley Road Johnsonville Wellington"
    
    56881;"2009-03-24";"2010-03-08";-41.3059115384889;174.763437048385;"205 Ohiro Road Brooklyn Wellington"
    56881;"2010-03-09";"2010-11-16";-41.3059115384889;174.763437048385;"205 Ohiro Road Brooklyn Wellington"
    56881;"2010-11-15";"2011-08-14";-41.3059115384889;174.763437048385;"205 Ohiro Road Brooklyn Wellington"
    56881;"2011-08-14";"2015-06-03";-41.3059115384889;174.763437048385;"205 Ohiro Road Brooklyn Wellington"
    56881;"2015-06-04";"";-41.3059115384889;174.763437048385;"205 Ohiro Road Brooklyn Wellington"
    
    56882;"2009-03-24";"2010-03-08";-41.3059115384889;174.763437048385;"205 Ohiro Road Brooklyn Wellington"
    56882;"2010-03-09";"2010-11-16";-41.3059115384889;174.763437048385;"205 Ohiro Road Brooklyn Wellington"
    56882;"2010-11-15";"2011-08-14";-41.3059115384889;174.763437048385;"205 Ohiro Road Brooklyn Wellington"
    56882;"2011-08-14";"2015-06-03";-41.3059115384889;174.763437048385;"205 Ohiro Road Brooklyn Wellington"
    56882;"2015-06-04";"";-41.3059115384889;174.763437048385;"205 Ohiro Road Brooklyn Wellington"
    
    56883;"2009-03-24";"2010-03-08";-41.3059115384889;174.763437048385;"205 Ohiro Road Brooklyn Wellington"
    56883;"2010-03-09";"2010-11-16";-41.3059115384889;174.763437048385;"205 Ohiro Road Brooklyn Wellington"
    56883;"2010-11-15";"2011-08-14";-41.3059115384889;174.763437048385;"205 Ohiro Road Brooklyn Wellington"
    56883;"2011-08-14";"2015-06-03";-41.3059115384889;174.763437048385;"205 Ohiro Road Brooklyn Wellington"
    56883;"2015-06-04";"";-41.3059115384889;174.763437048385;"205 Ohiro Road Brooklyn Wellington"
    
    56891;"2008-10-01";"2010-03-02";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    56891;"2010-03-03";"2010-10-05";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    56891;"2010-10-06";"2010-11-09";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    56891;"2010-11-08";"2011-08-21";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    56891;"2011-08-21";"2013-06-18";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    56891;"2013-06-18";"2014-05-16";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    56891;"2014-05-16";"2014-11-04";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    56891;"2014-11-05";"2015-09-29";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    56891;"2015-09-30";"";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    
    56892;"2008-10-01";"2010-03-02";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    56892;"2010-03-03";"2010-10-05";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    56892;"2010-10-06";"2010-11-09";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    56892;"2010-11-08";"2011-08-21";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    56892;"2011-08-21";"2013-06-18";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    56892;"2013-06-18";"2014-05-16";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    56892;"2014-05-16";"2014-11-04";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    56892;"2014-11-05";"2015-09-29";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    56892;"2015-09-30";"";-40.9890410154293;174.956387658801;"The Perkins Farm Paekakariki"
    
    56901;"2008-10-01";"2010-03-02";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56901;"2010-03-03";"2010-10-05";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56901;"2010-10-06";"2010-11-09";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56901;"2010-11-08";"2011-08-21";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56901;"2011-08-21";"2013-06-18";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56901;"2013-06-18";"2014-05-16";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56901;"2014-05-16";"2015-09-29";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56901;"2015-09-30";"";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    
    56902;"2008-10-01";"2010-03-02";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56902;"2010-03-03";"2010-10-05";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56902;"2010-10-06";"2010-11-09";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56902;"2010-11-08";"2011-08-21";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56902;"2011-08-21";"2013-06-18";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56902;"2013-06-18";"2014-05-16";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56902;"2014-05-16";"2015-09-29";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56902;"2015-09-30";"";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    
    56903;"2008-10-01";"2010-03-02";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56903;"2010-03-03";"2010-10-05";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56903;"2010-10-06";"2010-11-09";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56903;"2010-11-08";"2011-08-21";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56903;"2011-08-21";"2013-06-18";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56903;"2013-06-18";"2014-05-16";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56903;"2014-05-16";"2015-09-29";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56903;"2015-09-30";"";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    
    56904;"2010-07-30";"2010-10-05";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56904;"2010-10-06";"2010-11-09";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56904;"2010-11-08";"2011-03-06";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56904;"2011-03-07";"2011-08-21";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56904;"2011-08-21";"2013-06-18";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56904;"2013-06-18";"2014-05-16";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56904;"2014-05-16";"2015-09-29";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56904;"2015-09-30";"";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    
    56905;"2010-07-30";"2010-10-05";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56905;"2010-10-06";"2010-11-09";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56905;"2010-11-08";"2011-03-06";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56905;"2011-03-07";"2011-08-21";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56905;"2011-08-21";"2013-06-18";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56905;"2013-06-18";"2014-05-16";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56905;"2014-05-16";"2015-09-29";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56905;"2015-09-30";"";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    
    56906;"2010-07-30";"2010-10-05";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56906;"2010-10-06";"2010-11-09";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56906;"2010-11-08";"2011-03-06";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56906;"2011-03-07";"2011-08-21";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56906;"2011-08-21";"2013-06-18";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56906;"2013-06-18";"2014-05-16";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56906;"2014-05-16";"2015-09-29";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    56906;"2015-09-30";"";-40.9203307617448;175.006283590842;"Paraparaumu Reservoir Riwai Street Paraparaumu"
    
    56911;"2008-10-01";"2010-03-02";-40.8935713830873;174.980548512833;"16 Seaview Road Paraparaumu Beach"
    56911;"2010-03-03";"2010-10-05";-40.8935713830873;174.980548512833;"16 Seaview Road Paraparaumu Beach"
    56911;"2010-10-06";"2010-11-09";-40.8935713830873;174.980548512833;"16 Seaview Road Paraparaumu Beach"
    56911;"2010-11-08";"2011-08-21";-40.8935713830873;174.980548512833;"16 Seaview Road Paraparaumu Beach"
    56911;"2011-08-21";"2013-06-18";-40.8935713830873;174.980548512833;"16 Seaview Road Paraparaumu Beach"
    56911;"2013-06-18";"2014-05-16";-40.8935713830873;174.980548512833;"16 Seaview Road Paraparaumu Beach"
    56911;"2014-05-16";"2015-09-29";-40.8935713830873;174.980548512833;"16 Seaview Road Paraparaumu Beach"
    56911;"2015-09-30";"";-40.8935713830873;174.980548512833;"16 Seaview Road Paraparaumu Beach"
    
    56912;"2008-10-01";"2010-03-02";-40.8935713830873;174.980548512833;"16 Seaview Road Paraparaumu Beach"
    56912;"2010-03-03";"2010-10-05";-40.8935713830873;174.980548512833;"16 Seaview Road Paraparaumu Beach"
    56912;"2010-10-06";"2010-11-09";-40.8935713830873;174.980548512833;"16 Seaview Road Paraparaumu Beach"
    56912;"2010-11-08";"2011-08-21";-40.8935713830873;174.980548512833;"16 Seaview Road Paraparaumu Beach"
    56912;"2011-08-21";"2013-06-18";-40.8935713830873;174.980548512833;"16 Seaview Road Paraparaumu Beach"
    56912;"2013-06-18";"2014-05-16";-40.8935713830873;174.980548512833;"16 Seaview Road Paraparaumu Beach"
    56912;"2014-05-16";"2015-09-29";-40.8935713830873;174.980548512833;"16 Seaview Road Paraparaumu Beach"
    56912;"2015-09-30";"";-40.8935713830873;174.980548512833;"16 Seaview Road Paraparaumu Beach"
    
    56921;"2008-10-01";"2010-03-07";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56921;"2010-03-08";"2010-11-11";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56921;"2010-11-10";"2011-08-14";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56921;"2011-08-14";"2011-08-25";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56921;"2011-08-26";"2013-05-15";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56921;"2013-05-15";"2013-07-15";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56921;"2013-07-15";"2014-04-02";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56921;"2014-04-02";"2014-04-04";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56921;"2014-04-05";"2014-04-06";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56921;"2015-07-03";"2015-09-29";-40.9201745162201;175.01835591955;"100 Nikau Valley Road Nikau Valley Paraparaumu"
    56921;"2015-09-30";"";-40.9201745162201;175.01835591955;"100 Nikau Valley Road Nikau Valley Paraparaumu"
    
    56922;"2008-10-01";"2010-03-07";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56922;"2010-03-08";"2010-11-11";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56922;"2010-11-10";"2011-08-14";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56922;"2011-08-14";"2011-08-25";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56922;"2011-08-26";"2013-05-15";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56922;"2013-05-15";"2013-07-15";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56922;"2013-07-15";"2014-04-02";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56922;"2014-04-02";"2014-04-04";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56922;"2014-04-05";"2014-04-06";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56922;"2015-07-03";"2015-09-29";-40.9201745162201;175.01835591955;"100 Nikau Valley Road Nikau Valley Paraparaumu"
    56922;"2015-09-30";"";-40.9201745162201;175.01835591955;"100 Nikau Valley Road Nikau Valley Paraparaumu"
    
    56923;"2008-10-01";"2010-03-07";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56923;"2010-03-08";"2010-11-11";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56923;"2010-11-10";"2011-08-14";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56923;"2011-08-14";"2011-08-25";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56923;"2011-08-26";"2013-05-15";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56923;"2013-05-15";"2013-07-15";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56923;"2013-07-15";"2014-04-02";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56923;"2014-04-02";"2014-04-04";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    56923;"2014-04-05";"2014-04-06";-41.136369967399;174.840259537453;"Rooftop Wrightson Building Hartham Place Porirua"
    
    56931;"2010-05-28";"2010-11-11";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56931;"2010-11-10";"2011-08-14";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56931;"2011-08-14";"2011-08-25";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56931;"2011-08-26";"2013-05-15";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56931;"2013-05-15";"2013-07-15";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56931;"2013-07-15";"2014-04-02";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56931;"2014-04-02";"2014-05-27";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56931;"2014-05-27";"2015-09-29";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56931;"2015-09-30";"2015-10-22";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56932;"2010-05-28";"2010-11-11";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56932;"2010-11-10";"2011-08-14";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56932;"2011-08-14";"2011-08-25";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56932;"2011-08-26";"2013-05-15";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56932;"2013-05-15";"2013-07-15";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56932;"2013-07-15";"2014-04-02";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56932;"2014-04-02";"2014-05-27";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56932;"2014-05-27";"2015-09-29";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56932;"2015-09-30";"2015-10-22";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56933;"2010-05-28";"2010-11-11";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56933;"2010-11-10";"2011-08-14";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56933;"2011-08-14";"2011-08-25";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56933;"2011-08-26";"2013-05-15";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56933;"2013-05-15";"2013-07-15";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56933;"2013-07-15";"2014-04-02";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56933;"2014-04-02";"2014-05-27";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56933;"2014-05-27";"2015-09-29";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    56933;"2015-09-30";"2015-10-22";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    
    56941;"2009-02-20";"2010-03-07";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56941;"2010-03-08";"2010-11-11";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56941;"2010-11-10";"2011-08-14";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56941;"2011-08-14";"2011-08-25";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56941;"2011-08-26";"2013-05-15";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56941;"2013-05-15";"2013-07-15";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56941;"2013-07-15";"2014-04-02";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56941;"2014-04-02";"2014-05-27";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56941;"2014-05-27";"2015-09-29";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56941;"2015-09-30";"2015-11-19";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56942;"2009-02-20";"2010-03-07";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56942;"2010-03-08";"2010-11-11";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56942;"2010-11-10";"2011-08-14";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56942;"2011-08-14";"2011-08-25";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56942;"2011-08-26";"2013-05-15";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56942;"2013-05-15";"2013-07-15";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56942;"2013-07-15";"2014-04-02";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56942;"2014-04-02";"2014-05-27";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56942;"2014-05-27";"2015-09-29";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56942;"2015-09-30";"2015-11-19";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56943;"2009-02-20";"2010-03-07";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56943;"2010-03-08";"2010-11-11";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56943;"2010-11-10";"2011-08-14";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56943;"2011-08-14";"2011-08-25";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56943;"2011-08-26";"2013-05-15";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56943;"2013-05-15";"2013-07-15";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56943;"2013-07-15";"2014-04-02";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56943;"2014-04-02";"2014-05-27";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56943;"2014-05-27";"2015-09-29";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    56943;"2015-09-30";"2015-11-19";-41.1423461386782;174.866399595521;"174 Bedford Street Cannons Creek Porirua"
    
    56951;"2008-10-01";"2010-03-10";-41.2939838527045;174.832093749864;"Massey Road Watts Peninsula Wellington"
    56951;"2010-03-11";"2010-11-17";-41.2939838527045;174.832093749864;"Massey Road Watts Peninsula Wellington"
    56951;"2010-11-16";"2011-08-14";-41.2939838527045;174.832093749864;"Massey Road Watts Peninsula Wellington"
    56951;"2011-08-14";"2011-08-25";-41.2939838527045;174.832093749864;"Massey Road Watts Peninsula Wellington"
    56951;"2011-08-26";"2013-05-15";-41.2939838527045;174.832093749864;"Massey Road Watts Peninsula Wellington"
    56951;"2013-05-15";"2013-07-15";-41.2939838527045;174.832093749864;"Massey Road Watts Peninsula Wellington"
    56951;"2013-07-15";"";-41.2939838527045;174.832093749864;"Massey Road Watts Peninsula Wellington"
    
    56952;"2008-10-01";"2010-03-10";-41.2939838527045;174.832093749864;"Massey Road Watts Peninsula Wellington"
    56952;"2010-03-11";"2010-11-17";-41.2939838527045;174.832093749864;"Massey Road Watts Peninsula Wellington"
    56952;"2010-11-16";"2011-08-14";-41.2939838527045;174.832093749864;"Massey Road Watts Peninsula Wellington"
    56952;"2011-08-14";"2011-08-25";-41.2939838527045;174.832093749864;"Massey Road Watts Peninsula Wellington"
    56952;"2011-08-26";"2013-05-15";-41.2939838527045;174.832093749864;"Massey Road Watts Peninsula Wellington"
    56952;"2013-05-15";"2013-07-15";-41.2939838527045;174.832093749864;"Massey Road Watts Peninsula Wellington"
    56952;"2013-07-15";"";-41.2939838527045;174.832093749864;"Massey Road Watts Peninsula Wellington"
    
    56961;"2008-10-01";"2010-03-10";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56961;"2010-03-11";"2010-11-17";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56961;"2010-11-16";"2011-08-14";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56961;"2011-08-14";"2011-08-25";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56961;"2011-08-26";"2013-05-15";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56961;"2013-05-15";"2013-07-15";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56961;"2013-07-15";"";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    
    56962;"2008-10-01";"2010-03-10";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56962;"2010-03-11";"2010-11-17";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56962;"2010-11-16";"2011-08-14";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56962;"2011-08-14";"2011-08-25";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56962;"2011-08-26";"2013-05-15";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56962;"2013-05-15";"2013-07-15";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56962;"2013-07-15";"";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    
    56963;"2008-10-01";"2010-03-10";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56963;"2010-03-11";"2010-11-17";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56963;"2010-11-16";"2011-08-14";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56963;"2011-08-14";"2011-08-25";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56963;"2011-08-26";"2013-05-15";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56963;"2013-05-15";"2013-07-15";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    56963;"2013-07-15";"";-41.1773065308661;174.982590947033;"249 Stokes Valley Road Stokes Valley Lower Hutt"
    
    57001;"2008-10-01";"2010-03-07";-41.273702661339;174.764712616815;"126 Orangi Kaupapa Road Northland Wellington"
    57001;"2010-03-08";"2010-11-14";-41.273702661339;174.764712616815;"126 Orangi Kaupapa Road Northland Wellington"
    57001;"2010-11-13";"2011-08-14";-41.273702661339;174.764712616815;"126 Orangi Kaupapa Road Northland Wellington"
    57001;"2011-08-14";"2015-06-03";-41.273702661339;174.764712616815;"126 Orangi Kaupapa Road Northland Wellington"
    57001;"2015-06-04";"";-41.273702661339;174.764712616815;"126 Orangi Kaupapa Road Northland Wellington"
    
    57002;"2008-10-01";"2010-03-07";-41.273702661339;174.764712616815;"126 Orangi Kaupapa Road Northland Wellington"
    57002;"2010-03-08";"2010-11-14";-41.273702661339;174.764712616815;"126 Orangi Kaupapa Road Northland Wellington"
    57002;"2010-11-13";"2011-08-14";-41.273702661339;174.764712616815;"126 Orangi Kaupapa Road Northland Wellington"
    57002;"2011-08-14";"2015-06-03";-41.273702661339;174.764712616815;"126 Orangi Kaupapa Road Northland Wellington"
    57002;"2015-06-04";"";-41.273702661339;174.764712616815;"126 Orangi Kaupapa Road Northland Wellington"
    
    57053;"2008-10-01";"2010-03-07";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57053;"2010-03-08";"2010-11-11";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57053;"2010-11-10";"2011-08-14";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57053;"2011-08-14";"2011-08-25";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57053;"2011-08-26";"2013-05-15";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57053;"2013-05-15";"2013-07-15";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57053;"2013-07-15";"2014-04-02";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57053;"2014-04-02";"2014-05-27";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57053;"2014-05-27";"2015-09-29";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57053;"2015-09-30";"2015-11-17";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    
    57060;"2008-10-01";"2010-03-07";-41.1170531783324;174.90514439029;"Top End of Samwell Drive Whitby"
    57060;"2010-03-08";"2010-11-11";-41.1170531783324;174.90514439029;"Top End of Samwell Drive Whitby"
    57060;"2010-11-10";"2010-11-21";-41.1170531783324;174.90514439029;"Top End of Samwell Drive Whitby"
    
    57071;"2008-10-01";"2010-03-10";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57071;"2010-03-11";"2010-11-17";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57071;"2010-11-16";"2011-08-14";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57071;"2011-08-14";"2011-08-25";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57071;"2011-08-26";"2013-05-15";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57071;"2013-05-15";"2013-07-15";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57071;"2013-07-15";"2016-10-26";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57072;"2008-10-01";"2010-03-10";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57072;"2010-03-11";"2010-11-17";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57072;"2010-11-16";"2011-08-14";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57072;"2011-08-14";"2011-08-25";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57072;"2011-08-26";"2013-05-15";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57072;"2013-05-15";"2013-07-15";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57072;"2013-07-15";"2016-10-26";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57073;"2008-10-01";"2010-03-10";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57073;"2010-03-11";"2010-11-17";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57073;"2010-11-16";"2011-08-14";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57073;"2011-08-14";"2011-08-25";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57073;"2011-08-26";"2013-05-15";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57073;"2013-05-15";"2013-07-15";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    57073;"2013-07-15";"2016-10-26";-41.1878372312201;174.957630219185;"15-17 Eastern Hutt Road Wingate Lower Hutt"
    
    57081;"2010-05-27";"2010-10-05";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57081;"2010-10-06";"2010-11-09";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57081;"2010-11-08";"2011-08-21";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57081;"2011-08-21";"2013-06-18";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57081;"2013-06-18";"2014-05-16";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57081;"2014-05-16";"2015-09-29";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57081;"2015-09-30";"";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    
    57082;"2010-05-27";"2010-10-05";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57082;"2010-10-06";"2010-11-09";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57082;"2010-11-08";"2011-08-21";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57082;"2011-08-21";"2013-06-18";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57082;"2013-06-18";"2014-05-16";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57082;"2014-05-16";"2015-09-29";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57082;"2015-09-30";"";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    
    57083;"2010-05-27";"2010-10-05";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57083;"2010-10-06";"2010-11-09";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57083;"2010-11-08";"2011-08-21";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57083;"2011-08-21";"2013-06-18";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57083;"2013-06-18";"2014-05-16";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57083;"2014-05-16";"2015-09-29";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    57083;"2015-09-30";"";-40.8700909515123;175.050050882477;"Ngarara Road Waikanae"
    
    57111;"2010-03-24";"2010-10-03";-43.4950736781656;171.540672077309;"Mt Hutt Ski Field Road Mt Hutt South Canterbury"
    57111;"2010-10-04";"2010-11-24";-43.4950736781656;171.540672077309;"Mt Hutt Ski Field Road Mt Hutt South Canterbury"
    57111;"2010-11-23";"2011-04-18";-43.4950736781656;171.540672077309;"Mt Hutt Ski Field Road Mt Hutt South Canterbury"
    57111;"2011-04-19";"2011-05-29";-43.4950736781656;171.540672077309;"Mt Hutt Ski Field Road Mt Hutt South Canterbury"
    57111;"2011-05-29";"2012-08-09";-43.4950736781656;171.540672077309;"Mt Hutt Ski Field Road Mt Hutt South Canterbury"
    57111;"2012-08-09";"2013-03-08";-43.4950736781656;171.540672077309;"Mt Hutt Ski Field Road Mt Hutt South Canterbury"
    57111;"2013-03-08";"2014-02-11";-43.4950736781656;171.540672077309;"Mt Hutt Ski Field Road Mt Hutt South Canterbury"
    57111;"2014-02-11";"2014-03-18";-43.4950736781656;171.540672077309;"Mt Hutt Ski Field Road Mt Hutt South Canterbury"
    57111;"2014-03-18";"2015-09-24";-43.4950736781656;171.540672077309;"Mt Hutt Ski Field Road Mt Hutt South Canterbury"
    57111;"2015-09-25";"2016-10-12";-43.4950736781656;171.540672077309;"Mt Hutt Ski Field Road Mt Hutt South Canterbury"
    
    57161;"2009-12-18";"2010-02-25";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57161;"2010-02-26";"2010-10-05";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57161;"2010-10-06";"2010-11-01";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57161;"2010-10-31";"2011-08-21";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57161;"2011-08-21";"2013-05-15";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57161;"2013-05-15";"2013-07-15";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57161;"2013-07-15";"2014-04-02";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57161;"2014-04-02";"2014-05-27";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57161;"2014-05-27";"2015-09-29";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57161;"2015-09-30";"2015-12-13";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57162;"2009-12-18";"2010-02-25";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57162;"2010-02-26";"2010-10-05";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57162;"2010-10-06";"2010-11-01";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57162;"2010-10-31";"2011-08-21";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57162;"2011-08-21";"2013-05-15";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57162;"2013-05-15";"2013-07-15";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57162;"2013-07-15";"2014-04-02";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57162;"2014-04-02";"2014-05-27";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57162;"2014-05-27";"2015-09-29";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57162;"2015-09-30";"2015-12-13";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57163;"2009-12-18";"2010-02-25";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57163;"2010-02-26";"2010-10-05";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57163;"2010-10-06";"2010-11-01";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57163;"2010-10-31";"2011-08-21";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57163;"2011-08-21";"2013-05-15";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57163;"2013-05-15";"2013-07-15";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57163;"2013-07-15";"2014-04-02";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57163;"2014-04-02";"2014-05-27";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57163;"2014-05-27";"2015-09-29";-41.0252215566298;175.527830762448;"High Street South Carterton"
    57163;"2015-09-30";"2015-12-13";-41.0252215566298;175.527830762448;"High Street South Carterton"
    
    57171;"2009-12-18";"2010-03-01";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57171;"2010-03-02";"2010-10-05";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57171;"2010-10-06";"2010-11-06";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57171;"2010-11-05";"2011-08-21";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57171;"2011-08-21";"2015-05-12";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57171;"2015-05-12";"2015-05-12";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57171;"2015-05-13";"2015-06-24";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57172;"2009-12-18";"2010-03-01";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57172;"2010-03-02";"2010-10-05";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57172;"2010-10-06";"2010-11-06";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57172;"2010-11-05";"2011-08-21";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57172;"2011-08-21";"2015-05-12";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57172;"2015-05-12";"2015-05-12";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57172;"2015-05-13";"2015-06-24";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57173;"2009-12-18";"2010-03-01";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57173;"2010-03-02";"2010-10-05";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57173;"2010-10-06";"2010-11-06";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57173;"2010-11-05";"2011-08-21";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57173;"2011-08-21";"2015-05-12";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57173;"2015-05-12";"2015-05-12";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57173;"2015-05-13";"2015-06-24";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57174;"2010-10-22";"2010-11-06";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57174;"2010-11-05";"2011-06-16";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57174;"2011-06-17";"2011-08-21";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57174;"2011-08-21";"2015-05-12";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57174;"2015-05-12";"2015-05-12";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57174;"2015-05-13";"2015-06-24";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57175;"2010-10-22";"2010-11-06";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57175;"2010-11-05";"2011-06-16";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57175;"2011-06-17";"2011-08-21";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57175;"2011-08-21";"2015-05-12";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57175;"2015-05-12";"2015-05-12";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57175;"2015-05-13";"2015-06-24";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57176;"2010-10-22";"2010-11-06";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57176;"2010-11-05";"2011-06-16";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57176;"2011-06-17";"2011-08-21";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57176;"2011-08-21";"2015-05-12";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57176;"2015-05-12";"2015-05-12";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    57176;"2015-05-13";"2015-06-24";-40.3509922940501;175.582120540373;"291 Tremaine Avenue Cloverlea Palmerston North"
    
    57181;"2009-12-18";"2010-03-01";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57181;"2010-03-02";"2010-10-05";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57181;"2010-10-06";"2010-11-06";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57181;"2010-11-05";"2011-08-21";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57181;"2011-08-21";"2015-05-12";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57181;"2015-05-12";"2015-05-12";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57181;"2015-05-13";"2015-06-26";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57182;"2009-12-18";"2010-03-01";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57182;"2010-03-02";"2010-10-05";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57182;"2010-10-06";"2010-11-06";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57182;"2010-11-05";"2011-08-21";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57182;"2011-08-21";"2015-05-12";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57182;"2015-05-12";"2015-05-12";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57182;"2015-05-13";"2015-06-26";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57183;"2009-12-18";"2010-03-01";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57183;"2010-03-02";"2010-10-05";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57183;"2010-10-06";"2010-11-06";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57183;"2010-11-05";"2011-08-21";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57183;"2011-08-21";"2015-05-12";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57183;"2015-05-12";"2015-05-12";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    57183;"2015-05-13";"2015-06-26";-40.3399944610284;175.611125325434;"690 Tremaine Avenue Papaioea Palmerston North"
    
    57501;"2009-12-18";"2010-03-08";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57501;"2010-03-09";"2010-11-16";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57501;"2010-11-15";"2011-08-14";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57501;"2011-08-14";"2015-06-03";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57501;"2015-06-04";"";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    
    57502;"2009-12-18";"2010-03-08";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57502;"2010-03-09";"2010-11-16";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57502;"2010-11-15";"2011-08-14";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57502;"2011-08-14";"2015-06-03";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57502;"2015-06-04";"";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    
    57503;"2009-12-18";"2010-03-08";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57503;"2010-03-09";"2010-11-16";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57503;"2010-11-15";"2011-08-14";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57503;"2011-08-14";"2015-06-03";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57503;"2015-06-04";"";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    
    57504;"2009-12-18";"2010-03-08";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57504;"2010-03-09";"2010-11-16";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57504;"2010-11-15";"2011-08-14";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57504;"2011-08-14";"2014-02-27";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57504;"2014-02-28";"2014-03-10";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57504;"2014-03-11";"2016-04-20";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57504;"2015-03-05";"2015-06-03";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57504;"2015-06-04";"";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    
    57505;"2009-12-18";"2010-03-08";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57505;"2010-03-09";"2010-11-16";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57505;"2010-11-15";"2011-08-14";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57505;"2011-08-14";"2014-02-27";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57505;"2014-02-28";"2014-03-10";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57505;"2014-03-11";"2016-04-20";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57505;"2015-03-05";"2015-06-03";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57505;"2015-06-04";"";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    
    57506;"2009-12-18";"2010-03-08";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57506;"2010-03-09";"2010-11-16";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57506;"2010-11-15";"2011-08-14";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57506;"2011-08-14";"2014-02-27";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57506;"2014-02-28";"2014-03-10";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57506;"2014-03-11";"2016-04-20";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57506;"2015-03-05";"2015-06-03";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    57506;"2015-06-04";"";-41.3093951447796;174.780634844893;"Riddiford Street Newtown Wellington"
    
    57511;"2010-04-07";"2010-11-11";-41.2058868411013;174.798916656709;"272 Ohariu Valley Road Churton Park Wellington"
    57511;"2010-11-10";"2011-08-14";-41.2058868411013;174.798916656709;"272 Ohariu Valley Road Churton Park Wellington"
    57511;"2011-08-14";"2011-08-25";-41.2058868411013;174.798916656709;"272 Ohariu Valley Road Churton Park Wellington"
    57511;"2011-08-26";"2013-05-15";-41.2058868411013;174.798916656709;"272 Ohariu Valley Road Churton Park Wellington"
    57511;"2013-05-15";"2013-07-15";-41.2058868411013;174.798916656709;"272 Ohariu Valley Road Churton Park Wellington"
    57511;"2013-07-15";"";-41.2058868411013;174.798916656709;"272 Ohariu Valley Road Churton Park Wellington"
    
    57512;"2010-04-07";"2010-11-11";-41.2058868411013;174.798916656709;"272 Ohariu Valley Road Churton Park Wellington"
    57512;"2010-11-10";"2011-08-14";-41.2058868411013;174.798916656709;"272 Ohariu Valley Road Churton Park Wellington"
    57512;"2011-08-14";"2011-08-25";-41.2058868411013;174.798916656709;"272 Ohariu Valley Road Churton Park Wellington"
    57512;"2011-08-26";"2013-05-15";-41.2058868411013;174.798916656709;"272 Ohariu Valley Road Churton Park Wellington"
    57512;"2013-05-15";"2013-07-15";-41.2058868411013;174.798916656709;"272 Ohariu Valley Road Churton Park Wellington"
    57512;"2013-07-15";"";-41.2058868411013;174.798916656709;"272 Ohariu Valley Road Churton Park Wellington"
    
    57521;"2010-03-23";"2010-10-05";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57521;"2010-10-06";"2010-11-08";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57521;"2010-11-07";"2011-08-21";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57521;"2011-08-21";"2015-05-12";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57521;"2015-05-12";"2015-05-12";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57521;"2015-05-13";"2015-09-14";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57522;"2010-03-23";"2010-10-05";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57522;"2010-10-06";"2010-11-08";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57522;"2010-11-07";"2011-08-21";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57522;"2011-08-21";"2015-05-12";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57522;"2015-05-12";"2015-05-12";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57522;"2015-05-13";"2015-09-14";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57523;"2010-03-23";"2010-10-05";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57523;"2010-10-06";"2010-11-08";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57523;"2010-11-07";"2011-08-21";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57523;"2011-08-21";"2015-05-12";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57523;"2015-05-12";"2015-05-12";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    57523;"2015-05-13";"2015-09-14";-39.9445016500473;174.990067351569;"5 Morrison Street Castlecliff Wanganui"
    
    57531;"2010-02-06";"2010-02-16";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57531;"2010-02-17";"2010-10-05";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57531;"2010-10-06";"2010-10-30";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57531;"2010-10-29";"2011-08-21";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57531;"2011-08-21";"2013-05-15";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57531;"2013-05-15";"2013-07-15";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57531;"2013-07-15";"2014-04-02";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57531;"2014-04-02";"2014-05-27";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57531;"2014-05-27";"2015-09-29";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57531;"2015-09-30";"2015-12-13";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57532;"2010-02-06";"2010-02-16";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57532;"2010-02-17";"2010-10-05";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57532;"2010-10-06";"2010-10-30";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57532;"2010-10-29";"2011-08-21";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57532;"2011-08-21";"2013-05-15";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57532;"2013-05-15";"2013-07-15";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57532;"2013-07-15";"2014-04-02";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57532;"2014-04-02";"2014-05-27";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57532;"2014-05-27";"2015-09-29";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57532;"2015-09-30";"2015-12-13";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57533;"2010-02-06";"2010-02-16";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57533;"2010-02-17";"2010-10-05";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57533;"2010-10-06";"2010-10-30";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57533;"2010-10-29";"2011-08-21";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57533;"2011-08-21";"2013-05-15";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57533;"2013-05-15";"2013-07-15";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57533;"2013-07-15";"2014-04-02";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57533;"2014-04-02";"2014-05-27";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57533;"2014-05-27";"2015-09-29";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    57533;"2015-09-30";"2015-12-13";-41.217462628882;175.457645384904;"13 Kitchener Street Martinborough"
    
    57540;"2010-03-15";"2010-10-05";-40.2331308148349;175.560967890265;"Manfield Park Fielding"
    57540;"2010-10-06";"2010-11-06";-40.2331308148349;175.560967890265;"Manfield Park Fielding"
    57540;"2010-11-05";"2010-12-14";-40.2331308148349;175.560967890265;"Manfield Park Fielding"
    
    57031;"2008-10-01";"2010-03-10";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57031;"2010-03-11";"2010-10-05";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57031;"2010-10-06";"2010-11-18";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57031;"2010-11-17";"2011-08-21";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57031;"2011-08-21";"2013-05-15";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57031;"2013-05-15";"2013-07-15";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57031;"2013-07-15";"2013-11-20";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57032;"2008-10-01";"2010-03-10";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57032;"2010-03-11";"2010-10-05";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57032;"2010-10-06";"2010-11-18";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57032;"2010-11-17";"2011-08-21";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57032;"2011-08-21";"2013-05-15";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57032;"2013-05-15";"2013-07-15";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57032;"2013-07-15";"2013-11-20";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57033;"2008-10-01";"2010-03-10";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57033;"2010-03-11";"2010-10-05";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57033;"2010-10-06";"2010-11-18";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57033;"2010-11-17";"2011-08-21";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57033;"2011-08-21";"2013-05-15";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57033;"2013-05-15";"2013-07-15";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    57033;"2013-07-15";"2013-11-20";-41.1299046374894;175.0466061842;"584-586 Fergusson Drive Upper Hutt"
    
    57041;"2008-10-01";"2010-03-07";-41.2830447397829;174.716630592458;"Makara Road Karori Wellington"
    57041;"2010-03-08";"2010-11-12";-41.2830447397829;174.716630592458;"Makara Road Karori Wellington"
    57041;"2010-11-11";"2011-08-14";-41.2830447397829;174.716630592458;"Makara Road Karori Wellington"
    57041;"2011-08-14";"2015-06-03";-41.2830447397829;174.716630592458;"Makara Road Karori Wellington"
    57041;"2015-06-04";"";-41.2830447397829;174.716630592458;"Makara Road Karori Wellington"
    
    57042;"2008-10-01";"2010-03-07";-41.2830447397829;174.716630592458;"Makara Road Karori Wellington"
    57042;"2010-03-08";"2010-11-12";-41.2830447397829;174.716630592458;"Makara Road Karori Wellington"
    57042;"2010-11-11";"2011-08-14";-41.2830447397829;174.716630592458;"Makara Road Karori Wellington"
    57042;"2011-08-14";"2015-06-03";-41.2830447397829;174.716630592458;"Makara Road Karori Wellington"
    57042;"2015-06-04";"";-41.2830447397829;174.716630592458;"Makara Road Karori Wellington"


    ## consolidate all but first

    35141;"2011-12-07";"2012-01-11";-37.6314744060781;176.177120399725;"Surf Club Corner Adams Avenue and Marine Parade  Mt Maunganui"
    35141;"2012-01-26";"2012-08-08";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"
    35141;"2012-08-08";"2012-08-31";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"
    35141;"2013-06-03";"2014-07-02";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"
    35141;"2014-05-22";"2014-07-02";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"
    35141;"2014-07-03";"2015-03-09";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"
    35141;"2015-03-09";"2015-03-30";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"

    35142;"2011-12-07";"2012-01-11";-37.6314744060781;176.177120399725;"Surf Club Corner Adams Avenue and Marine Parade  Mt Maunganui"
    35142;"2012-01-26";"2012-02-08";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"
    35142;"2013-05-29";"2014-05-23";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"
    35142;"2014-05-22";"2014-07-02";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"
    35142;"2014-07-03";"2015-03-09";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"
    35142;"2015-03-09";"2015-03-30";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"

    35144;"2011-12-07";"2012-01-11";-37.6314744060781;176.177120399725;"Surf Club Corner Adams Avenue and Marine Parade  Mt Maunganui"
    35144;"2012-08-31";"2014-05-23";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"
    35144;"2013-01-18";"2013-01-30";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"
    35144;"2014-05-22";"2014-07-02";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"
    35144;"2014-07-03";"2015-03-09";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"
    35144;"2015-03-10";"2015-03-30";-37.8736292093219;175.350517657156;"Mystery Creek Hamilton"


    ## consolidate all but last

    50511;"2008-10-01";"2010-02-13";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50511;"2010-02-14";"2010-10-03";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50511;"2010-10-04";"2010-11-24";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50511;"2010-11-23";"2011-04-18";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50511;"2011-04-19";"2011-05-29";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50511;"2011-05-29";"2012-08-09";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50511;"2012-08-09";"2013-03-08";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50511;"2013-03-08";"2014-02-11";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50511;"2014-02-11";"2014-03-18";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    50511;"2015-02-12";"2015-02-16";-43.0022738441025;171.589219229014;"Klondyke Corner SH73 Bealey"

    51311;"2008-10-01";"2010-10-11";-41.0443587455842;173.011481912197;"Riwaka to Kaiteriteri Road Kaiteriteri Tasman"
    51311;"2010-10-12";"2010-11-03";-41.0443587455842;173.011481912197;"Riwaka to Kaiteriteri Road Kaiteriteri Tasman"
    51311;"2010-11-02";"2011-05-29";-41.0443587455842;173.011481912197;"Riwaka to Kaiteriteri Road Kaiteriteri Tasman"
    51311;"2011-05-29";"2013-11-28";-41.0443587455842;173.011481912197;"Riwaka to Kaiteriteri Road Kaiteriteri Tasman"
    51311;"2013-11-28";"2014-03-26";-41.0443587455842;173.011481912197;"Riwaka to Kaiteriteri Road Kaiteriteri Tasman"
    51311;"2014-03-26";"2014-11-24";-41.0443587455842;173.011481912197;"Riwaka to Kaiteriteri Road Kaiteriteri Tasman"
    51311;"2014-11-25";"2014-11-25";-41.0443587455842;173.011481912197;"Riwaka to Kaiteriteri Road Kaiteriteri Tasman"
    51311;"2015-10-09";"2016-07-05";-45.9090805057346;168.81832387753;""Otama Road Riversdale Southland""

    51312;"2008-10-01";"2010-10-11";-41.0443587455842;173.011481912197;"Riwaka to Kaiteriteri Road Kaiteriteri Tasman"
    51312;"2010-10-12";"2010-11-03";-41.0443587455842;173.011481912197;"Riwaka to Kaiteriteri Road Kaiteriteri Tasman"
    51312;"2010-11-02";"2011-05-29";-41.0443587455842;173.011481912197;"Riwaka to Kaiteriteri Road Kaiteriteri Tasman"
    51312;"2011-05-29";"2013-11-28";-41.0443587455842;173.011481912197;"Riwaka to Kaiteriteri Road Kaiteriteri Tasman"
    51312;"2013-11-28";"2014-03-26";-41.0443587455842;173.011481912197;"Riwaka to Kaiteriteri Road Kaiteriteri Tasman"
    51312;"2014-03-26";"2014-11-24";-41.0443587455842;173.011481912197;"Riwaka to Kaiteriteri Road Kaiteriteri Tasman"
    51312;"2014-11-25";"2014-11-25";-41.0443587455842;173.011481912197;"Riwaka to Kaiteriteri Road Kaiteriteri Tasman"
    51312;"2015-10-09";"2016-07-05";-45.9090805057346;168.81832387753;""Otama Road Riversdale Southland""

    51561;"2008-10-01";"2010-10-11";-41.1237104100916;172.852900609185;"Mariri Loop Road SH61 Motueka River Valley Nelson"
    51561;"2010-10-12";"2010-11-03";-41.1237104100916;172.852900609185;"Mariri Loop Road SH61 Motueka River Valley Nelson"
    51561;"2010-11-02";"2011-05-29";-41.1237104100916;172.852900609185;"Mariri Loop Road SH61 Motueka River Valley Nelson"
    51561;"2011-05-29";"2013-11-28";-41.1237104100916;172.852900609185;"Mariri Loop Road SH61 Motueka River Valley Nelson"
    51561;"2013-11-28";"2013-12-02";-41.1237104100916;172.852900609185;"Mariri Loop Road SH61 Motueka River Valley Nelson"
    51561;"2013-12-03";"2013-12-11";-41.1237104100916;172.852900609185;"Mariri Loop Road SH61 Motueka River Valley Nelson"
    51561;"2016-10-31";"2016-11-07";-45.8729529792764;170.387005595241;"Gladstone Road North Wingatui Racecourse Mosgiel Dunedin"

    55231;"2008-10-01";"2010-03-09";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55231;"2010-03-10";"2010-11-15";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55231;"2010-11-14";"2011-08-14";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55231;"2011-08-14";"2013-09-29";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55231;"2014-05-27";"";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"

    55232;"2008-10-01";"2010-03-09";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55232;"2010-03-10";"2010-11-15";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55232;"2010-11-14";"2011-08-14";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55232;"2011-08-14";"2013-09-29";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55232;"2014-05-27";"";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"

    55233;"2008-10-01";"2010-03-09";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55233;"2010-03-10";"2010-11-15";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55233;"2010-11-14";"2011-08-14";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55233;"2011-08-14";"2013-09-29";-41.290018825512;174.774391684714;"Manners Street Wellington"
    55233;"2014-05-27";"";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55191;"2008-10-01";"2010-03-10";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55191;"2010-03-11";"2010-11-17";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55191;"2010-11-16";"2011-08-14";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55191;"2011-08-14";"2011-08-25";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55191;"2011-08-26";"2013-05-15";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55191;"2013-05-15";"2013-07-15";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55191;"2013-07-15";"2015-10-20";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55191;"2016-02-12";"";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"

    55192;"2008-10-01";"2010-03-10";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55192;"2010-03-11";"2010-11-17";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55192;"2010-11-16";"2011-08-14";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55192;"2011-08-14";"2011-08-25";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55192;"2011-08-26";"2013-05-15";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55192;"2013-05-15";"2013-07-15";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55192;"2013-07-15";"2015-10-20";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55192;"2016-02-12";"";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"

    55193;"2008-10-01";"2010-03-10";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55193;"2010-03-11";"2010-11-17";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55193;"2010-11-16";"2011-08-14";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55193;"2011-08-14";"2011-08-25";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55193;"2011-08-26";"2013-05-15";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55193;"2013-05-15";"2013-07-15";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55193;"2013-07-15";"2015-10-20";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55193;"2016-02-12";"";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"

    55194;"2009-04-30";"2010-03-10";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55194;"2010-03-11";"2010-11-17";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55194;"2010-11-16";"2011-08-14";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55194;"2011-08-14";"2011-08-25";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55194;"2011-08-26";"2013-05-15";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55194;"2013-05-15";"2013-07-15";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55194;"2013-07-15";"2013-12-19";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55194;"2013-12-20";"2015-10-21";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55194;"2016-02-12";"";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"

    55195;"2009-04-30";"2010-03-10";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55195;"2010-03-11";"2010-11-17";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55195;"2010-11-16";"2011-08-14";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55195;"2011-08-14";"2011-08-25";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55195;"2011-08-26";"2013-05-15";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55195;"2013-05-15";"2013-07-15";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55195;"2013-07-15";"2013-12-19";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55195;"2013-12-20";"2015-10-20";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55195;"2016-02-12";"";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"

    55196;"2009-04-30";"2010-03-10";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55196;"2010-03-11";"2010-11-17";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55196;"2010-11-16";"2011-08-14";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55196;"2011-08-14";"2011-08-25";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55196;"2011-08-26";"2013-05-15";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55196;"2013-05-15";"2013-07-15";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55196;"2013-07-15";"2013-12-19";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55196;"2013-12-20";"2015-10-20";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    55196;"2016-02-12";"";-41.3201560628299;174.796503937111;"55 Coutts Street Kilbirnie (Kilbirnie Exchange) Wellington"

    55351;"2008-10-01";"2010-03-09";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55351;"2010-03-10";"2010-11-15";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55351;"2010-11-14";"2011-08-14";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55351;"2011-08-14";"2013-12-05";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55351;"2013-12-06";"2014-02-23";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55351;"2015-10-21";"";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"

    55352;"2008-10-01";"2010-03-09";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55352;"2010-03-10";"2010-11-15";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55352;"2010-11-14";"2011-08-14";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55352;"2011-08-14";"2013-12-05";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55352;"2013-12-06";"2014-02-23";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55352;"2015-10-21";"";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    
    55353;"2009-04-13";"2010-03-09";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55353;"2010-03-10";"2010-11-15";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55353;"2010-11-14";"2011-07-13";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55353;"2015-10-21";"";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"
    
    55354;"2009-04-13";"2010-03-09";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55354;"2010-03-10";"2010-11-15";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55354;"2010-11-14";"2011-07-13";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55354;"2011-07-14";"2011-08-14";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55354;"2011-08-14";"2013-12-05";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55354;"2013-12-06";"2014-02-23";-41.291223800678;174.779212558858;"Colmar Bruntor House and Aulsebrooks Building Cnr Taranaki Steet and Wakefield Street Wellington"
    55354;"2015-10-21";"";-41.211346757294;174.902997033752;"23-25 Laings Road Lower Hutt"

    55511;"2008-10-01";"2010-03-09";-41.2985592650847;174.778944787211;"169 Tory Street"
    55511;"2010-03-10";"2010-11-15";-41.2985592650847;174.778944787211;"169 Tory Street"
    55511;"2010-11-14";"2011-08-14";-41.2985592650847;174.778944787211;"169 Tory Street"
    55511;"2011-08-14";"2012-09-25";-41.2985592650847;174.778944787211;"169 Tory Street"
    55511;"2015-07-12";"";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"

    55512;"2008-10-01";"2010-03-09";-41.2985592650847;174.778944787211;"169 Tory Street"
    55512;"2010-03-10";"2010-11-15";-41.2985592650847;174.778944787211;"169 Tory Street"
    55512;"2010-11-14";"2011-08-14";-41.2985592650847;174.778944787211;"169 Tory Street"
    55512;"2011-08-14";"2012-09-25";-41.2985592650847;174.778944787211;"169 Tory Street"
    55512;"2015-07-12";"";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"

    55513;"2008-10-01";"2010-03-09";-41.2985592650847;174.778944787211;"169 Tory Street"
    55513;"2010-03-10";"2010-11-15";-41.2985592650847;174.778944787211;"169 Tory Street"
    55513;"2010-11-14";"2011-08-14";-41.2985592650847;174.778944787211;"169 Tory Street"
    55513;"2011-08-14";"2012-09-25";-41.2985592650847;174.778944787211;"169 Tory Street"
    55513;"2015-07-12";"";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    
    55514;"2009-04-13";"2010-03-09";-41.2985592650847;174.778944787211;"169 Tory Street"
    55514;"2010-03-10";"2010-11-15";-41.2985592650847;174.778944787211;"169 Tory Street"
    55514;"2010-11-14";"2011-08-14";-41.2985592650847;174.778944787211;"169 Tory Street"
    55514;"2011-08-14";"2011-09-28";-41.2985592650847;174.778944787211;"169 Tory Street"
    55514;"2011-09-29";"2012-09-25";-41.2985592650847;174.778944787211;"169 Tory Street"
    55514;"2015-07-12";"";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    
    55515;"2009-04-13";"2010-03-09";-41.2985592650847;174.778944787211;"169 Tory Street"
    55515;"2010-03-10";"2010-11-15";-41.2985592650847;174.778944787211;"169 Tory Street"
    55515;"2010-11-14";"2011-08-14";-41.2985592650847;174.778944787211;"169 Tory Street"
    55515;"2011-08-14";"2011-09-28";-41.2985592650847;174.778944787211;"169 Tory Street"
    55515;"2011-09-29";"2012-09-25";-41.2985592650847;174.778944787211;"169 Tory Street"
    55515;"2015-07-12";"";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"
    
    55516;"2009-04-13";"2010-03-09";-41.2985592650847;174.778944787211;"169 Tory Street"
    55516;"2010-03-10";"2010-11-15";-41.2985592650847;174.778944787211;"169 Tory Street"
    55516;"2010-11-14";"2011-08-14";-41.2985592650847;174.778944787211;"169 Tory Street"
    55516;"2011-08-14";"2011-09-28";-41.2985592650847;174.778944787211;"169 Tory Street"
    55516;"2011-09-29";"2012-09-25";-41.2985592650847;174.778944787211;"169 Tory Street"
    55516;"2015-07-12";"";-41.2982030124072;174.775161337388;"224 Taranaki Street Te Aro Wellington"    
    
    55621;"2010-04-14";"2010-10-05";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55621;"2010-10-06";"2010-11-08";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55621;"2010-11-07";"2011-08-21";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55621;"2011-08-21";"2015-05-12";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55621;"2015-05-12";"2015-05-12";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55621;"2015-05-13";"2015-09-30";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55621;"2016-11-03";"";-40.2013953040018;176.085847868525;"Rule Road Dannevirke"
    
    55622;"2010-04-14";"2010-10-05";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55622;"2010-10-06";"2010-11-08";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55622;"2010-11-07";"2011-08-21";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55622;"2011-08-21";"2015-05-12";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55622;"2015-05-12";"2015-05-12";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55622;"2015-05-13";"2015-09-30";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55622;"2016-11-03";"";-40.2013953040018;176.085847868525;"Rule Road Dannevirke"
    
    55623;"2010-04-14";"2010-10-05";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55623;"2010-10-06";"2010-11-08";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55623;"2010-11-07";"2011-08-21";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55623;"2011-08-21";"2015-05-12";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55623;"2015-05-12";"2015-05-12";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55623;"2015-05-13";"2015-09-30";-40.1619243005622;175.363142011719;"Lake Alice Road Bulls"
    55623;"2016-11-03";"";-40.2013953040018;176.085847868525;"Rule Road Dannevirke"

    55981;"2008-10-01";"2010-03-01";-40.4130673679259;175.598386464186;"SH57 Linton Palmerston North"
    55981;"2010-03-02";"2010-10-05";-40.4130673679259;175.598386464186;"SH57 Linton Palmerston North"
    55981;"2010-10-06";"2010-11-06";-40.4130673679259;175.598386464186;"SH57 Linton Palmerston North"
    55981;"2010-11-05";"2011-08-21";-40.4130673679259;175.598386464186;"SH57 Linton Palmerston North"
    55981;"2011-08-21";"2015-03-09";-40.4130673679259;175.598386464186;"SH57 Linton Palmerston North"
    55981;"2015-03-10";"2015-03-11";-40.4130673679259;175.598386464186;"SH57 Linton Palmerston North"
    55981;"2015-10-14";"";-39.9534061569606;175.492584190705;"Mt Curl Road Mt Ashcroft"
    
    55982;"2008-10-01";"2010-03-01";-40.4130673679259;175.598386464186;"SH57 Linton Palmerston North"
    55982;"2010-03-02";"2010-10-05";-40.4130673679259;175.598386464186;"SH57 Linton Palmerston North"
    55982;"2010-10-06";"2010-11-06";-40.4130673679259;175.598386464186;"SH57 Linton Palmerston North"
    55982;"2010-11-05";"2011-08-21";-40.4130673679259;175.598386464186;"SH57 Linton Palmerston North"
    55982;"2011-08-21";"2015-03-09";-40.4130673679259;175.598386464186;"SH57 Linton Palmerston North"
    55982;"2015-03-10";"2015-03-11";-40.4130673679259;175.598386464186;"SH57 Linton Palmerston North"
    55982;"2015-10-14";"";-39.9534061569606;175.492584190705;"Mt Curl Road Mt Ashcroft"
    
    56081;"2008-10-01";"2010-02-25";-40.9484045663887;175.662580380018;"Pt Ground Floor Departmental Building 31 Chapel Street Masterton"
    56081;"2010-02-26";"2010-10-05";-40.9484045663887;175.662580380018;"Pt Ground Floor Departmental Building 31 Chapel Street Masterton"
    56081;"2010-10-06";"2010-11-01";-40.9484045663887;175.662580380018;"Pt Ground Floor Departmental Building 31 Chapel Street Masterton"
    56081;"2010-10-31";"2011-08-20";-40.9484045663887;175.662580380018;"Pt Ground Floor Departmental Building 31 Chapel Street Masterton"
    56081;"2012-02-20";"2013-10-02";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    
    56082;"2008-10-01";"2010-02-25";-40.9484045663887;175.662580380018;"Pt Ground Floor Departmental Building 31 Chapel Street Masterton"
    56082;"2010-02-26";"2010-10-05";-40.9484045663887;175.662580380018;"Pt Ground Floor Departmental Building 31 Chapel Street Masterton"
    56082;"2010-10-06";"2010-11-01";-40.9484045663887;175.662580380018;"Pt Ground Floor Departmental Building 31 Chapel Street Masterton"
    56082;"2010-10-31";"2011-08-20";-40.9484045663887;175.662580380018;"Pt Ground Floor Departmental Building 31 Chapel Street Masterton"
    56082;"2012-02-20";"2013-10-02";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    
    56083;"2008-10-01";"2010-02-25";-40.9484045663887;175.662580380018;"Pt Ground Floor Departmental Building 31 Chapel Street Masterton"
    56083;"2010-02-26";"2010-10-05";-40.9484045663887;175.662580380018;"Pt Ground Floor Departmental Building 31 Chapel Street Masterton"
    56083;"2010-10-06";"2010-11-01";-40.9484045663887;175.662580380018;"Pt Ground Floor Departmental Building 31 Chapel Street Masterton"
    56083;"2010-10-31";"2011-08-20";-40.9484045663887;175.662580380018;"Pt Ground Floor Departmental Building 31 Chapel Street Masterton"
    56083;"2012-02-20";"2013-10-02";-41.278816753887;174.775427434939;"Corner Terrace and Bown Street Wellington"
    
    56472;"2008-10-01";"2010-03-02";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56472;"2010-03-03";"2010-10-05";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56472;"2010-10-06";"2010-11-08";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56472;"2010-11-07";"2011-04-19";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56472;"2011-04-20";"2011-08-21";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56472;"2011-08-21";"2014-09-10";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56472;"2014-09-11";"2014-10-04";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56472;"2015-05-13";"2017-03-16";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    
    56473;"2008-10-01";"2010-03-02";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56473;"2010-03-03";"2010-10-05";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56473;"2010-10-06";"2010-11-08";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56473;"2010-11-07";"2011-04-19";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56473;"2011-04-20";"2011-08-21";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56473;"2011-08-21";"2014-09-10";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56473;"2014-09-11";"2014-10-04";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56473;"2016-07-04";"2017-03-16";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"

    56475;"2010-07-07";"2010-10-05";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56475;"2010-10-06";"2010-11-08";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56475;"2010-11-07";"2011-04-19";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56475;"2011-04-20";"2011-08-21";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56475;"2011-08-21";"2014-09-10";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56475;"2014-09-11";"2014-10-04";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56475;"2015-05-13";"2017-03-16";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    
    56476;"2010-07-07";"2010-10-05";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56476;"2010-10-06";"2010-11-08";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56476;"2010-11-07";"2011-04-19";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56476;"2011-04-20";"2011-08-21";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56476;"2011-08-21";"2014-09-10";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56476;"2014-09-11";"2014-10-04";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56476;"2016-07-04";"2017-03-16";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    
    56631;"2008-10-01";"2010-03-08";-41.4075503860125;174.871252298885;"Coast Road Wainuiomata Lower Hutt"
    56631;"2010-03-09";"2010-11-16";-41.4075503860125;174.871252298885;"Coast Road Wainuiomata Lower Hutt"
    56631;"2010-11-15";"2011-08-14";-41.4075503860125;174.871252298885;"Coast Road Wainuiomata Lower Hutt"
    56631;"2011-08-14";"2015-06-03";-41.4075503860125;174.871252298885;"Coast Road Wainuiomata Lower Hutt"
    56631;"2015-06-04";"2015-10-07";-41.4075503860125;174.871252298885;"Coast Road Wainuiomata Lower Hutt"
    56631;"2015-11-15";"";-38.8840174216755;175.204882397148;"Off Kururau Road Taumaranui"
    
    56641;"2008-10-01";"2010-03-07";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56641;"2010-03-08";"2010-11-11";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56641;"2010-11-10";"2011-08-14";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56641;"2011-08-14";"2011-08-25";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56641;"2011-08-26";"2013-05-15";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56641;"2013-05-15";"2013-07-15";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56641;"2013-07-15";"2014-04-02";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56641;"2014-04-02";"2014-05-27";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56641;"2014-05-27";"2015-09-29";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56641;"2015-09-30";"2015-11-11";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56641;"2016-12-09";"";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    
    56642;"2008-10-01";"2010-03-07";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56642;"2010-03-08";"2010-11-11";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56642;"2010-11-10";"2011-08-14";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56642;"2011-08-14";"2011-08-25";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56642;"2011-08-26";"2013-05-15";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56642;"2013-05-15";"2013-07-15";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56642;"2013-07-15";"2014-04-02";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56642;"2014-04-02";"2014-05-27";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56642;"2014-05-27";"2015-09-29";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56642;"2015-09-30";"2015-11-11";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56642;"2016-12-09";"";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"
    
    56643;"2008-10-01";"2010-03-07";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56643;"2010-03-08";"2010-11-11";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56643;"2010-11-10";"2011-08-14";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56643;"2011-08-14";"2011-08-25";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56643;"2011-08-26";"2013-05-15";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56643;"2013-05-15";"2013-07-15";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56643;"2013-07-15";"2014-04-02";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56643;"2014-04-02";"2014-05-27";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56643;"2014-05-27";"2015-09-29";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56643;"2015-09-30";"2015-11-11";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    56643;"2016-12-09";"";-41.1472563777852;174.809046178789;"Bluff Road Elsdon Porirua"

    57051;"2008-10-01";"2010-03-07";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57051;"2010-03-08";"2010-11-11";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57051;"2010-11-10";"2011-08-14";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57051;"2011-08-14";"2011-08-25";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57051;"2011-08-26";"2013-05-15";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57051;"2013-05-15";"2013-07-15";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57051;"2013-07-15";"2014-04-02";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57051;"2014-04-02";"2014-05-27";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57051;"2014-05-27";"2015-09-29";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57051;"2015-09-30";"2015-11-17";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57051;"2016-10-13";"";-41.2576563514747;174.955117539129;"Wright Street Wainuiomata"
    
    57052;"2008-10-01";"2010-03-07";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57052;"2010-03-08";"2010-11-11";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57052;"2010-11-10";"2011-08-14";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57052;"2011-08-14";"2011-08-25";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57052;"2011-08-26";"2013-05-15";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57052;"2013-05-15";"2013-07-15";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57052;"2013-07-15";"2014-04-02";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57052;"2014-04-02";"2014-05-27";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57052;"2014-05-27";"2015-09-29";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57052;"2015-09-30";"2015-11-17";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    57052;"2016-10-13";"";-41.2576563514747;174.955117539129;"Wright Street Wainuiomata"
        
    
    56971;"2008-10-01";"2010-03-10";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56971;"2010-03-11";"2010-10-05";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56971;"2010-10-06";"2010-11-18";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56971;"2010-11-17";"2011-08-21";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56971;"2011-08-21";"2013-05-15";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56971;"2013-05-15";"2013-07-15";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56971;"2013-07-15";"2014-04-02";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56971;"2014-04-02";"2014-05-27";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56971;"2014-05-27";"2015-09-29";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56971;"2015-09-30";"2015-10-05";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56971;"2015-11-12";"";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    
    56972;"2008-10-01";"2010-03-10";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56972;"2010-03-11";"2010-10-05";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56972;"2010-10-06";"2010-11-18";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56972;"2010-11-17";"2011-08-21";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56972;"2011-08-21";"2013-05-15";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56972;"2013-05-15";"2013-07-15";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56972;"2013-07-15";"2014-04-02";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56972;"2014-04-02";"2014-05-27";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56972;"2014-05-27";"2015-09-29";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56972;"2015-09-30";"2015-10-05";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56972;"2015-11-12";"";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    
    56973;"2008-10-01";"2010-03-10";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56973;"2010-03-11";"2010-10-05";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56973;"2010-10-06";"2010-11-18";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56973;"2010-11-17";"2011-08-21";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56973;"2011-08-21";"2013-05-15";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56973;"2013-05-15";"2013-07-15";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56973;"2013-07-15";"2014-04-02";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56973;"2014-04-02";"2014-05-27";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56973;"2014-05-27";"2015-09-29";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56973;"2015-09-30";"2015-10-05";-41.1469042978892;175.009419193431;"behind 150 Fergusson Drive Silverstream Upper Hutt"
    56973;"2015-11-12";"";-41.0894143311991;174.87124100137;"Plimmerton Reservoir Cnr Tremaine Place and Pope Street Camborne Porirua City"
    
    56981;"2010-04-09";"2010-11-11";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56981;"2010-11-10";"2011-08-14";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56981;"2011-08-14";"2011-08-25";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56981;"2011-08-26";"2013-05-15";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56981;"2013-05-15";"2013-07-15";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56981;"2013-07-15";"2014-04-02";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56981;"2014-04-02";"2014-05-27";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56981;"2014-05-27";"2014-11-12";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56981;"2014-11-13";"2014-11-15";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56981;"2015-10-23";"";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    
    56982;"2010-04-09";"2010-11-11";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56982;"2010-11-10";"2011-08-14";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56982;"2011-08-14";"2011-08-25";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56982;"2011-08-26";"2013-05-15";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56982;"2013-05-15";"2013-07-15";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56982;"2013-07-15";"2014-04-02";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56982;"2014-04-02";"2014-05-27";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56982;"2014-05-27";"2014-11-12";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56982;"2014-11-13";"2014-11-15";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56982;"2015-10-23";"";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    
    56983;"2010-04-09";"2010-11-11";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56983;"2010-11-10";"2011-08-14";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56983;"2011-08-14";"2011-08-25";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56983;"2011-08-26";"2013-05-15";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56983;"2013-05-15";"2013-07-15";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56983;"2013-07-15";"2014-04-02";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56983;"2014-04-02";"2014-05-27";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56983;"2014-05-27";"2014-11-12";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56983;"2014-11-13";"2014-11-15";-41.1815680773479;174.829954043148;"Tawa Wellington"
    56983;"2015-10-23";"";-41.0370655775066;174.877828494618;"Rawhiti Road Pukerua Bay Porirua City"
    
    57011;"2008-10-01";"2010-03-10";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57011;"2010-03-11";"2010-10-05";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57011;"2010-10-06";"2010-11-18";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57011;"2010-11-17";"2011-08-21";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57011;"2011-08-21";"2013-05-15";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57011;"2013-05-15";"2013-07-15";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57011;"2013-07-15";"2014-04-02";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57011;"2014-04-02";"2014-05-27";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57011;"2014-05-27";"2015-09-29";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57011;"2015-09-30";"2015-10-07";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57011;"2015-11-18";"";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    
    57012;"2008-10-01";"2010-03-10";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57012;"2010-03-11";"2010-10-05";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57012;"2010-10-06";"2010-11-18";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57012;"2010-11-17";"2011-08-21";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57012;"2011-08-21";"2013-05-15";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57012;"2013-05-15";"2013-07-15";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57012;"2013-07-15";"2014-04-02";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57012;"2014-04-02";"2014-05-27";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57012;"2014-05-27";"2015-09-29";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57012;"2015-09-30";"2015-10-07";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57012;"2015-11-18";"";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"
    
    57013;"2008-10-01";"2010-03-10";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57013;"2010-03-11";"2010-10-05";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57013;"2010-10-06";"2010-11-18";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57013;"2010-11-17";"2011-08-21";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57013;"2011-08-21";"2013-05-15";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57013;"2013-05-15";"2013-07-15";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57013;"2013-07-15";"2014-04-02";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57013;"2014-04-02";"2014-05-27";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57013;"2014-05-27";"2015-09-29";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57013;"2015-09-30";"2015-10-07";-41.0888446948268;175.109066521586;"Access track at the end of Gillespies Road Te Marua"
    57013;"2015-11-18";"";-41.1303141449908;174.88077204563;"210 Warespite Avenue Waitangirua Porirua"

    ## consolidate into 2+ groups

    50561;"2008-10-01";"2010-10-11";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50561;"2010-10-12";"2010-11-04";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50561;"2010-11-03";"2011-05-29";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50561;"2011-05-29";"2013-11-28";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50561;"2013-11-28";"2014-03-06";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50561;"2014-03-07";"2014-03-19";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50561;"2014-09-18";"2015-09-24";-43.5139766705226;172.375115850725;"198 Halkett Road West Melton Selwyn District"
    50561;"2015-09-25";"2016-04-27";-43.5139766705226;172.375115850725;"198 Halkett Road West Melton Selwyn District"

    50562;"2008-10-01";"2010-10-11";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50562;"2010-10-12";"2010-11-04";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50562;"2010-11-03";"2011-05-29";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50562;"2011-05-29";"2013-11-28";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50562;"2013-11-28";"2014-03-06";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50562;"2014-03-07";"2014-03-19";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50562;"2014-09-18";"2015-09-24";-43.5139766705226;172.375115850725;"198 Halkett Road West Melton Selwyn District"
    50562;"2015-09-25";"2016-04-27";-43.5139766705226;172.375115850725;"198 Halkett Road West Melton Selwyn District"

    50563;"2008-10-01";"2010-10-11";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50563;"2010-10-12";"2010-11-04";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50563;"2010-11-03";"2011-05-29";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50563;"2011-05-29";"2013-11-28";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50563;"2013-11-28";"2014-03-06";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50563;"2014-03-07";"2014-03-19";-41.5140953763145;173.957112173768;"NZ Post Office cnr Main Steet and Scott Street Blenheim"
    50563;"2014-09-18";"2015-09-24";-43.5139766705226;172.375115850725;"198 Halkett Road West Melton Selwyn District"
    50563;"2015-09-25";"2016-04-27";-43.5139766705226;172.375115850725;"198 Halkett Road West Melton Selwyn District"

    51041;"2008-10-01";"2010-02-13";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51041;"2010-02-14";"2010-10-03";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51041;"2010-10-04";"2010-11-24";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51041;"2010-11-23";"2011-04-18";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51041;"2011-04-19";"2011-05-29";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51041;"2011-05-29";"2012-08-09";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51041;"2012-08-09";"2013-03-08";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51041;"2013-03-08";"2014-02-11";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51041;"2014-02-11";"2014-03-18";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51041;"2014-03-18";"2014-07-01";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51041;"2014-07-02";"2014-07-03";-44.3875037524407;171.202782171005;"391 Wai-iti Road Gleniti Timaru District"
    51041;"2015-03-03";"2015-04-21";-44.9483984021104;168.852588972082;"Glencoe Road Crown Terrace"
    51041;"2015-04-22";"2016-08-04";-44.9483984021104;168.852588972082;"Glencoe Road Crown Terrace"
    51041;"2016-08-05";"2016-08-15";-44.9483984021104;168.852588972082;"Glencoe Road Crown Terrace"
    51041;"2016-08-16";"2016-08-22";-44.9483984021104;168.852588972082;"Glencoe Road Crown Terrace"
    51041;"2016-08-23";"2016-11-26";-44.9483984021104;168.852588972082;"Glencoe Road Crown Terrace"

    51631;"2008-10-01";"2010-10-11";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51631;"2010-10-12";"2010-11-03";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51631;"2010-11-02";"2011-05-29";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51631;"2011-05-29";"2013-11-28";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51631;"2013-11-28";"2014-03-26";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51631;"2014-03-26";"2014-04-11";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51631;"2014-04-12";"2014-04-13";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51631;"2015-10-23";"2016-08-30";-45.9423679334578;170.226523093816;"State Highway 1 9 McLaren Gully Road Gledknowe Hill Taieri Plains (out of Dunedin)"
    51631;"2016-08-30";"";-45.9423679334578;170.226523093816;"State Highway 1 9 McLaren Gully Road Gledknowe Hill Taieri Plains (out of Dunedin)"

    51632;"2008-10-01";"2010-10-11";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51632;"2010-10-12";"2010-11-03";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51632;"2010-11-02";"2011-05-29";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51632;"2011-05-29";"2013-11-28";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51632;"2013-11-28";"2014-03-26";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51632;"2014-03-26";"2014-04-11";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51632;"2014-04-12";"2014-04-13";-41.2710682648392;173.283265495865;"110 Trafalgar Street Nelson"
    51632;"2015-10-23";"2016-08-30";-45.9423679334578;170.226523093816;"State Highway 1 9 McLaren Gully Road Gledknowe Hill Taieri Plains (out of Dunedin)"
    51632;"2016-08-30";"";-45.9423679334578;170.226523093816;"State Highway 1 9 McLaren Gully Road Gledknowe Hill Taieri Plains (out of Dunedin)"

    52354;"2010-05-17";"2010-10-11";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52354;"2010-10-12";"2010-11-04";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52354;"2010-11-03";"2011-05-29";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52354;"2011-05-29";"2012-02-15";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52354;"2012-02-16";"2013-11-28";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52354;"2013-11-28";"2014-02-28";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52354;"2014-03-01";"2014-03-03";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52354;"2014-07-11";"2015-05-27";-45.1169386387291;170.967719326512;"Bushey Beach Road Cape Wanbrow Oamaru"
    52354;"2015-05-27";"";-45.1169386387291;170.967719326512;"Bushey Beach Road Cape Wanbrow Oamaru"

    52355;"2010-05-17";"2010-10-11";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52355;"2010-10-12";"2010-11-04";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52355;"2010-11-03";"2011-05-29";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52355;"2011-05-29";"2012-02-15";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52355;"2012-02-16";"2013-11-28";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52355;"2013-11-28";"2014-02-28";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52355;"2014-03-01";"2014-03-03";-41.5480963738185;173.985406487946;"Wither Hills Cobb Cottage Road Riverlands Blenheim"
    52355;"2014-07-11";"2015-05-27";-45.1169386387291;170.967719326512;"Bushey Beach Road Cape Wanbrow Oamaru"
    52355;"2015-05-27";"";-45.1169386387291;170.967719326512;"Bushey Beach Road Cape Wanbrow Oamaru"
    
    55531;"2008-10-01";"2010-02-25";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55531;"2010-02-26";"2010-10-05";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55531;"2010-10-06";"2010-11-01";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55531;"2010-10-31";"2011-08-21";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55531;"2011-08-21";"2013-05-15";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55531;"2013-05-15";"2013-07-15";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55531;"2013-07-15";"2014-04-02";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55531;"2014-04-02";"2014-05-07";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55531;"2014-05-08";"2014-05-11";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55531;"2014-10-06";"2015-05-12";-39.9079770947753;175.05348230621;"20 Kaikokopu Road Wanganui"
    55531;"2015-05-12";"2015-05-12";-39.9079770947753;175.05348230621;"20 Kaikokopu Road Wanganui"
    55531;"2015-05-13";"2015-09-13";-39.9079770947753;175.05348230621;"20 Kaikokopu Road Wanganui"

    55532;"2008-10-01";"2010-02-25";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55532;"2010-02-26";"2010-10-05";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55532;"2010-10-06";"2010-11-01";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55532;"2010-10-31";"2011-08-21";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55532;"2011-08-21";"2013-05-15";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55532;"2013-05-15";"2013-07-15";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55532;"2013-07-15";"2014-04-02";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55532;"2014-04-02";"2014-05-07";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55532;"2014-05-08";"2014-05-11";-41.0208157510053;175.435414253167;"1km off Jervois Road Dalefield Carterton Wairarapa"
    55532;"2014-10-06";"2015-05-12";-39.9079770947753;175.05348230621;"20 Kaikokopu Road Wanganui"
    55532;"2015-05-12";"2015-05-12";-39.9079770947753;175.05348230621;"20 Kaikokopu Road Wanganui"
    55532;"2015-05-13";"2015-09-13";-39.9079770947753;175.05348230621;"20 Kaikokopu Road Wanganui"
    
    55551;"2008-10-01";"2010-03-05";-40.8501668834495;176.226916466321;"Whakataki Hill network site Masterton Castlepoint Road Castlepoint"
    55551;"2010-03-06";"2010-10-05";-40.8501668834495;176.226916466321;"Whakataki Hill network site Masterton Castlepoint Road Castlepoint"
    55551;"2010-10-06";"2010-11-05";-40.8501668834495;176.226916466321;"Whakataki Hill network site Masterton Castlepoint Road Castlepoint"
    55551;"2010-11-04";"2011-08-21";-40.8501668834495;176.226916466321;"Whakataki Hill network site Masterton Castlepoint Road Castlepoint"
    55551;"2011-08-21";"2013-06-18";-40.8501668834495;176.226916466321;"Whakataki Hill network site Masterton Castlepoint Road Castlepoint"
    55551;"2013-06-18";"2014-10-15";-40.8501668834495;176.226916466321;"Whakataki Hill network site Masterton Castlepoint Road Castlepoint"
    55551;"2014-10-16";"2014-10-16";-40.8501668834495;176.226916466321;"Whakataki Hill network site Masterton Castlepoint Road Castlepoint"
    55551;"2015-03-29";"2015-05-12";-39.4902959280203;176.91709108958;"Manchester Unity Building 100 Emerson Street Napier"
    55551;"2015-05-12";"2015-05-20";-39.4902959280203;176.91709108958;"Manchester Unity Building 100 Emerson Street Napier"
    
    55552;"2008-10-01";"2010-03-05";-40.8501668834495;176.226916466321;"Whakataki Hill network site Masterton Castlepoint Road Castlepoint"
    55552;"2010-03-06";"2010-10-05";-40.8501668834495;176.226916466321;"Whakataki Hill network site Masterton Castlepoint Road Castlepoint"
    55552;"2010-10-06";"2010-11-05";-40.8501668834495;176.226916466321;"Whakataki Hill network site Masterton Castlepoint Road Castlepoint"
    55552;"2010-11-04";"2011-08-21";-40.8501668834495;176.226916466321;"Whakataki Hill network site Masterton Castlepoint Road Castlepoint"
    55552;"2011-08-21";"2013-06-18";-40.8501668834495;176.226916466321;"Whakataki Hill network site Masterton Castlepoint Road Castlepoint"
    55552;"2013-06-18";"2014-10-15";-40.8501668834495;176.226916466321;"Whakataki Hill network site Masterton Castlepoint Road Castlepoint"
    55552;"2014-10-16";"2014-10-16";-40.8501668834495;176.226916466321;"Whakataki Hill network site Masterton Castlepoint Road Castlepoint"
    55552;"2015-03-29";"2015-05-12";-39.4902959280203;176.91709108958;"Manchester Unity Building 100 Emerson Street Napier"
    55552;"2015-05-12";"2015-05-20";-39.4902959280203;176.91709108958;"Manchester Unity Building 100 Emerson Street Napier"    

    55741;"2008-10-01";"2010-02-27";-39.6274355872961;176.786207441426;"Flaxmere Waterworld 30 Swansea Road Flaxmere"
    55741;"2010-02-28";"2011-05-29";-39.6274355872961;176.786207441426;"Flaxmere Waterworld 30 Swansea Road Flaxmere"
    55741;"2011-05-29";"2011-11-08";-39.6274355872961;176.786207441426;"Flaxmere Waterworld 30 Swansea Road Flaxmere"
    55741;"2011-11-08";"2012-12-15";-39.6274355872961;176.786207441426;"Flaxmere Waterworld 30 Swansea Road Flaxmere"
    55741;"2011-11-15";"2013-09-18";-39.0831775310476;174.084463623907;"20 Saltash Street Brooklands New Plymouth"
    55741;"2013-09-19";"2015-05-06";-39.0831775310476;174.084463623907;"20 Saltash Street Brooklands New Plymouth"
    55741;"2015-05-06";"2015-05-28";-39.0831775310476;174.084463623907;"20 Saltash Street Brooklands New Plymouth"
    
    55742;"2008-10-01";"2010-02-27";-39.6274355872961;176.786207441426;"Flaxmere Waterworld 30 Swansea Road Flaxmere"
    55742;"2010-02-28";"2011-05-29";-39.6274355872961;176.786207441426;"Flaxmere Waterworld 30 Swansea Road Flaxmere"
    55742;"2011-05-29";"2011-11-08";-39.6274355872961;176.786207441426;"Flaxmere Waterworld 30 Swansea Road Flaxmere"
    55742;"2011-11-08";"2012-12-15";-39.6274355872961;176.786207441426;"Flaxmere Waterworld 30 Swansea Road Flaxmere"
    55742;"2011-11-15";"2013-09-18";-39.0831775310476;174.084463623907;"20 Saltash Street Brooklands New Plymouth"
    55742;"2013-09-19";"2015-05-06";-39.0831775310476;174.084463623907;"20 Saltash Street Brooklands New Plymouth"
    55742;"2015-05-06";"2015-05-28";-39.0831775310476;174.084463623907;"20 Saltash Street Brooklands New Plymouth"
    
    55743;"2008-10-01";"2010-02-27";-39.6274355872961;176.786207441426;"Flaxmere Waterworld 30 Swansea Road Flaxmere"
    55743;"2010-02-28";"2011-05-29";-39.6274355872961;176.786207441426;"Flaxmere Waterworld 30 Swansea Road Flaxmere"
    55743;"2011-05-29";"2011-11-08";-39.6274355872961;176.786207441426;"Flaxmere Waterworld 30 Swansea Road Flaxmere"
    55743;"2011-11-08";"2012-12-15";-39.6274355872961;176.786207441426;"Flaxmere Waterworld 30 Swansea Road Flaxmere"
    55743;"2011-11-15";"2013-09-18";-39.0831775310476;174.084463623907;"20 Saltash Street Brooklands New Plymouth"
    55743;"2013-09-19";"2015-05-06";-39.0831775310476;174.084463623907;"20 Saltash Street Brooklands New Plymouth"
    55743;"2015-05-06";"2015-05-28";-39.0831775310476;174.084463623907;"20 Saltash Street Brooklands New Plymouth"
    
    55761;"2008-10-01";"2010-03-01";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55761;"2010-03-02";"2010-10-05";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55761;"2010-10-06";"2010-11-09";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55761;"2010-11-08";"2011-08-21";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55761;"2011-08-21";"2013-12-05";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55761;"2013-12-06";"2013-12-08";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55761;"2014-06-19";"2015-05-06";-39.4284850894751;175.287749108547;"53 Raetihi Ohakune Road Raetihi"
    55761;"2015-05-07";"";-39.4284850894751;175.287749108547;"53 Raetihi Ohakune Road Raetihi"
    
    55762;"2008-10-01";"2010-03-01";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55762;"2010-03-02";"2010-10-05";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55762;"2010-10-06";"2010-11-09";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55762;"2010-11-08";"2011-08-21";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55762;"2011-08-21";"2013-12-05";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55762;"2013-12-06";"2013-12-08";-40.4779383472848;175.281470109429;"Water Tower Foxton Horowhenua"
    55762;"2014-06-19";"2015-05-06";-39.4284850894751;175.287749108547;"53 Raetihi Ohakune Road Raetihi"
    55762;"2015-05-07";"";-39.4284850894751;175.287749108547;"53 Raetihi Ohakune Road Raetihi"
    56271;"2008-10-01";"2010-02-25";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56271;"2010-02-26";"2010-10-05";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56271;"2010-10-06";"2010-11-01";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56271;"2010-10-31";"2011-08-21";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56271;"2011-08-21";"2013-05-15";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56271;"2013-05-15";"2013-07-15";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56271;"2013-07-15";"2014-02-18";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56271;"2014-02-19";"2014-02-24";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56271;"2014-06-26";"2014-09-19";-39.4012321006849;175.41248691442;"Old Station Road Ohakune"
    56271;"2015-03-31";"2015-05-12";-40.7292825533926;175.145254180519;"156 Waiorongomai Road Otaki Beach"
    56271;"2015-05-12";"2015-05-12";-40.7292825533926;175.145254180519;"156 Waiorongomai Road Otaki Beach"
    56271;"2015-05-13";"2017-03-16";-40.7292825533926;175.145254180519;"156 Waiorongomai Road Otaki Beach"
    
    56272;"2008-10-01";"2010-02-25";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56272;"2010-02-26";"2010-10-05";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56272;"2010-10-06";"2010-11-01";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56272;"2010-10-31";"2011-08-21";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56272;"2011-08-21";"2013-05-15";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56272;"2013-05-15";"2013-07-15";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56272;"2013-07-15";"2014-02-18";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56272;"2014-02-19";"2014-02-24";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56272;"2014-06-26";"2014-09-19";-39.4012321006849;175.41248691442;"Old Station Road Ohakune"
    56272;"2015-03-31";"2015-05-12";-40.7292825533926;175.145254180519;"156 Waiorongomai Road Otaki Beach"
    56272;"2015-05-12";"2015-05-12";-40.7292825533926;175.145254180519;"156 Waiorongomai Road Otaki Beach"
    56272;"2015-05-13";"2017-03-16";-40.7292825533926;175.145254180519;"156 Waiorongomai Road Otaki Beach"
    
    56273;"2008-10-01";"2010-02-25";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56273;"2010-02-26";"2010-10-05";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56273;"2010-10-06";"2010-11-01";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56273;"2010-10-31";"2011-08-21";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56273;"2011-08-21";"2013-05-15";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56273;"2013-05-15";"2013-07-15";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56273;"2013-07-15";"2014-02-18";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56273;"2014-02-19";"2014-02-24";-40.8493642847878;175.736241486389;"Off James Road Rangitumau"
    56273;"2015-03-31";"2015-05-12";-40.7292825533926;175.145254180519;"156 Waiorongomai Road Otaki Beach"
    56273;"2015-05-12";"2015-05-12";-40.7292825533926;175.145254180519;"156 Waiorongomai Road Otaki Beach"
    56273;"2015-05-13";"2017-03-16";-40.7292825533926;175.145254180519;"156 Waiorongomai Road Otaki Beach"
    
    
    56401;"2008-10-01";"2010-02-26";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56401;"2010-02-27";"2010-10-05";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56401;"2010-10-06";"2010-11-05";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56401;"2010-11-04";"2011-08-21";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56401;"2011-08-21";"2014-03-04";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56401;"2014-03-05";"2014-03-05";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56401;"2014-06-25";"2015-05-06";-39.0102363928792;174.395969141049;"158 Kaipikari Road Upper Urenui"
    56401;"2015-05-06";"2015-05-27";-39.0102363928792;174.395969141049;"158 Kaipikari Road Upper Urenui"
    
    56402;"2008-10-01";"2010-02-26";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56402;"2010-02-27";"2010-10-05";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56402;"2010-10-06";"2010-11-05";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56402;"2010-11-04";"2011-08-21";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56402;"2011-08-21";"2014-03-04";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56402;"2014-03-05";"2014-03-05";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56402;"2014-06-25";"2015-05-06";-39.0102363928792;174.395969141049;"158 Kaipikari Road Upper Urenui"
    56402;"2015-05-06";"2015-05-27";-39.0102363928792;174.395969141049;"158 Kaipikari Road Upper Urenui"
    
    56403;"2008-10-01";"2010-02-26";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56403;"2010-02-27";"2010-10-05";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56403;"2010-10-06";"2010-11-05";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56403;"2010-11-04";"2011-08-21";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56403;"2011-08-21";"2014-03-04";-40.0965888237453;176.321241879231;"Microwave Station Paulsen Road (off Rangitoto Road) Takapau"
    56403;"2014-06-25";"2015-05-06";-39.0102363928792;174.395969141049;"158 Kaipikari Road Upper Urenui"
    56403;"2015-05-06";"2015-05-27";-39.0102363928792;174.395969141049;"158 Kaipikari Road Upper Urenui"    
    
    56471;"2008-10-01";"2010-03-02";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56471;"2010-03-03";"2010-10-05";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56471;"2010-10-06";"2010-11-08";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56471;"2010-11-07";"2011-04-19";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56471;"2011-04-20";"2011-08-21";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56471;"2011-08-21";"2014-09-10";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56471;"2014-09-11";"2014-10-04";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56471;"2015-03-06";"2015-03-12";-40.3865989533622;175.540465419766;"Works Road Longburn Palmerston North"
    56471;"2015-05-13";"2017-03-16";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    
    56474;"2010-07-07";"2010-10-05";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56474;"2010-10-06";"2010-11-08";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56474;"2010-11-07";"2011-04-19";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56474;"2011-04-20";"2011-08-21";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56474;"2011-08-21";"2014-09-10";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56474;"2014-09-11";"2014-10-04";-39.9322563291572;175.046646489906;"Cooks Street Wanganui"
    56474;"2015-03-06";"2015-03-12";-40.3865989533622;175.540465419766;"Works Road Longburn Palmerston North"
    56474;"2015-05-13";"2017-03-16";-40.5552121879033;175.208835369584;"Forest Road  Waitarere Beach"
    
    57021;"2008-10-01";"2010-03-10";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57021;"2010-03-11";"2010-10-05";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57021;"2010-10-06";"2010-11-18";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57021;"2010-11-17";"2011-08-21";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57021;"2011-08-21";"2013-05-15";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57021;"2013-05-15";"2013-07-15";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57021;"2013-07-15";"2014-04-02";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57021;"2014-04-02";"2014-05-27";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57021;"2014-05-27";"2014-11-10";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57021;"2014-11-11";"2014-11-20";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57021;"2015-03-11";"2015-03-23";-39.624805402347;176.85758278369;"Cnr Coventry Road and Tomoana Road Hastings"
    57021;"2015-10-15";"";-40.0287014680415;175.193836245752;"Glasgow Property SH3 Turakina Wanganui"

    57022;"2008-10-01";"2010-03-10";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57022;"2010-03-11";"2010-10-05";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57022;"2010-10-06";"2010-11-18";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57022;"2010-11-17";"2011-08-21";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57022;"2011-08-21";"2013-05-15";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57022;"2013-05-15";"2013-07-15";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57022;"2013-07-15";"2014-04-02";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57022;"2014-04-02";"2014-05-27";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57022;"2014-05-27";"2014-11-10";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57022;"2014-11-11";"2014-11-20";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57022;"2015-03-11";"2015-03-23";-39.624805402347;176.85758278369;"Cnr Coventry Road and Tomoana Road Hastings"
    57022;"2015-10-15";"";-40.0287014680415;175.193836245752;"Glasgow Property SH3 Turakina Wanganui"

    57023;"2008-10-01";"2010-03-10";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57023;"2010-03-11";"2010-10-05";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57023;"2010-10-06";"2010-11-18";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57023;"2010-11-17";"2011-08-21";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57023;"2011-08-21";"2013-05-15";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57023;"2013-05-15";"2013-07-15";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57023;"2013-07-15";"2014-04-02";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57023;"2014-04-02";"2014-05-27";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57023;"2014-05-27";"2014-11-10";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57023;"2014-11-11";"2014-11-20";-41.1223077688016;175.081164019321;"King Charles Drive Upper Hutt"
    57023;"2015-03-11";"2015-03-23";-39.624805402347;176.85758278369;"Cnr Coventry Road and Tomoana Road Hastings"
    57023;"2015-10-15";"";-40.0287014680415;175.193836245752;"Glasgow Property SH3 Turakina Wanganui"
    """

def differing_relocations(cur_local, audit=False):
    reprel.ReportRelocations(cur_local, src=conf.MOD_P_LOCATIONS,
        prog_feedback_weekly=True, audit=False)

def phantom_relocations(cur_local):
    """
    cell_id_rti = 320881,
    origin_coords = (-37.046113, 175.520369)  ## Te Puru
    origin_date_str = '2016-11-04'  ## earliest date this cell_id_rti appears in connections data
    dest_coords = (-37.666873, 176.205025)  ## Tauranga
    dest_date_str = '2016-11-24'
    cell_id_rtis_to_exclude = [320891, 320901]

    cell_id_rti = 382063
    origin_coords = (-46.020285, 168.859755)  ## Southland
    origin_date_str = '2016-09-01'
    dest_coords = (-34.969764, 173.660780)  ## Northland
    dest_date_str = '2016-10-30'
    cell_id_rtis_to_exclude = [382073, 382083, ]

    cell_id_rti = 382063
    start_datetime_str = '2016-08-01 00:00:00'
    end_datetime_str = '2016-08-01 23:59:59'
    cell_id_rtis_to_exclude = [382073, 382083, ]
    """
    display_maps = True
    display_misses = True
    use_latest = True

    ## Phantom time in Te Puru
    cell_id_rti = 320881,
    origin_coords = (-37.046113, 175.520369)  ## Te Puru
    origin_date_str = '2016-11-04'  ## earliest date this cell_id_rti appears in connections data
    dest_coords = (-37.666873, 176.205025)  ## Tauranga
    dest_date_str = '2016-11-24'
    cell_id_rtis_to_exclude = [320891, 320901]
    (end_of_origin, start_of_dest, pre_coord_weights,
     post_coord_weights) = findrel.FindRelocations.get_relocation_dts(cur_local,
        cell_id_rti, origin_coords, origin_date_str, dest_coords, dest_date_str,
        cell_id_rtis_to_exclude, display_maps, display_misses, use_latest,
        examine_inner_gaps=False)
    print(end_of_origin, start_of_dest, pre_coord_weights, post_coord_weights)

    ## Phantom time in Southland - showing only ever at dest as Mod_P claimed (unlike RR)
    """
    Actually it _was_ at Southland for about a month (2016-04-04 to 2016-05-18)
    and mod_p got it wrong.
    """
    cell_id_rti = 382063
    origin_coords = (-46.020285, 168.859755)  ## Southland
    origin_date_str = '2016-08-01'
    dest_coords = (-34.969764, 173.660780)  ## Northland
    dest_date_str = '2016-10-30'
    cell_id_rtis_to_exclude = [382073, 382083, ]
    (end_of_origin, start_of_dest, pre_coord_weights,
     post_coord_weights) = findrel.FindRelocations.get_relocation_dts(cur_local,
        cell_id_rti, origin_coords, origin_date_str, dest_coords, dest_date_str,
        cell_id_rtis_to_exclude, display_maps, display_misses, use_latest,
        examine_inner_gaps=False)
    print(end_of_origin, start_of_dest, pre_coord_weights, post_coord_weights)
    
    ## more proof the cell_id_rti wan't in use any earlier than in Mod_P (in spite of what RR alleged). Looked at different datetimes prior in case
    """
    Fail ;-) it was actually in use for about a month (2016-04-04 to 2016-05-18).
    """
    cell_id_rti = 382063
    start_datetime_str = '2015-02-01 00:00:00'
    end_datetime_str = '2015-02-01 23:59:59'
    cell_id_rtis_to_exclude = [382073, 382083, ]
    findrel.FindRelocations.get_likely_location_dts(cur_local,
        start_datetime_str, end_datetime_str, cell_id_rti,
        cell_id_rtis_to_exclude)

def swap_lat_lon_if_sane(cur_local):
    """
    Look for lat/lons that are outside of the NZ bounding box. Note -- will
    include Scott Base in group that falls outside of NZ bounding box. Tries to
    swap and see if that result falls within NZ bounding box. If so, swaps
    lat/lon.
    """
    sql_swap = """\
    UPDATE {mod_p_locations}
    SET tower_lat = %s, tower_lon = %s
    WHERE id = %s
    """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
    sql = """\
    SELECT *
    FROM {mod_p_locations}
    ORDER BY cell_id_rti, effective_start
    """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
    cur_local.execute(sql)
    data = cur_local.fetchall()
    for n, row in enumerate(data, 1):
        print(row)
        lat = row['tower_lat']
        lon = row['tower_lon']
        in_bb = (
            (conf.NZ_BB_MIN_LAT < lat < conf.NZ_BB_MAX_LAT)
            and
            (conf.NZ_BB_MIN_LON < lon < conf.NZ_BB_MAX_LON)
        )
        if not in_bb:
            maybe_lat, maybe_lon = lon, lat
            maybe_in_bb = (
                (conf.NZ_BB_MIN_LAT < maybe_lat < conf.NZ_BB_MAX_LAT)
                and
                (conf.NZ_BB_MIN_LON < maybe_lon < conf.NZ_BB_MAX_LON)
            )
            if maybe_in_bb:
                ## make swap
                tbl_id = row['id']
                cur_local.execute(sql_swap, (maybe_lat, maybe_lon, tbl_id))
                print("On row {:,} found swapped coordinates so "
                "successfully corrected them".format(n))
            else:
                print("Unable to correct coordinates. Description: {}"
                    .format(row['description']))
    #con_local.commit()

def find_original_name_cell_id_rtis(cur_local):
    """
    If you are looking for historical information on a person you may need to
    search for them using their maiden name rather than their married name.
    Similarly, if we are trying to get locations for historical connection data
    we will need to look for it under the cell_id_rti it was associated with at
    the time. Unfortunately, the Mod_P data records its whole history under the
    "married name"/most recent cell_id_rti. So we need to correct the
    cell_id_rti's for some records to be for the earlier cell_id_rti e.g.

    Mrs Smith    2015-01-01  2016-05-01  Auckland
    Mrs Smith    2016-07-01              Wellington

    needs correction to

    Ms  Redrock  2015-01-01  2016-05-01  Auckland
    Mrs Smith    2016-07-01              Wellington

    so when we have data on Ms Redrock in 2015 we can find that she lived in
    Auckland. If we leave the data as it was there is no record for Ms Redrock
    and we won't be able to get a location.

    Similarly we might need to change
    290869       2015-01-01  2016-05-01  Auckland
    290869       2016-07-01              Wellington

    to

    151227189    2015-01-01  2016-05-01  Auckland
    290869       2016-07-01              Wellington
    """
    latest_name_cell_id_rtis = [151227189, 151227188, 151227187, 151227149,
        151227148, 151227147, 23227189, 23227188, 23227187, 23227149, 23227148,
        23227147, 15168135, 15168125, 15168115, 15168033, 15168032, 15168031,
        15166497, 15166496, 15166495, 15161121, 15161120, 15161119, 15156513,
        15156512, 15156511, 15144737, 15144736, 15144735, 15143811, 15143801,
        15143791, 15143713, 15143712, 15143711, 15138435, 15138425, 15138415,
        15137825, 15137824, 15137823, 15137313, 15137312, 15137311, 15135521,
        15135520, 15135519, 15134497, 15134496, 15134495, 15134241, 15134240,
        15134239, 15133985, 15133984, 15133983, 15133831, 15133821, 15133811,
        15133729, 15133728, 15133727, 15130401, 15130400, 15130399, 15129377,
        15129376, 15129375, 15128609, 15128608, 15128607, 15122689, 15115297,
        15115296, 15115295, 15115041, 15115039, 15114783, 15114529, 15114528,
        15114527, 15109921, 15109919, 15105313, 15105312, 15105311, 13930785,
        13930784, 13930783, 13929761, 13929760, 13929759, 13929249, 13929248,
        13929247, 13926434, 13926433, 13926432, 13926431, 13921313, 13921312,
        13921311, 13920289, 13920288, 13920287, 13912865, 13912864, 13912863,
        13901089, 13901087, 13897249, 13897248, 13897247, 13896737, 13896736,
        13896735, 13895201, 13895200, 13895199, 13894689, 13894688, 13894687,
        13882145, 13882144, 13882143, 13881121, 13881120, 13881119, 13879329,
        13879328, 13879327, 13871649, 13871648, 13871647, 13871393, 13871392,
        13871391, 13865761, 13865760, 13865759, 13864737, 13864736, 13864735,
        13863457, 13863456, 13863455, 13862177, 13862176, 13862175, 13861665,
        13861664, 13861663, 13857825, 13857824, 13857823, 13855265, 13855264,
        13855263, 13853473, 13853472, 13853471, 13853217, 13853216, 13853215,
        13844001, 13844000, 13843999, 13841185, 13841184, 13841183, 13837345,
        13837344, 13837343, 13835553, 13835552, 13835551, 13832481, 13832480,
        13832479, 13830177, 13830176, 13830175, 13829921, 13829920, 13829919,
        13829409, 13829408, 13829407, 13827873, 13827872, 13827871, 13387789,
        13387788, 13387787, 13368097, 13368096, 13368095, 13356163, 13356153,
        13356143, 13356065, 13356064, 13356063, 13349921, 13349920, 13349919,
        13307681, 13307680, 13307679, 13306145, 13306144, 13306143, 13304609,
        13304608, 13304607, 13254689, 13254688, 13254687, 13217569, 13217568,
        13217567, 13209120, 13209119, 13188385, 13188384, 13188383, 13152131,
        13152121, 13152111, 13152033, 13152032, 13152031, 13147936, 13147935,
        13147681, 13147680, 13147679, 13128992, 13128991, 13120288, 13120287,
        13103648, 13103647, 13103393, 13103392, 13103391, 13087521, 13087520,
        13087519, 13078561, 13078560, 13078559, 13071905, 13071904, 13071903,
        13071393, 13071392, 13071391, 13064737, 13064736, 13064735, 13055521,
        13055520, 13055519, 13051679, 13045025, 13045024, 13045023, 13044769,
        13044768, 13044767, 13025057, 13025056, 13025055, 13001249, 13001248,
        13001247, 12975137, 12975136, 12975135, 12973089, 12973088, 12973087,
        12957473, 12957472, 12957471, 12954655, 12954145, 12954144, 12954143,
        12952097, 12952096, 12952095, 12948769, 12948768, 12948767, 12944672,
        12944671, 12936225, 12936224, 12936223, 12935201, 12935200, 12935199,
        12929569, 12929568, 12929567, 12922125, 12922124, 12922123, 12911905,
        12911904, 12911903, 12911649, 12911648, 12911647, 12911393, 12911392,
        12911391, 12910113, 12910112, 12910111, 12909857, 12909856, 12909855,
        12909601, 12909600, 12909599, 12909345, 12909344, 12909343, 12908321,
        12908320, 12908319, 12882721, 12882720, 12882719, 12871969, 12871968,
        12871967, 12871713, 12871712, 12871711, 12870433, 12870432, 12870431,
        12863521, 12863520, 12863519, 12863265, 12863264, 12863263, 12863009,
        12863008, 12863007, 12862753, 12862752, 12862751, 12860705, 12860704,
        12860703, 12856865, 12856864, 12856863, 12844833, 12844832, 12844831,
        12844321, 12844320, 12844319, 12837409, 12837408, 12837407, 12836641,
        12836640, 12836639, 12831265, 12831264, 12831263, 12830771, 12830732,
        12830731, 12828192, 12828191, 12824097, 12824096, 12824095, 12822817,
        12822816, 12822815, 12815137, 12815136, 12815135, 12812063, 12807457,
        12807456, 12807455, 430727, 430725, 430717,
        430707, 317305, 316027, 274465, 274464, 274463, 264736, 264735, 264225,
        264224, 264223, 262689, 262688, 262687, 262177, 262176, 262175, 257825,
        257824, 257823, 54877, 54874, 54871, 54475, 54474, 54472, 54471, 54442,
        45649, 45648, 45647, 45646, 45645, 45644, 45643, 45642, 45641, 44133,
        44132, 44131, 40349, 40348, 40347, 38876, 38875, 38874, 38873, 38872,
        38871, 38479, 38476, 38473, 25428]
    cell_id_rti_alternatives = []
    """
    We have a reference cell_id_rti from the CSM data and its cell_id e.g.
    151227189 and 53 (node id is 590731). We also have its lat/lon. So what in
    the Mod_P data has same cell_id and lat/lon (within, say, 100m)?

    Or use csm.tower = mod_p.location_alpha
    """
    sql_alternatives_tpl = """\
    SELECT *
    FROM (
      SELECT DISTINCT
        csm_ref.cell_id_rti AS
      csm_cell_id_rti,
        mod_p_src.cell_id_rti AS
      alt_mod_p_cell_id_rti,
        mod_p_src.location_alpha AS
      mod_p_tower,
        csm_ref.tower AS
      csm_tower,
        %s AS
      csm_cell_id,  -- not from CSM but derived from cell_id_rti from CMS
        mod_p_src.cell_id AS
      mod_p_cell_id
      FROM {mod_p_locations} AS mod_p_src, (
        SELECT DISTINCT ON (cell_id_rti)
        cell_id_rti,
        tower
        FROM {csm_history}
        WHERE cell_id_rti = %s
        ORDER BY cell_id_rti, cob_date DESC
      ) AS csm_ref
    ) AS inputs
    WHERE mod_p_tower = csm_tower
    AND mod_p_cell_id = csm_cell_id
    """.format(csm_history=conf.CSM_HISTORY,
        mod_p_locations=conf.MOD_P_LOCATIONS)
    unfound_cell_id_rtis = []
    for latest_name_cell_id_rti in latest_name_cell_id_rtis:
        unused_node_id, cell_id = utils.extract_4g_node_and_cell_id(
            latest_name_cell_id_rti)
        cur_local.execute(sql_alternatives_tpl,
            (cell_id, latest_name_cell_id_rti))
        data = cur_local.fetchall()
        if not data:
            unfound_cell_id_rtis.append(latest_name_cell_id_rti)
            print("Unable to find alternative to {} in Mod_P data.".format(
                latest_name_cell_id_rti))
        else:
            cell_id_rti_alternatives.extend([dict(row) for row in data])
    print(unfound_cell_id_rtis)
    pp(cell_id_rti_alternatives)

def missing_cell_id_rtis(cur_local):
    """
    OK - say we start using the new Mod_P data as our source - will it lose us
    anything?

    Don't care much about cell_id_rti's missing from CSM/redrocks as I have low
    expectations of that data. Instead, I am interested in whether Mod_P is
    missing anything. Most of the cell_id_rti's missing from Mod_P relative to
    legacy data have very long cell_id_rti's e.g. 151227189 or 23227189. It
    appears this is because they used to use long eNodeB ID's e.g. 590731.
    """
    sql_missing = """\
    SELECT DISTINCT legacy.cell_id_rti
    FROM mod_p_locations_orig AS modp
    FULL JOIN (
    SELECT DISTINCT cell_id_rti
    FROM (
      SELECT cell_id_rti
      FROM csm
      UNION
      SELECT cell_id_rti
      FROM csm
    ) AS legacy_src
    ) AS legacy
    USING(cell_id_rti)
    WHERE modp.cell_id_rti IS NULL
    ORDER BY cell_id_rti DESC
    """
    #'''
    cur_local.execute(sql_missing)
    cell_id_rtis_missing_in_mod_p = [row[0] for row in cur_local.fetchall()]
    pp(cell_id_rtis_missing_in_mod_p)
    return



    #'''
    #unused_con_rem, cur_rem = utils.Pg.get_rem()
    ## check some of the missing-from-mod_p client_id_rti's for connection data
    """
    ## in legacy but not mod_p - see if there is another guise
    ## Caro St/Anglesea St Hamilton
    ref_lat = -37.7863695430369
    ref_lon = 175.281001317404

    ## Masterton
    ref_lat = -40.948592
    ref_lon = 175.659686
    """
    ref_lat = -40.948592
    ref_lon = 175.659686
    data = rti_mod.Rti.nearby_cell_id_rti_dets(cur_local, ref_lat, ref_lon,
        location_src=conf.MOD_P_LOCATIONS, min_nearness_m=600)
    pp(data, width=100)
    """
    * Hamilton example ****

    Looks like the 4G sectors at Caro St/Anglesea St Hamilton got new eNodeB ID
    - changed from 590731 to 1136

    -37.788872         175.281408
    vs
    -37.7888696415904  175.281424301018
    i.e. same

    MHNXA vs MHNX
    i.e. same

    |      Redrock data           |  Mod_P data  |
    | HUA_LTE_201606191809.txt    |              |
    ----------------------------------------------
    | Sector | cell_id   | eNB_ID |  cell_id_rti |
    ----------------------------------------------
    |     11 | 151227147 | 590731 |       290827
    |     12 | 151227148 | 590731 |       290828
    |     13 | 151227149 | 590731 |       290829
    |     51 | 151227187 | 590731 |       290867
    |     52 | 151227188 | 590731 |       290868
    |     53 | 151227189 | 590731 |       290869
    ----------------------------------------------

    ## Hamilton from Redrock (found in multiple dated source files)
    590731,MHNXA,11,151227147,MHNXA1_J,0,,,-37.788872,175.281408,4,110,69,11,30,24,4301,15.2,43.8,15.1,3,Huawei,A704516R0,700 MHz,MIMO,9310,20,0,0
    590731,MHNXA,12,151227148,MHNXA2_J,0,,,-37.788872,175.281408,6,230,69,11,30,25,4301,15.2,45.8,15.1,3,Huawei,A704516R0,700 MHz,MIMO,9310,20,0,0
    590731,MHNXA,13,151227149,MHNXA3_J,0,,,-37.788872,175.281408,9,350,69,11,30,26,4301,15.2,46,15,3,Huawei,A704516R0,700 MHz,MIMO,9310,20,0,0
    590731,MHNXA,51,151227187,MHNXA1_N,0,,,-37.788872,175.281408,1,110,60,5,30,30,4301,18.2,45.8,18.2,3,Huawei,ADU4518R6,2600 MHz,MIMO,3050,20,0,0
    590731,MHNXA,52,151227188,MHNXA2_N,0,,,-37.788872,175.281408,1,230,60,5,30,31,4301,18.2,46,18.2,3,Huawei,ADU4518R6,2600 MHz,MIMO,3050,20,0,0
    590731,MHNXA,53,151227189,MHNXA3_N,0,,,-37.788872,175.281408,1,350,60,5,30,32,4301,18.2,46,18.2,3,Huawei,ADU4518R6,2600 MHz,MIMO,3050,20,0,0

    1136,  MHNXA,53,290869,   MHNXA3_N,0,,,-37.788872,175.281408,6,350,60,5,30,32,4301,18.2,46,18.7,3,Huawei,ADU4518R6,2600 MHz,MIMO,3050,20,0,0
    ... i.e. the legacy data contains both
    Confirmation that 151227189 and 290827 are the same cell_id
    53 = cell_id for sector antenna in question.
    When connected to eNodeB 590731 has cell_id_rti of (256*590731) + 53 i.e. 151227189.
    When connected to eNodeB 1136 has cell_id_rti of (256*1136) + 53 i.e. 290869.

    ## Hamilton from Mod_P
    48728;290827;"2015-05-27";"";-37.7888696415904;175.281424301018;"Caro Street/Anglesea Street
    Hamilton";"Hamilton Central 1";136331;1;"L";"MHNXL1";"2015-05-27";"";1363;"O";"MHNX";2711133;6376787;"O";1
    48729;290828;"2015-05-27";"";-37.7888696415904;175.281424301018;"Caro Street/Anglesea Street
    Hamilton";"Hamilton Central 2";136332;1;"L";"MHNXL2";"2015-05-27";"";1363;"O";"MHNX";2711133;6376787;"O";1
    48730;290829;"2015-05-27";"";-37.7888696415904;175.281424301018;"Caro Street/Anglesea Street
    Hamilton";"Hamilton Central 3";136333;1;"L";"MHNXL3";"2015-05-27";"";1363;"O";"MHNX";2711133;6376787;"O";1
    48731;290867;"2015-05-27";"";-37.7888696415904;175.281424301018;"Caro Street/Anglesea Street
    Hamilton";"Hamilton Central 1";136331;5;"L";"MHNXL1";"2015-05-27";"";1363;"O";"MHNX";2711133;6376787;"O";1
    48732;290868;"2015-05-27";"";-37.7888696415904;175.281424301018;"Caro Street/Anglesea Street
    Hamilton";"Hamilton Central 2";136332;5;"L";"MHNXL2";"2015-05-27";"";1363;"O";"MHNX";2711133;6376787;"O";1
    48733;290869;"2015-05-27";"";-37.7888696415904;175.281424301018;"Caro Street/Anglesea Street
    Hamilton";"Hamilton Central 3";136333;5;"L";"MHNXL3";"2015-05-27";"";1363;"O";"MHNX";2711133;6376787;"O";1


    * Masterton ****

    -40.948592         175.659686
    vs
    -40.9485785918344  175.659712043265

    CMSVA/CMSVL vs CMSV

    ## Masterton from Redrock (not in same individual dated source files)
    1731, CMSVA,135,  443271,...-40.948592,175.659686,3,235,67,4,25,401,38141

    59250,CMSVL,135,15168135,...-40.948592,175.659686,3,235,67,4,25,149,38141

    ## Masterton from Mod_P
    51192;443271;"2015-10-19";"";-40.9485785918344;175.659712043265;"33 Lincoln Street
    Masterton";"Masterton Exchange 3";245223;5;"L";"CMSVL3";"2014-06-12";"";2452;"O";"CMSV";2733885;6025265;"O";1

    Implications - need to modify cell_id_rti's in Mod_P for periods when under
    a different eNodeB ID. In legacy data (built up over time) we know about
    both old and new versions and they'll be matchable more or less.
    """

def report_cell_id_contribs_old_vs_new_name(cur_local):
    make_fresh_range_src = False
    overall_start_date_str = '2015-01-01'
    overall_end_date_str = '2017-05-07'
    lbl = 'Old vs new 4G cell_id_rti because of eNodeB_Id shift 493344'
    rto = conf.AUCK_RTO
    cell_id_rtis = [12822816, 493344]  ## Avondale
    if make_fresh_range_src:
        cell_id_rti_ranges = locsrcs.Deprecated.get_cell_id_rti_ranges(
            cur_local, cell_id_rtis, overall_end_date_str)
        pp(cell_id_rti_ranges, width=150)
        utils.warn("Note - won't include cell_id_rtis not found in mod_p. "
            "Update cell_id_rti_ranges and set make_fresh_range_src to False "
            "again")
        return
    else:
        cell_id_rti_ranges = [
            (12822816, overall_start_date_str, overall_end_date_str, -36.8721057272134, 174.669874499954, '658 Rosebank Road Avondale'),  ## date range unknown but location is apparently the same
            (493344, '2013-08-08', '2017-05-07', -36.8721057272134, 174.669874499954, '658 Rosebank Road Avondale'),
        ]
    connections.Connections.report_changes_by_cell_id_rti(conf.CSM_ROOT,
        cur_local, lbl, rto, cell_id_rti_ranges, overall_start_date_str,
        overall_end_date_str, make_fresh_src=True)

def report_cell_id_contribs_westport(cur_local):
    lbl = "Westport using stale csm mapping"
    rto = conf.WEST_COAST_RTO
    ## Everything actually runs off cell_id_rti_ranges so can set both fresh settings to false if hand-hacking the hard-wired version.
    make_fresh_cell_src = False
    make_fresh_range_src = False
    overall_start_date_str = '2015-01-01' #'2015-01-02' #'2016-12-14' #'2015-01-01'
    overall_end_date_str = '2017-05-07'
    if make_fresh_cell_src:
        cell_id_rti = 52012
        tower_lat = -41.7558413485251
        tower_lon = 171.599343188854
        cell_dets_when_near = rti_mod.Rti.nearby_cell_id_rti_dets(cur_local,
            cell_id_rti, tower_lat, tower_lon,
            location_src=conf.MOD_P_LOCATIONS, min_nearness_m=40*1000)  ## 40km from centre of Westport
        pp(sorted(cell_dets_when_near), width=120)
        utils.warn("Update cell_dets_when_near and set "
            "make_fresh_cell_src to False again")
        return
    else:
        cell_dets_when_near = [
            [42521, '131 Palmerston Street\nWestport', 0.0],
            [42522, '131 Palmerston Street\nWestport', 0.0],
            [42523, '131 Palmerston Street\nWestport', 0.0],
            [42524, '131 Palmerston Street\nWestport', 0.0],
            [42525, '131 Palmerston Street\nWestport', 0.0],
            [42526, '131 Palmerston Street\nWestport', 0.0],
            [52010, 'Mt Rochfort\nWestport', 14270.6],
            [52011, 'Mt Rochfort\nWestport', 14270.6],
            [52012, 'Mt Rochfort\nWestport', 14270.6],
            [52013, 'Mt Rochfort\nWestport', 14270.6],
            [52014, 'Mt Rochfort\nWestport', 14270.6],
            [52015, 'Mt Rochfort\nWestport', 14270.6],
            [52016, 'Mt Rochfort\nWestport', 14270.6],
            [52021, 'Stockton Road\nMillerton', 28681.2],
            [52022, 'Stockton Road\nMillerton', 28681.2],
            [52520, '131 Palmerston Street\nWestport', 0.0],
            [52521, '131 Palmerston Street\nWestport', 0.0],
            [52522, '131 Palmerston Street\nWestport', 0.0],
            [52523, '131 Palmerston Street\nWestport', 0.0],
            [52524, '131 Palmerston Street\nWestport', 0.0],
            [52525, '131 Palmerston Street\nWestport', 0.0],
            [52526, '131 Palmerston Street\nWestport', 0.0],
            [53191, 'Mt Rochfort\nWestport', 14270.6],
            [53192, 'Mt Rochfort\nWestport', 14270.6],
            [53193, 'Mt Rochfort\nWestport', 14270.6],
            [53194, 'Mt Rochfort\nWestport', 14270.6],
            [53195, 'Mt Rochfort\nWestport', 14270.6],
            [53196, 'Mt Rochfort\nWestport', 14270.6],
            [53197, 'Mt Rochfort\nWestport', 14270.6],
            [53198, 'Mt Rochfort\nWestport', 14270.6],
            [53199, 'Mt Rochfort\nWestport', 14270.6],
            [393327, '131 Palmerston Street\nWestport', 0.0],
            [393331, '131 Palmerston Street\nWestport', 0.0],
            [393332, '131 Palmerston Street\nWestport', 0.0],
            [393337, '131 Palmerston Street\nWestport', 0.0],
            [393341, '131 Palmerston Street\nWestport', 0.0],
            [393342, '131 Palmerston Street\nWestport', 0.0],
            [393347, '131 Palmerston Street\nWestport', 0.0],
            [393351, '131 Palmerston Street\nWestport', 0.0],
            [393352, '131 Palmerston Street\nWestport', 0.0],
        ]
    if make_fresh_range_src:
        cell_id_rtis = [row[0] for row in cell_dets_when_near]
        cell_id_rti_ranges = locsrcs.Deprecated.get_cell_id_rti_ranges(
            cur_local, cell_id_rtis, overall_end_date_str)
        pp(cell_id_rti_ranges, width=150)
        utils.warn("Update cell_id_rti_ranges and set make_fresh_range_src to "
            "False again")
        return
    else:
        cell_id_rti_ranges_westport_best = [
             (42521, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (42522, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (42523, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (42524, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (42525, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (42526, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52010, '2008-10-01', '2010-10-11', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             (52010, '2010-10-12', '2010-12-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             (52010, '2010-12-07', '2011-05-29', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             (52010, '2011-05-29', '2013-02-11', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             (52011, '2013-02-12', '2013-11-28', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             (52011, '2013-11-28', '2015-09-30', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             #(52011, '2015-12-02', '2016-10-13', -44.5843795107931, 170.177315461237, 'Otematata Station\nOtematata - Kurow Road (SH 83)\nOtematata'),
             (52012, '2013-02-12', '2013-11-28', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             (52012, '2013-11-28', '2015-09-30', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             #(52012, '2015-12-02', '2016-10-13', -44.5843795107931, 170.177315461237, 'Otematata Station\nOtematata - Kurow Road (SH 83)\nOtematata'),
             (52013, '2013-02-12', '2013-11-28', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             (52013, '2013-11-28', '2015-09-30', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             (52014, '2012-10-02', '2013-02-11', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             (52014, '2013-02-12', '2013-11-28', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             (52014, '2013-11-28', '2015-09-30', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             #(52014, '2015-12-02', '2016-10-13', -44.5843795107931, 170.177315461237, 'Otematata Station\nOtematata - Kurow Road (SH 83)\nOtematata'),
             (52015, '2013-02-12', '2013-11-28', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             (52015, '2013-11-28', '2015-09-30', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             #(52015, '2015-12-02', '2016-10-13', -44.5843795107931, 170.177315461237, 'Otematata Station\nOtematata - Kurow Road (SH 83)\nOtematata'),
             (52016, '2013-02-12', '2013-11-28', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             (52016, '2013-11-28', '2015-09-30', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             #(52021, '2008-10-01', '2010-02-13', -43.5867758600525, 172.386442255883, 'Cnr Jones Road and Hoskyns Road\nRolleston'),
             #(52021, '2010-02-14', '2011-06-12', -43.5867758600525, 172.386442255883, 'Cnr Jones Road and Hoskyns Road\nRolleston'),
             #(52021, '2011-06-12', '2013-12-10', -43.5867758600525, 172.386442255883, 'Cnr Jones Road and Hoskyns Road\nRolleston'),
             #(52021, '2013-12-11', '2013-12-11', -43.5867758600525, 172.386442255883, 'Cnr Jones Road and Hoskyns Road\nRolleston'),
             (52021, '2015-12-08', '2017-05-07', -41.6353896711318, 171.859636338202, 'Stockton Road\nMillerton'),
             #(52022, '2008-10-01', '2010-02-13', -43.5867758600525, 172.386442255883, 'Cnr Jones Road and Hoskyns Road\nRolleston'),
             #(52022, '2010-02-14', '2011-06-12', -43.5867758600525, 172.386442255883, 'Cnr Jones Road and Hoskyns Road\nRolleston'),
             #(52022, '2011-06-12', '2013-12-10', -43.5867758600525, 172.386442255883, 'Cnr Jones Road and Hoskyns Road\nRolleston'),
             #(52022, '2013-12-11', '2013-12-11', -43.5867758600525, 172.386442255883, 'Cnr Jones Road and Hoskyns Road\nRolleston'),
             (52022, '2015-12-08', '2017-05-07', -41.6353896711318, 171.859636338202, 'Stockton Road\nMillerton'),
             (52520, '2008-10-01', '2010-05-17', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52521, '2010-05-17', '2010-10-11', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52521, '2010-10-12', '2010-12-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52521, '2010-12-07', '2011-05-29', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52521, '2011-05-29', '2013-11-28', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52521, '2013-11-28', '2015-10-10', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52522, '2010-05-17', '2010-10-11', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52522, '2010-10-12', '2010-12-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52522, '2010-12-07', '2011-05-29', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52522, '2011-05-29', '2013-11-28', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52522, '2013-11-28', '2015-10-10', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52523, '2010-05-17', '2010-10-11', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52523, '2010-10-12', '2010-12-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52523, '2010-12-07', '2011-05-29', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52523, '2011-05-29', '2013-11-28', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52523, '2013-11-28', '2015-10-10', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52524, '2013-05-22', '2013-11-28', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52524, '2013-11-28', '2015-10-10', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52525, '2013-05-22', '2013-11-28', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52525, '2013-11-28', '2015-10-10', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52526, '2013-05-22', '2013-11-28', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (52526, '2013-11-28', '2015-10-10', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (53191, '2013-03-04', '2013-03-10', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
             (53191, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
             (53191, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             #(53192, '2013-03-04', '2013-03-10', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
             #(53192, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
             (53192, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             #(53193, '2013-03-04', '2013-03-10', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
             #(53193, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
             (53193, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             #(53194, '2013-03-04', '2013-03-10', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
             #(53194, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
             (53194, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             #(53195, '2013-03-04', '2013-03-10', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
             #(53195, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
             (53195, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             #(53196, '2013-03-04', '2013-03-10', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
             #(53196, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
             (53196, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             #(53197, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
             (53197, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             #(53198, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
             (53198, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             #(53199, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
             (53199, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
             (393327, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (393331, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (393332, '2015-10-11', '2016-07-19', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (393337, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (393341, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (393342, '2015-10-11', '2016-07-19', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (393347, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (393351, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
             (393352, '2015-10-11', '2016-07-19', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
        ]
        """
        Taken from csm_cell_id_to_s2_12_index.csv filtered to 734 (Westport) and
        733 (Mt Rochfort to east) ['393327', '393331', '393332', '393337',
        '393341', '393342', '393347', '393351', '393352', '42521', '42522',
        '42523', '42524', '42525', '42526', '52013', '52016', '52521', '52522',
        '52523', '52524', '52525', '52526', '53191', '53192', '53193', '53194',
        '53195', '53196', '53197', '53198', '53199']
        """
        cell_id_rti_ranges_westport_stale = [
              (42521, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (42522, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (42523, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (42524, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (42525, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (42526, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
#              (52010, '2008-10-01', '2010-10-11', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
#              (52010, '2010-10-12', '2010-12-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
#              (52010, '2010-12-07', '2011-05-29', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
#              (52010, '2011-05-29', '2013-02-11', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
#              (52011, '2013-02-12', '2013-11-28', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
#              (52011, '2013-11-28', '2015-09-30', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
#              (52011, '2015-12-02', '2016-10-13', -44.5843795107931, 170.177315461237, 'Otematata Station\nOtematata - Kurow Road (SH 83)\nOtematata'),
#              (52012, '2013-02-12', '2013-11-28', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
#              (52012, '2013-11-28', '2015-09-30', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
#              (52012, '2015-12-02', '2016-10-13', -44.5843795107931, 170.177315461237, 'Otematata Station\nOtematata - Kurow Road (SH 83)\nOtematata'),
              (52013, '2013-02-12', '2013-11-28', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
              (52013, '2013-11-28', '2015-09-30', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
#              (52014, '2012-10-02', '2013-02-11', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
#              (52014, '2013-02-12', '2013-11-28', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
#              (52014, '2013-11-28', '2015-09-30', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
#              (52014, '2015-12-02', '2016-10-13', -44.5843795107931, 170.177315461237, 'Otematata Station\nOtematata - Kurow Road (SH 83)\nOtematata'),
#              (52015, '2013-02-12', '2013-11-28', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
#              (52015, '2013-11-28', '2015-09-30', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
#              (52015, '2015-12-02', '2016-10-13', -44.5843795107931, 170.177315461237, 'Otematata Station\nOtematata - Kurow Road (SH 83)\nOtematata'),
              (52016, '2013-02-12', '2013-11-28', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
              (52016, '2013-11-28', '2015-09-30', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
#              (52021, '2008-10-01', '2010-02-13', -43.5867758600525, 172.386442255883, 'Cnr Jones Road and Hoskyns Road\nRolleston'),
#              (52021, '2010-02-14', '2011-06-12', -43.5867758600525, 172.386442255883, 'Cnr Jones Road and Hoskyns Road\nRolleston'),
#              (52021, '2011-06-12', '2013-12-10', -43.5867758600525, 172.386442255883, 'Cnr Jones Road and Hoskyns Road\nRolleston'),
#              (52021, '2013-12-11', '2013-12-11', -43.5867758600525, 172.386442255883, 'Cnr Jones Road and Hoskyns Road\nRolleston'),
#              (52021, '2015-12-08', '2017-05-07', -41.6353896711318, 171.859636338202, 'Stockton Road\nMillerton'),
#              (52022, '2008-10-01', '2010-02-13', -43.5867758600525, 172.386442255883, 'Cnr Jones Road and Hoskyns Road\nRolleston'),
#              (52022, '2010-02-14', '2011-06-12', -43.5867758600525, 172.386442255883, 'Cnr Jones Road and Hoskyns Road\nRolleston'),
#              (52022, '2011-06-12', '2013-12-10', -43.5867758600525, 172.386442255883, 'Cnr Jones Road and Hoskyns Road\nRolleston'),
#              (52022, '2013-12-11', '2013-12-11', -43.5867758600525, 172.386442255883, 'Cnr Jones Road and Hoskyns Road\nRolleston'),
#              (52022, '2015-12-08', '2017-05-07', -41.6353896711318, 171.859636338202, 'Stockton Road\nMillerton'),
#              (52520, '2008-10-01', '2010-05-17', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52521, '2010-05-17', '2010-10-11', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52521, '2010-10-12', '2010-12-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52521, '2010-12-07', '2011-05-29', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52521, '2011-05-29', '2013-11-28', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52521, '2013-11-28', '2015-10-10', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52522, '2010-05-17', '2010-10-11', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52522, '2010-10-12', '2010-12-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52522, '2010-12-07', '2011-05-29', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52522, '2011-05-29', '2013-11-28', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52522, '2013-11-28', '2015-10-10', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52523, '2010-05-17', '2010-10-11', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52523, '2010-10-12', '2010-12-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52523, '2010-12-07', '2011-05-29', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52523, '2011-05-29', '2013-11-28', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52523, '2013-11-28', '2015-10-10', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52524, '2013-05-22', '2013-11-28', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52524, '2013-11-28', '2015-10-10', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52525, '2013-05-22', '2013-11-28', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52525, '2013-11-28', '2015-10-10', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52526, '2013-05-22', '2013-11-28', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (52526, '2013-11-28', '2015-10-10', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (53191, '2013-03-04', '2013-03-10', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
              (53191, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
              (53191, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
              (53192, '2013-03-04', '2013-03-10', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
              (53192, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
              (53192, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
              (53193, '2013-03-04', '2013-03-10', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
              (53193, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
              (53193, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
              (53194, '2013-03-04', '2013-03-10', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
              (53194, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
              (53194, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
              (53195, '2013-03-04', '2013-03-10', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
              (53195, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
              (53195, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
              (53196, '2013-03-04', '2013-03-10', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
              (53196, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
              (53196, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
              (53197, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
              (53197, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
              (53198, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
              (53198, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
              (53199, '2014-03-11', '2014-03-17', -40.234358832667, 175.558827660212, 'Manfield Park\t\t\t\nFielding'),
              (53199, '2015-10-01', '2017-05-07', -41.7784394317734, 171.740248178619, 'Mt Rochfort\nWestport'),
              (393327, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (393331, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (393332, '2015-10-11', '2016-07-19', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (393337, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (393341, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (393342, '2015-10-11', '2016-07-19', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (393347, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (393351, '2015-10-11', '2017-05-07', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
              (393352, '2015-10-11', '2016-07-19', -41.7558413485251, 171.599343188854, '131 Palmerston Street\nWestport'),
        ]
        cell_id_rti_ranges = cell_id_rti_ranges_westport_stale
        
    connections.Connections.report_changes_by_cell_id_rti(conf.CSM_ROOT, cur_local,
        lbl, rto, cell_id_rti_ranges, overall_start_date_str,
        overall_end_date_str, make_fresh_src=True)

def confirm_relocations(cur_local):
    """
    Pick a sample from the time we can check against connection data i.e. 2015
    onwards.
    """
    display_maps = True
    display_misses = True
    use_latest = True

    '''
    cell_id_rti = 58503
    origin_coords = (-41.2710682648392, 173.283265495865)  ## Nelson
    origin_date_str = '2015-06-09'  # 10 - going wider
    dest_coords = (-43.5304664094319, 172.599211191898)  ## Riccarton
    dest_date_str = '2015-09-17'  # 16
    cell_id_rtis_to_exclude = [58506, 58509, ]
    '''
    cell_id_rti = 10030
    origin_coords = (-36.8542893133162, 174.763988849045)  ## Mayoral Drive Auckland
    origin_date_str = '2015-01-02'
    dest_coords = (-36.8524039730814, 174.764086571267)  ## Albert St Auckland
    dest_date_str = '2015-06-03'
    cell_id_rtis_to_exclude = []
    (end_of_origin, start_of_dest, pre_coord_weights,
     post_coord_weights) = findrel.FindRelocations.get_relocation_dts(cur_local,
        cell_id_rti, origin_coords, origin_date_str, dest_coords, dest_date_str,
        cell_id_rtis_to_exclude, display_maps, display_misses, use_latest,
        examine_inner_gaps=True)
    print(end_of_origin, start_of_dest, pre_coord_weights, post_coord_weights)

def _make_switch_over_chart(cell_id_rti_a, cell_id_rti_b, new_data):
    fig = plt.figure(figsize=(150, 9))
    ax = fig.add_subplot(111)
    chart_title = ("Switchover between {} and {}?".format(cell_id_rti_a,
        cell_id_rti_b))
    x_a = [row['day'] for row in new_data
        if row['cell_id_rti'] == cell_id_rti_a]
    y_a = [row['imsi_n'] for row in new_data
        if row['cell_id_rti'] == cell_id_rti_a]
    x_b = [row['day'] for row in new_data
        if row['cell_id_rti'] == cell_id_rti_b]
    y_b = [row['imsi_n'] for row in new_data
        if row['cell_id_rti'] == cell_id_rti_b]
    mpl.Mpl.set_titles(ax, chart_title)
    mpl.Mpl.set_ticks(ax)
    mpl.Mpl.set_axis_labels(ax, y_label='N unique IMSIs')
    line_a, = plt.plot(x_a, y_a, 'red', linewidth=4)
    line_b, = plt.plot(x_b, y_b, 'blue', linewidth=4)
    handles = [line_a, line_b]
    lbls = ["cell_id_rti: {}".format(cell_id_rti) for cell_id_rti
        in (cell_id_rti_a, cell_id_rti_b)]
    mpl.Mpl.set_legends(ax, handles, lbls, fontsize=30)
    #plt.show()
    html_rel_img_path = ('images/charts/{} and {} switch_over.png'
        .format(cell_id_rti_a, cell_id_rti_b))
    fs_rel_img_path = 'reports/' + html_rel_img_path 
    plt.savefig(fs_rel_img_path, bbox_inches='tight')
    plt.close(fig)
    return html_rel_img_path

def make_changeover_charts(renamed_cell_id_rti_dets):
    """
    renamed_cell_id_rti_dets generated in find_original_name_cell_id_rtis() from
    source list of missing cell_id_rti's (in CSM but not mod_p).
    """
    debug = False
    cell_id_rti_pairs = [(d['csm_cell_id_rti'], d['alt_mod_p_cell_id_rti'])
        for d in renamed_cell_id_rti_dets]
    unused_con_rem, cur_rem = db.Pg.get_rem()
    sql = """\
    SELECT
    cell_id_rti,
    date,
      SUM(distinct_imsi) AS
    imsi_n
    FROM {daily_connections}
    WHERE cell_id_rti IN(%s, %s)
    GROUP BY cell_id_rti, date
    ORDER BY date, cell_id_rti
    """.format(daily_connections=conf.DAILY_CONNECTION_DATA)
    title_main = "Switch over from old to new 4G cell_id_rti"
    html = [conf.HTML_START_TPL % {'title': title_main, 'more_styles': ''},
        "<p>Identify time to switch from one to the other</p>",
    ]
    for cell_id_rti_a, cell_id_rti_b in cell_id_rti_pairs:
        cur_rem.execute(sql, (cell_id_rti_a, cell_id_rti_b))
        data = cur_rem.fetchall()
        new_data = [
            {'cell_id_rti': cell_id_rti,
             'day': datetime.strptime(date_str, '%Y-%m-%d'),
             'imsi_n': int(imsi_n)}
            for cell_id_rti, date_str, imsi_n in data]
        if debug: pp(new_data)
        html_rel_img_path = _make_switch_over_chart(cell_id_rti_a,
            cell_id_rti_b, new_data)
        html.append("<img width='3200px' padding=0 margin=0 src='{}'>".format(
            html_rel_img_path))
        #break
    ## assemble
    content = "\n".join(html)
    fpath = ("{}/reports/switch_over_from_old_to_new_4G_cell_id_rtis.html"
        .format(conf.CSM_ROOT))
    with open(fpath, 'w') as f:
        f.write(content)
    utils.open_new_tab("file://{}".format(fpath))

def corrections2make(con_local, cur_local):
    """
    Mod_P data is not perfect but it is a much better starting point for
    analyses than our existing CSM because:

    1) It explicitly captures relocations

    2) We can update it with all location data, not just those that meet the
    (irrelevant to us) trucall requirements (e.g. matching power requirements
    when compared with the Performance Management system; or appearing in
    Atoll).

    The data corrections required are:

    a) Switching lat/lon (Northing and Easting) in the handful of cases where it
    is back-the-front.

    b) Fix overlaps. Finishing on one day and starting on the same day counts as
    an overlap.
    #1 12345 2015-01-01  2016-01-01
    #2 12345 2016-01-01

    c) Changing 4G cell_id_rti's where they are recorded against their new
    cell_id_rti instead of their older one (the one that was in use when
    connection data was being recorded) ROSHNI handling creation of spreadsheet

    * RESULTS ******************************************************************

    * a) One dozen lat/lon swaps

    On row 8,500 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
    On row 8,500 found swapped coordinates so successfully corrected them
    On row 15,580 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
    On row 15,580 found swapped coordinates so successfully corrected them
    On row 20,377 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
    On row 20,377 found swapped coordinates so successfully corrected them
    On row 21,733 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
    On row 21,733 found swapped coordinates so successfully corrected them
    On row 27,541 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
    On row 27,541 found swapped coordinates so successfully corrected them
    On row 31,086 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
    On row 31,086 found swapped coordinates so successfully corrected them
    On row 33,531 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
    On row 33,531 found swapped coordinates so successfully corrected them
    On row 36,479 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
    On row 36,479 found swapped coordinates so successfully corrected them
    On row 38,654 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
    On row 38,654 found swapped coordinates so successfully corrected them
    On row 40,751 -51.4926371877991, -145.447506066183 not in credible NZ bounding box (Original easting: 5980910; northing: 2522359)
    On row 40,751 found swapped coordinates so successfully corrected them
    On row 42,354 -14.5661367723493, -143.880814346576 not in credible NZ bounding box (Original easting: 5832135; northing: 2344920)
    On row 42,354 found swapped coordinates so successfully corrected them
    On row 46,460 -50.667303833819, -145.241693512267 not in credible NZ bounding box (Original easting: 6292873; northing: 2765012)
    On row 46,460 found swapped coordinates so successfully corrected them

    On row 35,818 -22.3157358868727, -176.100348960025 not in credible NZ bounding box (Original easting: 3679467; northing: 8115446)
    Unable to correct coordinates. Description: TESTFEMTO 0
    [In ocean near Tonga]

    On row 37,955 -28.0013066273392, 179.677330498955 not in credible NZ bounding box (Original easting: 3172631; northing: 7452393)
    Unable to correct coordinates. Description: Raven Garcia 0
    [In ocean but near to Raoul Island]

    On row 41,259 -21.8839590211607, -177.978684040979 not in credible NZ bounding box (Original easting: 3452974; northing: 8179997)
    Unable to correct coordinates. Description: John Nicolas 0
    [In ocean near Tonga]
    
    On row 22,375 -42.7030902915785, 143.958827404529 not in credible NZ bounding box (Original easting: 315735; northing: 5427047)
    Unable to correct coordinates. Description: Region_4_Test 0
    [In ocean west of Tasmania]

    b)
    /*
    Won't find overlap below unless subtract time dimension on left edge otherwise < vs >= results in no match
    Need to replace missing effective_end's with value in distant future like 9999-01-01 so we can detect overlaps caused by two unended ranges.
    58398;"2015-03-11";"2015-05-12";-40.4130673679259;175.598386464186;"SH57 Linton Palmerston North"
    58398;"2015-05-12";"2015-05-12";-40.4130673679259;175.598386464186;"SH57 Linton Palmerston North"
    58398;"2015-05-13";"2015-06-24";-40.4130673679259;175.598386464186;"SH57 Linton Palmerston North"

    306299;"2017-03-10";"";-37.9579117950078;175.745636275417;"McMillan Road Tirau";"Tirau 2"
    306299;"2017-03-13";"";-37.9579117950078;175.745636275417;"McMillan Road Tirau";"Tirau 2"
    */
    SELECT
    cell_id_rti,
    effective_start,
    effective_end,
    tower_lat,
    tower_lon,
    physical_address
    FROM mod_p_locations
    WHERE cell_id_rti IN (
        SELECT
        mod1.cell_id_rti
        FROM (
          SELECT
            id,
            cell_id_rti,
            effective_start,
            CASE WHEN effective_end IS NULL THEN '9999-01-01' ELSE effective_end END AS
          effective_end  -- Need a date so we can find overlaps between two rows where effective_end is null
          FROM mod_p_locations
        ) AS mod1
        INNER JOIN
        (
          SELECT
            id,
            cell_id_rti,
            effective_start,
            CASE WHEN effective_end IS NULL THEN '9999-01-01' ELSE effective_end END AS
          effective_end
          FROM mod_p_locations
        ) AS mod2
        ON (mod1.cell_id_rti = mod2.cell_id_rti AND mod1.id != mod2.id -- so not finding overlaps with own row
        AND (
            (mod1.effective_start, mod1.effective_end)
            OVERLAPS
            (mod2.effective_start - interval '1 second', mod2.effective_end) -- less a second so same date is considered an overlap even though inclusive starts and exclusive ends to ranges
          )
        )
    )
    ORDER BY cell_id_rti, effective_start;
    """
    
    """
    SOLVED
    Negligible traffic ever e.g. between 1 and 100 unique IMSIs
    Verdict - remove ;-)
    10020;"2014-06-30";"2015-12-28";-37.7888696415904;175.281424301018;""
    10020;"2015-03-05";"2016-08-18";-43.5678287732647;172.686530519393;"11,Lock Crescent, Hillsborough"
    10020;"2016-08-19";"";-43.5678287732647;172.686530519393;"11 Lock Crescent, Hillsborough"

    cell_id_rti = 10020
    findrel.FindRelocations.get_daily_tots(cell_id_rti)

    origin_coords = (-37.7888696415904, 175.281424301018)
    origin_date_str = '2015-03-01'  ## before start of overlap
    dest_coords = (-43.5678287732647, 172.686530519393)
    dest_date_str = '2016-01-07'
    findrel.FindRelocations.get_relocation_dts(cur_local, cell_id_rti,
            origin_coords, origin_date_str,
            dest_coords, dest_date_str,
            cell_id_rtis_to_exclude=None,
            display_maps=True, display_misses=True,
            use_latest=True,  ## much faster
            examine_inner_gaps=True)

    SOLVED for both
    54551;"2014-03-18";"2015-09-24";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    54551;"2014-11-10";"2014-11-17";-43.5457695753381;172.599729312951;"Jack Hinton Drive             Addington             Christchurch"
    54551;"2015-09-25";"2016-07-03";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    
    Middle one (one week long) a mistake? Check where 54551 is in the middle of
    that period. If Ashburton then consolidate on Ashburton.

    Nowhere on 2014-11-13 but what about 2015-11-13 (perhaps a year typo)? In Ashburton on 2015-11-13.

    In Ashburton on 2015-01-01 and 2016-01-01.

    Nowhere again 2016-07-21 (as expected given end date).

    Make it Ashburton from 2014-03-18 - 2016-07-03 at -43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"

    Same for 54554? Several checks suggested it was so let's keep it in Ashburton as per 54551.

    54554;"2014-03-18";"2015-09-24";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    54554;"2014-11-10";"2014-11-17";-43.5457695753381;172.599729312951;"Jack Hinton Drive             Addington             Christchurch"
    54554;"2015-09-25";"2016-07-03";-43.8891231089925;171.735410393098;"153 Alford Forest Road Allenton Ashburton"
    cell_id_rti = 54554
    start_datetime_str = '2015-11-13 00:00:00'
    end_datetime_str = '2015-11-13 23:59:59'
    findrel.FindRelocations.get_likely_location_dts(cur_local, start_datetime_str,
        end_datetime_str, cell_id_rti, tis_to_exclude=[])
    """

    """
    SOLVED
    Chart for each date roughly to see where cell_id_rti actually is at these times.
    2015-02-01 nowhere
    2015-12-20 nowhere
    2016-01-20 nowhere
    2016-02-20 nowhere
    2016-03-10 probably Western Springs
    2016-03-15 probably Western Springs
    2016-03-21 barely at Western Springs
    2016-05-01 nowhere
    2016-07-01 nowhere
    2016-07-14 nowhere
    2016-07-17 nowhere
    2016-07-19 barely Beaumont St
    2016-07-20 probably Beaumont St
    2016-07-21 probably Beaumont St
    2016-08-01 probably Beaumont St
    2016-08-20 probably Beaumont St
    2016-08-30 probably Beaumont St
    2017-01-01 probably Beaumont St
    2017-05-01 probably Beaumont St

    31032;"2015-12-08";"2016-08-25";-36.8425524651737;174.753675569527;"142-148 Beaumont Street Auckland"
    31032;"2016-03-10";"2016-03-21";-36.8657944271395;174.726681941382;"731 Great North Road Western Springs Auckland"
    31032;"2016-08-26";"";-36.8425524651737;174.753675569527;"142-148 Beaumont Street Auckland"

    change to:

    31032;"2016-03-10";"2016-03-20";-36.8657944271395;174.726681941382;"731 Great North Road Western Springs Auckland"
    31032;"2016-07-20";"";-36.8425524651737;174.753675569527;"142-148 Beaumont Street Auckland"

    cell_id_rti = 31032
    date_str = "2016-07-20"
    start_datetime_str = date_str + ' 00:00:00'
    end_datetime_str = date_str + ' 23:59:59'
    utils.FindRelocations.get_likely_location_dts(cur_local, start_datetime_str,
        end_datetime_str, cell_id_rti, rtis_to_exclude=[])

    SOLVED
    Consolidate to one row but use the second location (-39.6144...) and remove end date
    10430;"2015-04-16";"2017-01-17";-39.615240561593;176.739251635288;"2375 State Highway 50, Te Awa Restaurant, Hawkes Bay"
    10430;"2016-08-19";"";-39.6144862848235;176.735550798771;"2375 State Highway 50, Te Awa Restaurant, Hawkes Bay ????"
    """

    """
    SOLVED
    Consolidate up to 2011-08-31 and after 2013-10-04
    31234;"2009-01-23";"2010-09-28";-36.8542893133162;174.763988849045;"31 Airedale Street Auckland CBD Auckland"
    31234;"2010-09-27";"2011-04-17";-36.8542893133162;174.763988849045;"31 Airedale Street Auckland CBD Auckland"
    31234;"2011-04-17";"2011-08-31";-36.8542893133162;174.763988849045;"31 Airedale Street Auckland CBD Auckland"
    31234;"2013-10-04";"2016-08-25";-36.8542893133162;174.763988849045;"31 Airedale Street Auckland CBD Auckland"
    31234;"2016-08-26";"";-36.8542893133162;174.763988849045;"31 Airedale Street Auckland CBD Auckland"
    """

    #fpath = '{}/CELLID_SECTOR_TECHNOLOGY.dlm'.format(conf.CSM_ROOT)
    #utils.LocationSources.make_mod_p_locations(con_local, cur_local, fpath)

def impact_of_stale():
    """
    Looking at the impact of using correct (relocation-aware) vs stale
    (relocation-unaware) data for Westport area (towers within 40km of centre of
    Westport). Overall picture changes noticeably when looking at traffic per
    cell_id_rti as would be expected but little different for IMSI's (people) by
    day. However, the difference is relevant for year-on-year trends.

    Using time-linked i.e. correct data: tot_bar_chart_source.pkl
    Using stale data: tot_bar_chart_source_Westport_using_stale_csm_mapping.pkl
    """
    fpath_time_linked = os.path.join(conf.CSM_ROOT, "tot_bar_chart_source.pkl")
    with open(fpath_time_linked, 'rb') as f:
        time_linked_daily_tots = pickle.load(f)
    fpath_stale_csm_mapping = os.path.join(conf.CSM_ROOT,
        "tot_bar_chart_source_Westport_using_stale_csm_mapping.pkl")
    with open(fpath_stale_csm_mapping, 'rb') as f:
        stale_csm_mapping_daily_tots = pickle.load(f)
    ## Do month on month for Jan etc
    var_fnames = [
        (time_linked_daily_tots, 'time_linked_westport.csv'),
        (stale_csm_mapping_daily_tots, 'stale_csm_mapping_westport.csv'),
    ]
    for var, fname in var_fnames:
        with open(os.path.join(conf.CSM_ROOT, fname), 'w') as f:
            f.write('date,freq\n')
            f.write('\n'.join(["{},{}".format(date_str, freq)
                for date_str, freq in sorted(var.items())]))

def check_unfindable():
    """
    The real question is whether any of these cell_id_rti's relocated or appear
    in mod_P under a different cell_id_rti.

    Note -- all in CSM but not Mod_P. Could there be data for them under another
    id on the dates they don't cover in connections data using the CSM
    cell_id_rti that appears in Mod_P.

    15168033 2014-12-18 to 2017-02-09
    15109919 2014-12-18 to 2016-11-13
    13912863 2014-12-18 to 2016-05-03
    13128991 2014-12-18 to 2016-04-13
    13001247 2014-12-18 to 2016-10-12
    12908319 2014-12-18 to 2016-10-03
    12807455 2014-12-18 to 2016-10-21
    430727   2017-03-29 to 2017-03-31  Mar-Apr 2017 only 100s then 1000s (2017-03-29 to 2017-03-31) then 10s
    316027   2014-12-18 to 2015-06-09
    274463   2016-04-14 to 2016-10-04 then 1s afterwards
    262687   2016-04-14 to 2016-05-19
    257823   2016-04-14 onwards
    54442    dribble
    45643    1s etc i.e. dribble
    44131    2016-06-29 to 2016-11-22 and 1s afterwards and 1s backwards
    40349    2016-07-22 to 2016-12-13 and 1s afterwards and 1s backwards
    38871    2014-12-18 to 2015-03-29 and 1s afterwards
    38473    dribble
    25428    2016-06-15 to 2016-06-18 and 1s afterwards and 10s then 1s before
    """
    #cell_id_rti = 44131
    #utils.FindRelocations.get_daily_tots(cell_id_rti)

def get_dribblers_and_bounds(cell_id_rtis):
    """
    Dribbler - no more than 100 on any day.
    """
    debug = True
    verbose = False
    cell_id_rti_clause = db.Pg.nums2clause(cell_id_rtis)
    sql = """\
    SELECT
    cell_id_rti,
      AVG(CASE WHEN distinct_imsi >= 100 THEN distinct_imsi ELSE 0 END) AS -- ignore dribblers when determining importance
    avg_imsis_ex_dribblers,
      MIN(CASE WHEN distinct_imsi >= 800 THEN date ELSE null END) AS  -- ignore tails
    start_date_major,
      MAX(CASE WHEN distinct_imsi >= 800 THEN date ELSE null END) AS
    end_date_major,
      MIN(CASE WHEN distinct_imsi >= 100 THEN date ELSE null END) AS  -- ignore tails
    start_date_effective,
      MAX(CASE WHEN distinct_imsi >= 100 THEN date ELSE null END) AS
    end_date_effective
    FROM {daily_connections}
    WHERE cell_id_rti IN {cell_id_rti_clause}
    GROUP BY cell_id_rti
    ORDER BY SUM(distinct_imsi), cell_id_rti
    """.format(cell_id_rti_clause=cell_id_rti_clause,
        daily_connections=conf.DAILY_CONNECTION_DATA)
    unused_con_rem, cur_rem = db.Pg.get_rem()
    cur_rem.execute(sql)
    data = cur_rem.fetchall()
    dribbler_cell_id_rtis = []
    bounds = []
    for row in data:
        (cell_id_rti, avg_imsis_ex_dribblers, start_date_major, end_date_major,
             start_date_effective, end_date_effective) = row
        if debug and verbose:
            print(cell_id_rti, avg_imsis_ex_dribblers, start_date_major,
                end_date_major, start_date_effective, end_date_effective)
        dribbler = (avg_imsis_ex_dribblers < 2000)
        if dribbler:
            dribbler_cell_id_rtis.append(cell_id_rti)
        else:
            bounds.append(
                {'cell_id_rti': cell_id_rti,
                 'start_date_major': start_date_major,
                 'end_date_major': end_date_major,
                 'start_date_effective': start_date_effective,
                 'end_date_effective': end_date_effective,
                })
    dribbler_cell_id_rtis.sort()
    bounds.sort(key=lambda d: d['cell_id_rti'])
    if debug:
        print(dribbler_cell_id_rtis)
        pp(bounds)
    return dribbler_cell_id_rtis, bounds

def check_ends_the_same(cur_local):
    sql_unfindables = """\
    SELECT DISTINCT cell_id_rti
    FROM {csm}
    LEFT JOIN
    mod_p_locations_orig
    USING(cell_id_rti)
    WHERE mod_p_locations_orig IS NULL
    ORDER BY cell_id_rti;
    """.format(csm=conf.CSM_HISTORY)
    cur_local.execute(sql_unfindables)
    cell_id_rtis = [row['cell_id_rti'] for row in cur_local.fetchall()]
    dribbler_cell_id_rtis, bounds = get_dribblers_and_bounds(cell_id_rtis)
    ok_to_use_effective_range = [262175, 262177, 262687,
        262689, 264224, 264225, 274465, 262688,
        12807455, 12807456, 12815136, 12815137,  ## 12xxxxxx all in ChCh as are most of the others (unless in Auckland)
        12836639, 12836640, 12836641, 12844319, 12844320, 12844321, 12844833,
        12860705, 12862752, 12862753, 12863007, 12863008, 12863009, 12863263,
        12863264, 12863265, 12863520, 12871968, 12871969, 12908319, 12908320,
        12908321, 12909343, 12909344, 12909345, 12909600, 12909855, 12909856,
        12909857, 12910113, 12911392, 12911393, 12911903, 12911904, 12935199,
        12935201, 12954143, 12954144, 12957471, 12957472, 12973087, 12973088,
        12973089, 13001247, 13001248, 13001249, 13045024, 13045025, 13055520,
        13152111, 13152121, 13152131, 13304607, 13356063, 13356064, 13356065,
        13356143, 13356153, 13356163, 13827871, 13827872, 13827873, 13829408,
        13829409, 13829921, 13830176, 13830177, 13832479, 13832480, 13837345,
        13841183, 13841185, 13843999, 13844001, 13853471, 13853473, 13855263,
        13861664, 13862176, 13863455, 13863456, 13863457, 13864736, 13864737,
        13865760, 13871391, 13871392, 13879328, 13879329, 13895200, 13895201,
        13920288, 13921311, 13921313, 13929247, 13929249, 13930783, 13930784,
        15109919, 15109921, 15133983, 15133984, 15133985, 15134239, 15134240,
        15134495, 15134496, 15134497, 15135519, 15135520, 15137312, 15137823,
        15137824, 15137825, 15138415, 15138435, 15143712, 15161120, 15168033,
        15161121, ## wondered if shifted within Christchurch but probably didn't having looked at CSM data for it
    ]
    print(len(dribbler_cell_id_rtis))
    print(dribbler_cell_id_rtis)
    print(len(ok_to_use_effective_range))
    #return

    for bound_dets in bounds:
        cell_id_rti = bound_dets['cell_id_rti']
        if cell_id_rti in ok_to_use_effective_range:
            continue
        min_date_str = max(bound_dets['start_date_major'], '2015-01-02')
        max_date_str = bound_dets['end_date_major']
        findrel.FindRelocations.compare_likely_locations_day_level(cur_local,
            min_date_str, max_date_str, cell_id_rti, cell_id_rtis_to_exclude=[])

def _date_str_or_empty_str(date_obj):
    try:
        date_str = date_obj.strftime('%Y-%m-%d')
    except AttributeError:
        date_str = ''
    return date_str

def add_existing_mod_p_data_to_roshni_data(cur_local):
    """
    If just one match, grab key date details to display first. If more than one,
    make new, special list for custom handling.
    """
    debug = False
    fpath = os.path.join(conf.CSM_ROOT, 'storage',
        'alternative_4G_cell_id_rtis_and_ranges.csv')
    with open(fpath) as csvfile:
        reader = csv.DictReader(csvfile)
        sql_cell_id_rti_rows_tpl = """\
        SELECT *
        FROM {mod_p_locations}
        WHERE cell_id_rti = %s
        ORDER BY effective_start
        """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
        easy_data = []
        tricky_data = []
        for row in reader:
            if debug: pp(row)
            cell_id_rti = row['cell_id_rti']
            effective_start = row['effective_start']
            effective_end = (row['effective_end']
                if row['effective_end'] != '1899-12-30' else None)
            cur_local.execute(sql_cell_id_rti_rows_tpl, (cell_id_rti, ))
            cell_id_rti_data = cur_local.fetchall()
            n_rows = len(cell_id_rti_data)
            if n_rows > 1:
                ## spit out into new file
                for row in cell_id_rti_data:
                    old_effective_start = dates.Dates.date_to_date_str_or_empty_str(
                        row['effective_start'])
                    old_effective_end = dates.Dates.date_to_date_str_or_empty_str(
                        row['effective_end'])
                    tricky_data.append(
                        (cell_id_rti, effective_start, effective_end,
                         old_effective_start, old_effective_end, row)
                    )
            elif n_rows == 1:
                row = cell_id_rti_data[0]
                old_effective_start = dates.Dates.date_to_date_str_or_empty_str(
                    row['effective_start'])
                old_effective_end = dates.Dates.date_to_date_str_or_empty_str(
                    row['effective_end'])
                easy_data.append(
                    (cell_id_rti, effective_start, effective_end,
                     old_effective_start, old_effective_end, row)
                )
            else:
                if debug: print(cell_id_rti)
    dest_path_easy = os.path.join(conf.CSM_ROOT, 'storage',
        'alternative_4G_cell_id_rtis_and_ranges_with_context_easy.csv')
    with open(dest_path_easy, 'w') as f:
        f.write("cell_id_rti,effective_start,effective_end,old_effective_start,old_effective_end,context")
        for line in easy_data:
            (cell_id_rti, effective_start, effective_end,
             old_effective_start, old_effective_end, row) = line
            if old_effective_start > effective_start:
                print("Aaaargggghh - we're screwed {} vs {} for {}".format(
                    old_effective_start, effective_start, cell_id_rti))  ## heh - fortunately didn't find any
            f.write("\n{},{},{},{},{},{}".format(
                cell_id_rti,
                effective_start,
                effective_end,
                old_effective_start,
                old_effective_end,
                row))
    dest_path_tricky = os.path.join(conf.CSM_ROOT, 'storage',
        'alternative_4G_cell_id_rtis_and_ranges_with_context_tricky.csv')
    with open(dest_path_tricky, 'w') as f:
        f.write("cell_id_rti,effective_start,effective_end,context")
        for line in tricky_data:
            f.write("\n{}".format(line))
    print("Finished!")

def main():
    """
    None of the CSM results were additional to those in Mod_P but Mod_P had
    few that were Mod_P only.
    [42521, 42522, 42523, 42524, 42525, 42526, 52010, 52011, 52012, 52013, 52014, 52015, 52016, 52021, 52022,
    52520, 52521, 52522, 52523, 52524, 52525, 52526, 53191, 53192, 53193, 53194, 53195, 53196, 53197, 53198, 53199,
    393327, 393331, 393332, 393337, 393341, 393342, 393347, 393351, 393352]
    Now look for relocations that are not already factored into John's attempted
    validation chart. The period of the surviving dip is Oct 1 2015- Dec 24 2015

    Westport data will have been attributed to Otematata until 2015-09-30 for 52011, 52012, 52014, and 52015.
    Rolleston (Christchurch) data will have been attributed to Westport up till 2013-12-11 for 52021 and 52022.
    These cell_id_rti's only began contributing to Westport again on 2015-12-08.
    Fielding data will have been attributed to Westport till 2014-03-17 for 53191, 53192, 53193, 53194, 53195, 53196, 53197, 53198, 53199.
    These cell_id_rti's only started contributing to Westport again on 2015-10-01.
    Impact on data over 2015.

    * Westport to Otematata relocations ****
    Westport data will have been attributed to Otematata until 2015-09-30 for 52011, 52012, 52014, and 52015.

    Note - 52010 and 52016 were always in Mt Rochfort Westport so are missing from list of relocations below.

    52011;"2013-02-12";"2015-09-30";-41.7784394317734;171.740248178619;"Mt Rochfort Westport";"U"
    52011;"2015-12-02";"2016-10-13";-44.5843795107931;170.177315461237;"Otematata Station Otematata - Kurow Road (SH 83) Otematata";"U"

    52012;"2013-02-12";"2015-09-30";-41.7784394317734;171.740248178619;"Mt Rochfort Westport";"U"
    52012;"2015-12-02";"2016-10-13";-44.5843795107931;170.177315461237;"Otematata Station Otematata - Kurow Road (SH 83) Otematata";"U"

    52014;"2012-10-02";"2015-09-30";-41.7784394317734;171.740248178619;"Mt Rochfort Westport";"U"
    52014;"2015-12-02";"2016-10-13";-44.5843795107931;170.177315461237;"Otematata Station Otematata - Kurow Road (SH 83) Otematata";"U"

    52015;"2013-02-12";"2015-09-30";-41.7784394317734;171.740248178619;"Mt Rochfort Westport";"U"
    52015;"2015-12-02";"2016-10-13";-44.5843795107931;170.177315461237;"Otematata Station Otematata - Kurow Road (SH 83) Otematata";"U"

    * Rolleston (Christchurch) to Millerton (Westport) relocations ****
    Rolleston (Christchurch) data will have been attributed to Westport up till 2013-12-11 for 52021 and 52022.
    These cell_id_rti's only began contributing to Westport again on 2015-12-08.

    52021;"2008-10-01";"2013-12-11";-43.5867758600525;172.386442255883;"Cnr Jones Road and Hoskyns Road Rolleston";"U"
    52021;"2015-12-08";"          ";-41.6353896711318;171.859636338202;"Stockton Road Millerton";"U"

    52022;"2008-10-01";"2013-12-11";-43.5867758600525;172.386442255883;"Cnr Jones Road and Hoskyns Road Rolleston";"U"
    52022;"2015-12-08";"          ";-41.6353896711318;171.859636338202;"Stockton Road Millerton";"U"

    * Fielding (actually Feilding but I'll let that pass ;-)) to Westport ****
    Fielding data will have been attributed to Westport till 2014-03-17 for 53191, 53192, 53193, 53194, 53195, 53196, 53197, 53198, 53199.
    These cell_id_rti's only started contributing to Westport again on 2015-10-01.

    On Oct 1 it arrived in Westport. So, prior to this, we will have been
    including Feilding data towards Westport till March 2014, then no data, and then losing it
    In CSM assumed to always be at -41.778769;171.740146 i.e. Mt Rochfort Westport

    53191;"2013-03-04";"2014-03-17";-40.234358832667;175.558827660212;"Manfield Park Fielding";"U"
    53191;"2015-10-01";"          ";-41.7784394317734;171.740248178619;"Mt Rochfort Westport";"U"

    53192;"2013-03-04";"2014-03-17";-40.234358832667;175.558827660212;"Manfield Park Fielding";"U"
    53192;"2015-10-01";"          ";-41.7784394317734;171.740248178619;"Mt Rochfort Westport";"U"

    53193;"2013-03-04";"2014-03-17";-40.234358832667;175.558827660212;"Manfield Park Fielding";"U"
    53193;"2015-10-01";"          ";-41.7784394317734;171.740248178619;"Mt Rochfort Westport";"U"

    53194;"2013-03-04";"2014-03-17";-40.234358832667;175.558827660212;"Manfield Park Fielding";"U"
    53194;"2015-10-01";"          ";-41.7784394317734;171.740248178619;"Mt Rochfort Westport";"U"

    53195;"2013-03-04";"2014-03-17";-40.234358832667;175.558827660212;"Manfield Park Fielding";"U"
    53195;"2015-10-01";"          ";-41.7784394317734;171.740248178619;"Mt Rochfort Westport";"U"

    53196;"2013-03-04";"2014-03-17";-40.234358832667;175.558827660212;"Manfield Park Fielding";"U"
    53196;"2015-10-01";"          ";-41.7784394317734;171.740248178619;"Mt Rochfort Westport";"U"

    53197;"2014-03-11";"2014-03-17";-40.234358832667;175.558827660212;"Manfield Park Fielding";"U"
    53197;"2015-10-01";"          ";-41.7784394317734;171.740248178619;"Mt Rochfort Westport";"U"

    53198;"2014-03-11";"2014-03-17";-40.234358832667;175.558827660212;"Manfield Park Fielding";"U"
    53198;"2015-10-01";"          ";-41.7784394317734;171.740248178619;"Mt Rochfort Westport";"U"

    53199;"2014-03-11";"2014-03-17";-40.234358832667;175.558827660212;"Manfield Park Fielding";"U"
    53199;"2015-10-01";"          ";-41.7784394317734;171.740248178619;"Mt Rochfort Westport";"U"

    What John's mappings/analyses assumed:
    tnz_loc=> SELECT cell_id_rti FROM tourism_input.csm_cell_id_to_s2_12_index WHERE s2_12_index = 734 ORDER BY cell_id_rti;  cell_id_rti 
    -------------
           42521 Stable Westport 3G 2015-10-11 onwards 131 Palmerston St
           42522
           42523
           42524
           42525
           42526

           52521 Stable Westport 3G up till 2015-10-10
           52522
           52523
           52524
           52525
           52526

          393327 Stable Westport 4G 2015-10-11 onwards
          393331
          393332
          393337
          393341
          393342
          393347
          393351
          393352
    (21 rows)

    Note - even though s2_12_index 734 might only have one tower or so, changes
    to other towers would affect what it picks up for first in day. A new tower
    further away could take all the traffic for people in that s2_12 thus
    lowering the numbers.

    whereas 53191, 53192, 53193, 53194, 53195, 53196, 53197, 53198, 53199 on
    Mt Rochfort (to east) started potentially cannibalising traffic on 2015-10-01.

    and 52021 and 52022 in Stockton (east north east) started potentially
    cannibalising traffic on 2015-12-08

    and 52011, 52012, 52014, and 52015 on Mt Rochfort (east) started potentially
    cannibalising traffic on 2015-12-02

    Westport data will have been attributed to Otematata until 2015-09-30 for 52011, 52012, 52014, and 52015.
    Rolleston (Christchurch) data will have been attributed to Westport up till 2013-12-11 for 52021 and 52022.
    These cell_id_rti's only began contributing to Westport again on 2015-12-08.
    Fielding data will have been attributed to Westport till 2014-03-17 for 53191, 53192, 53193, 53194, 53195, 53196, 53197, 53198, 53199.
    These cell_id_rti's only started contributing to Westport again on 2015-10-01.
    """
    con_local, cur_local, unused_cur_local2 = db.Pg.get_local()
    
    #con_local.commit()
    #differing_relocations(cur_local)
    #phantom_relocations(cur_local)
    #missing_cell_id_rtis(cur_local)

    ## use coordinates of 
    #pp(westport_cell_id_rtis, width=150)
    
    
    #report_cell_id_contribs_old_vs_new_name(cur_local)
    
    #locsrcs.make_csm_history(con_local, cur_local)
    
    #confirm_relocations(cur_local)
    #pp(reprel.mod_p_relocation_gaps(cur_local))
    
    #locsrcs.make_redrock_history(con_local, cur_local,
    #    extract_from_zips=False)
    #impact_of_stale()
    
    #find_original_name_cell_id_rtis(cur_local)
    #check_unfindable()
    
    #corrections2make(con_local, cur_local)
    
    #make_changeover_charts()
    
    #consolidate_rows(con_local, cur_local)
    
    #get_dribblers_and_bounds()
    
    #check_ends_the_same(cur_local)
    
    '''
    findrel.FindRelocations.get_likely_location_dts(cur_local,
        start_datetime_str='2017-01-01 00:00:00',
        end_datetime_str='2017-01-01 23:59:59',
        cell_id_rti=12818209, rtis_to_exclude=None)

    findrel.FindRelocations.get_likely_location_dts(cur_local,
        start_datetime_str='2017-01-01 00:00:00',
        end_datetime_str='2017-01-01 23:59:59',
        cell_id_rti=257825, rtis_to_exclude=None)
    '''

    #add_existing_mod_p_data_to_roshni_data(cur_local)
    
    #locsrcs.make_redrock_error_history(con_local, cur_local,
    #    extract_from_zips=False)
    #locsrcs.make_csm_2_csv(cur_local, lbl='20170530')
    #locsrcs.make_csm_2_csv_ivan(cur_local, lbl='20170530')
    locsrcs.Deprecated.make_test_csm2(cur_local)

if __name__ == '__main__':
    main()
