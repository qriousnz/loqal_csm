#! /usr/bin/env python3

"""
Add 4G and 3G data to a redrocks_history table for data several weeks either
side of when Queenstown data plunged (i.e. approx Nov 29 2016).

Note - detected 45 records that couldn't be added because a cell_id_rti was
already entered for that date. E.g. in HUA_LTE_201611131936.txt the only
difference between the following is
1418,CPHUA,111,363119,CPHUA1_J:1,1,,,-40.742353,175.1897,3,10 ,65,10,24,273,38131,20,47.8,15.8,.5,Kathrein,80010735V01,700 MHz,MIMO,9310,20,0,0
1418,CPHUA,111,363119,CPHUA1_J:2,2,,,-40.742353,175.1897,3,100,65,10,24,273,38131,20,47.8,15.8,.5,Kathrein,80010735V01,700 MHz,MIMO,9310,20,0,0
in Azimuth (10 vs 100).

similar in another example

1418,CPHUA,111,363119,CPHUA1_J:1,1,,,-40.742353,175.1897,3,10 ,65,10,24,273,38131,20,47.8,15.8,.5,Kathrein,80010735V01,700 MHz,MIMO,9310,20,0,0
1418,CPHUA,111,363119,CPHUA1_J:2,2,,,-40.742353,175.1897,3,100,65,10,24,273,38131,20,47.8,15.8,.5,Kathrein,80010735V01,700 MHz,MIMO,9310,20,0,0

Note - in the sample taken only LTE data generated any duplicates.
"""

import base64
from datetime import date, datetime, timedelta
from statistics import mean, median
from webbrowser import open_new_tab

import folium
import matplotlib.dates as mdates
import matplotlib.pyplot as plt

import sys
sys.path.insert(0, '../voyager_shared')
import conf #@UnresolvedImport
import utils #@UnresolvedImport

MAX_subcharts = None  ## None for unlimited

QT_CONNECTION_DATA = 'qt_connection_data'
LOCAL_RTIS_2017_01_20 = 'cell_id_rtis_2017_01_20'
REDROCK_LATEST = 'redrock_latest'
MAIN_IMG_WIDTH = 25
MAIN_IMG_HEIGHT = 5
DROP_DT = datetime.strptime('2016-11-29', '%Y-%m-%d')
X_OFFSET = timedelta(hours=24)
MIN_HEIGHT2USE = 0.25
DIST2M_SCALAR = 100000

"""
http://www.rtonz.org.nz/rto-location-map.html
and looking at AU2013_GV_Full.shp to get clear sense of boundaries (and likely
boundaries)
"""
QT_BOUNDING_BOXES = [  ## these boxes together roughly cover the Queenstown RTO and don't include too much outside - fit for purpose given margin of area using coverage etc. No harm if these boxes may overlap a fair bit
    {'top': -44.746697, 'left': 168.277761, 'bottom': -45.282098, 'right': 168.857687},  #-44.746697, 168.277761 -45.282098, 168.857687
    {'top': -44.761932, 'left': 168.185882, 'bottom': -45.027782, 'right': 169.158447},  #-44.761932, 168.185882 -45.027782, 169.158447
    {'top': -44.713657, 'left': 168.348556, 'bottom': -44.904332, 'right': 169.038104},  #-44.713657, 168.348556 -44.904332, 169.038104
    {'top': -44.560672, 'left': 168.573263, 'bottom': -44.788114, 'right': 168.826769},  #-44.560672, 168.573263 -44.788114, 168.826769
    {'top': -45.227539, 'left': 168.644329, 'bottom': -45.381347, 'right': 168.824064},  #-45.227539, 168.644329 -45.381347, 168.824064
]

HTML_START_TPL = """\
<!DOCTYPE html>
<html>
<head>
<title>%(title)s</title>
<style>
body {
  padding: 0;
  margin: 0;
  font-size: 12px;
  font-family: arial;
}
#content {
  padding: 12px;
}
#banner {
  background-color: #241c54;
  background-image: url("images/format/Qrious-Logo-White-Trans.png");
  background-repeat: no-repeat;
  background-size: 160px;
  background-position: 18px 50%%;
  height: 65px;
}
h1, h2 {
  color: #241c54;
}
%(more_styles)s
</style>
</head>
<body>
<div id='banner'></div>
<div id='content'>
<h1>%(title)s</h1>
"""

def _get_one_dataset_per_day(raw=True):
    src = conf.REDROCK_HISTORY if raw else conf.SECTOR_LOCATION_HISTORY 
    return """\
    SELECT * FROM {src} WHERE as_at_datetime IN (
    
        SELECT DISTINCT ON (as_at_date) as_at_datetime  /* exclude src data from datetimes
          where another datetime src record later on same date
          e.g. use "2016-11-28 21:54:00+13" instead of "2016-11-28 00:59:00+13"
          */
        FROM (
          SELECT DISTINCT as_at_date, as_at_datetime FROM {src}
        ) AS qry
        ORDER BY as_at_date, as_at_datetime DESC
    )
    """.format(src=src)

def _get_WHERE_qt_coords():
    sql_tpl = """(
    (tower_lat BETWEEN {bottom} AND {top})
    AND
    (tower_lon BETWEEN {left} AND {right})
    )"""
    bbox_clauses = [sql_tpl.format(**bbox_dic)
        for bbox_dic in QT_BOUNDING_BOXES]
    qt_filter = "WHERE (" + " OR ".join(bbox_clauses) + ")"
    return qt_filter

def _add_drop_line(plt, ax, max_y, inc_text=True):
    plt.axvline(x=DROP_DT, linewidth=4, color='red')
    if inc_text:
        ax.text(DROP_DT + X_OFFSET, 0.95*max_y, 'The DROP', ha='left', va='top',
            fontsize=18)

def _get_missing_dts(min_dt, max_dt, data_dts):
    """
    No borders so no need to bunch together into larger contiguous bands.
    """
    data_dts = set(data_dts)
    all_dts = set()
    dt = min_dt 
    while True:
        all_dts.add(dt)
        dt = dt + timedelta(days=1)
        if dt > max_dt:
            break
    missing_dts = sorted(list(all_dts - data_dts))
    return missing_dts

def _add_missing_band(ax, dt):
    """
    Indicate where data is missing.
    """
    start_dt = dt
    end_dt = dt + timedelta(hours=24)
    ax.axvspan(start_dt, end_dt, facecolor='grey', edgecolor='white',
        alpha=0.15)

def make_qt_connection_data_tbl(con_local, cur_local):
    """
    Build connection data table for Queenstown

    Can only filter connection data by cell_id_rti and date so need to get all
    cell_id_rti's that (have ever) fitted within Queenstown RTO and filter
    product db using those. Don't worry about accidentally including
    cell_id_rti's that were not inside Queenstown for a particular date even
    though they were at some stage within the month - we will be matching
    specifically off the local table - this is just to shrink the amount of
    local data we have to handle.

    Running one per month to avoid a query which dies after almost 2 hours ;-).
    """
    unused, cur_rem = utils.Pg.get_rem()
    utils.Pg.drop_tbl(con_local, cur_local, tbl=QT_CONNECTION_DATA)
    sql_make_tbl = """\
    CREATE TABLE {qt_connection_data} (
        id SERIAL,
        cell_id_rti integer,
        date date,
        n_sims integer,
        PRIMARY KEY(id)
    )
    """.format(qt_connection_data=QT_CONNECTION_DATA)
    cur_local.execute(sql_make_tbl)
    ## get remote data
    min_dt, max_dt, unused, unused = utils.LocationSources.get_rr_history_dates(
        cur_local)
    WHERE_qt_coords = _get_WHERE_qt_coords()
    #print(WHERE_qt_coords)
    sql_rtis = """\
    SELECT DISTINCT cell_id_rti
    FROM {redrock_history}
    {WHERE_qt_coords}
    ORDER BY cell_id_rti
    """.format(redrock_history=conf.REDROCK_HISTORY,
        WHERE_qt_coords=WHERE_qt_coords)
    cur_local.execute(sql_rtis)
    qt_cell_id_rtis = ','.join(str(row[0]) for row in cur_local.fetchall())
    ## much faster and safer if split into months
    month_range_dates = utils.get_month_range_dates(min_dt, max_dt)
    sql_insert_tpl = """\
    INSERT INTO {qt_connection_data}
    (cell_id_rti, date, n_sims)
    VALUES(%s, %s, %s)
    """.format(qt_connection_data=QT_CONNECTION_DATA)
    for range_dates in month_range_dates:
        month_start_date, month_end_date = range_dates
        sql_get_months_data = ("""\
        SELECT
        cell_id_rti,
        date,
          COUNT(*) AS
        n_sims
        FROM (
          SELECT DISTINCT cell_id_rti, date, imsi_rti
          FROM {connection_data}
          WHERE date BETWEEN '{month_start_date}' AND '{month_end_date}'
          AND cell_id_rti IN ({qt_cell_id_rtis})
        ) AS imsi_agg
        GROUP BY cell_id_rti, date
        ORDER by cell_id_rti, date
        """.format(connection_data=conf.CONNECTION_DATA,
            month_start_date=month_start_date, month_end_date=month_end_date,
            qt_cell_id_rtis=qt_cell_id_rtis))
        cur_rem.execute(sql_get_months_data)
        n = 1
        while True:
            row = cur_rem.fetchone()
            if not row:
                break
            cur_local.execute(sql_insert_tpl, tuple(row))
            if n % 250 == 0:
                print("Completed {:,} rows for {}-{}".format(
                    n, month_start_date, month_end_date))
            n += 1
    con_local.commit()

def chart_cell_id_rti(cur, cell_id_rti, min_dt, max_dt, missing_dts,
        total_height, raw=True):
    src_lbl_long = 'Raw Redrock Data' if raw else 'Consolidated Redrock Data'
    src_lbl_short = 'raw' if raw else 'consolidated'
    sql_one_dataset_per_day = _get_one_dataset_per_day(raw)
    WHERE_qt_coords = _get_WHERE_qt_coords()
    sql_qt_cell_id_rti = """\
    SELECT
    src.as_at_date,
    calls.n_sims
    FROM ({sql_one_dataset_per_day}) AS src
    LEFT JOIN {local_qt_call_data} AS calls
    ON src.cell_id_rti = calls.cell_id_rti AND src.as_at_date=calls.date
    {WHERE_qt_coords}
    AND src.cell_id_rti = %s
    ORDER BY src.as_at_date
    """.format(sql_one_dataset_per_day=sql_one_dataset_per_day,
        local_qt_call_data=QT_CONNECTION_DATA,
        WHERE_qt_coords=WHERE_qt_coords)
    cur.execute(sql_qt_cell_id_rti, (cell_id_rti, ))
    data = cur.fetchall()
    x = [row['as_at_date'] for row in data]
    y = [row['n_sims'] or 0 for row in data]
    max_y = max(y)
    shrink_by = total_height/max_y
    height2use = (25*MAIN_IMG_HEIGHT)/shrink_by
    height_lbl = ''
    if height2use < MIN_HEIGHT2USE:
        height2use = MIN_HEIGHT2USE
        height_lbl = ' (not to scale - too small to show true height)'
    fig = plt.figure(figsize=(MAIN_IMG_WIDTH, height2use))
    ax = fig.add_subplot(111)
    ax.set_title("N SIMs connecting to cell_id_rti from {}: {}{}".format(
        src_lbl_long, cell_id_rti, height_lbl), fontsize=16, fontweight='bold')
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%b-%d\n%Y'))
    ax.tick_params(axis='x', which='major', labelsize=8)
    ax.tick_params(axis='y', which='major', labelsize=5)
    ax.bar(x, y, color='grey', align='center')
    ax.set_xlim([min_dt, max_dt])
    ax.set_ylim([0, max(y)])
    #ax.set_xlabel('Date', fontsize=14)
    #ax.set_ylabel('SIMs connecting', fontsize=14)
    _add_drop_line(plt, ax, max_y, inc_text=False)
    for missing_dt in missing_dts:
        _add_missing_band(ax, missing_dt)
    #plt.show()
    ## Create
    img_name = "SIMs connected to {} {}-{} {}.png".format(cell_id_rti, min_dt,
        max_dt, src_lbl_short)
    img_fpath = "reports/images/charts/{}".format(img_name)
    img_rel_path = "images/charts/{}".format(img_name)
    fig.savefig(img_fpath, bbox_inches='tight')
    plt.close(fig)
    return img_rel_path

def chart_cell_id_rtis(cur, min_dt, max_dt, missing_dts, total_height,
        raw=True):
    WHERE_qt_coords = _get_WHERE_qt_coords()
    sql_qt_cell_id_rtis = """\
    SELECT DISTINCT cell_id_rti
    FROM {redrock_history}
    {WHERE_qt_coords}
    ORDER BY cell_id_rti
    """.format(redrock_history=conf.REDROCK_HISTORY,
        WHERE_qt_coords=WHERE_qt_coords)
    cur.execute(sql_qt_cell_id_rtis)
    cell_id_rtis = [row['cell_id_rti'] for row in cur.fetchall()]
    imgs_dets = []
    for n, cell_id_rti in enumerate(cell_id_rtis, 1):
        img_rel_path = chart_cell_id_rti(cur, cell_id_rti, min_dt, max_dt,
            missing_dts, total_height, raw)
        imgs_dets.append({'rel_path': img_rel_path, 'cell_id_rti': cell_id_rti})
        if n % 10 == 0:
            print("Completed {:,} charts ...".format(n))
        if MAX_subcharts is not None and n >= MAX_subcharts:
            print("\n\n" + "*"*100 +
                "\nChange code to make all charts after testing\n" + "*"*100)
            break
    return imgs_dets, cell_id_rtis

def chart_tot(cur, min_dt, max_dt, min_date, max_date, raw=True):
    sql_one_dataset_per_day = _get_one_dataset_per_day(raw)
    WHERE_qt_coords = _get_WHERE_qt_coords()
    sql_qt_tot = """\
    SELECT
    src.as_at_date,
      SUM(calls.n_sims) AS
    tot_sim_days
    FROM ({sql_one_dataset_per_day}) AS src
    LEFT JOIN {local_qt_call_data} AS calls
    ON src.cell_id_rti = calls.cell_id_rti AND src.as_at_date=calls.date
    {WHERE_qt_coords}
    GROUP BY src.as_at_date
    ORDER BY src.as_at_date
    """.format(sql_one_dataset_per_day=sql_one_dataset_per_day,
        local_qt_call_data=QT_CONNECTION_DATA,
        WHERE_qt_coords=WHERE_qt_coords)
    #print(sql_qt_tot); return
    cur.execute(sql_qt_tot)
    data = cur.fetchall()
    #pp(data)
    x = [row['as_at_date'] for row in data]
    y = [row['tot_sim_days'] for row in data]
    max_y = max(y)
    fig = plt.figure(figsize=(MAIN_IMG_WIDTH, MAIN_IMG_HEIGHT))
    ax = fig.add_subplot(111)
    ax.xaxis.set_major_formatter(mdates.DateFormatter('%b-%d\n%Y'))
    ax.tick_params(axis='x', which='major', labelsize=12)
    ax.tick_params(axis='y', which='major', labelsize=12)
    ax.bar(x, y, color='grey', align='center')
    ax.set_xlim([min_dt, max_dt])
    ax.set_ylim([0, max(y)])
    ax.set_xlabel('Date', fontsize=14)
    ax.set_ylabel('SIM-Days', fontsize=14)
    _add_drop_line(plt, ax, max_y)
    ## missing data bands (to be visually distinguished from 0 values)
    missing_dts = _get_missing_dts(min_dt, max_dt, data_dts=x)
    for missing_dt in missing_dts:
        _add_missing_band(ax, missing_dt)
    #plt.show()
    ## Create
    src_lbl = 'raw' if raw else 'consolidated'
    img_name = "SIM-days {}-{} {}.png".format(min_date, max_date, src_lbl)
    img_fpath = "reports/images/charts/{}".format(img_name)
    img_rel_path = "images/charts/{}".format(img_name)
    fig.savefig(img_fpath, bbox_inches='tight')
    plt.close(fig)
    return max_y, missing_dts, {'rel_path': img_rel_path}  ## even though not using multiple attributes useful to be able to e.g. so we can do something different with different charts at the HTML end of things

def create_nat_map(cur, cell_id_rtis):
    debug = False
    map_osm = folium.Map(location=[-45.0143975, 168.567724], zoom_start=10)
    cell_id_rtis_clause = ",".join(map(str, cell_id_rtis))
    sql = """\
    SELECT
    tower_lat,
    tower_lon,
      array_to_string(array_agg(cell_id_rti), ', ') AS
    rtis
    FROM (
      SELECT DISTINCT
      tower_lat,
      tower_lon,
      cell_id_rti
      FROM {redrock_history}
      ORDER BY tower_lat, tower_lon, cell_id_rti
    ) AS rr
    WHERE cell_id_rti IN ({cell_id_rtis_clause})
    GROUP BY tower_lat, tower_lon
    """.format(redrock_history=conf.REDROCK_HISTORY,
        cell_id_rtis_clause=cell_id_rtis_clause)
    if debug: print(sql)
    cur.execute(sql)
    for lat, lon, rtis in cur.fetchall():
        html_pointer = ('<div style="font-size: 20pt; '
        'color: {}">x</div>'.format('red'))
        marker_icon_pointer = folium.features.DivIcon(
            icon_size=(450, 50),
            icon_anchor=(0, 0),  ## x, y from top left corner.
            html=html_pointer, )
        folium.map.Marker(  ## https://github.com/python-visualization/folium/issues/340#issuecomment-179673692
            (lat, lon),
            icon=marker_icon_pointer
        ).add_to(map_osm)
        html_text = ('<div style="font-size: 10pt; '
        'color: {}">{}</div>'.format('red', rtis))
        marker_icon = folium.features.DivIcon(
            icon_size=(450, 50),
            icon_anchor=(-16, 0),  ## x, y from top left corner.
            html=html_text, )
        folium.map.Marker(  ## https://github.com/python-visualization/folium/issues/340#issuecomment-179673692
            (lat, lon),
            icon=marker_icon
        ).add_to(map_osm)
    fpath = "/home/gps/Documents/tourism/csm/qt_cell_id_rtis_mapped.html"
    map_url = "file://{}".format(fpath)
    map_osm.save(fpath)
    return map_url

def make_area_report(con, cur, raw=True):
    """
    raw -- live or die on what is actually in the redrock file for the datetime.
    No looking backwards at previous redrock data. When false, using
    sector_location_history.
    """
    (min_dt, max_dt, min_date,
     max_date) = utils.LocationSources.get_rr_history_dates(cur)
    imgs_dets = []
    (total_height, missing_dts,
     tot_img_dets) = chart_tot(cur, min_dt, max_dt, min_date, max_date, raw)
    imgs_dets.append(tot_img_dets)
    cell_id_rti_imgs_dets, cell_id_rtis = chart_cell_id_rtis(cur, min_dt,
        max_dt, missing_dts, total_height, raw)
    imgs_dets.extend(cell_id_rti_imgs_dets)
    more_styles = """\
    #content {
      width: 1200px;
    }
    #missing {
      background-color: #ECECEC;
      padding: 0;
      margin: 0;
    }
    #data {
      background-color: grey;
      padding: 0;
      margin: 0;
    }
    .link {
      font-size: 8px;
      padding: 0 6px 0 0;
      float: left;
    }
    a:link, a:visited, a:hover {
      color: grey;
    } 
    """
    html = [HTML_START_TPL % {
        'title': "Investigating the Queenstown Drop Nov/Dec 2016",
        'more_styles': more_styles}, ]
    html.append("""
    <table>
      <tbody>
      <tr>
        <td>Data from redrock files</td>
        <td id='data'>&nbsp;&nbsp;&nbsp;&nbsp;</td>
      </tr>
      <tr>
        <td>Missing redrock files</td>
        <td id='missing'>&nbsp;&nbsp;&nbsp;&nbsp;</td>
      </tr>
      </tbody>
    </table>
    """)
    links_html = []
    img_html = []
    for img_dets in imgs_dets:
        if 'cell_id_rti' in img_dets:
            links_html.append("<a href='#{client_id_rti}' "
                "class='link'>{client_id_rti}</a>".format(
                    client_id_rti=img_dets['cell_id_rti']))
            img_html.append("<a name='{}'>&nbsp;</a>"
                .format(img_dets['cell_id_rti']))
        img_html.append("<img width='1000px' padding=0 margin=0 src='{}'>"
            .format(img_dets['rel_path']))
    html.extend(links_html)
    html.extend(img_html)
    ## add map focused on QT towers inc shifts
    map_url = create_nat_map(cur, cell_id_rtis)
    html.append("<iframe src='{src}' width=1500px height=2500px></iframe>"
        .format(src=map_url))
    content = "\n".join(html)
    src_lbl = 'raw' if raw else 'consolidated'
    fpath = ("/home/gps/Documents/tourism/csm/reports/queenstown_drop_{}.html"
        .format(src_lbl))
    with open(fpath, 'w') as f:
        f.write(content)
    open_new_tab("file://{}".format(fpath))

def explain_drop(cur):
    """
    Link the call data for a specific date (we already have 2017-01-20
    available) to a stale CSM (from CSM history e.g. Oct 31 2016) and a fresh
    CSM (2017-01-21). How many SIMs do we lose? And what changes in
    cell_id_rti's are involved.
    """
    sql_stale_cell_call_dets = """\
    SELECT DISTINCT cell_id_rti, n_sims /* DISTINCT should be redundant because cell_id_rti unique on each source */
    FROM {calls_2017_01_20} AS calls
    INNER JOIN
    {csm} AS locs
    USING(cell_id_rti)
    WHERE cob_date = '2016-10-31'
    """.format(calls_2017_01_20=LOCAL_RTIS_2017_01_20, csm=conf.CSM_HISTORY)
    sql_fresh_cell_call_dets = """\
    SELECT DISTINCT cell_id_rti, n_sims
    FROM {calls_2017_01_20} AS calls
    INNER JOIN
    {csm} AS locs
    USING(cell_id_rti)
    WHERE cob_date = '2017-01-20'
    """.format(calls_2017_01_20=LOCAL_RTIS_2017_01_20, csm=conf.CSM_HISTORY)
    ## GAINS
    ## Note -- by virtue of how the CSM is made it never loses cell_id_rti's even if the source redrock files drop them
    ## so we will never gain anything by using stale if looking at CSM (as opposed to Redrock files)
    sql_gains_when_stale = """\
    SELECT stale.*, fresh.*
    FROM ({stale}) AS stale
    FULL JOIN
    ({fresh}) AS fresh
    USING(cell_id_rti)
    WHERE fresh.cell_id_rti IS NULL  /* i.e. only appears in stale */
    """.format(stale=sql_stale_cell_call_dets, fresh=sql_fresh_cell_call_dets)
    cur.execute(sql_gains_when_stale)
    print("cell_id_rti's gained when using stale CSM: {:,}".format(
        len(cur.fetchall())))
    ## LOSSES
    sql_losses_when_stale = """\
    SELECT stale.*, fresh.*
    FROM ({stale}) AS stale
    FULL JOIN
    ({fresh}) AS fresh
    USING(cell_id_rti)
    WHERE stale.cell_id_rti IS NULL  /* i.e. only appears in fresh */
    ORDER BY fresh.n_sims DESC
    """.format(stale=sql_stale_cell_call_dets, fresh=sql_fresh_cell_call_dets)
    cur.execute(sql_losses_when_stale)
    losses_when_stale = cur.fetchall()
    n_lost_cell_id_rtis = len(losses_when_stale)
    n_sims_data = [row['n_sims'] for row in losses_when_stale]
    median_sims_lost = median(n_sims_data)
    mean_sims_lost = int(round(mean(n_sims_data), 0))
    tot_sims_lost = sum(n_sims_data)
    extreme_example = losses_when_stale[0]
    print("No. lost cell_id_rti's when using stale CSM: {:,}"
        .format(n_lost_cell_id_rtis))
    print("Median SIMs lost per cell_id_rti: {:,}".format(median_sims_lost))
    print("Mean SIMs lost per cell_id_rti: {:,}".format(mean_sims_lost))
    print("Upper limit of SIMS lost (SIMs can be picked up by multiple "
        "cell_id_rti's over time): {:,}".format(tot_sims_lost))
    print("Extreme lost cell_id_rti: {} ({:,} SIMs)".format(
        extreme_example['cell_id_rti'], extreme_example['n_sims']))

def create_relocation_map(coord_1, coord_2, centroid_coord):
    """
    Plot line on map joining two coordinates.
    """
    map_osm = folium.Map(location=list(centroid_coord), zoom_start=14)
    points = [coord_1, coord_2]
    folium.PolyLine(points).add_to(map_osm)
    raw = bytes("{},{},{},{}".format(*(coord_1+coord_2)), 'utf-8')
    print(raw)
    label = str(base64.b64encode(raw), 'utf-8')
    print(label)
    fpath = "/home/gps/Documents/tourism/csm/{}.html".format(label)
    map_url = "file://{}".format(fpath)
    map_osm.save(fpath)
    return map_url

def _relocated_towers(cur):
    """
    Any towers changed lat/lon more than 100m? Sometimes fewer decimal points is
    enough to shift a tower that much.
    """
    debug = False
    html = []
    sql_moved = """\
    SELECT *,
      ST_Y(centroid)
    centroid_lat,
      ST_X(centroid)
    centroid_lon
    FROM (
      SELECT
      *,
        ST_DISTANCE(
          ST_SetSRID(ST_POINT(prev_lon, prev_lat), 4326),
          ST_SetSRID(ST_POINT(latest_lon, latest_lat), 4326)
        )*{dist2m_scalar} AS
      gap,
        ST_Centroid(
          ST_UNION(
            ST_SetSRID(ST_POINT(prev_lon, prev_lat), 4326),
            ST_SetSRID(ST_POINT(latest_lon, latest_lat), 4326)
          )
        ) AS
      centroid  /* so we can set focus for map display of gap */
      FROM (
        SELECT
        cell_id_rti,
          prev.tower_lat AS
        prev_lat,
          prev.tower_lon AS
        prev_lon,
          latest.tower_lat AS
        latest_lat,
          latest.tower_lon AS
        latest_lon
        FROM {redrock_latest} AS latest
        INNER JOIN  /* if not in prev not a problem so IJ OK */
        (
          SELECT *
          FROM {sector_location_history}
          WHERE as_at_datetime = (
            SELECT MAX(as_at_datetime) FROM {sector_location_history}
            )
        ) AS prev
        USING (cell_id_rti)
      ) AS calc_gaps
    ) AS gaps
    WHERE gap > 100
    ORDER BY gap DESC
    """.format(redrock_latest=REDROCK_LATEST,
        sector_location_history=conf.SECTOR_LOCATION_HISTORY,
        dist2m_scalar=DIST2M_SCALAR)
    cur.execute(sql_moved)
    data = cur.fetchall()
    for row in data:
        if debug: print(dict(row))
        html.append("<h2>{cell_id_rti} shifted {gap:,}m</h2>"
            .format(cell_id_rti=row['cell_id_rti'], gap=round(row['gap'])))
        map_url = create_relocation_map(
            coord_1=(row['prev_lat'], row['prev_lon']),
            coord_2=(row['latest_lat'], row['latest_lon']),
            centroid_coord=(row['centroid_lat'], row['centroid_lon']))
        html.append("<iframe src='{src}' width=600px height=300px></iframe>"
            .format(src=map_url))
    return html

def make_error_report(cur):
    """
    Report errors and warnings for latest Redrock file compared with history and
    itself.
    
    Not yet looking for lost call data - will require a call to greenplum namely
    lbs_agg.fact_rti_summary.

    Made fake test data starting from:
    SELECT 
    cell_id_rti,
    tower_lat,
    tower_lon
    INTO redrock_latest
    FROM sector_location_history
    WHERE as_at_datetime = (
      SELECT MAX(as_at_datetime) FROM sector_location_history
    )
    ORDER BY cell_id_rti;

    UPDATE redrock_latest
    SET tower_lat = -37.156861, tower_lon = 174.981304
    WHERE cell_id_rti = 20171;
    """
    more_styles = """\
    """
    html = [HTML_START_TPL % {
        'title': "Data Quality Report on Latest Redrocks Inputs",
        'more_styles': more_styles}, ]
    html.extend(_relocated_towers(cur))
    content = "\n".join(html)
    fpath = ("/home/gps/Documents/tourism/csm/reports/"
        "redrocks_data_quality_report.html")
    with open(fpath, 'w') as f:
        f.write(content)
    open_new_tab("file://{}".format(fpath))

def main():
    con, cur, cur2 = utils.Pg.get_local()
    #utils.LocationSources.make_redrock_history(con, cur,
    #    extract_from_trucall_zips=True)  ## takes a long time to run again and will need to get a fresh (larger) copy of qt_call_data to match
    #make_qt_connection_data_tbl(con, cur)
    #utils.LocationSources.make_sector_locations_history(con, cur, cur2)
    #explain_drop(cur)
    #make_area_report(con, cur, raw=True)
    make_area_report(con, cur, raw=False)
    #make_error_report(cur)

main()

