#! /usr/bin/env python3

import sys
sys.path.insert(0, '../voyager_shared')
#import conf #@UnresolvedImport
import utils #@UnresolvedImport

def main():
    con_local, cur_local, unused_cur_local2 = utils.Pg.get_local()
    utils.LocationSources.make_redrock_time_linked_data(con_local, cur_local,
        make_fresh_src=False, extract_from_zips=True)

if __name__ == '__main__':
    main()
