# README #

### What is this repository for? ###

Help identify issues with CSM creation process. Look for anomalies.

### How do I get set up? ###

Run the_drop.py in Python 3 having commented in or out various lines in main().

Runs off lbs_agg.fact_rti_summary and misc undocumented files. Run get_filtered_data_sql() to get SQL to build grantps_test.qt_call_data. Can be brought across to local database month by month and consolidated into qt_call_data.

Also need to make cell_id_rtis_2017_01_20 from lbs_agg.fact_rti_summary with a query like:

SELECT cell_id_rti, COUNT(*) AS n_sims
FROM lbs_agg.fact_rti_summary
WHERE date = '2017-01-20'
GROUP BY cell_id_rti
ORDER BY cell_id_rti;

### Who do I talk to? ###

Grant Paton-Simpson (Grant.PatonSimpson@qrious.co.nz)
