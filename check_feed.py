import csv
import datetime
from datetime import timedelta
from itertools import count
from pprint import pprint as pp

import requests

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import conf, dates, db, find_relocations as findrel, utils

import not4git #@UnresolvedImport

GOOGLE_API_KEY = not4git.google_api_key
GOOGLE_API_URL = ("https://www.googleapis.com/geolocation/v1/geolocate?key="
    + GOOGLE_API_KEY)

def get_location(config):
    """
    config -- mcc, mnc, tech_class (lte, gsm, cdma, or wcdma but should only use
      LTE because we won't find a match on 3G 2**16xRNC + cell_id),
      carrier_name (Spark), rti
    https://developers.google.com/maps/documentation/geolocation/intro
    """
    debug = True
    data = {
        "homeMobileCountryCode": config['mcc'],
        "homeMobileNetworkCode": config['mnc'],
        "radioType": config['tech_class'],  ## Supported values are lte, gsm, cdma, and wcdma
        "carrier": config['carrier_name'],  ## e.g. Vodafone
        "cellTowers": [
            {
                'cellId': config['rti'],
                'locationAreaCode': 1,  ## The Location Area Code (LAC) for GSM and WCDMA networks. The Network ID (NID) for CDMA networks. 2256 for 23333?
                'mobileCountryCode': config['mcc'],
                'mobileNetworkCode': config['mnc'],
                'age': 0,  ## 0 = current
            }],
        "considerIp": 'false',  ## Specifies whether to fall back to IP geolocation if wifi and cell tower signals are not available. Note that the IP address in the request header may not be the IP of the device. Defaults to true. Set considerIp to false to disable fall back. 
    }
    resp = requests.post(GOOGLE_API_URL, json=data)
    data = resp.json()
    if "error" not in data:
        lat = data["location"]["lat"]
        lon = data["location"]["lng"]
        cell_id = config["rti"]
        if debug: print("{}, {}, {}".format(cell_id, lat, lon))
    else:
        error = resp.json()["error"]["errors"][0]["reason"]
        if "dailyLimitExceeded" in error:
            raise Exception("API limit exceeded - slack!")
        if debug: print("Error: {}".format(error))
        lat, lon = None, None
    return lat, lon

def rti_dets_from_csv(fpath):
    """
    0       1    2           3      4      5      6  7   8    9                             10    11    12  13
    256286 | 30 | WPIEA1_L | 1850 | 1001 | WPIEA |  |  | HL | Petone Integration and Eval | -10 | 100 | 1 | 15/08/2017 09:33:08
    rti      cell_id                node_id

    Note – the feed has duplicate records by design (Daniel's design at
    least ;-))**. If there are differences in lat/lon/name we get both records.
    We should only use the first when sorted by lat, lon, and name.

    ** Apparently so we can investigate issues when there is a dispute about
    lat/lons for a cell_id_rti. Otherwise the choice is made out-of-sight by
    Redrock.
    """
    debug = False
    rti_dets = []
    IDX_RTI = 0
    IDX_LAT = 10
    IDX_LON = 11
    IDX_DESC = 9
    IDX_TECH_CLASS = 8
    IDX_TOWER = 5
    with open(fpath, encoding='utf-8', errors='ignore') as csvfile:
        reader = csv.reader(csvfile)
        for n, row in enumerate(reader, 1):
            if debug: print(row)
            rti = row[IDX_RTI]
            tech_class = ('4G' if row[IDX_TECH_CLASS].lower().endswith('l')
                else '3G')
            rti_dets.append(
                (rti, row[IDX_LAT], row[IDX_LON],
                 row[IDX_DESC], tech_class, row[IDX_TOWER]))
            if n % 1000 == 0:
                print(utils.prog(n, "RTI's"))
    #c = Counter(rtis)
    #print(c.most_common(4))
    return sorted(list(set(rti_dets)))

def make_feed_rti_tbl(rti_dets, local=False):
    """
    256286 | 30 | WPIEA1_L | 1850 | 1001 | WPIEA | HL | Petone Integration and Eval | -10 | 100 | 1 | 15/08/2017 09:33:08
    """
    debug = False
    CHUNKS = 1000
    if local:
        con, cur, unused_cur_local2 = db.Pg.get_local()
        tblname = conf.FEED_RTIS_LOCAL
    else:
        con, cur = db.Pg.get_rem(user='gpadmin')
        tblname = conf.FEED_RTIS
    db.Pg.drop_tbl(con, cur, tbl=tblname)
    sql_make_tbl = """\
    CREATE TABLE {feed_rtis} (
      cell_id_rti bigint,
      tower_lat double precision,
      tower_lon double precision,
      description text,
      tech_class text,
      tower text
    )
    """.format(feed_rtis=tblname)
    cur.execute(sql_make_tbl)
    con.commit()
    if not local:
        db.Pg.grant_public_read_permissions(tblname)
    sql_insert_tpl = """\
    INSERT INTO {feed_rtis}
    (cell_id_rti, tower_lat, tower_lon, description, tech_class, tower)
    VALUES
    """.format(feed_rtis=tblname)
    for i in count():
        print("Slice {:,}".format(i+1))
        start_idx = i*CHUNKS
        end_idx = (i+1)*CHUNKS
        rti_dets2use = rti_dets[start_idx: end_idx]
        if not rti_dets2use:
            break
        n_rti_dets2use = len(rti_dets2use)
        sql_insert = (sql_insert_tpl
            + ',\n'.join(["(%s, %s, %s, %s, %s, %s)"]*n_rti_dets2use))
        dets = []
        for row in rti_dets2use:
            row = list(row)
            for idx in (1, 2):
                if row[idx] == '': row[idx] = None
            dets.extend(row)
        if debug: print(sql_insert, dets)
        cur.execute(sql_insert, dets)
        con.commit()
    print("Finished making '{}' table".format(tblname))

def make_feed_tbl(fname, local=False):
    fpath = os.path.join(conf.CSM_ROOT, 'storage', fname)
    rti_dets = rti_dets_from_csv(fpath=fpath)
    make_feed_rti_tbl(rti_dets, local)

def _rti_dets_where_seed_vs_feed(cur_local, min_distance):
    sql = """\
    SELECT * FROM (
      SELECT
        cell_id_rti,
          ROUND((ST_DISTANCE(
            ST_GeomFromText('POINT(' || mod_p.tower_lon || ' ' ||
                mod_p.tower_lat || ')', {srid}),
            ST_GeomFromText('POINT(' || feed.tower_lon || ' ' ||
                feed.tower_lat || ')', {srid})
          )*{scalar})::numeric, 2)::float AS
        dist,
          ROUND(mod_p.tower_lat::numeric, 4) AS
        mod_p_lat,
          ROUND(mod_p.tower_lon::numeric, 4) AS
        mod_p_lon,
          ROUND(feed.tower_lat::numeric, 4) AS
        feed_lat,
          ROUND(feed.tower_lon::numeric, 4) AS
        feed_lon,
          mod_p.description AS
        mod_p_desc,
          feed.description AS
        feed_desc,
        tech_class
      FROM (
        SELECT
          cell_id_rti,
          tower_lat,
          tower_lon,
          description
        FROM {mod_p}
        WHERE effective_end IS NULL
      ) AS mod_p
      INNER JOIN
      (
        SELECT DISTINCT ON (cell_id_rti)  -- leave out duplicates
          *
        FROM {feed}
        ORDER BY cell_id_rti, tower_lat, tower_lon, description
      ) AS feed
      USING(cell_id_rti)
    ) AS src
    WHERE dist > %s
    ORDER BY cell_id_rti
    """.format(srid=conf.WGS84_SRID, scalar=conf.ST_DISTANCE2KM_SCALAR,
        mod_p=conf.MOD_P_LOCATIONS, feed=conf.FEED_RTIS_LOCAL)
    cur_local.execute(sql, (min_distance, ))
    return cur_local.fetchall()

def feed_distant_from_seed_coord(cur_local):
    """
    Compare all cell_id_rti's 
    """
    data = _rti_dets_where_seed_vs_feed(cur_local, min_distance=3)
    print("{:>13} {:>17} {:>10} {:>10} {:>10} {:>10}  {:<30}  {:<30}".format(
        "cell_id_rti", "Distance apart", "Seed Lat", "Seed Lon", "Feed Lat",
        "Feed Lon", "Seed Desc", "Feed Desc"))
    for row in data:
        (rti, dist, seed_lat, seed_lon, feed_lat, feed_lon, seed_desc,
         feed_desc) = row
        print("{:>13} {:>14} km {:>10} {:>10} {:>10} {:>10}  {:<30}  {:<30}"
            .format(rti, "{:,}".format(dist), seed_lat, seed_lon,
            feed_lat, feed_lon, seed_desc, feed_desc))

def feed_out_of_nz_bb(cur_local):
    """
    Could have done something aggregated to handle all rti's in one go (much
    faster etc) but given small number I wanted to eyeball all the results in
    case it revealed something (it did - some had 1 day of 300 and all 1-5s
    otherwise. A couple had only a couple of days and these were 200-300. 439151
    is the only one which is not a dribbler and it only has a couple of days so
    far BUT it could be important BUT its IMSI's don't connect to enough to
    report on so we can ignore them I think.

    [[439151, '2016-11-20', Decimal('37')],
     [439151, '2017-08-30', Decimal('262')],
     [439151, '2017-08-31', Decimal('222')]]
    """
    check_all = False
    check_indiv = True
    if check_all:
        rtis2check = """\
        256286,30,WPIEA1_L,1850,1001,WPIEA,,,HL,Petone Integration and Eval,-10,100,1,10/08/2017 09:28:07,3434822991723102208,24991626
        256542,30,WPIEB1_L,1850,1002,WPIEB,,,HL,Petone Integration and Eval 1 SRAN,-10,100,1,10/08/2017 09:28:07,3434822991723102208,24991626
        439151,111,WTKHA1_J,38171,1715,WTKHA,,,HL,Tinakori Hill Disaster Recovery Site,-30,120,1,10/08/2017 09:28:07,3149277760405372928,22914011
        439153,113,WTKHA1_L,38171,1715,WTKHA,,,HL,Tinakori Hill Disaster Recovery Site,-30,120,1,10/08/2017 09:28:07,3149277760405372928,22914011
        439155,115,WTKHA1_N,38171,1715,WTKHA,,,HL,Tinakori Hill Disaster Recovery Site,-30,120,1,10/08/2017 09:28:07,3149277760405372928,22914011
        439157,117,WTKHA1_Q,38171,1715,WTKHA,,,HL,Tinakori Hill Disaster Recovery Site,-30,120,1,10/08/2017 09:28:07,3149277760405372928,22914011
        439158,118,WTKHA1_R,38171,1715,WTKHA,,,HL,Tinakori Hill Disaster Recovery Site,-30,120,1,10/08/2017 09:28:07,3149277760405372928,22914011
        439159,119,WTKHA1_S,38171,1715,WTKHA,,,HL,Tinakori Hill Disaster Recovery Site,-30,120,1,10/08/2017 09:28:07,3149277760405372928,22914011
        29001,29001,WPIEA1_1,38301,1001,WPIEA,38,WN-PRNC01,HU,Petone Integration and Eval,-10,100,1,10/08/2017 09:27:18,3434822991723102208,24991626
        29002,29002,WPIEA1_2,38301,1001,WPIEA,38,WN-PRNC01,HU,Petone Integration and Eval,-10,100,1,10/08/2017 09:27:18,3434822991723102208,24991626
        29004,29004,WPIEA1_4,38301,1001,WPIEA,38,WN-PRNC01,HU,Petone Integration and Eval,-10,100,1,10/08/2017 09:27:18,3434822991723102208,24991626
        29005,29005,WPIEA1_5,38301,1001,WPIEA,38,WN-PRNC01,HU,Petone Integration and Eval,-10,100,1,10/08/2017 09:27:18,3434822991723102208,24991626
        29011,29011,WPIEB1_1,28301,1002,WPIEB,28,CH-PRNC01,HU,Petone Integration and Eval 1 SRAN,-10,100,1,10/08/2017 09:27:18,3434822991723102208,24991626
        29012,29012,WPIEB1_2,28301,1002,WPIEB,28,CH-PRNC01,HU,Petone Integration and Eval 1 SRAN,-10,100,1,10/08/2017 09:27:18,3434822991723102208,24991626
        29014,29014,WPIEB1_4,28301,1002,WPIEB,28,CH-PRNC01,HU,Petone Integration and Eval 1 SRAN,-10,100,1,10/08/2017 09:27:18,3434822991723102208,24991626
        29015,29015,WPIEB1_5,28301,1002,WPIEB,28,CH-PRNC01,HU,Petone Integration and Eval 1 SRAN,-10,100,1,10/08/2017 09:27:18,3434822991723102208,24991626
        47141,47141,WTKHA1_1,48392,1715,WTKHA,48,HN-PRNC01,HU,Tinakori Hill Disaster Recovery Site,-30,120,1,10/08/2017 09:27:18,3149277760405372928,22914011
        47144,47144,WTKHA1_2,48392,1715,WTKHA,48,HN-PRNC01,HU,Tinakori Hill Disaster Recovery Site,-30,120,1,10/08/2017 09:27:18,3149277760405372928,22914011
        47147,47147,WTKHA1_3,48392,1715,WTKHA,48,HN-PRNC01,HU,Tinakori Hill Disaster Recovery Site,-30,120,1,10/08/2017 09:27:18,3149277760405372928,22914011
        10031,10031,TCE3U1_1,140,,TCE3U,11,RNC1,AU,TCE3U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        10032,10032,TCE3U2_1,140,,TCE3U,11,RNC1,AU,TCE3U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        10033,10033,TCE3U3_1,140,,TCE3U,11,RNC1,AU,TCE3U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        10034,10034,TCE3U1_2,140,,TCE3U,11,RNC1,AU,TCE3U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        10035,10035,TCE3U2_2,140,,TCE3U,11,RNC1,AU,TCE3U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        10036,10036,TCE3U3_2,140,,TCE3U,11,RNC1,AU,TCE3U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        10037,10037,TCE3U1_4,140,,TCE3U,11,RNC1,AU,TCE3U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        10038,10038,TCE3U2_4,140,,TCE3U,11,RNC1,AU,TCE3U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        10039,10039,TCE3U3_4,140,,TCE3U,11,RNC1,AU,TCE3U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        10061,10061,TCE1U1_1,110,,TCE1U,11,RNC1,AU,,,,1,26/11/2013 14:24:53,,
        10062,10062,TCE1U2_1,110,,TCE1U,11,RNC1,AU,,,,1,26/11/2013 14:24:53,,
        10063,10063,TCE1U3_1,110,,TCE1U,11,RNC1,AU,,,,1,26/11/2013 14:24:53,,
        10064,10064,TCE1U1_2,110,,TCE1U,11,RNC1,AU,,,,1,26/11/2013 14:24:53,,
        10065,10065,TCE1U2_2,110,,TCE1U,11,RNC1,AU,,,,1,26/11/2013 14:24:53,,
        10066,10066,TCE1U3_2,110,,TCE1U,11,RNC1,AU,,,,1,26/11/2013 14:24:53,,
        10067,10067,TCE1U1_3,110,,TCE1U,11,RNC1,AU,,,,1,26/11/2013 14:24:53,,
        10068,10068,TCE1U2_3,110,,TCE1U,11,RNC1,AU,,,,1,26/11/2013 14:24:53,,
        10069,10069,TCE1U3_3,110,,TCE1U,11,RNC1,AU,,,,1,26/11/2013 14:24:53,,
        10071,10071,TCE1U1_4,110,,TCE1U,11,RNC1,AU,,,,1,26/11/2013 14:24:53,,
        10072,10072,TCE1U2_4,110,,TCE1U,11,RNC1,AU,,,,1,26/11/2013 14:24:53,,
        10073,10073,TCE1U3_4,110,,TCE1U,11,RNC1,AU,,,,1,26/11/2013 14:24:53,,
        20061,20061,TCE7U1_1,110,,TCE7U,11,RNC1,AU,TCE4U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        20062,20062,TCE7U2_1,110,,TCE7U,11,RNC1,AU,TCE4U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        20063,20063,TCE7U3_1,110,,TCE7U,11,RNC1,AU,TCE4U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        20064,20064,TCE7U1_2,110,,TCE7U,11,RNC1,AU,TCE4U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        20065,20065,TCE7U2_2,110,,TCE7U,11,RNC1,AU,TCE4U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        20066,20066,TCE7U3_2,110,,TCE7U,11,RNC1,AU,TCE4U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        21101,21101,TCE4U1_1,110,,TCE4U,11,RNC1,AU,TCE4U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        21102,21102,TCE4U2_1,110,,TCE4U,11,RNC1,AU,TCE4U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        21103,21103,TCE4U3_1,110,,TCE4U,11,RNC1,AU,TCE4U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        21104,21104,TCE4U1_4,110,,TCE4U,11,RNC1,AU,TCE4U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        21105,21105,TCE4U2_4,110,,TCE4U,11,RNC1,AU,TCE4U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        21106,21106,TCE4U3_4,110,,TCE4U,11,RNC1,AU,TCE4U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        21107,21107,TCE4U1_2,110,,TCE4U,11,RNC1,AU,TCE4U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        21108,21108,TCE4U2_2,110,,TCE4U,11,RNC1,AU,TCE4U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        21109,21109,TCE4U3_2,110,,TCE4U,11,RNC1,AU,TCE4U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        41201,41201,TCE6U1_1,240,,TCE6U,12,RNC2,AU,TCE6U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        41202,41202,TCE6U2_1,240,,TCE6U,12,RNC2,AU,TCE6U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        41203,41203,TCE6U3_1,240,,TCE6U,12,RNC2,AU,TCE6U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        41204,41204,TCE6U1_4,240,,TCE6U,12,RNC2,AU,TCE6U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        41205,41205,TCE6U2_4,240,,TCE6U,12,RNC2,AU,TCE6U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        41206,41206,TCE6U3_4,240,,TCE6U,12,RNC2,AU,TCE6U,0,0,1,26/11/2013 14:24:53,1152921573326323712,8388608
        """
        all_rtis = []
        for line in rtis2check.split('\n'):
            rti = line.split(',')[0]
            if rti:
                all_rtis.append(int(rti))
        all_rtis.sort()
        print(len(all_rtis), all_rtis)
        rtis_clause = db.Pg.nums2clause(all_rtis)
        sql = """\
        SELECT *
        FROM {mod_p_locations}
        WHERE cell_id_rti IN {rtis_clause}
        AND effective_end IS NULL
        AND tower_lat IS NOT NULL AND tower_lon IS NOT NULL
        ORDER BY cell_id_rti, effective_start
        """.format(mod_p_locations=conf.MOD_P_LOCATIONS,
            rtis_clause=rtis_clause)
        cur_local.execute(sql)
        data = cur_local.fetchall()
        print(len(data))
        for row in data:
            print(row)
        for rti in all_rtis:
            findrel.FindRelocations.get_daily_tots(rti)
            input("Continue?")
    if check_indiv:
        (start_datetime_str,
         end_datetime_str) = dates.Dates.date_str_to_datetime_str_range(
             '2017-08-31')
        rti = 439151
        rtis_to_exclude = [439153, 439155, 439157, 439158, 439159]
        findrel.FindRelocations.get_likely_location_dts(cur_local,
            start_datetime_str, end_datetime_str, rti, rtis_to_exclude)

def feed_cf_google_cell_id(cur_local, min_rti=None):
    """
    Feed VS GOOGLE CELL ID - look up coords where possible based on cell_id_rti
    - contrast with those in the Feed. Can only check 4G because Google uses
    3G values "namespaced" by RNC so we won't find matches. [unless we know what
    the RNC number is for NZ, or are there multiple?]
    """
    sql = """\
    SELECT cell_id_rti, tower_lat, tower_lon
    FROM {feed}
    ORDER BY cell_id_rti
    """.format(feed=conf.FEED_RTIS_LOCAL)
    cur_local.execute(sql)
    data = cur_local.fetchall()
    fname = "google_vs_feed_known_false_feed_coords_{}.txt".format(  ## we can't test all of them so this is the minimum number of false ones
        datetime.datetime.today().strftime('%Y%m%d%H%M%S'))
    fpath = os.path.join(conf.CSM_ROOT, fname)
    with open(fpath, 'w') as f:
        n = 1
        for rti, feed_lat, feed_lon in data:
            rti = int(rti)
            if min_rti and rti < min_rti:
                continue
            print("{} - about to search for in Google API".format(rti))
            config = {
                'mcc': 530,
                'mnc': 5,
                'tech_class': 'lte',
                'carrier_name': 'Spark',
                'rti': rti,
            }
            try:
                api_lat, api_lon = get_location(config)
            except Exception as e:
                utils.warn("Have to stop :-( ({})".format(e))
                break
            if feed_lat and feed_lon and api_lat and api_lon:  ## None or 0 are both ignored which is a feature not a bug ;-)
                feed_coord = (feed_lat, feed_lon)
                api_coord = (api_lat, api_lon)
                dist_km = db.Pg.gap_km(cur_local, coord_a=feed_coord,
                    coord_b=api_coord, srid=conf.WGS84_SRID)
                if dist_km > 3:
                    f.write("\n{}".format(
                        {'rti': rti,
                         'dist_km': dist_km,
                        'feed_coord': feed_coord,
                        'api_coord': api_coord
                    }))
                    ## https://stackoverflow.com/questions/7127075/what-exactly-the-pythons-file-flush-is-doing
                    f.flush()
                    os.fsync(f)
            print(utils.prog(n, 'google api searches'))
            n += 1

def _rtis_within_regions(cur_local, rtis2filter):
    rtis_clause = db.Pg.nums2clause(rtis2filter)
    sql = """\
    SELECT cell_id_rti
    FROM (
      SELECT
        cell_id_rti,
        tower_lat,
        tower_lon,
          MAX(contained::int) = 1 AS
        in_region
      FROM (
        SELECT
          cell_id_rti,
          tower_lat,
          tower_lon,
            ST_CONTAINS(
            region_geom,
            ST_GeomFromText(
              'POINT(' || tower_lon || ' ' || tower_lat || ')', {srid}
            )) AS
          contained
        FROM (
          SELECT *
          FROM {feed}
          WHERE cell_id_rti IN {rtis_clause}
        ) AS feed
        CROSS JOIN
        (
          SELECT geom AS
            region_geom
          FROM {local_region}
        ) AS region_geoms
      ) AS src
      GROUP BY
        cell_id_rti,
        tower_lat,
        tower_lon
      ) AS src2
    WHERE in_region
    ORDER BY cell_id_rti
    """.format(srid=conf.WGS84_SRID, feed=conf.FEED_RTIS_LOCAL,
        local_region='region', rtis_clause=rtis_clause)
    cur_local.execute(sql)
    rtis_within_regions = [row['cell_id_rti'] for row in cur_local.fetchall()]
    return rtis_within_regions

def _in_water(cur_local):
    """
    In NZ but not in any region = in water. Assumes we have filtered by NZ
    bounding box already. But, either way, anything found is not on NZ land.
    """
    debug = False
    sql = """\
    SELECT *
    FROM (
      SELECT
        cell_id_rti,
        tower_lat,
        tower_lon,
          MAX(contained::int) = 0 AS
        in_water
      FROM (
        SELECT
          cell_id_rti,
          tower_lat,
          tower_lon,
            ST_CONTAINS(
            region_geom,
            ST_GeomFromText(
              'POINT(' || tower_lon || ' ' || tower_lat || ')', {srid}
            )) AS
          contained
        FROM {feed}
        CROSS JOIN
        (
          SELECT geom AS
            region_geom
          FROM {local_region}
        ) AS region_geoms
      ) AS src
      GROUP BY
        cell_id_rti,
        tower_lat,
        tower_lon
      ) AS src2
    WHERE in_water
    ORDER BY tower_lat, tower_lon, cell_id_rti
    """.format(srid=conf.WGS84_SRID, feed=conf.FEED_RTIS_LOCAL,
        local_region='region')
    cur_local.execute(sql)
    data = cur_local.fetchall()
    if debug: pp(data)
    return data

def check_in_nz_regions(cur_local):
    """
    Check feed coords in NZ REGION SHAPES
    """
    water_dets = _in_water(cur_local)
    pp(water_dets)
    if water_dets:
        utils.warn("Water dets found - investigate")

def google_and_likely_location_map(cur_local, rti,
        seed_lat, seed_lon, seed_desc,
        feed_lat, feed_lon, feed_desc,
        use_google_api):
    ## Google API    
    if use_google_api:
        config = {
            'mcc': 530,
            'mnc': 5,
            'tech_class': 'lte',
            'carrier_name': 'Spark',
            'rti': rti,
        }
        try:
            api_lat, api_lon = get_location(config)
        except Exception as e:
            utils.warn("Have to stop :-( ({})\nStart again on {}"
                .format(e, rti))
            raise
        if feed_lat and feed_lon and api_lat and api_lon:  ## None or 0 are both ignored which is a feature not a bug ;-)
            feed_coord = (feed_lat, feed_lon)
            api_coord = (api_lat, api_lon)
            dist_km = db.Pg.gap_km(cur_local, coord_a=feed_coord,
                coord_b=api_coord, srid=conf.WGS84_SRID)
            if dist_km > 3:
                print("FEED coords are off by {:,} kms\nFEED ({}, {})"
                    "\nGoogle ({}, {})".format(dist_km, feed_lat, feed_lon,
                    api_lat, api_lon))
    ## Likely location map
    yesterday = ((datetime.datetime.today() - timedelta(days=1))
        .strftime('%Y-%m-%d'))
    (start_datetime_str,
     end_datetime_str) = dates.Dates.date_str_to_datetime_str_range(yesterday)
    findrel.FindRelocations.get_likely_location_dts(cur_local,
        start_datetime_str, end_datetime_str, rti)
    print("Map closer to SEED ({}, {}) {} or FEED ({}, {}) {}?".format(
        seed_lat, seed_lon, seed_desc, feed_lat, feed_lon, feed_desc))

def feed_seed_google_and_imsi_linked(cur_local, start_rti=None):
    """
    For each rti that is in both SEED and FEED, where there is more than a 3km
    difference, and where both are in an NZ region, look up in google (if 4G),
    and create Likely Location map. Trying to estimate how many coordinates in
    the FEED are actually significantly incorrect. How wrong could our data be
    if we accept FEED coordinates at face value without corrections?

    Being more than 3km from SEED coord is strike 1. And given we have found one
    strike we should see if it is confirmed - whether Google and the map supply
    more strikes.
    """
    debug = True
    rti_dets_where_seed_vs_feed = _rti_dets_where_seed_vs_feed(cur_local,
        min_distance=3)
    rtis_where_seed_vs_feed = [row['cell_id_rti']
        for row in rti_dets_where_seed_vs_feed]
    rtis_within_regions = _rtis_within_regions(cur_local,
        rtis2filter=rtis_where_seed_vs_feed)
    rti_dets_to_check = [row for row in rti_dets_where_seed_vs_feed
        if row['cell_id_rti'] in rtis_within_regions]
    if debug: print(len(rti_dets_to_check))
    skip = (start_rti is not None)
    if debug:
        for row in rti_dets_to_check:
            print(row)
    for row in rti_dets_to_check:
        rti = row['cell_id_rti']
        use_google_api = (row['tech_class'] == '4G')
        seed_lat, seed_lon = row['mod_p_lat'], row['mod_p_lon']
        feed_lat, feed_lon = row['feed_lat'], row['feed_lon']
        seed_desc, feed_desc = row['mod_p_desc'], row['feed_desc']
        if start_rti is not None and rti == start_rti:
            skip = False
        if skip:
            continue
        resp = (input("About to process {} - continue? (y/n): ".format(rti))
            .lower())
        if resp != 'y':
            print("Start again with {}".format(rti))
            break
        google_and_likely_location_map(cur_local, rti, seed_lat, seed_lon,
            seed_desc, feed_lat, feed_lon, feed_desc, use_google_api)

def main():
    make_feed_tbl(fname='lbs_cell_location_20170815093309.csv', local=True)
    unused_con_local, cur_local, unused_cur_local2 = db.Pg.get_local()
    #feed_distant_from_seed_coord(cur_local)
    #feed_out_of_nz_bb(cur_local)
    #check_in_nz_regions(cur_local)
    #feed_cf_google_cell_id(cur_local, min_rti=266873)  ## 4G kicks in > 60000 according to mod_p_locations
    #feed_seed_google_and_imsi_linked(cur_local, start_rti=13133087)

if __name__ == '__main__':
    main()
