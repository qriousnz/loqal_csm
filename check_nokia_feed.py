#! /usr/bin/env python3

import csv
from pprint import pprint as pp
from textwrap import wrap

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import (conf, dates, db, find_locations as findloc, spatial,
    utils)

csv.register_dialect('piper', delimiter='|', quoting=csv.QUOTE_MINIMAL)

def get_inc_nokia_feed_dets(fpath):
    debug = False
    nokia_rows = []
    rtis = []
    with open(fpath, encoding='utf-8', errors='ignore') as csvfile:
        reader = csv.DictReader(csvfile, dialect='piper')
        for row in reader:
            if debug: print(row)
            rtis.append(int(row['CELL_ID_RTI']))
            if row['SOURCE_TYPE'].startswith('N'):
                nokia_rows.append(row)
        if debug:
            for row in nokia_rows:
                print(row)
    nokia_coords = {
        int(row['CELL_ID_RTI']):
        (float(row['LATITUDE']), float(row['LONGITUDE']))
        for row in nokia_rows}
    if debug: pp(nokia_coords)
    return nokia_coords, rtis

def compare_coords(nokia_coords):
    unused, cur_local = db.Pg.local()
    sql = """\
    SELECT DISTINCT ON (cell_id_rti)
    cell_id_rti, tower_lat, tower_lon
    FROM {mod_p_locations}
    ORDER BY cell_id_rti, effective_start DESC
    """.format(mod_p_locations=conf.MOD_P_LOCATIONS)
    cur_local.execute(sql)
    data = cur_local.fetchall()
    mod_p_coords = {row['cell_id_rti']: (row['tower_lat'], row['tower_lon'])
        for row in data}
    too_far_rtis = []
    close_enough_rtis = []
    feed_only_rtis = []
    zero_zero_rtis = []
    for rti in sorted(nokia_coords.keys()):
        if nokia_coords[rti] == (0, 0):
            zero_zero_rtis.append(rti)
        try:
            gap_km = spatial.Pg.gap_km(cur_local,
                nokia_coords[rti], mod_p_coords[rti])
        except KeyError:
            feed_only_rtis.append(rti)
            continue
        else:
            if gap_km < 1:
                close_enough_rtis.append(rti)
            else:
                too_far_rtis.append(rti)
                print("{}: nokia coord {}, mod_p coord {}, gap {:,}".format(
                    rti, nokia_coords[rti], mod_p_coords[rti], gap_km))
    print('(0, 0) Nokia coords (n={:,}):'.format(len(zero_zero_rtis)))
    for line in wrap(str(zero_zero_rtis), 100): print(line)
    print('Close enough (n={:,}):'.format(len(close_enough_rtis)))
    for line in wrap(str(close_enough_rtis), 100):
        print(line)
    print('FEED-only (n={:,}): {}'.format(len(feed_only_rtis), feed_only_rtis))
    return {'too_far_rtis': too_far_rtis}

def check_mislocated(rti, rtis_to_exclude, date_str='2017-10-01'):
    """
    OK - presumably mod_p and the Nokia FEED disagree about where an rti is. So
    where is it likely to be. Supposedly current so look at a recent date.
    """
    unused, cur_local = db.Pg.local()
    (start_datetime_str,
     end_datetime_str) = dates.Dates.date_str_to_datetime_str_range(date_str)
    findloc.LikelyLocations.likely_location_map(cur_local,
        start_datetime_str, end_datetime_str,
        rti, rtis_to_exclude)

def check_coord_event_traffic(rtis):
    """
    OK - we don't have a good location for this coord - do we even need it? Can
    we afford to have it land in the bin (e.g. if (0, 0) will be excluded as
    being outside the crude NZ bounding box.

    47941-9, 51-9, 61-9 - dribbled till 2017-10-03 (0.5K-7K etc)
    461183-5, 93-7, 462207-9, 17-9 - few or many hundreds 2016-11-23 till 2017-10-02
    461705-7 - many hundreds 2016-11-23 onwards
    469361/71/81 - many hundreds  - several K 2017-03-25 till 2017-04-02
    478063/83 478831/41/51 479087/97/107 - several thousand 2017-07-10 onwards 
    478065/73/75/85 478833/43/53 479089/99/109 - several to many thousand 2017-06-19 ish onwards

    VERDICT -- need to send corrections to Tavleen for all of these - just use Mod P locations
    """
    for rti in rtis:
        utils.get_daily_tots(rti)
        input('Much traffic?\nNext ...')

def unlocated(rtis, date_str):
    unused, cur_gp = db.Pg.gp()
    sql = """
    SELECT 
    cell_id_rti,
      SUM(distinct_imsi) AS
    imsi_n
    FROM {daily_connections}
    WHERE date = %s
    GROUP BY cell_id_rti
    HAVING SUM(distinct_imsi) > 1000
    ORDER BY cell_id_rti
    """.format(daily_connections=conf.DAILY_CONNECTION_DATA)
    cur_gp.execute(sql, (date_str, ))
    sig_events_rtis = [row['cell_id_rti'] for row in cur_gp.fetchall()]
    missing_rtis = set(sig_events_rtis) - set(rtis)
    return missing_rtis

def main():
    '''
    utils.report_on_imsi(imsi=4052001692915109139,
        min_date_str='2017-10-10', max_date_str='2017-10-10')
    fpath = os.path.join(conf.CSM_ROOT, 'storage',
        'lbs_cell_location_v2_20171010.dlm')
    nokia_coords, rtis = get_inc_nokia_feed_dets(fpath)
    unlocated_rtis = unlocated(rtis, date_str='2017-10-10')
    print(len(unlocated_rtis), sorted(list(unlocated_rtis)))
    dets = compare_coords(nokia_coords)
    too_far_rtis = dets['too_far_rtis']


    check_mislocated(rti, rtis_to_exclude, date_str='2017-10-05')

    #check_coord_event_traffic(too_far_rtis)
    '''
    rti = 20141
    rtis_to_exclude = [20142, ]
    unused, cur_local = db.Pg.local()
    start_datetime_str = '2017-10-05 09:00:00'
    end_datetime_str = '2017-10-05 12:00:00'
    findloc.LikelyLocations.likely_location_map(cur_local,
        start_datetime_str, end_datetime_str,
        rti, rtis_to_exclude)

if __name__ == '__main__':
    main()
