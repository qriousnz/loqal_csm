#! /usr/bin/env python3

"""
https://github.com/MagicStack/asyncpg
https://magicstack.github.io/asyncpg/current/
http://lucumr.pocoo.org/2016/10/30/i-dont-understand-asyncio/ ;-)

Event loop goes through code until having to wait then moves to next unblocked
step and so on. Eventually everything moves beyond all its awaits and we are
finished. Can then get all the results of the completed futures.
"""

from datetime import datetime, timedelta 
#import getpass
import psycopg2 as pg
import psycopg2.extras as pg_extras
import time

import asyncio
import asyncpg

import not4git

def _dates_from_min_max_strs(min_date_str, max_date_str):
    min_date = datetime.strptime(min_date_str, '%Y-%m-%d')
    max_date = datetime.strptime(max_date_str, '%Y-%m-%d')
    return min_date, max_date

def _date_strs_from_min_max_dates(min_date, max_date):
    date_strs = []
    date2use = min_date
    while date2use <= max_date:
        date_str = date2use.strftime('%Y-%m-%d')
        date_strs.append(date_str)
        date2use += timedelta(days=1)
    return date_strs

def date_strs_from_min_max_date_strs(min_date_str, max_date_str):
    min_date, max_date = _dates_from_min_max_strs(min_date_str,
        max_date_str)
    return _date_strs_from_min_max_dates(min_date, max_date)

run_local = True
run_async = True

max_date_str = '2016-09-04'
date_strs = date_strs_from_min_max_date_strs('2016-09-01', max_date_str)

con_kwargs_local = {
    'database': 'tourism',  ## not dbname as in psycopg2
    'user': 'postgres',
    'host': 'localhost',
    'port': 5433,
    'password': not4git.access_local,  #getpass.getpass('Enter local postgres password: ')
}

con_kwargs_rem = {
    'database': 'tnz_loc',  ## not dbname as in psycopg2
    'user': 'gpadmin',
    'host': '10.100.99.121',
    'port': 5432,
    'password': not4git.access_rem,  #getpass.getpass('Enter remote postgres password: ')
}

## Local *******************************
## psycopg2 (local)
if run_local:
    t1 = time.time()
    def run(con_kwargs):
        kwargs = con_kwargs.copy()
        kwargs['dbname'] = kwargs['database']
        del kwargs['database']
        con = pg.connect(**kwargs)
        cur = con.cursor(cursor_factory=pg_extras.DictCursor)
        sql_tpl = """\
          SELECT COUNT(*) AS freq
          FROM csm
          WHERE cob_date = %s
        """
        results = []
        for date_str in date_strs:
            cur.execute(sql_tpl, (date_str, ))
            values = cur.fetchone()
            results.append(values)
        return results

    results = run(con_kwargs_local)
    print(', '.join(str(row['freq']) for row in results))
    t2 = time.time()
    print("Sync took {} seconds".format(round(t2 - t1, 1)))

    ## asyncpg (local)
    if run_async:
        t1 = time.time()
        async def get_vals(pool, sql, args):
            async with pool.acquire() as con:  ## handles releasing the connection back to the pool
                job = await con.fetchrow(sql, *args)  ## fetchrow (by keys) vs fetch (list of tups)
            return job

        async def run(con_kwargs):
            sql = """\
            SELECT COUNT(*) AS freq
            FROM csm
            WHERE cob_date = $1
            """
            jobs = []
            pool = await asyncpg.create_pool(**con_kwargs_local)
            for date_str in date_strs:
                args = [datetime.strptime(date_str, '%Y-%m-%d'), ]  ## don't pass in string if expecting date object (unless you like toordinal error messages) ;-)
                get_val_job = get_vals(pool, sql, args)
                jobs.append(get_val_job)
            print(type(jobs[0]))
            completed, unused_pending = await asyncio.wait(jobs)
            results = [done_job.result() for done_job in completed]
            return results

        loop = asyncio.get_event_loop()
        results = loop.run_until_complete(run(con_kwargs_local))
        print(', '.join(str(row['freq']) for row in results))
        t2 = time.time()
        print("Async took {} seconds".format(round(t2 - t1, 1)))

## Greenplum *******************************
else:
    ## psycopg2 (Greenplum)
    t1 = time.time()
    def run(con_kwargs):
        con = pg.connect(**con_kwargs)
        cur = con.cursor(cursor_factory=pg_extras.DictCursor)
        sql_tpl = """\
        SELECT COUNT(*) AS freq
        FROM lbs_agg.fact_rti_summary
        WHERE date = %s
        """
        results = []
        for date_str in date_strs:
            cur.execute(sql_tpl, (date_str, ))
            values = cur.fetchone()
            results.append(values)
        return results

    results = run(con_kwargs_rem)
    print(', '.join(str(row['freq']) for row in results))
    t2 = time.time()
    print("Sync took {} seconds".format(round(t2 - t1, 1)))
    ## asyncpg (Greenplum)
    if run_async:
        """
        Connection pool and Greenplum didn't work together it seems (using code
        which worked identically on local standard PostgreSQL server) so a fresh
        connection opened and closed each time.
        """
        t1 = time.time()
        async def get_vals(sql, args):
            con = await asyncpg.connect(**con_kwargs_rem)
            job = await con.fetchrow(sql, *args)
            await con.close()
            return job

        async def run(con_kwargs):
            sql = """\
            SELECT COUNT(*) AS freq
            FROM lbs_agg.fact_rti_summary
            WHERE date = $1  -- Yes, a different placeholder in asyncpg cf psycopg2
            """
            jobs = []
            for date_str in date_strs:
                args = [date_str, ]
                get_val_job = get_vals(sql, args)
                jobs.append(get_val_job)
            completed, unused_pending = await asyncio.wait(jobs)
            results = [done_job.result() for done_job in completed]
            return results

        loop = asyncio.get_event_loop()
        results = loop.run_until_complete(run(con_kwargs_local))
        print(', '.join(str(row['freq']) for row in results))
        t2 = time.time()
        print("Async took {} seconds".format(round(t2 - t1, 1)))
