#! /usr/bin/env python3
"""
Quarry grinds through redrock data looking for data problems.
"""
from collections import OrderedDict
from datetime import timedelta
import json
from pprint import pformat as pf
from statistics import median
from webbrowser import open_new_tab

from dateutil.parser import parse as date_parse
import matplotlib.pyplot as plt
import numpy as np

import sys
sys.path.insert(0, '../voyager_shared')
import conf #@UnresolvedImport
import utils #@UnresolvedImport
import report_relocations as reprel #@UnresolvedImport
from folium_map import FoliumMap #@UnresolvedImport

MIN_SHIFT_TO_BE_RELOCATION_KM = 1
N_FIRST2SHOW = 5
ROUNDING = 3
IMG_WIDTH = 380
IMG_HEIGHT = 30
REFRESH_SRC_DATA = False

def _get_next_coord(cur, cell_id_rti, datetime):
    """
    Return earliest coord for cell_id_rti after the datetime. Return None if
    nothing to return.

    Note -- the missing percentage for a given batch may go up if it later turns
    out the cell_id_rti returns to a similar location.
    """
    sql_next_coord = """\
    SELECT DISTINCT ON (cell_id_rti)
    tower_lat,
    tower_lon
    FROM {redrock_history}
    WHERE cell_id_rti = %s
    AND as_at_datetime > %s
    ORDER BY cell_id_rti, as_at_datetime ASC
    """.format(redrock_history=conf.REDROCK_HISTORY)
    cur.execute(sql_next_coord, (cell_id_rti, datetime))
    next_coord_data = cur.fetchall()
    n_next = len(next_coord_data)
    if n_next == 0:
        next_coord = None
    elif n_next == 1:
        next_coord = (next_coord_data[0]['tower_lat'],
            next_coord_data[0]['tower_lon'])
    else:
        raise Exception("Unexpected number of next_data items ({:,})"
            .format(n_next))
    return next_coord

def _added_dropped_missing(cur):
    """
    Cycle through each redrock file and identify additions, losses, and missing
    cell_id_rtis.

    cell_id_rtis should only appear once they are active and operational
    according to David Yu. They should only disappear once they are
    decommissioned. They should only reappear if they are being reused in a
    different location.

    Missing is defined as cell_id_rti having been present before, and not being
    present now even though will appear again and next appearance will be in
    same (or close) location.
    """
    ## datetimes (dates not always unique e.g. 22 hour gap)
    sql_dates = """\
    SELECT DISTINCT as_at_datetime
    FROM {redrock_history}
    ORDER BY as_at_datetime
    """.format(redrock_history=conf.REDROCK_HISTORY)
    cur.execute(sql_dates)
    dates_data = cur.fetchall()
    rr_datetimes = [row['as_at_datetime'] for row in dates_data]
    ## process each batch of rr data
    sql_dets4date_tpl = """\
    SELECT DISTINCT ON (cell_id_rti)
    cell_id_rti,
    tower_lat,
    tower_lon
    FROM {redrock_history}
    WHERE as_at_datetime = %s
    """.format(redrock_history=conf.REDROCK_HISTORY)
    latest_prev_coords = {}
    ever = set()
    prev = set()
    details = []
    for rr_datetime in rr_datetimes:
        cur.execute(sql_dets4date_tpl, (rr_datetime, ))
        data4date = cur.fetchall()
        n_prev = len(prev)
        n_current = len(data4date)
        current_coords = {
            row['cell_id_rti']: (row['tower_lat'], row['tower_lon'])
            for row in data4date}
        current = set([row['cell_id_rti'] for row in data4date])
        added = current.difference(prev)  ## not in immediate prev
        lost = prev.difference(current)
        n_added = len(added)
        pct_added = round(100*(n_added/n_prev), ROUNDING) if prev else 'N/A'
        n_lost = len(lost)
        pct_lost = round(100*(n_lost/n_prev), ROUNDING) if prev else 'N/A'
        first_added = sorted(list(added))[:N_FIRST2SHOW]
        first_lost = sorted(list(lost))[:N_FIRST2SHOW]
        ## missing
        missing = []
        absent = ever.difference(current)
        for cell_id_rti in absent:
            latest_prev_coord = latest_prev_coords[cell_id_rti]
            next_coord = _get_next_coord(cur, cell_id_rti, rr_datetime)
            if next_coord:
                km_moved = utils.km_moved(cur, latest_prev_coord, next_coord)
                approx_same_place = (km_moved < MIN_SHIFT_TO_BE_RELOCATION_KM)
                if approx_same_place:
                    missing.append(cell_id_rti)
        n_missing = len(missing)
        pct_missing = round(100*(n_missing/n_current), ROUNDING)
        first_missing = sorted(list(missing))[:N_FIRST2SHOW]
        ## return
        details.append(OrderedDict([
             ('datetime', rr_datetime.isoformat()),  ## not storing actual datetime as not JSON-serializable
             ('n_missing', n_missing),
             ('n_added', n_added),
             ('n_lost', n_lost),
             ('n_current', n_current),
             ('pct_missing', pct_missing),
             ('pct_added', pct_added),
             ('pct_lost', pct_lost),
             ('first_added', first_added),
             ('first_lost', first_lost),
             ('first_missing', first_missing),
            ])
        )
        ## init next loop
        ever.update(current)
        prev = current
        latest_prev_coords.update(current_coords)
    return details

def report_cell_id_rti_churn(con, cur, refresh_analysis=True):
    if not refresh_analysis:
        utils.warn(msg="Not refreshing analysis - testing chart?")
    if refresh_analysis:
        details = _added_dropped_missing(cur)
        with open('details.json', 'w') as f:
            json.dump(details, f)
    with open('details.json') as f:
        details = json.load(f)
    ## make ALL chart
    fig_all = plt.figure(figsize=(IMG_WIDTH, IMG_HEIGHT))
    ax_all = fig_all.add_subplot(111)
    utils.Mpl.set_ticks(ax_all, interval=1, inc_minor=True)
    x = [date_parse(row['datetime']) for row in details]
    x1 = [date_parse(row['datetime']) + timedelta(hours=-6) for row in details]
    x2 = [date_parse(row['datetime']) + timedelta(hours=6) for row in details]
    y_added = [row['n_added'] for row in details]
    y_missing = [-row['n_missing'] for row in details]
    y_dropped = [-row['n_lost'] for row in details]
    ax_bar_added = ax_all.bar(x, y_added, width=1, color='green',
        align='center')
    ax_bar_missing = ax_all.bar(x1, y_missing, width=0.5, color='red',
        align='center')
    ax_bar_dropped = ax_all.bar(x2, y_dropped, width=0.5, color='grey',
        align='center')
    lbls_common = ['Added', 'Missing (inc dropped)', 'Dropped']
    handles_all = [ax_bar_added, ax_bar_missing, ax_bar_dropped]
    utils.Mpl.set_legends(ax_all, handles_all, lbls_common, fontsize=40,
        lower_propn=0.88)
    utils.Mpl.add_year_vlines(ax_all, timedelta(hours=24), x, lower_propn_line=0,
        lower_propn_text=0.95, rotate=False, fontsize=40)
    #plt.show(); return
    utils.Mpl.set_axis_labels(ax_all)
    title_all = 'Redrock cell_id_rti churn'
    utils.Mpl.set_titles(ax_all, title_all)
    plt.axhline(0, color='black', lw=2)  ## needs to come after ticks are set it seems
    img_name_all = "{}.png".format(title_all)
    img_fpath_all = "reports/images/charts/{}".format(img_name_all)
    html_rel_all_img_path = "images/charts/{}".format(img_name_all)
    fig_all.savefig(img_fpath_all, bbox_inches='tight')
    fs_rel_all_img_path = 'reports/' + html_rel_all_img_path        
    #plt.show(); return
    plt.savefig(fs_rel_all_img_path, bbox_inches='tight')
    plt.close(fig_all)
    html = [conf.HTML_START_TPL % {
        'title': title_all,
        'more_styles': """
        .left-align {
          text-align: left;
        }
        .right-align {
          text-align: right;
        }
        .spaced th, .spaced td {
          padding: 0 6px;
        }
        th {
          font-weight: bold;
          font-size: 14px;
        }
        .amber {
          background-color: #ffd894;
        }
        .red {
          background-color: #e8626e;
        }
        """}, ]
    html.append("<h2>Churn charts</h2>")
    html.append("""\
    <p>Note - the number of missing records per day possibly understates the
    total number in that it can only identify missing cell_id_rti's that have
    already appeared at least once before.
    </p>
    <p>Also note that missing values can be remedied if they are duplicates of
    earlier values assuming a system of storing last known values is implemented
    like in the CSM code or the sector_location_history
    </p>""")
    html.append("<img width='3200px' padding=0 margin=0 src='{}'>".format(
        html_rel_all_img_path))

    ## make ZOOMED in chart
    CAP = 300
    fig_zoom = plt.figure(figsize=(IMG_WIDTH, IMG_HEIGHT))
    ax_zoom = fig_zoom.add_subplot(111)
    ax_zoom.grid(b=True)
    utils.Mpl.set_ticks(ax_zoom, interval=1)
    y_added_capped = [min(row['n_added'], CAP) for row in details]
    y_missing_capped = [max(-row['n_missing'], -CAP) for row in details]
    y_dropped_capped = [max(-row['n_lost'], -CAP) for row in details]
    ax_bar_added_capped = ax_zoom.bar(x, y_added_capped, width=1, color='green',
        align='center')
    ax_bar_missing_capped = ax_zoom.bar(x1, y_missing_capped, width=0.5,
        color='grey', align='center')
    ax_bar_dropped_capped = ax_zoom.bar(x2, y_dropped_capped, width=0.5,
        color='red', align='center')
    handles_zoom = [ax_bar_added_capped, ax_bar_missing_capped,
        ax_bar_dropped_capped]
    #plt.show(); return
    utils.Mpl.set_axis_labels(ax_zoom)
    title_zoom = ('Redrock cell_id_rti churn - values cropped to +- {}'
        .format(CAP))
    utils.Mpl.set_titles(ax_zoom, title_zoom)
    plt.axhline(0, color='black', lw=2)  ## needs to come after ticks are set it seems
    img_name_zoom = "{} zoomed.png".format(title_zoom)
    img_fpath_zoom = "reports/images/charts/{}".format(img_name_zoom)
    html_rel_zoom_img_path = "images/charts/{}".format(img_name_zoom)
    fig_zoom.savefig(img_fpath_zoom, bbox_inches='tight')
    fs_rel_zoom_img_path = 'reports/' + html_rel_zoom_img_path
    utils.Mpl.set_legends(ax_zoom, handles_zoom, lbls_common, fontsize=40,
        lower_propn=0.88)
    utils.Mpl.add_year_vlines(ax_zoom, timedelta(hours=24), x,
        lower_propn_line=0, lower_propn_text=0.95, rotate=False, fontsize=40)
    #plt.show(); return
    plt.savefig(fs_rel_zoom_img_path, bbox_inches='tight')
    plt.close(fig_zoom)
    html.append("<img width='3200px' padding=0 margin=0 src='{}'>".format(
        html_rel_zoom_img_path))

    ## DETAILS
    html.append("<h2>Details</h2>")
    html.append("""\
    <table class='spaced'>
      <thead>
        <tr>
          <th class='left-align'>Date-time</th>
          <th class='right-align'>N. missing</th>
          <th class='left-align'>Missing cell_id_rti's</th>
          <th class='right-align'>N cell id rti's</th>
          <th class='right-align'>N added</th>
          <th class='right-align'>N dropped</th>
        </tr>
      </thead>
      <tbody>
    """)
    for det in details:
        n_missing = det['n_missing']
        if n_missing == 0:
            n_missing_item = n_missing
        elif 1 <= n_missing <= 20:
            n_missing_item = "<span class='amber'>{:,}</span>".format(n_missing)
        elif n_missing > 20:
            n_missing_item = "<span class='red'>{:,}</span>".format(n_missing)
        else:
            raise Exception("Unexpected value for n_missing ({})"
                .format(n_missing)) 
        html.append("""\
        <tr>
          <td class='left-align'>{}</td>
          <td class='right-align'>{}</td>
          <td class='left-align'>{}</td>
          <td class='right-align'>{:,}</td>
          <td class='right-align'>{:,}</td>
          <td class='right-align'>{:,}</td>
        </tr>
        """.format(
            date_parse(det['datetime']).strftime('%Y-%m-%d %-I:%m %p'),
            n_missing_item,
            ', '.join(str(x) for x in det['first_missing']) + '...' if det['first_missing'] else '',
            det['n_current'],
            det['n_added'],
            det['n_lost']))
    html.append("""\
      </tbody>
    </table>
    """)
    html.append("""\
    <h2>Audit</h2>
    <pre>SELECT *
    FROM {redrock_history} 
    WHERE cell_id_rti = &LT;cell_id_rti&GT;
    ORDER BY as_at_datetime</pre>
    """)
    ## assemble
    content = "\n".join(html)
    fpath = ("/home/gps/Documents/tourism/csm/reports/"
        "redrock_cell_id_rti_churn.html")
    with open(fpath, 'w') as f:
        f.write(content)
    open_new_tab("file://{}".format(fpath))

def get_cell_id_rtis_mass_added(cur, min_appearance_number=100):
    sql_mass_appearance_dts = """\
    SELECT
    as_at_datetime
    FROM (
      SELECT DISTINCT ON (cell_id_rti)
      cell_id_rti,
      as_at_datetime
      FROM {redrock_history}
      ORDER BY cell_id_rti, as_at_datetime ASC  /* earliest i.e. first when asc sort order */
    ) AS src
    GROUP BY as_at_datetime
    HAVING COUNT(*) > %s
    AND as_at_datetime > (SELECT MIN(as_at_datetime) FROM {redrock_history})
    ORDER BY as_at_datetime;
    """.format(redrock_history=conf.REDROCK_HISTORY)
    cur.execute(sql_mass_appearance_dts, (min_appearance_number, ))
    dts = [x[0] for x in cur.fetchall()]
    sql_cell_id_rtis_tpl = """\
    SELECT cell_id_rti
    FROM (
        SELECT DISTINCT ON (cell_id_rti)
        cell_id_rti,
        as_at_datetime
        FROM {redrock_history}
        ORDER BY cell_id_rti, as_at_datetime ASC  /* earliest i.e. first when asc sort order */
    ) AS src
    WHERE as_at_datetime = %s
    ORDER BY cell_id_rti
    """.format(redrock_history=conf.REDROCK_HISTORY)
    dt_with_cell_id_rtis = []
    for dt in dts:
        cur.execute(sql_cell_id_rtis_tpl, (dt, ))
        cell_id_rtis_appearing = tuple([x[0] for x in cur.fetchall()])
        dt_with_cell_id_rtis.append((dt, cell_id_rtis_appearing))
    return dt_with_cell_id_rtis

def map_mass_additions(cur, min_appearance_number=100):
    dt_with_cell_id_rtis = get_cell_id_rtis_mass_added(cur,
        min_appearance_number)
    html = [conf.HTML_START_TPL % {
        'title': "Mass Additions  (>= {:,})".format(min_appearance_number),
        'more_styles': """\
          .mini-map {
            float: left;
          }
        """}]
    for dt, cell_id_rtis_appearing in dt_with_cell_id_rtis:
        datestr = dt.strftime('%d-%m-%Y')
        n_cell_id_rtis = len(cell_id_rtis_appearing)
        map_url = FoliumMap.map_rti_current_locations(cur,
            cell_id_rtis_appearing, as_at_dt=dt)
        html.append("""\
        <div class='mini-map'>
          <h2>{date} ({n_cell_id_rtis})</h2>
          <iframe class='mini-map' src='{src}' width=350px height=700px>
          </iframe>
        </div>
        """.format(src=map_url, date=datestr, n_cell_id_rtis=n_cell_id_rtis))
    fpath = "/home/gps/Documents/tourism/csm/reports/mapped_cell_id_rtis.html"
    utils.html_opened(html, fpath)

def lag_analysis(cur):
    """
    How long between a cell_id_rti appearing in the Redrock error files as not
    having an Atoll match and then finally appearing for first time in LTE or
    UMTS files? Make some descriptive stats on the lag. Then analyse by date
    appearing and show descriptive stats for lag. Average lag by area finally
    appearing?

    LTE (cell id and ECI which is the cell_id_rti):
    STOXA          121     397433 Error   No matching Atoll lte cell record

    UMTS (cell id which is the cell_id_rti):
    SWHTA1_4                42931 Error   No matching Atoll umts cell record

    Looping through time, looking at cell_id_rti's in both the data and the
    errors files. Record first time appearing in data and also in errors. Also
    record all dates it appears in errors to allow more detailed future
    analysis. We're interested in when it appears in errors before in actual
    data. Presumably can't be in both on same day, and errors after the first
    appearance in data are potentially interesting for other analyses.
    """
    sql_dates = """\
    SELECT DISTINCT as_at_datetime
    FROM (
      SELECT DISTINCT as_at_datetime FROM {redrock_history}
      UNION ALL
      SELECT DISTINCT as_at_datetime FROM {redrock_error_history}
    ) AS src
    ORDER BY as_at_datetime
    """.format(redrock_history=conf.REDROCK_HISTORY,
        redrock_error_history=conf.REDROCK_ERROR_HISTORY)
    cur.execute(sql_dates)
    cell_id_rtis_dets = {}
    as_at_dates = [x[0] for x in cur.fetchall()]
    sql_data_tpl = """\
    SELECT cell_id_rti
    FROM {redrock_history}
    WHERE as_at_datetime = %s
    """.format(redrock_history=conf.REDROCK_HISTORY)
    sql_errors_tpl = """\
    SELECT cell_id_rti, raw_msg
    FROM {redrock_error_history}
    WHERE as_at_datetime = %s
    """.format(redrock_error_history=conf.REDROCK_ERROR_HISTORY)
    seconds_in_a_day = 60*60*24
    for as_at_date in as_at_dates:
        ## data
        cur.execute(sql_data_tpl, (as_at_date, ))
        cell_id_rtis = [x[0] for x in cur.fetchall()]
        for cell_id_rti in cell_id_rtis:
            if cell_id_rti not in cell_id_rtis_dets:
                cell_id_rtis_dets[cell_id_rti] = {'first_appearance': as_at_date}
            elif 'first_appearance' not in cell_id_rtis_dets[cell_id_rti]:
                cell_id_rtis_dets[cell_id_rti]['first_appearance'] = as_at_date
            else:
                pass  ## already appeared - nothing to do apart from errors
        ## errors
        cur.execute(sql_errors_tpl, (as_at_date, ))
        for row in cur.fetchall():
            cell_id_rti = row['cell_id_rti']
            if cell_id_rti not in cell_id_rtis_dets:
                cell_id_rtis_dets[cell_id_rti] = {'first_error': as_at_date}
            elif 'first_error' not in cell_id_rtis_dets[cell_id_rti]:
                cell_id_rtis_dets[cell_id_rti]['first_error'] = as_at_date
            if 'errors' not in cell_id_rtis_dets[cell_id_rti]:
                cell_id_rtis_dets[cell_id_rti]['errors'] = [
                    {'as_at_date': as_at_date.strftime("%Y-%m-%d %-I:%M"),
                     'raw_msg': utils.Text.space_diet(row['raw_msg'])}]
            else:
                cell_id_rtis_dets[cell_id_rti]['errors'].append(
                    {'as_at_date': as_at_date.strftime("%Y-%m-%d %-I:%M"),
                     'raw_msg': utils.Text.space_diet(row['raw_msg'])})
    ## add gaps
    gaps = []
    for cell_id_rti, cell_id_rti_dets in sorted(cell_id_rtis_dets.items()):
        ## get first appearance and first error
        if ('first_appearance' in cell_id_rti_dets
                and 'first_error' in cell_id_rti_dets
                and (cell_id_rti_dets['first_appearance']
                - cell_id_rti_dets['first_error']).total_seconds() > 0):
            gap = round((cell_id_rti_dets['first_appearance']
                - cell_id_rti_dets['first_error'])
                .total_seconds()/seconds_in_a_day, 1)
            cell_id_rtis_dets[cell_id_rti]['gap'] = gap
            gaps.append(gap)
    ## descriptive stats on gaps
    n_gaps_over_week = len([gap for gap in gaps if gap > 7])
    median_gap = median(gaps)
    max_gap = max(gaps)
    q25, q75, q95, q99 = np.percentile(gaps, [25, 75, 95, 99])
    print(n_gaps_over_week, q25, median_gap, q75, q95, q99, max_gap)
    ## report on gappiest cell_id_rti's
    gappiest_dets_first = sorted(cell_id_rtis_dets.items(),
        key=lambda item: item[1].get('gap', 0), reverse=True)
    with open('gappiest_dets.txt', 'w') as f:
        f.write("{} {} {} {} {} {} {}".format(n_gaps_over_week, q25, median_gap, q75, q95, q99, max_gap))
        for n, (cell_id_rti, cell_id_rti_dets) in enumerate(gappiest_dets_first, 1):
            f.write(pf(cell_id_rti_dets, width=200))
            if n > 200:
                break
    print("Finished")

def main():
    con, cur, cur2 = utils.Pg.get_local()
    if not REFRESH_SRC_DATA:
        utils.warn(msg="Not refreshing source Redrock data")
    else:
        utils.LocationSources.make_redrock_history(con, cur,
            extract_from_zips=True)
        utils.LocationSources.make_redrock_error_history(con, cur,
            extract_from_zips=False)
        
    #utils.LocationSources._extract_redrock_files()
    #utils.LocationSources.make_redrock_error_history(con, cur,
    #    extract_from_zips=False)
    #report_cell_id_rti_churn(con, cur, refresh_src_data=False,
    #    refresh_analysis=False)
    
    #reprel.report_relocations(cur, src=conf.SECTOR_LOCATION_HISTORY)
    #map_mass_additions(cur, min_appearance_number=100)
    #utils.LocationSources.make_redrock_error_history(con, cur,
    #    extract_from_zips=False)
    lag_analysis(cur)

if __name__ == '__main__':
    main()