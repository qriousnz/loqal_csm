#! /usr/bin/env python3

import sys
sys.path.insert(0, '../voyager_shared')
import conf #@UnresolvedImport
from dates import Dates
import utils #@UnresolvedImport
from find_relocations import FindRelocations

def comparison_around_gap(cur_local):
    """
    cell_id_rti = 382063
    origin_coords = (-46.041886, 168.852201)  ## Southland
    origin_date_str = '2016-05-16'
    dest_coords = (-34.983382, 173.693849)  ## Northland
    dest_date_str = '2016-10-21'
    cell_id_rtis_to_exclude = [382073, 382083, ]
    
    cell_id_rti = 52015
    origin_coords = (-41.771170, 171.606732),  ## Westport
    origin_date_str = '2015-01-02'
    dest_coords = (-44.599255, 170.187926)  ## Otematata
    dest_date_str = '2015-12-31'
    cell_id_rtis_to_exclude = [52012, ]

    cell_id_rti = 57011
    origin_coords = (-41.1303, 174.881)  ## Birchville (Upper Hutt)
    origin_date_str = '2015-09-14'
    dest_coords = (-41.0889, 175.109)  ## Porirua (Waitangirua)
    dest_date_str = '2015-10-21'
    cell_id_rtis_to_exclude = [57012, 57013, 57014, 57015, 57016]
    """
    display_maps = True
    display_misses = True
    use_latest = True

    cell_id_rti = 57011
    origin_coords = (-41.1303, 174.881)  ## Birchville (Upper Hutt)
    origin_date_str = '2015-09-28'
    dest_coords = (-41.0889, 175.109)  ## Porirua (Waitangirua)
    dest_date_str = '2015-11-21'
    cell_id_rtis_to_exclude = [57012, 57013, 57014, 57015, 57016]

    (end_of_origin, start_of_dest, pre_coord_weights,
     post_coord_weights) = FindRelocations.get_relocation_dts(cur_local,
        cell_id_rti, origin_coords, origin_date_str, dest_coords, dest_date_str,
        cell_id_rtis_to_exclude, display_maps, display_misses, use_latest,
        examine_inner_gaps=True)
    print(end_of_origin, start_of_dest, pre_coord_weights, post_coord_weights)

def comparison_by_dates(cur_local):
    """
    cell_id_rti = 52521
    origin_coords = (-41.771170, 171.606732)  ## Westport
    origin_date_str = '2015-09-01'
    dest_coords = (-44.599255, 170.187926)  ## Otematata
    dest_date_str = '2015-10-30'
    cell_id_rtis_to_exclude = [52522, 52523, 52524, 52526, ]

    cell_id_rti = 52011
    origin_coords = (-41.771170, 171.606732)  ## Westport
    origin_date_str = '2015-10-01'
    dest_coords = (-44.599255, 170.187926)  ## Otematata
    dest_date_str = '2015-10-07'
    cell_id_rtis_to_exclude = [52012, 52014, 52015, ]

    cell_id_rti = 52014
    origin_coords = (-41.771170, 171.606732)  ## Westport
    origin_date_str = '2015-10-01'
    dest_coords = (-44.599255, 170.187926)  ## Otematata
    dest_date_str = '2015-12-03'
    cell_id_rtis_to_exclude = [52011, 52012, 52015, ]

    cell_id_rti = 52012
    origin_coords = (-41.771170, 171.606732)  ## Westport
    origin_date_str = '2015-10-01'
    dest_coords = (-44.599255, 170.187926)  ## Otematata
    dest_date_str = '2015-12-03'
    cell_id_rtis_to_exclude = [52015, ]

    cell_id_rti = 320881,
    origin_coords = (-37.046113, 175.520369)  ## Te Puru
    origin_date_str = '2016-11-04'  ## earliest date this cell_id_rti appears in connections data
    dest_coords = (-37.666873, 176.205025)  ## Tauranga
    dest_date_str = '2016-11-24'
    cell_id_rtis_to_exclude = [320891, 320901]

    cell_id_rti = 57011
    origin_coords = (-41.1303, 174.881)  ## Birchville (Upper Hutt)
    origin_date_str = '2015-04-14'
    dest_coords = (-41.0889, 175.109)  ## Porirua (Waitangirua)
    dest_date_str = '2016-11-30'
    cell_id_rtis_to_exclude = [57012, 57013, 57014, 57015, 57016]
    """
    display_maps = True
    display_misses = True
    use_latest = True

    cell_id_rti = 52521
    origin_coords = (-41.771170, 171.606732)  ## Westport
    origin_date_str = '2015-09-01'
    dest_coords = (-44.599255, 170.187926)  ## Otematata
    dest_date_str = '2015-10-30'
    cell_id_rtis_to_exclude = [52522, 52523, 52524, 52526, ]
    (end_of_origin, start_of_dest, pre_coord_weights,
     post_coord_weights) = FindRelocations.get_relocation_dts(cur_local,
        cell_id_rti, origin_coords, origin_date_str, dest_coords, dest_date_str,
        cell_id_rtis_to_exclude, display_maps, display_misses, use_latest,
        examine_inner_gaps=False)
    print(end_of_origin, start_of_dest, pre_coord_weights, post_coord_weights)
    
def display_for_date(cur_local):
    '''
    rti = 57011
    start_datetime_str = '2015-09-28 00:00:00'
    end_datetime_str = '2015-09-28 23:59:59'
    rtis_to_exclude = [57012, 57013, 57014, 57015, 57016]
    '''
    rti = 37821
    rtis_to_exclude = [37822, 37823, 37824, 37826, ]
    date_strs = ['2015-10-20', '2015-09-20']  # ['2015-11-20', '2015-11-28', '2016-01-30']
    for date_str in date_strs:
        (start_datetime_str,
         end_datetime_str) = Dates.date_str_to_datetime_str_range(date_str)
        FindRelocations.get_likely_location_dts(cur_local, start_datetime_str,
            end_datetime_str, rti, rtis_to_exclude)

def main():
    con_local, cur_local, unused_cur_local2 = utils.Pg.get_local()
    display_for_date(cur_local)
    #comparison_around_gap(cur_local)
    #fpath = '{}/CELLID_SECTOR_TECHNOLOGY.dlm'.format(conf.CSM_ROOT)
    #utils.LocationSources.make_mod_p_locations(con_local, cur_local, fpath)

if __name__ == '__main__':
    main()
