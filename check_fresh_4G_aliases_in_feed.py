#! /usr/bin/env python3

"""
The feed tells us about all the rti's configured to operate - which doesn't mean
that all of them are active. So if an RF engineer is relocating a node they
might add the new configuration even if the old configuration is what is
currently active. Later on the physical switch-over occurs and the first
configuration is not active and the new one is. Ideally the RF engineer will
purge the unused config eventually. So a finding of, for example 250 (1%) rti's
in the feed but not in the event data is not alarming. Note - the two
configurations will be 4G aliases of each other.

What really matters is that every significant rti (in terms of distinct IMSI's
in a day) has a matching location in the feed. If this is the case, we have no
problem to solve - everything can be located.

Create table in GP with every cell_id_rti in the feed. Do LJ from rows in
cell_summary table where daily total >= 50 and the feed rti's. How many
significant rti's cannot be located from feed rti list? Hopefully none (except
for 0 and 65535).
"""

from collections import Counter
import csv
from datetime import datetime
from itertools import count
from pprint import pprint as pp

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import conf, db, utils

MIN_SIG_IMSIS = 100

def rti_dets_from_csv(fpath):
    """
    0       1    2          3 4 5      6      7       8    9                             10    11    12  13
    256286 | 30 | WPIEA1_L | | | 1850 | 1001 | WPIEA | HL | Petone Integration and Eval | -10 | 100 | 1 | 15/08/2017 09:33:08
    """
    debug = False
    rtis = []
    rti_dets = []
    IDX_RTI = 0
    IDX_LAT = 10
    IDX_LON = 11
    IDX_DESC = 9
    with open(fpath, encoding='utf-8', errors='ignore') as csvfile:
        reader = csv.reader(csvfile)
        for n, row in enumerate(reader, 1):
            if debug: print(row)
            rti = row[IDX_RTI]
            if rti not in rtis:
                rti_dets.append((rti, row[IDX_LAT], row[IDX_LON], row[IDX_DESC]))
                rtis.append(rti)
            if n % 1000 == 0:
                print(utils.prog(n, "RTI's"))
    #c = Counter(rtis)
    #print(c.most_common(4))
    return sorted(list(set(rti_dets)))

def make_feed_rti_tbl(rti_dets, local=False):
    """
    256286 | 30 | WPIEA1_L | 1850 | 1001 | WPIEA | HL | Petone Integration and Eval | -10 | 100 | 1 | 15/08/2017 09:33:08
    """
    debug = False
    CHUNKS = 1000
    if local:
        con, cur, unused_cur_local2 = db.Pg.get_local()
        tblname = conf.FEED_RTIS_LOCAL
    else:
        con, cur = db.Pg.get_rem(user='gpadmin')
        tblname = conf.FEED_RTIS
    db.Pg.drop_tbl(con, cur, tbl=tblname)
    sql_make_tbl = """\
    CREATE TABLE {feed_rtis} (
      cell_id_rti bigint,
      tower_lat double precision,
      tower_lon double precision,
      description text,
      PRIMARY KEY(cell_id_rti)
    )
    """.format(feed_rtis=tblname)
    cur.execute(sql_make_tbl)
    con.commit()
    if not local:
        db.Pg.grant_public_read_permissions(tblname)
    sql_insert_tpl = """\
    INSERT INTO {feed_rtis}
    (cell_id_rti, tower_lat, tower_lon, description)
    VALUES
    """.format(feed_rtis=tblname)
    for i in count():
        print("Slice {:,}".format(i+1))
        start_idx = i*CHUNKS
        end_idx = (i+1)*CHUNKS
        rti_dets2use = rti_dets[start_idx: end_idx]
        if not rti_dets2use:
            break
        n_rti_dets2use = len(rti_dets2use)
        sql_insert = (sql_insert_tpl
            + ',\n'.join(["(%s, %s, %s, %s)"]*n_rti_dets2use))
        dets = []
        for row in rti_dets2use:
            row = list(row)
            for idx in (1, 2):
                if row[idx] == '': row[idx] = None
            dets.extend(row)
        if debug: print(sql_insert, dets)
        cur.execute(sql_insert, dets)
        con.commit()
    print("Finished making '{}' table".format(tblname))

def check_for_mismatches(date_str):
    unused_con_rem, cur_rem = db.Pg.get_rem()
    ## event data only
    sql_events_only = """\
    SELECT sig_event_rtis.cell_id_rti, n_imsis
    FROM (
      SELECT cell_id_rti, SUM(distinct_imsi) AS n_imsis
      FROM {daily_connections}
      WHERE date = %s
      GROUP BY cell_id_rti
      HAVING SUM(distinct_imsi) > {min_sig_imsis}
    ) AS sig_event_rtis
    LEFT JOIN
    {feed_rtis}
    USING(cell_id_rti)
    WHERE {feed_rtis}.cell_id_rti IS NULL
    ORDER BY n_imsis DESC
    """.format(min_sig_imsis=MIN_SIG_IMSIS, feed_rtis=conf.FEED_RTIS,
        daily_connections=conf.DAILY_CONNECTION_DATA)
    cur_rem.execute(sql_events_only, (date_str, ))
    sig_events_only_rti_dets = [(row['cell_id_rti'], int(row['n_imsis']))
        for row in cur_rem.fetchall()]
    ## feed only
    sql_feed_only = """\
    SELECT {feed_rtis}.cell_id_rti
    FROM {feed_rtis}
    LEFT JOIN
    (
      SELECT DISTINCT cell_id_rti FROM {daily_connections} WHERE date = %s
    ) AS event_rtis
    USING(cell_id_rti)
    WHERE event_rtis.cell_id_rti IS NULL
    """.format(feed_rtis=conf.FEED_RTIS,
        daily_connections=conf.DAILY_CONNECTION_DATA)
    cur_rem.execute(sql_feed_only, (date_str, ))
    feed_only_rtis = sorted([row['cell_id_rti'] for row in cur_rem.fetchall()])
    return sig_events_only_rti_dets, feed_only_rtis

def _fname2date_str(fname):
    """
    E.g. lbs_cell_location_20170815093309.csv to '2017-08-15'
    """
    raw_date_str = fname.split('_')[-1].split('.')[0][:8]
    date_str = datetime.strptime(raw_date_str, '%Y%m%d').strftime('%Y-%m-%d')
    return date_str

def make_feed_tbl(fname, local=False):
    fpath = os.path.join(conf.CSM_ROOT, 'storage', fname)
    rti_dets = rti_dets_from_csv(fpath=fpath)
    make_feed_rti_tbl(rti_dets, local)

def identify_mismatches(fname, make_fresh_src=True):
    date_str = _fname2date_str(fname)
    if make_fresh_src:
        fpath = os.path.join(conf.CSM_ROOT, 'storage', fname)
        rti_dets = rti_dets_from_csv(fpath=fpath)
        make_feed_rti_tbl(rti_dets, local=False)
    sig_events_only_rti_dets, feed_only_rtis = check_for_mismatches(date_str)
    print(len(sig_events_only_rti_dets))
    pp(sig_events_only_rti_dets)
    print(len(feed_only_rtis), feed_only_rtis)

def main():
    fname = 'lbs_cell_location_20170815093309.csv'
    make_feed_tbl(fname, local=True)
    #identify_mismatches(fname, make_fresh_src=True)
    

if __name__ == '__main__':
    main()
