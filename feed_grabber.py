#! /usr/bin/env python3

"""
Look at all files in transfer and work out which ones (after 2017-08-01)
are required. Then get them and report back how many it added.

Save this script to /home/grant.patonsimpson/scripts on HDFS server.

The following command connects to the HDFS server, runs this script, and exits.
All files should now be on under /home/grant.patonsimpson/transfer.

   ssh grant.patonsimpson@cdhclient-lbs01.corp.qrious.co.nz -t 'python3 scripts/feed_grabber.py'

Then FTP to /home/gps/Documents/tourism/csm/storage/feed_csvs and grab whatever is needed.
"""

import datetime
import glob
from subprocess import call

START = 'lbs_cell_location_'
ROOT = '/home/grant.patonsimpson/transfer/'

def fdets2get():
    debug = True
    ## Find what has already been grabbed
    csvs = glob.glob('{}{}20*.csv'.format(ROOT, START))
    start_idx = len(ROOT) + len(START)
    found_date_strs = [fname[start_idx:start_idx + len('YYYYMMDD')] for fname in csvs]
    if debug: print(found_date_strs)
    ## Find out what ought to have been grabbed
    required_dates = []
    date2add = datetime.date(2017, 7, 1)
    stop_date = datetime.datetime.today().date()
    while True:
        required_dates.append(date2add)
        if date2add == stop_date:
            break
        date2add = date2add + datetime.timedelta(days=1)
    required_date_strs = [required_date.strftime('%Y%m%d')
        for required_date in required_dates]
    if debug: print(required_date_strs)
    ## Find out what needs grabbing
    all_needed_date_strs = sorted(list(set(required_date_strs)
        - set(found_date_strs)))
    if debug: print(all_needed_date_strs)
    needed_file_dets = [
        (datetime.datetime.strptime(needed_date_str, '%Y%m%d').strftime('%Y-%m-%d'),
         "{}{}*.csv".format(START, needed_date_str))
        for needed_date_str in all_needed_date_strs]
    return needed_file_dets

def transfer_to_server_fs(fdets):
    for folder, fname in fdets:
        cmd = ['hadoop', 'fs', '-get', '/redrock/processed/{}/{}'
            .format(folder, fname), '/home/grant.patonsimpson/transfer']
        print(cmd)
        call(cmd)
    print("Finished")

def main():
    fdets = fdets2get()
    transfer_to_server_fs(fdets)

if __name__ == '__main__':
    main()
