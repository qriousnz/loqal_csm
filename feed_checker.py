"""
Feed data is stored as CSV files in the HDFS. So we need to connect to the
server and run a python script there to extract data from the HDFS and put it on
the server's native file system. From there we can FTP it over ssh2 onto our
local machine, and then create a table in our local PostgreSQL database.

We specify the date we are interested in (assuming it hasn't already been made
into a table) and the extraction from HDFS is brought up-to-date (if necessary),
the CSV is brought across, and the table is made.
"""
from collections import Counter, defaultdict
import csv
import datetime
from itertools import count
import pysftp
from random import sample
from subprocess import call

## See https://stackoverflow.com/questions/16981921/relative-imports-in-python-3
import os, sys

PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(),
    os.path.expanduser(__file__))))
sys.path.insert(0, os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))

from voyager_shared import conf, dates, db, folium_map, spatial, utils

import not4git #@UnresolvedImport

FEED_FOLDER_LOCAL = os.path.join(conf.CSM_ROOT, 'storage/feed_csvs/')
FEED_FOLDER_REMOTE = '/home/grant.patonsimpson/transfer/'
REMOTE_USERNAME = 'grant.patonsimpson'
REMOTE_HOST = '10.100.99.137'
START = 'lbs_cell_location_'


class GetFeed():

    @staticmethod
    def update_remote_transfer_folder():
        """
        Runs a python script on the server which transfers any missing feed
        files across to a transfer folder I can sftp them from. Very quick if
        only one file. Even quicker if none ;-).
        """
        cmd = ['ssh', 'grant.patonsimpson@cdhclient-lbs01.corp.qrious.co.nz',
            '-t', 'python3 scripts/feed_grabber.py']
        retcode = call(cmd)
        print("Completed\n{}\nwith retcode {}".format(cmd, retcode))

    @staticmethod
    def sftp_feed(date_compact):
        """
        date_compact -- e.g. '20190904'

        http://www.pythonforbeginners.com/modules-in-python/python-secure-ftp-module

        Connect to HDFS server, run feed_grabber script to populate remote
        folder, then ftp to local folder.
        """
        debug = True
        GetFeed.update_remote_transfer_folder()
        pwd = not4git.access_hive
        with pysftp.Connection(
                host=REMOTE_HOST,
                username=REMOTE_USERNAME,
                password=pwd) as sftp:
            fnames = sftp.listdir(FEED_FOLDER_REMOTE)
            if debug: print(fnames)
            feed_fnames = [fname for fname in fnames
                if fname.startswith(
                    'lbs_cell_location_{}'.format(date_compact))]
            if len(feed_fnames) != 1:
                raise Exception("Didn't find one, and only one, feed file for "
                    "date '{}'. Run update_remote manually?"
                    .format(date_compact))
            fname = feed_fnames[0]
            remote_path = os.path.join(FEED_FOLDER_REMOTE, fname)
            local_path = os.path.join(FEED_FOLDER_LOCAL, fname)
            sftp.get(remote_path, local_path)
            print("Transferred '{}' to '{}'".format(remote_path, local_path))
            return fname


class MakeTbl():
    """
    0       1    2           3      4      5      6  7   8    9                             10    11    12  13
    256286 | 30 | WPIEA1_L | 1850 | 1001 | WPIEA |  |  | HL | Petone Integration and Eval | -10 | 100 | 1 | 15/08/2017 09:33:08
    rti      cell_id                node_id
    """

    IDX_RTI = 0
    IDX_LAT = 10
    IDX_LON = 11
    IDX_DESC = 9
    IDX_TECH_CLASS = 8
    IDX_TOWER = 5

    @staticmethod
    def rti_dets_from_csv(fpath):
        """
        Note – the feed has duplicate records by design (Daniel's design at
        least ;-))**. If there are differences in lat/lon/name we get both
        records. We should only use the first when sorted by lat, lon, and name.

        ** Apparently so we can investigate issues when there is a dispute about
        lat/lons for a cell_id_rti. Otherwise the choice is made out-of-sight by
        Redrock.
        """
        debug = False
        rti_dets = []
        with open(fpath, encoding='utf-8', errors='ignore') as csvfile:
            reader = csv.reader(csvfile)
            for n, row in enumerate(reader, 1):
                if debug: print(row)
                rti = row[MakeTbl.IDX_RTI]
                tech_class = ('4G'
                    if row[MakeTbl.IDX_TECH_CLASS].lower().endswith('l')
                    else '3G')
                rti_dets.append(
                    (rti, row[MakeTbl.IDX_LAT], row[MakeTbl.IDX_LON],
                     row[MakeTbl.IDX_DESC], tech_class, row[MakeTbl.IDX_TOWER]))
                if n % 1000 == 0:
                    print(utils.prog(n, "RTI's"))
        #c = Counter(rtis)
        #print(c.most_common(4))
        return sorted(list(set(rti_dets)))

    @staticmethod
    def make_feed_tbl(con, cur, tblname, rti_dets, local=False):
        """
        local -- usually want to make table in GreenPlum so can do joins there
        with lbs_agg.fact_rti_summary.
        """
        debug = False
        CHUNK_SIZE = 1000
        db.Pg.drop_tbl(con, cur, tbl=tblname)
        sql_make_tbl = """\
        CREATE TABLE {feed} (
          cell_id_rti bigint,
          tower_lat double precision,
          tower_lon double precision,
          description text,
          tech_class text,
          tower text
        )
        """.format(feed=tblname)
        cur.execute(sql_make_tbl)
        con.commit()
        if not local:
            db.Pg.grant_public_read_permissions(tblname)
        sql_insert_tpl = """\
        INSERT INTO {feed}
        (cell_id_rti, tower_lat, tower_lon, description, tech_class, tower)
        VALUES
        """.format(feed=tblname)
        for i in count():
            print("Slice {:,} of {:,} records".format(i+1, CHUNK_SIZE))
            start_idx = i*CHUNK_SIZE
            end_idx = (i+1)*CHUNK_SIZE
            rti_dets2use = rti_dets[start_idx: end_idx]
            if not rti_dets2use:
                break
            n_rti_dets2use = len(rti_dets2use)
            sql_insert = (sql_insert_tpl
                + ',\n'.join(["(%s, %s, %s, %s, %s, %s)"]*n_rti_dets2use))
            dets = []
            for row in rti_dets2use:
                row = list(row)
                for idx in (1, 2):
                    if row[idx] == '': row[idx] = None
                dets.extend(row)
            if debug: print(sql_insert, dets)
            cur.execute(sql_insert, dets)
            con.commit()
        print("Finished making '{}' table".format(tblname))

    @staticmethod
    def make_feed_tbl_from_csv(con, cur, csv_fname, date_compact, local=False,
            force_fresh=False):
        schema = 'public' if local else 'grantps_test'
        tblname_no_schema = 'feed_{}'.format(date_compact)
        tblname = '{}.{}'.format(schema, tblname_no_schema)
        missing = not db.Pg.table_exists(cur, tblname_no_schema, schema)
        if missing or force_fresh:
            fpath = os.path.join(FEED_FOLDER_LOCAL, csv_fname)
            rti_dets = MakeTbl.rti_dets_from_csv(fpath=fpath)
            MakeTbl.make_feed_tbl(con, cur, tblname, rti_dets, local)
        else:
            utils.warn("Not making fresh version of {} - it already exists and "
                "force_fresh was False".format(tblname))
        return tblname


class CheckFeed():

    sql_rti_coords_tpl = """\
        SELECT DISTINCT ON (cell_id_rti)
          cell_id_rti,
          tower_lat,
          tower_lon
        FROM {tblname}
        ORDER BY cell_id_rti, tower_lat, tower_lon, description
        """

    @staticmethod
    def get_oth_coords(rti, tblname, date_compact):
        """
        Touching IMSI's are those that have connected to the rti in question at
        least once in the time period.

        We only count rti's once per touching-IMSI because some IMSI's generate
        vast numbers of events compared with others. Has side effect of reducing
        computational load.

        Return list of coords. Coords will appear more than once depending on
        how many touching-IMSI's connected to them at least once in the time
        period.

        The more IMSI's connecting to a coordinate, the more of a "vote" it gets
        which is appropriate.
        """
        unused, cur_gp = db.Pg.gp()
        start_date = (datetime.datetime.strptime(date_compact, '%Y%m%d')
            + datetime.timedelta(hours=8))  ## 8am
        end_date = start_date + datetime.timedelta(hours=4)  ## i.e. Noon
        start_epoch_int = dates.Dates.get_epoch_int_from_date(start_date)
        end_epoch_int = dates.Dates.get_epoch_int_from_date(end_date)
        date_str = start_date.strftime('%Y-%m-%d')
        sql_touched_imsis = """\
        SELECT DISTINCT imsi_rti
        FROM lbs_agg.fact_rti_summary
        WHERE date = '{date_str}'
        AND start_date >= {start_epoch_int}
        AND end_date <= {end_epoch_int}
        AND cell_id_rti = {rti}
        """.format(date_str=date_str, start_epoch_int=start_epoch_int,
            end_epoch_int=end_epoch_int, rti=rti)
        sql_rti_coords = CheckFeed.sql_rti_coords_tpl.format(tblname=tblname)
        sql_coords_for_touched_imsis = """\
        SELECT tower_lat, tower_lon
        FROM (
          SELECT DISTINCT cell_id_rti, imsi_rti
          FROM {events}
          INNER JOIN ({sql_touched_imsis}) AS touched_imsis /* filter to IMSI's we care about */
          USING(imsi_rti)
          WHERE date = '{date_str}'
          AND start_date >= {start_epoch_int}
          AND end_date <= {end_epoch_int}
        ) AS rti_imsis
        INNER JOIN
        ({sql_rti_coords}) AS rti_coords
        USING(cell_id_rti)
        """.format(events=conf.CONNECTION_DATA,
            sql_touched_imsis=sql_touched_imsis, date_str=date_str,
            start_epoch_int=start_epoch_int, end_epoch_int=end_epoch_int,
            sql_rti_coords=sql_rti_coords)
        cur_gp.execute(sql_coords_for_touched_imsis)
        oth_coords = cur_gp.fetchall()
        return oth_coords

    @staticmethod
    def _get_coord_dists(cur, ref_coord, coords):
        """
        May have thousands of coords so need to break into chunks.
        """
        debug = False
        coord_clauses = ["SELECT {oth_lat} AS oth_lat, {oth_lon} AS oth_lon".
            format(oth_lat=lat, oth_lon=lon) for lat, lon in coords]
        oth_coords_src = "\nUNION ALL\n".join(coord_clauses)
        ref_lat, ref_lon = ref_coord
        sql_coord_dists = """\
        SELECT
          oth_lat::float,
          oth_lon::float,
          dist_km
        FROM (
          SELECT
            oth_lat,
            oth_lon,
              (ST_DISTANCE(
                ST_GeomFromText('POINT({ref_lon} {ref_lat})',
                  {srid}),
                ST_GeomFromText('POINT(' || oth_lon || ' ' || oth_lat || ')',
                  {srid})
              )*{km_scalar}) AS
            dist_km
          FROM (
            {oth_coords_src}
          ) AS oth_coords
        ) AS dists
        ORDER BY dist_km DESC
        """.format(ref_lat=ref_lat, ref_lon=ref_lon, srid=conf.WGS84_SRID,
            km_scalar=conf.ST_DISTANCE2KM_SCALAR, oth_coords_src=oth_coords_src)
        if debug: print(sql_coord_dists)
        cur.execute(sql_coord_dists)
        coord_dists = cur.fetchall()
        return coord_dists

    @staticmethod
    def check_rti(rti, rti_coord, coords, show_map=False, force_map=True):
        """
        rti_coord -- the coordinate we are checking.

        coords -- for each IMSI within time period we collected each rti they
        also touched. We didn't care how often they touched it just that they
        did. This simplified the volume of data and analysis.

        Cases to handle:

        * People flying around e.g. mainly associated with Auckland rti's but
        also with Christchurch rti's. Extremes mess with the true centre.

        * Transit cell towers e.g. in middle of South Island. No-one stays there
        they all shift in either direction so the middle should be OK.

        * People staying put - no impact.

        Technique - reduce number of calculations by rounding coords and
        grouping, then getting distances from reference coord.

        Exclude top decile (to remove most extremes caused by flying).

        Get centroid and measure distance of that to rti_coord. Too big?
        """
        DP = 2
        if not coords:
            raise Exception("Unable to proceed with {} - no coordinates to "
                "check rti coordinate against".format(rti))
        unused, cur_local = db.Pg.local()  ## faster - and no need to connect to anything remote - all spatial calculations
        ## work with unique rounded coords to reduce number of expensive distance calculations
        lat, lon = rti_coord
        rti_coord = (round(lat, DP), round(lon, DP))  ## might as well snap this to the same grid. Simplify 
        rounded_coords = [(round(lat, DP), round(lon, DP))
            for lat, lon in coords]
        coord_freqs = dict(Counter(rounded_coords))  ## so we can re-expand the results to give appropriate weight to coords according to number of IMSI's
        coord_dists = CheckFeed._get_coord_dists(cur_local, ref_coord=rti_coord,
            coords=coord_freqs.keys())
        ## re-expand results to give appropriate weight to coords according to number of IMSI's
        all_dist_coords = []
        for oth_lat, oth_lon, dist_km in coord_dists:
            oth_coord = (oth_lat, oth_lon)
            freq = coord_freqs[oth_coord]  ## if not found then blow up - reality has failed, cats are chasing dogs etc
            all_dist_coords.extend([(dist_km, oth_coord), ]*freq)
        all_dist_coords.sort()  ## by distance
        ## eliminate the top 10% of the most distant coord records. Note - there are likely to be if lots of extreme values then most will
        unfiltered_coords2use = [coord for unused_dist_km, coord
            in all_dist_coords] 
        n_unfiltered_coords = len(unfiltered_coords2use)
        if n_unfiltered_coords < 10:  ## WAT?! Very light traffic with this tower obviously
            coords2use = unfiltered_coords2use
        else:
            n_to_exclude = round(0.1*n_unfiltered_coords)
            coords2use = unfiltered_coords2use[:-n_to_exclude]
        ## get centroid of coords
        likely_coord = spatial.Spatial.get_coords_centroid(coords2use)
        ## get distance between rti coord and probable location
        gap_km = spatial.Pg.gap_km(cur_local, rti_coord, likely_coord)
        certain_fail = (gap_km > 150)
        possible_fail = False if certain_fail else (gap_km > 30)
        if show_map:
            if (certain_fail or force_map):
                title = "Rti {} a certain mislocation".format(rti)
                filtered_coord_freqs = list(dict(Counter(coords2use)).items())
                coord_data_a = [
                    (coord, freq) for coord, freq in filtered_coord_freqs]
                coord_data_b = [
                    (rti_coord, folium_map.DEFAULT_BLACK_THRESHOLD),
                    (likely_coord, folium_map.DEFAULT_RED_THRESHOLD), ]
                folium_map.Map.map_coord_data(title,
                    lbl_a="Other coordinates for {}".format(rti),
                    coord_data_a=coord_data_a,
                    is_n_val=True, subtitle=None,
                    lbl_b=("{} coordinate (black) vs likely coordinate (red)"
                        .format(rti)),
                    coord_data_b=coord_data_b, marker_dets_fn=None,
                    icon_for_all_n_vals=False, zoom_start=5)                
            else:
                print("No map because not a certain fail and force_map not "
                    "True")
        return certain_fail, possible_fail

    @staticmethod
    def check_locations_from_tbl(cur, tblname, date_compact, local=False,
            show_map=False, force_map=True,
            sample_size=None, start_rec=None, filter_rtis=None):
        """
        Check each cell_id_rti's location against the locations of all other
        rtis its connecting IMSIs also connected to. We want one record per IMSI
        per other coordinate. If lots of IMSIs connect that is a vote for that
        location.

        OK we know where the rti is meant to be and where its surrounding rti's
        are (with emphasis according to number of IMSI's connecting). A
        contradiction?
        """
        if local:
            utils.warn("Unable to check locations against events table locally "
                "but local is True. Not running checks.")
            return
        if filter_rtis is None: filter_rtis = []
        sql_rti_coords = CheckFeed.sql_rti_coords_tpl.format(tblname=tblname)
        cur.execute(sql_rti_coords)
        rtis_coords = cur.fetchall()
        if start_rec:
            rtis_coords = rtis_coords[start_rec-1:]
        certain_fails = []
        possible_fails = []
        if sample_size:
            rtis_coords = sample(rtis_coords, sample_size)
        n = start_rec if start_rec else 1
        for rti_and_coord in rtis_coords:
            rti, tower_lat, tower_lon = rti_and_coord
            if filter_rtis and rti not in filter_rtis:
                continue
            if not spatial.Spatial.in_NZ_bb(tower_lat, tower_lon):
                n += 1
                continue  ## not even being included in data being ingested
            rti_coord = (tower_lat, tower_lon)
            oth_coords = CheckFeed.get_oth_coords(rti, tblname, date_compact)
            if not oth_coords:
                certain_fail, possible_fail = True, False
            try:
                certain_fail, possible_fail = CheckFeed.check_rti(
                    rti, rti_coord, oth_coords, show_map, force_map)
            except Exception as e:
                print("Failure reason: {}".format(e))
                certain_fail, possible_fail = False, False
            if certain_fail:
                print("Record {:,}: {} failed".format(n, rti))
                certain_fails.append(rti)
            if possible_fail:
                print("Record {:,}: {} a possible fail".format(n, rti))
                possible_fails.append(rti)
            if n % 10 == 0:
                print(utils.prog(n, 'rti checks'))
            n += 1
        print("certain_fails: {}".format(certain_fails))
        print("possible_fails: {}".format(possible_fails))

def get_con_cur(local=False):
    if local:
        con, cur = db.Pg.local()
    else:
        con, cur = db.Pg.gp(user='gpadmin')
    return con, cur

def make_feed_tbl_for_date(date_compact, local):
    if '-' in date_compact:
        raise Exception("You idiot! Date compact should NOT have hyphens "
            "e.g. 20171011 is OK but 2017-10-11 is not")
    con, cur = get_con_cur(local)
    csv_fname = GetFeed.sftp_feed(date_compact)
    tblname = MakeTbl.make_feed_tbl_from_csv(con, cur, csv_fname, date_compact,
        local)
    return tblname

def check_locations_from_date(date_compact, local,
        show_map=False, force_map=True,
        sample_size=None, start_rec=None, filter_rtis=None):
    input("Ensure VPN is up before pressing Enter key to proceed")
    tblname = make_feed_tbl_for_date(date_compact, local)
    unused, cur = get_con_cur(local)
    CheckFeed.check_locations_from_tbl(cur, tblname, date_compact, local,
        show_map, force_map, sample_size, start_rec, filter_rtis)

def check_for_corrections(date_compact, rtis, src, local=False):
    """
    Report coordinates for rtis in network configuration FEED data on given
    date. Can presumably check to see if any corrections for those rtis have
    been made in the network configuration data.
    """
    tblname = make_feed_tbl_for_date(date_compact, local)
    rtis_clause = db.Pg.nums2clause(rtis)
    sql = """\
    SELECT cell_id_rti, tower_lat, tower_lon
    FROM {tblname}
    WHERE cell_id_rti IN {rtis_clause}
    ORDER BY cell_id_rti, tower_lat, tower_lon
    """.format(tblname=tblname, rtis_clause=rtis_clause)
    unused, cur = get_con_cur(local)
    cur.execute(sql)
    date_str = (datetime.datetime.strptime(date_compact, '%Y%m%d')
        .strftime('%Y-%m-%d'))
    print("The following location corrections were supplied in '{}'"
        .format(src))
    print("Have they been made in the FEED csv for '{}'?".format(date_str))
    print("Check against the following:")
    for row in cur.fetchall():
        print(row)

def _fpath2date_str(fpath):
    start_idx = fpath.index(START) + len(START)
    compact_date = fpath[start_idx:start_idx + len('YYYYMMDD')]
    date_str = (datetime.datetime.strptime(compact_date, '%Y%m%d')
        .strftime('%Y-%m-%d'))
    return date_str

def check_for_data_gaps():
    debug = True
    verbose = False
    fnames = sorted(os.listdir(FEED_FOLDER_LOCAL))
    fpaths = [os.path.join(FEED_FOLDER_LOCAL, fname) for fname in fnames]
    rti_date_strs = defaultdict(set)
    for fpath in fpaths:
        date_str = _fpath2date_str(fpath)
        with open(fpath, encoding='utf-8', errors='ignore') as f:
            reader = csv.reader(f)
            for row in reader:
                if debug and verbose: print(row)
                rti = row[MakeTbl.IDX_RTI]
                rti_date_strs[rti].add(date_str)
    rtis = sorted(list(rti_date_strs.keys()))
    rti_ranges = {}
    for rti in rtis:
        date_strs = sorted(list(rti_date_strs[rti]))
        date_ranges = dates.Dates.date_strs_to_date_ranges(date_strs)
        rti_ranges[rti] = date_ranges
    range_types = defaultdict(list)
    for rti in rtis:
        range_types[tuple(rti_ranges[rti])].append(rti)
    for range_type, rtis in sorted(range_types.items(),
            key=lambda t: (-len(t[1]), t[0])):  ## sort on - so big first but dates forwards as expected
        print(range_type, len(rtis))

def check_for_tech_class_loss():
    debug = False
    verbose = False
    fnames = sorted(os.listdir(FEED_FOLDER_LOCAL))
    fpaths = [os.path.join(FEED_FOLDER_LOCAL, fname) for fname in fnames]
    for fpath in fpaths:
        date_str = _fpath2date_str(fpath)
        tech_classes = []
        with open(fpath, encoding='utf-8', errors='ignore') as f:
            reader = csv.reader(f)
            for row in reader:
                if debug and verbose: print(row)
                tech_class = row[MakeTbl.IDX_TECH_CLASS]
                tech_classes.append(tech_class)
        tech_class_freqs = dict(Counter(tech_classes))
        print(tech_class_freqs)

def compare_with_mod_p(date_compact, rtis2compare):
    unused, cur_local = db.Pg.local()
    feed_tblname = 'feed_{}'.format(date_compact)
    rtis_clause = db.Pg.nums2clause(rtis2compare)
    sql = """\
    SELECT
      mod.cell_id_rti,
        mod.tower_lat AS
      mod_lat,
        mod.tower_lon AS
      mod_lon,
        feed.tower_lat AS
      feed_lat,
        feed.tower_lon AS
      feed_lon
    FROM {mod} AS mod
    INNER JOIN
    {feed} AS feed
    USING (cell_id_rti)
    WHERE mod.cell_id_rti IN {rtis_clause}
    AND mod.effective_end IS NULL
    ORDER BY cell_id_rti
    """.format(mod=conf.MOD_P_LOCATIONS, feed=feed_tblname,
        rtis_clause=rtis_clause)
    cur_local.execute(sql)
    data = cur_local.fetchall()
    for row in data:
        rti = row['cell_id_rti']
        coord_mod = (row['mod_lat'], row['mod_lon'])
        coord_feed = (row['feed_lat'], row['feed_lon'])
        dist_km = spatial.Pg.gap_km(cur_local, coord_mod, coord_feed)
        print("RTI {}: Mod P {}, Feed {}, {} km apart".format(rti, coord_mod,
            coord_feed, dist_km))

def main():
    #make_feed_tbl_for_date(date_compact='20171011', local=True); return
    
    #rtis2compare = [20165, 31861, 31864, 31901, 31902, 31903, 31904, 31905,
    #    31906, 46941, 46942, 46944, 46945, 261663, 261665, 261743, 261763,
    #    400761, ]
    #compare_with_mod_p(date_compact='20171003', rtis2compare=rtis2compare); return
    
    #utils.get_daily_tots(rti=12180); return

    #check_for_tech_class_loss(); return
    #check_for_data_gaps(); return
#     filter_rtis = [20141, 20142, 20165, 31861, 31864, 31901, 31902, 31903,
#     31904, 31905, 31906, 46941, 46942, 46944, 46945, 261663, 261664, 261665,
#     261743, 261753, 261763, 400751, 400761]
    filter_rtis = [20165, ]
    check_locations_from_date(date_compact='20171010', local=False,
        show_map=True, force_map=True, filter_rtis=filter_rtis); return

    src = "initial_feed_correction_2017-09-14.ods"
    rtis = [
        31861, 31864,
        31901, 31902, 31903, 31904, 31905, 31906,
    ]
    check_for_corrections(date_compact='20171003', rtis=rtis, src=src,
        local=False)

if __name__ == '__main__':
    main()
