#! /usr/bin/env python3

import datetime
from pprint import pprint as pp
import shutil
from webbrowser import open_new_tab

import requests

from voyager_shared import conf, dates, db, find_relocations as findrel, utils #@UnusedImport


def get_dets_for_sig_rtis_missing_locations(reuse_stale=False,
        use_unfixed_csm=False):
    """
    For each cell_id_rti look at n distinct imsis per day that are over 100
    (i.e. ignore the dribblers) and get the average of those days. Filter down
    to those with an average of more than some number e.g. 500. Then see if
    matching location data in CSM 2.0.
    """
    debug = True
    stale_missing_rti_dets = [
    {'sig_range': 896, 'rti': 0, 'avg_imsis': 29473, 'min_sig_date': '2014-12-18', 'max_sig_date': '2017-06-01'},
    {'sig_range': 146, 'rti': 3337, 'avg_imsis': 3058, 'min_sig_date': '2015-09-24', 'max_sig_date': '2016-02-17'},
    {'sig_range': 15, 'rti': 25941, 'avg_imsis': 1311, 'min_sig_date': '2015-05-29', 'max_sig_date': '2015-06-13'},
    {'sig_range': 28, 'rti': 26126, 'avg_imsis': 1162, 'min_sig_date': '2016-11-24', 'max_sig_date': '2016-12-22'},
    {'sig_range': 102, 'rti': 27161, 'avg_imsis': 1534, 'min_sig_date': '2016-09-11', 'max_sig_date': '2016-12-22'},
    {'sig_range': 102, 'rti': 27164, 'avg_imsis': 1487, 'min_sig_date': '2016-09-11', 'max_sig_date': '2016-12-22'},
    {'sig_range': 101, 'rti': 38871, 'avg_imsis': 2407, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-03-29'},
    {'sig_range': 101, 'rti': 38872, 'avg_imsis': 2996, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-03-29'},
    {'sig_range': 101, 'rti': 38873, 'avg_imsis': 2471, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-03-29'},
    {'sig_range': 101, 'rti': 38874, 'avg_imsis': 3369, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-03-29'},
    {'sig_range': 101, 'rti': 38875, 'avg_imsis': 3698, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-03-29'},
    {'sig_range': 101, 'rti': 38876, 'avg_imsis': 2998, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-03-29'},
    {'sig_range': 144, 'rti': 40348, 'avg_imsis': 776, 'min_sig_date': '2016-07-22', 'max_sig_date': '2016-12-13'},
    {'sig_range': 145, 'rti': 40349, 'avg_imsis': 1362, 'min_sig_date': '2016-07-21', 'max_sig_date': '2016-12-13'},
    {'sig_range': 101, 'rti': 40920, 'avg_imsis': 2439, 'min_sig_date': '2016-09-12', 'max_sig_date': '2016-12-22'},
    {'sig_range': 147, 'rti': 44131, 'avg_imsis': 2375, 'min_sig_date': '2016-06-28', 'max_sig_date': '2016-11-22'},
    {'sig_range': 147, 'rti': 44132, 'avg_imsis': 1662, 'min_sig_date': '2016-06-28', 'max_sig_date': '2016-11-22'},
    {'sig_range': 147, 'rti': 44133, 'avg_imsis': 1077, 'min_sig_date': '2016-06-28', 'max_sig_date': '2016-11-22'},
    {'sig_range': 47, 'rti': 54871, 'avg_imsis': 502, 'min_sig_date': '2016-05-20', 'max_sig_date': '2016-07-06'},
    {'sig_range': 47, 'rti': 54874, 'avg_imsis': 909, 'min_sig_date': '2016-05-20', 'max_sig_date': '2016-07-06'},
    {'sig_range': 47, 'rti': 54877, 'avg_imsis': 609, 'min_sig_date': '2016-05-20', 'max_sig_date': '2016-07-06'},
    {'sig_range': 579, 'rti': 54902, 'avg_imsis': 2252, 'min_sig_date': '2015-09-10', 'max_sig_date': '2017-04-11'},
    {'sig_range': 120, 'rti': 58163, 'avg_imsis': 781, 'min_sig_date': '2015-06-30', 'max_sig_date': '2015-10-28'},
    {'sig_range': 120, 'rti': 58166, 'avg_imsis': 761, 'min_sig_date': '2015-06-30', 'max_sig_date': '2015-10-28'},
    {'sig_range': 897, 'rti': 65535, 'avg_imsis': 309577, 'min_sig_date': '2014-12-18', 'max_sig_date': '2017-06-02'},
    {'sig_range': 40, 'rti': 262176, 'avg_imsis': 3449, 'min_sig_date': '2016-04-14', 'max_sig_date': '2016-05-24'},
    {'sig_range': 176, 'rti': 264223, 'avg_imsis': 3324, 'min_sig_date': '2016-04-14', 'max_sig_date': '2016-10-07'},
    {'sig_range': 212, 'rti': 264735, 'avg_imsis': 3058, 'min_sig_date': '2016-04-14', 'max_sig_date': '2016-11-12'},
    {'sig_range': 212, 'rti': 264736, 'avg_imsis': 2122, 'min_sig_date': '2016-04-14', 'max_sig_date': '2016-11-12'},
    {'sig_range': 173, 'rti': 274463, 'avg_imsis': 1902, 'min_sig_date': '2016-04-14', 'max_sig_date': '2016-10-04'},
    {'sig_range': 173, 'rti': 274464, 'avg_imsis': 3459, 'min_sig_date': '2016-04-14', 'max_sig_date': '2016-10-04'},
    {'sig_range': 15, 'rti': 289135, 'avg_imsis': 5562, 'min_sig_date': '2017-05-18', 'max_sig_date': '2017-06-02'},
    {'sig_range': 15, 'rti': 289145, 'avg_imsis': 2956, 'min_sig_date': '2017-05-18', 'max_sig_date': '2017-06-02'},
    {'sig_range': 15, 'rti': 289155, 'avg_imsis': 2661, 'min_sig_date': '2017-05-18', 'max_sig_date': '2017-06-02'},
    {'sig_range': 180, 'rti': 311921, 'avg_imsis': 846, 'min_sig_date': '2015-06-21', 'max_sig_date': '2015-12-18'},
    {'sig_range': 173, 'rti': 316027, 'avg_imsis': 3798, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-06-09'},
    {'sig_range': 16, 'rti': 363891, 'avg_imsis': 1139, 'min_sig_date': '2015-05-28', 'max_sig_date': '2015-06-13'},
    {'sig_range': 31, 'rti': 364183, 'avg_imsis': 986, 'min_sig_date': '2015-05-13', 'max_sig_date': '2015-06-13'},
    {'sig_range': 21, 'rti': 385567, 'avg_imsis': 1124, 'min_sig_date': '2015-11-03', 'max_sig_date': '2015-11-24'},
    {'sig_range': 21, 'rti': 385568, 'avg_imsis': 1341, 'min_sig_date': '2015-11-03', 'max_sig_date': '2015-11-24'},
    {'sig_range': 20, 'rti': 385569, 'avg_imsis': 1877, 'min_sig_date': '2015-11-04', 'max_sig_date': '2015-11-24'},
    {'sig_range': 20, 'rti': 385823, 'avg_imsis': 2194, 'min_sig_date': '2015-11-04', 'max_sig_date': '2015-11-24'},
    {'sig_range': 21, 'rti': 385824, 'avg_imsis': 4312, 'min_sig_date': '2015-11-03', 'max_sig_date': '2015-11-24'},
    {'sig_range': 21, 'rti': 385825, 'avg_imsis': 2832, 'min_sig_date': '2015-11-03', 'max_sig_date': '2015-11-24'},
    {'sig_range': 27, 'rti': 396145, 'avg_imsis': 1626, 'min_sig_date': '2015-09-30', 'max_sig_date': '2015-10-27'},
    {'sig_range': 27, 'rti': 396155, 'avg_imsis': 2580, 'min_sig_date': '2015-09-30', 'max_sig_date': '2015-10-27'},
    {'sig_range': 147, 'rti': 399235, 'avg_imsis': 1843, 'min_sig_date': '2015-11-19', 'max_sig_date': '2016-04-14'},
    {'sig_range': 39, 'rti': 404337, 'avg_imsis': 526, 'min_sig_date': '2015-12-16', 'max_sig_date': '2016-01-24'},
    {'sig_range': 40, 'rti': 404345, 'avg_imsis': 1977, 'min_sig_date': '2015-12-15', 'max_sig_date': '2016-01-24'},
    {'sig_range': 39, 'rti': 404347, 'avg_imsis': 2532, 'min_sig_date': '2015-12-16', 'max_sig_date': '2016-01-24'},
    {'sig_range': 17, 'rti': 448121, 'avg_imsis': 997, 'min_sig_date': '2017-05-15', 'max_sig_date': '2017-06-01'},
    {'sig_range': 30, 'rti': 453497, 'avg_imsis': 611, 'min_sig_date': '2017-05-02', 'max_sig_date': '2017-06-01'},
    {'sig_range': 35, 'rti': 453999, 'avg_imsis': 875, 'min_sig_date': '2017-04-28', 'max_sig_date': '2017-06-02'},
    {'sig_range': 35, 'rti': 454009, 'avg_imsis': 1771, 'min_sig_date': '2017-04-28', 'max_sig_date': '2017-06-02'},
    {'sig_range': 22, 'rti': 460399, 'avg_imsis': 4676, 'min_sig_date': '2017-05-11', 'max_sig_date': '2017-06-02'},
    {'sig_range': 22, 'rti': 460409, 'avg_imsis': 2751, 'min_sig_date': '2017-05-11', 'max_sig_date': '2017-06-02'},
    {'sig_range': 22, 'rti': 460419, 'avg_imsis': 2472, 'min_sig_date': '2017-05-11', 'max_sig_date': '2017-06-02'},
    {'sig_range': 15, 'rti': 465009, 'avg_imsis': 17567, 'min_sig_date': '2017-05-18', 'max_sig_date': '2017-06-02'},
    {'sig_range': 15, 'rti': 465029, 'avg_imsis': 12677, 'min_sig_date': '2017-05-18', 'max_sig_date': '2017-06-02'},
    {'sig_range': 672, 'rti': 12807457, 'avg_imsis': 1987, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-10-21'},
    {'sig_range': 512, 'rti': 12815135, 'avg_imsis': 3194, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-13'},
    {'sig_range': 320, 'rti': 12831263, 'avg_imsis': 1809, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-11-03'},
    {'sig_range': 320, 'rti': 12831264, 'avg_imsis': 3914, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-11-03'},
    {'sig_range': 320, 'rti': 12831265, 'avg_imsis': 2384, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-11-03'},
    {'sig_range': 875, 'rti': 12832032, 'avg_imsis': 3226, 'min_sig_date': '2014-12-18', 'max_sig_date': '2017-05-11'},
    {'sig_range': 875, 'rti': 12832033, 'avg_imsis': 3820, 'min_sig_date': '2014-12-18', 'max_sig_date': '2017-05-11'},
    {'sig_range': 722, 'rti': 12837407, 'avg_imsis': 3891, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-12-09'},
    {'sig_range': 722, 'rti': 12837408, 'avg_imsis': 3340, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-12-09'},
    {'sig_range': 722, 'rti': 12837409, 'avg_imsis': 4067, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-12-09'},
    {'sig_range': 511, 'rti': 12844831, 'avg_imsis': 3038, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-12'},
    {'sig_range': 511, 'rti': 12844832, 'avg_imsis': 2709, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-12'},
    {'sig_range': 517, 'rti': 12856863, 'avg_imsis': 3144, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-18'},
    {'sig_range': 517, 'rti': 12856864, 'avg_imsis': 3274, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-18'},
    {'sig_range': 517, 'rti': 12856865, 'avg_imsis': 2904, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-18'},
    {'sig_range': 510, 'rti': 12860703, 'avg_imsis': 2887, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-11'},
    {'sig_range': 510, 'rti': 12860704, 'avg_imsis': 3377, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-11'},
    {'sig_range': 657, 'rti': 12862751, 'avg_imsis': 3716, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-05'},
    {'sig_range': 518, 'rti': 12863519, 'avg_imsis': 1286, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-05-20'},
    {'sig_range': 519, 'rti': 12863521, 'avg_imsis': 2240, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-20'},
    {'sig_range': 481, 'rti': 12868107, 'avg_imsis': 861, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-04-13'},
    {'sig_range': 481, 'rti': 12868108, 'avg_imsis': 701, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-04-13'},
    {'sig_range': 483, 'rti': 12868109, 'avg_imsis': 1645, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-04-14'},
    {'sig_range': 483, 'rti': 12868147, 'avg_imsis': 2196, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-04-14'},
    {'sig_range': 483, 'rti': 12868148, 'avg_imsis': 3890, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-04-14'},
    {'sig_range': 483, 'rti': 12868149, 'avg_imsis': 3955, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-04-14'},
    {'sig_range': 425, 'rti': 12870431, 'avg_imsis': 2695, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-02-16'},
    {'sig_range': 424, 'rti': 12870432, 'avg_imsis': 1357, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-02-16'},
    {'sig_range': 425, 'rti': 12870433, 'avg_imsis': 2449, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-02-16'},
    {'sig_range': 195, 'rti': 12871967, 'avg_imsis': 947, 'min_sig_date': '2014-12-19', 'max_sig_date': '2015-07-02'},
    {'sig_range': 509, 'rti': 12909599, 'avg_imsis': 1838, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-05-11'},
    {'sig_range': 509, 'rti': 12909601, 'avg_imsis': 1520, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-05-11'},
    {'sig_range': 525, 'rti': 12910111, 'avg_imsis': 3349, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-26'},
    {'sig_range': 525, 'rti': 12910112, 'avg_imsis': 2880, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-26'},
    {'sig_range': 519, 'rti': 12911391, 'avg_imsis': 2787, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-20'},
    {'sig_range': 525, 'rti': 12911647, 'avg_imsis': 3067, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-26'},
    {'sig_range': 525, 'rti': 12911648, 'avg_imsis': 2198, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-26'},
    {'sig_range': 525, 'rti': 12911649, 'avg_imsis': 2205, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-26'},
    {'sig_range': 673, 'rti': 12911905, 'avg_imsis': 3392, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-21'},
    {'sig_range': 644, 'rti': 12935200, 'avg_imsis': 1882, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-09-22'},
    {'sig_range': 325, 'rti': 12944671, 'avg_imsis': 1687, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-11-08'},
    {'sig_range': 325, 'rti': 12944672, 'avg_imsis': 2394, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-11-08'},
    {'sig_range': 545, 'rti': 12952095, 'avg_imsis': 734, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-06-16'},
    {'sig_range': 510, 'rti': 12954145, 'avg_imsis': 3276, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-11'},
    {'sig_range': 185, 'rti': 12954655, 'avg_imsis': 977, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-06-21'},
    {'sig_range': 522, 'rti': 12957473, 'avg_imsis': 2265, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-23'},
    {'sig_range': 482, 'rti': 13045023, 'avg_imsis': 1776, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-04-14'},
    {'sig_range': 420, 'rti': 13055519, 'avg_imsis': 3967, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-02-11'},
    {'sig_range': 419, 'rti': 13055521, 'avg_imsis': 1590, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-02-11'},
    {'sig_range': 663, 'rti': 13078559, 'avg_imsis': 3234, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-11'},
    {'sig_range': 663, 'rti': 13078560, 'avg_imsis': 3228, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-11'},
    {'sig_range': 482, 'rti': 13128991, 'avg_imsis': 1812, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-04-13'},
    {'sig_range': 482, 'rti': 13128992, 'avg_imsis': 1268, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-04-14'},
    {'sig_range': 661, 'rti': 13188383, 'avg_imsis': 612, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-10-10'},
    {'sig_range': 662, 'rti': 13188384, 'avg_imsis': 3436, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-10'},
    {'sig_range': 662, 'rti': 13188385, 'avg_imsis': 3726, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-10'},
    {'sig_range': 874, 'rti': 13221663, 'avg_imsis': 1093, 'min_sig_date': '2014-12-19', 'max_sig_date': '2017-05-11'},
    {'sig_range': 875, 'rti': 13221664, 'avg_imsis': 1760, 'min_sig_date': '2014-12-18', 'max_sig_date': '2017-05-11'},
    {'sig_range': 875, 'rti': 13221665, 'avg_imsis': 1845, 'min_sig_date': '2014-12-18', 'max_sig_date': '2017-05-11'},
    {'sig_range': 529, 'rti': 13304608, 'avg_imsis': 3642, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-30'},
    {'sig_range': 529, 'rti': 13304609, 'avg_imsis': 3251, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-30'},
    {'sig_range': 778, 'rti': 13307679, 'avg_imsis': 1498, 'min_sig_date': '2014-12-18', 'max_sig_date': '2017-02-03'},
    {'sig_range': 778, 'rti': 13307680, 'avg_imsis': 3375, 'min_sig_date': '2014-12-18', 'max_sig_date': '2017-02-03'},
    {'sig_range': 778, 'rti': 13307681, 'avg_imsis': 1896, 'min_sig_date': '2014-12-18', 'max_sig_date': '2017-02-03'},
    {'sig_range': 482, 'rti': 13829407, 'avg_imsis': 2018, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-04-13'},
    {'sig_range': 515, 'rti': 13829919, 'avg_imsis': 3123, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-16'},
    {'sig_range': 515, 'rti': 13829920, 'avg_imsis': 3304, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-16'},
    {'sig_range': 515, 'rti': 13830175, 'avg_imsis': 3152, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-16'},
    {'sig_range': 519, 'rti': 13832481, 'avg_imsis': 1790, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-20'},
    {'sig_range': 649, 'rti': 13835551, 'avg_imsis': 3326, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-09-28'},
    {'sig_range': 650, 'rti': 13835552, 'avg_imsis': 3016, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-09-28'},
    {'sig_range': 649, 'rti': 13835553, 'avg_imsis': 2304, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-09-28'},
    {'sig_range': 652, 'rti': 13837343, 'avg_imsis': 3428, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-09-30'},
    {'sig_range': 652, 'rti': 13837344, 'avg_imsis': 3546, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-09-30'},
    {'sig_range': 519, 'rti': 13841184, 'avg_imsis': 2524, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-20'},
    {'sig_range': 523, 'rti': 13844000, 'avg_imsis': 3597, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-24'},
    {'sig_range': 518, 'rti': 13853215, 'avg_imsis': 1351, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-19'},
    {'sig_range': 518, 'rti': 13853216, 'avg_imsis': 1594, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-19'},
    {'sig_range': 517, 'rti': 13853217, 'avg_imsis': 1094, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-05-19'},
    {'sig_range': 511, 'rti': 13853472, 'avg_imsis': 1618, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-12'},
    {'sig_range': 518, 'rti': 13855264, 'avg_imsis': 3987, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-19'},
    {'sig_range': 518, 'rti': 13855265, 'avg_imsis': 2860, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-19'},
    {'sig_range': 728, 'rti': 13857823, 'avg_imsis': 2662, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-12-15'},
    {'sig_range': 728, 'rti': 13857824, 'avg_imsis': 2115, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-12-15'},
    {'sig_range': 728, 'rti': 13857825, 'avg_imsis': 2066, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-12-15'},
    {'sig_range': 523, 'rti': 13861663, 'avg_imsis': 2984, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-24'},
    {'sig_range': 523, 'rti': 13861665, 'avg_imsis': 3693, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-24'},
    {'sig_range': 517, 'rti': 13862175, 'avg_imsis': 3341, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-18'},
    {'sig_range': 516, 'rti': 13862177, 'avg_imsis': 817, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-05-18'},
    {'sig_range': 526, 'rti': 13864735, 'avg_imsis': 2896, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-27'},
    {'sig_range': 526, 'rti': 13865761, 'avg_imsis': 3537, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-27'},
    {'sig_range': 680, 'rti': 13871393, 'avg_imsis': 2513, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-28'},
    {'sig_range': 483, 'rti': 13871647, 'avg_imsis': 3802, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-04-14'},
    {'sig_range': 483, 'rti': 13871648, 'avg_imsis': 2458, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-04-14'},
    {'sig_range': 483, 'rti': 13871649, 'avg_imsis': 3800, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-04-14'},
    {'sig_range': 683, 'rti': 13879327, 'avg_imsis': 2422, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-31'},
    {'sig_range': 517, 'rti': 13881119, 'avg_imsis': 1584, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-18'},
    {'sig_range': 516, 'rti': 13881120, 'avg_imsis': 1022, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-05-18'},
    {'sig_range': 517, 'rti': 13881121, 'avg_imsis': 1908, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-18'},
    {'sig_range': 320, 'rti': 13882143, 'avg_imsis': 957, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-11-03'},
    {'sig_range': 320, 'rti': 13882144, 'avg_imsis': 1247, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-11-03'},
    {'sig_range': 320, 'rti': 13882145, 'avg_imsis': 1647, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-11-03'},
    {'sig_range': 662, 'rti': 13894687, 'avg_imsis': 1190, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-10'},
    {'sig_range': 662, 'rti': 13894688, 'avg_imsis': 1459, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-10'},
    {'sig_range': 662, 'rti': 13894689, 'avg_imsis': 2991, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-10'},
    {'sig_range': 681, 'rti': 13895199, 'avg_imsis': 2476, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-10-30'},
    {'sig_range': 710, 'rti': 13897247, 'avg_imsis': 3734, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-11-27'},
    {'sig_range': 710, 'rti': 13897248, 'avg_imsis': 2482, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-11-27'},
    {'sig_range': 710, 'rti': 13897249, 'avg_imsis': 2200, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-11-27'},
    {'sig_range': 707, 'rti': 13901087, 'avg_imsis': 3643, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-11-24'},
    {'sig_range': 707, 'rti': 13901089, 'avg_imsis': 3373, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-11-24'},
    {'sig_range': 29, 'rti': 13903647, 'avg_imsis': 1940, 'min_sig_date': '2015-02-10', 'max_sig_date': '2015-03-11'},
    {'sig_range': 29, 'rti': 13903648, 'avg_imsis': 1613, 'min_sig_date': '2015-02-10', 'max_sig_date': '2015-03-11'},
    {'sig_range': 502, 'rti': 13912863, 'avg_imsis': 2180, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-03'},
    {'sig_range': 502, 'rti': 13912864, 'avg_imsis': 2494, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-03'},
    {'sig_range': 502, 'rti': 13912865, 'avg_imsis': 2248, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-03'},
    {'sig_range': 516, 'rti': 13920287, 'avg_imsis': 800, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-05-18'},
    {'sig_range': 517, 'rti': 13920289, 'avg_imsis': 3743, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-18'},
    {'sig_range': 516, 'rti': 13921312, 'avg_imsis': 2325, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-05-18'},
    {'sig_range': 483, 'rti': 13929248, 'avg_imsis': 3902, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-04-14'},
    {'sig_range': 682, 'rti': 13929759, 'avg_imsis': 2610, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-30'},
    {'sig_range': 682, 'rti': 13929760, 'avg_imsis': 2067, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-30'},
    {'sig_range': 682, 'rti': 13929761, 'avg_imsis': 3836, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-30'},
    {'sig_range': 681, 'rti': 13930785, 'avg_imsis': 3396, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-10-30'},
    {'sig_range': 52, 'rti': 13942816, 'avg_imsis': 1132, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-02-08'},
    {'sig_range': 19, 'rti': 13945119, 'avg_imsis': 1735, 'min_sig_date': '2015-02-05', 'max_sig_date': '2015-02-24'},
    {'sig_range': 19, 'rti': 13945120, 'avg_imsis': 1868, 'min_sig_date': '2015-02-05', 'max_sig_date': '2015-02-24'},
    {'sig_range': 30, 'rti': 13947699, 'avg_imsis': 1017, 'min_sig_date': '2015-02-11', 'max_sig_date': '2015-03-13'},
    {'sig_range': 700, 'rti': 15114783, 'avg_imsis': 752, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-11-18'},
    {'sig_range': 656, 'rti': 15115041, 'avg_imsis': 882, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-10-05'},
    {'sig_range': 265, 'rti': 15115295, 'avg_imsis': 1591, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-09-09'},
    {'sig_range': 265, 'rti': 15115296, 'avg_imsis': 1622, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-09-09'},
    {'sig_range': 265, 'rti': 15115297, 'avg_imsis': 2168, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-09-09'},
    {'sig_range': 335, 'rti': 15129375, 'avg_imsis': 2598, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-11-18'},
    {'sig_range': 335, 'rti': 15129376, 'avg_imsis': 1488, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-11-18'},
    {'sig_range': 336, 'rti': 15129377, 'avg_imsis': 2558, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-11-19'},
    {'sig_range': 52, 'rti': 15129459, 'avg_imsis': 2695, 'min_sig_date': '2015-09-27', 'max_sig_date': '2015-11-18'},
    {'sig_range': 52, 'rti': 15129469, 'avg_imsis': 1229, 'min_sig_date': '2015-09-27', 'max_sig_date': '2015-11-18'},
    {'sig_range': 52, 'rti': 15129479, 'avg_imsis': 2317, 'min_sig_date': '2015-09-27', 'max_sig_date': '2015-11-18'},
    {'sig_range': 484, 'rti': 15133727, 'avg_imsis': 3524, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-04-15'},
    {'sig_range': 484, 'rti': 15133728, 'avg_imsis': 1617, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-04-15'},
    {'sig_range': 484, 'rti': 15133729, 'avg_imsis': 3098, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-04-15'},
    {'sig_range': 654, 'rti': 15134241, 'avg_imsis': 944, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-10-03'},
    {'sig_range': 515, 'rti': 15135521, 'avg_imsis': 3598, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-16'},
    {'sig_range': 670, 'rti': 15137311, 'avg_imsis': 3719, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-18'},
    {'sig_range': 670, 'rti': 15137313, 'avg_imsis': 3047, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-18'},
    {'sig_range': 685, 'rti': 15143711, 'avg_imsis': 2634, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-11-02'},
    {'sig_range': 685, 'rti': 15143713, 'avg_imsis': 3940, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-11-02'},
    {'sig_range': 173, 'rti': 15144735, 'avg_imsis': 679, 'min_sig_date': '2014-12-19', 'max_sig_date': '2015-06-10'},
    {'sig_range': 174, 'rti': 15144736, 'avg_imsis': 1180, 'min_sig_date': '2014-12-18', 'max_sig_date': '2015-06-10'},
    {'sig_range': 173, 'rti': 15144737, 'avg_imsis': 897, 'min_sig_date': '2014-12-19', 'max_sig_date': '2015-06-10'},
    {'sig_range': 483, 'rti': 15156511, 'avg_imsis': 1340, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-04-14'},
    {'sig_range': 483, 'rti': 15156512, 'avg_imsis': 2284, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-04-14'},
    {'sig_range': 483, 'rti': 15156513, 'avg_imsis': 4018, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-04-14'},
    {'sig_range': 665, 'rti': 15161119, 'avg_imsis': 3053, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-10-13'},
    {'sig_range': 526, 'rti': 15166495, 'avg_imsis': 3695, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-27'},
    {'sig_range': 526, 'rti': 15166496, 'avg_imsis': 3657, 'min_sig_date': '2014-12-18', 'max_sig_date': '2016-05-27'},
    {'sig_range': 525, 'rti': 15166497, 'avg_imsis': 2178, 'min_sig_date': '2014-12-19', 'max_sig_date': '2016-05-27'},
    {'sig_range': 784, 'rti': 15168031, 'avg_imsis': 2432, 'min_sig_date': '2014-12-18', 'max_sig_date': '2017-02-09'},
    {'sig_range': 784, 'rti': 15168032, 'avg_imsis': 2788, 'min_sig_date': '2014-12-18', 'max_sig_date': '2017-02-09'},
    ]
    if reuse_stale:
        utils.warn("Using stale missing_rti_dets as at approx end of May 2017")
        missing_rti_dets = stale_missing_rti_dets
    else:
        csm2use = (conf.CSM2_UNFIXED_GREENPLUM if use_unfixed_csm
            else conf.CSM2_GREENPLUM)
        unused_con_rem, cur_rem = db.Pg.get_rem()
        sql = """\
        SELECT *
        FROM (
          SELECT
            sig_rtis.cell_id_rti AS
          rti,
          avg_imsis,
          min_sig_date,
          max_sig_date,
            DATE_PART('day', max_sig_date::timestamp - min_sig_date::timestamp)::int AS
          sig_range
          FROM (
            SELECT
            cell_id_rti,
              ROUND(AVG(n_imsis)::numeric)::int AS
            avg_imsis,
              MIN(date) AS
            min_sig_date,
              MAX(date) AS
            max_sig_date
            FROM (
              SELECT cell_id_rti, date, SUM(distinct_imsi) AS n_imsis
              FROM {daily_connections}
              GROUP BY cell_id_rti, date
              HAVING SUM(distinct_imsi) > 100
            ) AS rti_imsi_non_dribbing_freqs
            GROUP BY cell_id_rti
            HAVING AVG(n_imsis) > 500  
          ) AS sig_rtis
          LEFT JOIN
          (
            SELECT DISTINCT cell_id_rti  -- no point fussing about whether we have dates matching at this point - how many have nothing there ever?
            FROM {csm2use}
          ) AS csm2_rtis
          USING(cell_id_rti)
          WHERE csm2_rtis.cell_id_rti IS NULL
        ) AS key_data
        WHERE sig_range > 14 
        ORDER BY rti -- avg_imsis DESC
        """.format(csm2use=csm2use,
            daily_connections=conf.DAILY_CONNECTION_DATA)
        cur_rem.execute(sql)
        print(str(cur_rem.query, encoding='utf-8'))
        missing_rti_dets = [dict(row) for row in cur_rem.fetchall()]
    if debug:
        print("[")
        for row in missing_rti_dets:
            print("    {},".format(row))
        print("]")
    return missing_rti_dets


class Locator():
    """
    Based heavily on Daniel's code.
    """
    api_key = "AIzaSyBnLnNEuxl5vihsznzOR3s1kiTbmuTyzSg"
    url = "https://www.googleapis.com/geolocation/v1/geolocate?key=" + api_key
    carrier_name = "Spark NZ"
    mcc = 530
    mnc = 5

    @staticmethod
    def _handle_resp(obj, show_map=True):
        response = requests.post(Locator.url, json=obj)
        data = response.json()
        if "error" not in data:
            lat = data["location"]["lat"]
            lon = data["location"]["lng"]
            cell_id = obj["cellTowers"][0]["cellId"]
            print("{}, {}, {}".format(cell_id, lat, lon))
            if show_map:
                url = "https://www.google.co.nz/maps/place/{},{}".format(lat, lon)
                print(url)
                open_new_tab(url)
        else:
            print("Error: {}".format(
                response.json()["error"]["errors"][0]["reason"]))
            lat, lon = None, None
        return lat, lon

    @staticmethod
    def get_location(rti, lac_code, show_map=True):
        tower_data = [
            {
                "cellId": rti,
                "locationAreaCode": lac_code,
                "mobileCountryCode": Locator.mcc,
                "mobileNetworkCode": Locator.mnc,
                "age": 0,
            }]
        post_data = {
            "homeMobileCountryCode": Locator.mcc,
            "homeMobileNetworkCode": Locator.mnc,
            "radioType": "lte",  ## should only use LTE because we won't find a match on 3G 2**16xRNC + cell_id
            "carrier": Locator.carrier_name,
            "cellTowers": tower_data,
            "considerIp": 'false',
        }
        lat, lon = Locator._handle_resp(post_data, show_map)
        return lat, lon    


def map_likely_locations_for_missing_rtis(cur_local, date_str, missing_rtis):
    start_datetime_str = date_str + ' 00:00:00'
    end_datetime_str = date_str + ' 23:59:59'
    for rti in missing_rtis:
        findrel.FindRelocations.get_likely_location_dts(cur_local,
            start_datetime_str, end_datetime_str, rti, rtis_to_exclude=None)

def google_locate_rtis(rtis):
    for rti in rtis:
        Locator.get_location(rti, lac_code=1)

def display_spans(rtis):
    for rti in rtis:
        findrel.FindRelocations.get_daily_tots(rti)
        input("Record span(s) then press any key to proceed: ")

def _extract_cell_id_node_ids(rti):
    candidate_node_id = int(rti/256)
    candidate_cell_id = rti - (256*candidate_node_id)
    match = isinstance(candidate_cell_id, int)
    if match:
        dets4G = {
            'rti': rti,
            'node_id': candidate_node_id,
            'cell_id': candidate_cell_id,
        }
    else:
        dets4G = None
    return dets4G

def find_4G_rtis_dets(rtis):
    dets4Gs = []
    for rti in rtis:
        dets4G = _extract_cell_id_node_ids(rti)
        if dets4G:
            dets4Gs.append(dets4G)
    pp(dets4Gs)

def rangify(num, dp=1):
    factor = 10**dp
    multiplied = factor*num
    integered = int(multiplied)
    lower_mult = integered
    upper_mult = lower_mult + 1
    lower = lower_mult/factor
    upper = upper_mult/factor
    return lower, upper

def match_in_csm2(cur_local, rti, lat, lon, verbose=False):
    dets4G = find_4G_rtis_dets([rti, ])
    if dets4G:
        min_lat, max_lat = rangify(lat, dp=1)
        min_lon, max_lon = rangify(lon, dp=1)
        cell_id = dets4G['candidate_cell_id']
        sql = """\
        SELECT *
        FROM {mod_p_locations}
        WHERE cell_id = {cell_id}
        AND tower_lat BETWEEN {min_lat} AND {max_lat}
        AND tower_lon BETWEEN {min_lon} AND {max_lon}
        ORDER BY tower_lat, tower_lon 
        """.format(mod_p_locations=conf.MOD_P_LOCATIONS, cell_id=cell_id,
            min_lat=min_lat, max_lat=max_lat, min_lon=min_lon, max_lon=max_lon)
        cur_local.execute(sql)
        data = cur_local.fetchall()
        if verbose: pp(data)
        return data

def rti_locations_from_network_tbls(rti):
    db_path = '/home/gps/Documents/tourism/csm/storage/locations.db'
    unused_con, cur = db.Sqlite.get_con_cur(db_path)
    ## Dates are dd/mm/yy (blame Daniel ;-))
    ## see https://stackoverflow.com/questions/4428795/sqlite-convert-string-to-date
    sql = """\
    SELECT
      LATITUDE AS
    lat,
      LONGITUDE AS
    lon,
      '20'||substr(VERSION_DATE,7)||'-'||substr(VERSION_DATE,4,2)||'-'||substr(VERSION_DATE,1,2) AS
    date
    FROM loc_hist
    WHERE CELL_ID_RTI = ?
    ORDER BY substr(VERSION_DATE,7)||substr(VERSION_DATE,4,2)||substr(VERSION_DATE,1,2)
    """
    cur.execute(sql, (str(rti), ))
    data = cur.fetchall()
    for row in data:
        print((float(row['lat']), float(row['lon']), row['date']))
    return data

def get_missing_easy_additions(cur_local, tricky_rtis, skip_after_rti=None):
    debug = False
    missing_rtis_dets = get_dets_for_sig_rtis_missing_locations(
        reuse_stale=True)
    easy_rtis_dets = [rti_dets for rti_dets in missing_rtis_dets
        if rti_dets['rti'] not in tricky_rtis]
    if debug: print(easy_rtis_dets)
    more_tricky = []
    skip = bool(skip_after_rti)
    with open('easy_config.txt', 'w') as f:
        for rti_dets in easy_rtis_dets:
            rti = rti_dets['rti']
            if skip:
                if rti == skip_after_rti:
                    skip = False
                continue
            min_sig_date_str = rti_dets['min_sig_date']
            max_sig_date_str = rti_dets['max_sig_date']
            print("Processing rti {} for {} to {}".format(rti, min_sig_date_str,
                max_sig_date_str))
            google_lat, google_lon = Locator.get_location(rti, lac_code=1)
            print("Google lat, lon: {}, {}".format(google_lat, google_lon))
            print("Daniel's lat, lons (choose according to date)")
            data = rti_locations_from_network_tbls(rti)
            resp = input("Enter lat, lon to use: ")
            if not resp:
                more_tricky.append(rti)
                continue
            elif resp == 'g':
                lat, lon = google_lat, google_lon
            elif resp == 'd':
                row2use = data[-1]
                lat, lon = float(row2use['lat']), float(row2use['lon'])
            else:
                raw_lat, raw_lon = resp.replace(' ', '').split(',')
                lat, lon = float(raw_lat), float(raw_lon)
            dets4G = _extract_cell_id_node_ids(rti)
            if dets4G:
                cell_id = dets4G['cell_id']
                node_id = dets4G['node_id']
            else:
                cell_id = rti
                node_id = None
            start_date = datetime.datetime.strptime(
                min_sig_date_str, '%Y-%m-%d').date()
            if max_sig_date_str == '2017-06-01':
                end_date = None
            else:
                end_date = datetime.datetime.strptime(
                    max_sig_date_str, '%Y-%m-%d').date()
            addition_tup = (
                rti, cell_id, node_id, start_date, end_date, lat, lon,
                '', '', None, None, None,
                start_date, end_date,
                None, None, None, None, None, None)
            print("    {},".format(addition_tup))
            f.write("\n{}".format(addition_tup))
        print(more_tricky)

def get_missing_additions(cur_local, start_with_rti=None):
    """
    Look at each rti - IF we are able to get a google lat/lon,
    AND that is consistent with the location estimating maps at the start and
    ends of the significant range (ignoring dribbling)
    AND there is no existing candidate in the CSM for being a match
    THEN create an addition tuple to add to extra.connection_only_additions.
    OTHERWISE capture details so a human decision can be made and the
    appropriate addition(s) added to extra.connection_only_additions.
    """
    missing_rtis_dets = get_dets_for_sig_rtis_missing_locations(
        reuse_stale=True)
    fpath_easy = 'reports/easy2config.txt'
    fpath_custom = 'reports/custom_config_needed.html'
    fpath_easy_partial = 'reports/easy2config_partial'
    fpath_custom_partial = 'reports/custom_config_needed_partial'
    f_easy = open(fpath_easy, 'w')
    f_custom = open(fpath_custom, 'w')
    f_custom.write("""\
    <table>
      <thead>
        <tr>
          <th>cell_id_rti</th>
          <th>Likely locations</th>
          <th>Lat/Lon</th>
          <th>Possible matches in CSM 2</th>
        </tr>
        <tr>
          <th></th>
          <th>Likely locations same at start and end of range? If so, probably
          safe to assume no relocations within range</th>
          <th>Rough Lat/Lon as per Google API. Nice-to-have only. Can't get if
          cell_id_rti not currently active.</th>
          <th>Possible matches in CSM 2 based on having same cell_id and very
          similar lat/lon as per Google. If a genuine match need to look at
          pattern of connection data to see if changes over neatly (red and blue
          lines switch over at roughly same time).</th>
        </tr>
      </thead>
      <tbody>
    """)
    skip = True
    for rti_dets in missing_rtis_dets:
        rti = rti_dets['rti']
        if start_with_rti is None:
            skip = False
        else:
            if rti == start_with_rti:
                skip = False
        if skip:
            continue
        lat, lon = Locator.get_location(rti, lac_code=1, show_map=False)
        print(lat, lon)
        min_sig_date_str = rti_dets['min_sig_date']
        max_sig_date_str = rti_dets['max_sig_date']
        fpaths = []
        for i, date_str in enumerate([min_sig_date_str, max_sig_date_str]):
            is_start = (i == 0)
            if is_start:
                date_str2use = max(date_str, '2015-01-01')  ## no point looking at connections data from before 2015 as always empty map
            else:
                date_str2use = date_str
            start_datetime_str = date_str2use + ' 00:00:00'
            end_datetime_str = date_str2use + ' 23:59:59'
            fpath = findrel.FindRelocations.get_likely_location_dts(cur_local,
                start_datetime_str, end_datetime_str, rti, rtis_to_exclude=None)
            fpaths.append(fpath)
        if lat and lon:
            matches = match_in_csm2(cur_local, rti, lat, lon)
        else:
            matches = ''
        print(matches)
        resp = input("An easy case (y/n/c)? ").lower()
        if resp == 'y':
            dets4G = _extract_cell_id_node_ids(rti)
            if dets4G:
                cell_id = dets4G['cell_id']
                node_id = dets4G['node_id']
            else:
                cell_id = rti
                node_id = None
            start_date = datetime.datetime.strptime(
                min_sig_date_str, '%Y-%m-%d').date()
            if max_sig_date_str == '2017-06-01':
                end_date = None
            else:
                end_date = datetime.datetime.strptime(
                    max_sig_date_str, '%Y-%m-%d').date()
            addition_tup = (
                rti, cell_id, node_id, start_date, end_date, lat, lon,
                '', '', None, None, None,
                start_date, end_date,
                None, None, None, None, None, None)
            print(addition_tup)
            f_easy.write("\n{},".format(addition_tup))
        elif resp == 'n':
            print(rti)
            print(fpaths)
            print(lat, lon)
            print(matches)
            f_custom.write("""\
                <tr>
                  <td>{}</td><td>{}</td><td>{}, {}</td><td>{}</td>
                </tr>
            """.format(rti, fpaths, lat, lon, matches))
        elif resp == 'c':
            f_easy.close()
            f_custom.close()
            partial_fname_resp = input("Text to distinguish for partial file "
                "(to avoid overwriting anything): ")
            shutil.move(fpath_easy,
                fpath_easy_partial + partial_fname_resp + '.txt')
            shutil.move(fpath_custom,
                fpath_custom_partial + partial_fname_resp + '.html')
            print("Quit early")
            break
        else:
            raise Exception("Unexpected response '{}'".format(resp))
    f_easy.close()
    f_custom.write("""\
      </tbody>
    </table>
    """)
    f_custom.close()
    
def show_tricky_context(cur_local, tricky_rtis):
    debug = True
    missing_rtis_dets = get_dets_for_sig_rtis_missing_locations(
        reuse_stale=True)
    tricky_rtis_dets = [rti_dets for rti_dets in missing_rtis_dets
        if rti_dets['rti'] in tricky_rtis]
    for rti_dets in tricky_rtis_dets:
        rti = rti_dets['rti']
        lat, lon = Locator.get_location(rti, lac_code=1)
        if debug: print(lat, lon)
        min_sig_date_str = rti_dets['min_sig_date']
        max_sig_date_str = rti_dets['max_sig_date']
        if lat and lon:
            matches = match_in_csm2(cur_local, rti, lat, lon)
        else:
            matches = ''
        if debug: print(matches)
        print("\n\n{}\n{}, {}\n{} to {}\n{}".format(rti, lat, lon,
            min_sig_date_str, max_sig_date_str, matches))

def main():
    unused_con_local, cur_local, unused_cur_local2 = db.Pg.get_local()
    '''
    rti_excludes = [
        (52611, [52612, 52613]),
        (52612, [52611, 52613]),
        (52613, [52611, 52612]),
    ]
    date_str = '2016-01-01'
    (start_datetime_str,
     end_datetime_str) = dates.Dates.date_str_to_datetime_str_range(date_str)
    for rti, exclude in rti_excludes:
        utils.FindRelocations.get_likely_location_dts(cur_local,
            start_datetime_str, end_datetime_str, rti, rtis_to_exclude=exclude)
    '''
    #rti_locations_from_network_tbls(rti=256286); return
    #already_added_rtis = [453487, 453497, 453999, 454009, 12832032, 12832033, 13057903, 13057913, 13057923, 13151087, 13151097, 13151107, 13221663, 13221664, 13221665]
    
    
    get_dets_for_sig_rtis_missing_locations(reuse_stale=False,
        use_unfixed_csm=True)
    return
    
    
    
    get_missing_additions(cur_local, start_with_rti=None)
    tricky_rtis = [
        26126,
        44132,  ## maps put it in ChCh, google API near Manukau events centre
        364183,  ## err Hamilton? Auckland? Waiuku?!@#!? Roaming cell_id_rti in Oz airpport?
    ]
    
    
    #get_missing_easy_additions(cur_local, tricky_rtis)

    extra_tricky_cases = [ #0, 
        3337, 25941, 27161, 27164, 40920, 58163, 58166, #65535,
        12860703, 12860704, 12952095, 13221663, 13221664, 13221665, 13857823,
        13857824, 13857825, 13881119, 13881120, 13881121, 13929248]
    
    
    #utils.FindRelocations.get_daily_tots(12832032)
    #import mod_mod
    #mod_mod.get_dribblers_and_bounds(cell_id_rtis=[12832032, ])
    
    
    """
    extra tricky cases -

    Processing rti 12860703 for 2014-12-18 to 2016-05-11
    12860703, -43.555887899999995, 172.7069783
    https://www.google.co.nz/maps/place/-43.555887899999995,172.7069783
    Google lat, lon: -43.555887899999995, 172.7069783
    Daniel's lat, lons (choose according to date)
    (-38.1274, 176.231, '2015-01-01')
    (-38.1274, 176.231, '2016-01-01')
    
    Processing rti 12860704 for 2014-12-18 to 2016-05-11
    12860704, -43.562571899999995, 172.6933899
    https://www.google.co.nz/maps/place/-43.562571899999995,172.6933899
    Google lat, lon: -43.562571899999995, 172.6933899
    Daniel's lat, lons (choose according to date)
    (-38.1274, 176.231, '2015-01-01')
    (-38.1274, 176.231, '2016-01-01')
    
    Processing rti 12952095 for 2014-12-19 to 2016-06-16
    12952095, -39.4139492, 175.4064884
    https://www.google.co.nz/maps/place/-39.4139492,175.4064884
    Google lat, lon: -39.4139492, 175.4064884
    Daniel's lat, lons (choose according to date)
    (-45.033651, 168.710821, '2015-01-01')
    (-45.033651, 168.710821, '2016-01-01')
    
    Processing rti 13221663 for 2014-12-19 to 2017-05-11
    13221663, -41.305559099999996, 174.7646221
    https://www.google.co.nz/maps/place/-41.305559099999996,174.7646221
    Google lat, lon: -41.305559099999996, 174.7646221
    Daniel's lat, lons (choose according to date)
    (-41.1878, 174.958, '2015-01-01')
    (-41.1878, 174.958, '2016-01-01')
    (-41.305924, 174.76341, '2016-07-01')
    (-41.305924, 174.76341, '2016-08-01')
    (-41.305924, 174.76341, '2016-09-01')
    (-41.305924, 174.76341, '2016-10-01')
    (-41.305924, 174.76341, '2016-11-01')
    (-41.305924, 174.76341, '2016-12-01')
    (-41.305924, 174.76341, '2017-01-01')
    (-41.305924, 174.76341, '2017-02-01')
    (-41.305924, 174.76341, '2017-03-01')
    (-41.305924, 174.76341, '2017-04-01')
    (-41.305924, 174.76341, '2017-05-01')
    (-41.305924, 174.76341, '2017-05-08')
    
    Processing rti 13221664 for 2014-12-18 to 2017-05-11
    13221664, -41.3118944, 174.7629293
    https://www.google.co.nz/maps/place/-41.3118944,174.7629293
    Google lat, lon: -41.3118944, 174.7629293
    Daniel's lat, lons (choose according to date)
    (-41.1878, 174.958, '2015-01-01')
    (-41.1878, 174.958, '2016-01-01')
    (-41.305924, 174.76341, '2016-07-01')
    (-41.305924, 174.76341, '2016-08-01')
    (-41.305924, 174.76341, '2016-09-01')
    (-41.305924, 174.76341, '2016-10-01')
    (-41.305924, 174.76341, '2016-11-01')
    (-41.305924, 174.76341, '2016-12-01')
    (-41.305924, 174.76341, '2017-01-01')
    (-41.305924, 174.76341, '2017-02-01')
    (-41.305924, 174.76341, '2017-03-01')
    (-41.305924, 174.76341, '2017-04-01')
    (-41.305924, 174.76341, '2017-05-01')
    (-41.305924, 174.76341, '2017-05-08')
    
    Processing rti 13221665 for 2014-12-18 to 2017-05-11
    13221665, -41.3045416, 174.7640725
    https://www.google.co.nz/maps/place/-41.3045416,174.7640725
    Google lat, lon: -41.3045416, 174.7640725
    Daniel's lat, lons (choose according to date)
    (-41.1878, 174.958, '2015-01-01')
    (-41.1878, 174.958, '2016-01-01')
    (-41.305924, 174.76341, '2016-07-01')
    (-41.305924, 174.76341, '2016-08-01')
    (-41.305924, 174.76341, '2016-09-01')
    (-41.305924, 174.76341, '2016-10-01')
    (-41.305924, 174.76341, '2016-11-01')
    (-41.305924, 174.76341, '2016-12-01')
    (-41.305924, 174.76341, '2017-01-01')
    (-41.305924, 174.76341, '2017-02-01')
    (-41.305924, 174.76341, '2017-03-01')
    (-41.305924, 174.76341, '2017-04-01')
    (-41.305924, 174.76341, '2017-05-01')
    (-41.305924, 174.76341, '2017-05-08')
    
    Processing rti 13857823 for 2014-12-18 to 2016-12-15
    13857823, -43.581115399999995, 172.5209813
    https://www.google.co.nz/maps/place/-43.581115399999995,172.5209813
    Google lat, lon: -43.581115399999995, 172.5209813
    Daniel's lat, lons (choose according to date)
    (-39.4346, 175.402, '2015-01-01')
    (-39.4346, 175.402, '2016-01-01')
    (-39.4346, 175.402, '2016-07-01')
    (-39.4346, 175.402, '2016-08-01')
    (-39.4346, 175.402, '2016-09-01')
    (-39.4346, 175.402, '2016-10-01')
    (-39.4346, 175.402, '2016-11-01')
    (-39.4346, 175.402, '2016-12-01')
    
    Google lat, lon: -43.5904568, 172.5049531
    Daniel's lat, lons (choose according to date)
    (-39.4346, 175.402, '2015-01-01')
    (-39.4346, 175.402, '2016-01-01')standard_consolidation_replacements
    (-39.4346, 175.402, '2016-07-01')
    (-39.4346, 175.402, '2016-08-01')
    (-39.4346, 175.402, '2016-09-01')
    (-39.4346, 175.402, '2016-10-01')
    (-39.4346, 175.402, '2016-11-01')
    (-39.4346, 175.402, '2016-12-01')
    
    Processing rti 13857825 for 2014-12-18 to 2016-12-15
    13857825, -43.5735029, 172.51849600000003
    https://www.google.co.nz/maps/place/-43.5735029,172.51849600000003
    Google lat, lon: -43.5735029, 172.51849600000003
    Daniel's lat, lons (choose according to date)
    (-39.4346, 175.402, '2015-01-01')
    (-39.4346, 175.402, '2016-01-01')
    (-39.4346, 175.402, '2016-07-01')
    (-39.4346, 175.402, '2016-08-01')
    (-39.4346, 175.402, '2016-09-01')
    (-39.4346, 175.402, '2016-10-01')
    (-39.4346, 175.402, '2016-11-01')
    (-39.4346, 175.402, '2016-12-01')
    
    Processing rti 13881119 for 2014-12-18 to 2016-05-18
    13881119, -43.5177305, 172.5609556
    https://www.google.co.nz/maps/place/-43.5177305,172.5609556
    Google lat, lon: -43.5177305, 172.5609556
    Daniel's lat, lons (choose according to date)
    (-45.8953, 170.492, '2015-01-01')
    (-45.8953, 170.492, '2016-01-01')
    
    Processing rti 13881120 for 2014-12-19 to 2016-05-18
    13881120, -43.5194511, 172.55166219999998
    https://www.google.co.nz/maps/place/-43.5194511,172.55166219999998
    Google lat, lon: -43.5194511, 172.55166219999998
    Daniel's lat, lons (choose according to date)
    (-45.8953, 170.492, '2015-01-01')
    (-45.8953, 170.492, '2016-01-01')
    
    Processing rti 13881121 for 2014-12-18 to 2016-05-18
    13881121, -43.514143499999996, 172.5522992
    https://www.google.co.nz/maps/place/-43.514143499999996,172.5522992
    Google lat, lon: -43.514143499999996, 172.5522992
    Daniel's lat, lons (choose according to date)
    (-45.8953, 170.492, '2015-01-01')
    (-45.8953, 170.492, '2016-01-01')
    
    Processing rti 13929248 for 2014-12-18 to 2016-04-14
    13929248, -43.5745023, 172.5629595
    https://www.google.co.nz/maps/place/-43.5745023,172.5629595
    Google lat, lon: -43.5745023, 172.5629595
    Daniel's lat, lons (choose according to date)
    (-40.945304, 175.63944, '2015-01-01')
    (-40.945304, 175.63944, '2016-01-01')







    """
    
    #show_tricky_context(cur_local, tricky_rtis)



if __name__ == '__main__':
    main()
